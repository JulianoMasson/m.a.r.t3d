/*
 * Copyright (C) 2015, Nils Moehrle
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */
#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <math.h>
using namespace std;

#include <util/timer.h>
#include <util/system.h>
#include <util/file_system.h>
#include <mve/mesh_io_ply.h>

#include "tex/util.h"
#include "tex/timer.h"
#include "tex/debug.h"
#include "tex/texturing.h"
#include "tex/progress_counter.h"

#include "arguments.h"

#include <mve/image_io.h>
#include <mve/image_tools.h>
#include <mve/bundle_io.h>
#include <mve/scene.h>
#include <windows.h>
/*#include <vtkSmartPointer.h>
#include <vtkPLYReader.h>
#include <vtkTransformFilter.h>
#include <vtkTransform.h>
#include <vtkPLYWriter.h>*/

struct quaternion {
  double x;
  double y;
  double z;
  double w;
  quaternion(double x, double y, double z, double w)
  {
    this->x = x;
    this->y = y;
    this->z = z;
    this->w = w;
  }
  quaternion()
  {
    this->x = 0;
    this->y = 0;
    this->z = 0;
    this->w = 0;
  }

};

struct rotationMatrix {
  double matrix[3][3];
  double R90X[3][3] = { 1,0,0,
    0,0,-1,
    0,1,0 };
  rotationMatrix(quaternion q)
  {
    double sqw = q.w*q.w;
    double sqx = q.x*q.x;
    double sqy = q.y*q.y;
    double sqz = q.z*q.z;

    // invs (inverse square length) is only required if quaternion is not already normalised
    double invs = 1.0 / (sqx + sqy + sqz + sqw);
    matrix[0][0] = (sqx - sqy - sqz + sqw)*invs; // since sqw + sqx + sqy + sqz =1/invs*invs
    matrix[1][1] = (-sqx + sqy - sqz + sqw)*invs;
    matrix[2][2] = (-sqx - sqy + sqz + sqw)*invs;

    double tmp1 = q.x*q.y;
    double tmp2 = q.z*q.w;
    matrix[1][0] = 2.0 * (tmp1 + tmp2)*invs;
    matrix[0][1] = 2.0 * (tmp1 - tmp2)*invs;

    tmp1 = q.x*q.z;
    tmp2 = q.y*q.w;
    matrix[2][0] = 2.0 * (tmp1 - tmp2)*invs;
    matrix[0][2] = 2.0 * (tmp1 + tmp2)*invs;
    tmp1 = q.y*q.z;
    tmp2 = q.x*q.w;
    matrix[2][1] = 2.0 * (tmp1 + tmp2)*invs;
    matrix[1][2] = 2.0 * (tmp1 - tmp2)*invs;
  }

  rotationMatrix()
  {
    for (int i = 0; i < 3; ++i)
    {
      for (int k = 0; k < 3; ++k)
      {
        matrix[i][k] = 0;
      }
    }
  }

  rotationMatrix rotate90X()
  {
    rotationMatrix result;
    for (int a = 0; a < 3; ++a)
    {
      for (int b = 0; b < 3; ++b)
      {
        for (int k = 0; k < 3; ++k)
        {
          result.matrix[a][b] += matrix[a][k] * R90X[k][b];
        }
      }
    }
    return result;
  }

  quaternion toQuaternion()
  {
    quaternion q;
    float trace = matrix[0][0] + matrix[1][1] + matrix[2][2]; // I removed + 1.0f; see discussion with Ethan
    if (trace > 0)
    {// I changed M_EPSILON to 0
      float s = 0.5f / sqrtf(trace + 1.0f);
      q.w = 0.25f / s;
      q.x = (matrix[2][1] - matrix[1][2]) * s;
      q.y = (matrix[0][2] - matrix[2][0]) * s;
      q.z = (matrix[1][0] - matrix[0][1]) * s;
    }
    else
    {
      if (matrix[0][0] > matrix[1][1] && matrix[0][0] > matrix[2][2])
      {
        float s = 2.0f * sqrtf(1.0f + matrix[0][0] - matrix[1][1] - matrix[2][2]);
        q.w = (matrix[2][1] - matrix[1][2]) / s;
        q.x = 0.25f * s;
        q.y = (matrix[0][1] + matrix[1][0]) / s;
        q.z = (matrix[0][2] + matrix[2][0]) / s;
      }
      else if (matrix[1][1] > matrix[2][2])
      {
        float s = 2.0f * sqrtf(1.0f + matrix[1][1] - matrix[0][0] - matrix[2][2]);
        q.w = (matrix[0][2] - matrix[2][0]) / s;
        q.x = (matrix[0][1] + matrix[1][0]) / s;
        q.y = 0.25f * s;
        q.z = (matrix[1][2] + matrix[2][1]) / s;
      }
      else
      {
        float s = 2.0f * sqrtf(1.0f + matrix[2][2] - matrix[0][0] - matrix[1][1]);
        q.w = (matrix[1][0] - matrix[0][1]) / s;
        q.x = (matrix[0][2] + matrix[2][0]) / s;
        q.y = (matrix[1][2] + matrix[2][1]) / s;
        q.z = 0.25f * s;
      }
    }
    return q;
  }

  rotationMatrix getTranspose()
  {
    rotationMatrix result;
    for (int i = 0; i < 3; ++i)
    {
      for (int j = 0; j < 3; ++j)
      {
        result.matrix[j][i] = matrix[i][j];
      }
    }
    return result;
   }

};

void replaceRelativePathFromSFM(std::string const& filename, std::string const& newImgPath)
{
	std::ifstream in(filename.c_str());
	std::stringstream out;
	if (!in.good())
		throw util::FileException(filename, std::strerror(errno));

	/* Read number of views. */
	int num_views = 0;
	in >> num_views;
	out << num_views << "\n\n";
	/* Discard the next empty line */
	{
		std::string temp;
		std::getline(in, temp);
	}
	if (num_views < 0 || num_views > 10000)
		throw util::Exception("Invalid number of views: ",
			util::string::get(num_views));

	std::string path;
	std::string imgName;
  std::string pathSeparator = "";
	for (int i = 0; i < num_views; ++i)
	{
		/* Filename*/
		in >> path;
		//Find the image name in the relative path
    imgName = "";
    for (int k = path.size() - 1; k >= 0; k--)
    {
      if (path.at(k) == '\\' || path.at(k) == '/')
      {
        imgName = path.substr(k + 1, path.size());
        break;
      }
    }
    if (imgName != "")
    {
      out << newImgPath << "/" << imgName << " ";
    }
    else
    {
      out << newImgPath << "/" << path << " ";
    }

		double temp;
		for (int j = 0; j < 16; j++)
		{
			in >> temp;
			if (j != 15)
			{
				out << temp << " ";
			}
			else
			{
				out << temp;
			}
		}
		//Avoid double space when we finish the cameras
		if (i < num_views-1)
		{
			out << "\n";
		}
		in.eof();
	}
	std::string line;
	while (getline(in, line, '\n'))
	{
		out << line << "\n";
	}
	in.close();
	//Overwrite the file
	std::ofstream outFile(filename.c_str());
	if (!outFile.good())
		throw util::FileException(filename, std::strerror(errno));

	outFile << out.rdbuf();
	outFile.close();
}

void replaceRelativePathFromNVM(std::string const& filename, std::string const& newImgPath)
{
	std::ifstream in(filename.c_str());
	std::stringstream out;
	if (!in.good())
		throw util::FileException(filename, std::strerror(errno));

	//NVM_V3 line
	std::string line;
	getline(in, line, '\n');
	out << line << "\n\n";
	

	/* Read number of views. */
	int num_views = 0;
	in >> num_views;
	out << num_views << "\n";

	if (num_views < 0 || num_views > 10000)
		throw util::Exception("Invalid number of views: ",
			util::string::get(num_views));

	std::string path;
	std::string imgName;
  std::string pathSeparator = "";
	for (int i = 0; i < num_views; ++i)
	{
		/* Filename*/
		in >> path;
		//Find the image name in the relative path  
    imgName = "";
    for (int k = path.size() - 1; k >= 0; k--)
    {
      if (path.at(k) == '\\' || path.at(k) == '/')
      {
        imgName = path.substr(k + 1, path.size());
        break;
      }
    }
    if(imgName != "")
    {
      out << newImgPath << "/" << imgName << " ";
    }
    else
    {
      out << newImgPath << "/" << path << " ";
    }
    

		double temp;
		for (int j = 0; j < 10; j++)
		{
			in >> temp;
			if (j != 9)
			{
				out << temp << " ";
			}
			else
			{
				out << temp;
			}
		}
		//Avoid double space when we finish the cameras
		if (i < num_views - 1)
		{
			out << "\n";
		}
		in.eof();
	}
	while (getline(in, line, '\n'))
	{
		out << line << "\n";
	}
	in.close();
	//Overwrite the file
	std::ofstream outFile(filename.c_str());
	if (!outFile.good())
		throw util::FileException(filename, std::strerror(errno));

	outFile << out.rdbuf();
	outFile.close();
}

quaternion multiplyQuaternion(quaternion a, quaternion b)
{
  return quaternion(
    a.w * b.x + a.x * b.w + a.y * b.z - a.z * b.y, // x
    a.w * b.y - a.x * b.z + a.y * b.w + a.z * b.x, // y
    a.w * b.z + a.x * b.y - a.y * b.x + a.z * b.w, // z
    a.w * b.w - a.x * b.x - a.y * b.y - a.z * b.z  // w
  );
}

quaternion rotate(double x, double y, double z, double angle)
{
  double factor, newX, newY, newZ, newW, dot, inv;
  factor = sin(angle / 2.0);
  newX = x * factor;
  newY = y * factor;
  newZ = z * factor;
  newW = cos(angle / 2.0);
  dot = sqrt(newX*newX + newY*newY + newZ*newZ + newW*newW);
  inv = 1.0 / dot;
  newX *= inv;
  newY *= inv;
  newZ *= inv;
  newW *= inv;
  return quaternion(newX, newY, newZ, newW);
}

void rotateCameraNVM(std::string const& filename)
{
  std::ifstream in(filename.c_str());
  std::stringstream out;
  if (!in.good())
    throw util::FileException(filename, std::strerror(errno));

  //NVM_V3 line
  std::string line;
  getline(in, line, '\n');
  out << line << "\n\n";


  /* Read number of views. */
  int num_views = 0;
  in >> num_views;
  out << num_views << "\n";

  if (num_views < 0 || num_views > 10000)
    throw util::Exception("Invalid number of views: ",
      util::string::get(num_views));

  std::string path;
  double temp;
  quaternion inicial,rotated;
  double cameraCenter[3], translation[3];
  for (int i = 0; i < num_views; ++i)
  {
    /* Filename*/
    in >> path;
    out << path << " ";
    
    in >> temp;
    out << temp << " ";
    in >> inicial.w;
    in >> inicial.x;
    in >> inicial.y;
    in >> inicial.z;

    in >> cameraCenter[0];
    in >> cameraCenter[1];
    in >> cameraCenter[2];

    //Get 3x3 rotation matrix
    rotationMatrix initialRotation(inicial);

    //Transpose the rotation matrix
    rotationMatrix transposeRoriginal = initialRotation.getTranspose();

    //get the translation
    for (int j = 0; j < 3; j++)
    {
      translation[j] = -(cameraCenter[0]* transposeRoriginal.matrix[0][j] + cameraCenter[1] * transposeRoriginal.matrix[1][j] + cameraCenter[2] * transposeRoriginal.matrix[2][j]);
    }

    //Rotate the matrix +90 x
    rotationMatrix result = initialRotation.rotate90X();

    rotationMatrix resultTranspose = result.getTranspose();
    //get the new camera center
    for (int j = 0; j < 3; j++)
    {
      cameraCenter[j] = -(translation[0] * resultTranspose.matrix[j][0] + translation[1] * resultTranspose.matrix[j][1] + translation[2] * resultTranspose.matrix[j][2]);
    }

    //get the quaternion
    rotated = result.toQuaternion();
    //rotation = rotate(1,0,0,3.14159265359/2.0);//90graus X
    //result = multiplyQuaternion(rotation, inicial);
    out << rotated.w << " ";
    out << rotated.x << " ";
    out << rotated.y << " ";
    out << rotated.z << " ";

    out << cameraCenter[0] << " ";
    out << cameraCenter[1] << " ";
    out << cameraCenter[2] << " ";

    for (int j = 0; j < 2; j++)
    {
      in >> temp;
      if (j != 1)
      {
        out << temp << " ";
      }
      else
      {
        out << temp;
      }
    }
    //Avoid double space when we finish the cameras
    if (i < num_views - 1)
    {
      out << "\n";
    }
    in.eof();
  }
  while (getline(in, line, '\n'))
  {
    out << line << "\n";
  }
  in.close();
  //Overwrite the file
  std::ofstream outFile(filename.c_str());
  if (!outFile.good())
    throw util::FileException(filename, std::strerror(errno));

  outFile << out.rdbuf();
  outFile.close();
}
void rotateCameraSFM(std::string const& filename)
{
  std::ifstream in(filename.c_str());
  std::stringstream out;
  if (!in.good())
    throw util::FileException(filename, std::strerror(errno));

  //NVM_V3 line


  /* Read number of views. */
  int num_views = 0;
  in >> num_views;
  out << num_views << "\n";

  std::string line;
  getline(in, line, '\n');
  out << line << "\n";

  if (num_views < 0 || num_views > 10000)
    throw util::Exception("Invalid number of views: ",
      util::string::get(num_views));

  std::string path;
  double temp, x, y, z, w, factor, newX, newY, newZ, newW, dot, inv;
  quaternion inicial, rotation, result;
  double TOriginal[4][4];
  double Tresult[4][4];
  double T90X[4][4] = {1,0,0,0,
                       0,0,-1,0,
                       0,1,0,0,
                       0,0,0,1};
  double R90X[3][3] = { 1,0,0,
                        0,0,-1,
                        0,1,0};
  for (int i = 0; i < num_views; ++i)
  {
    /* Filename*/
    in >> path;
    out << path << " ";

    for (int j = 0; j < 3; j++)
    {
      for (int k = 0; k < 3; k++)
      {
        in >> TOriginal[j][k];
      }
    }
    for (int k = 0; k < 3; k++)
    {
      in >> TOriginal[k][3];
    }
    TOriginal[3][0] = 0; TOriginal[3][1] = 0; TOriginal[3][2] = 0; TOriginal[3][3] = 1;

    for (int j = 0; j < 4; j++)
    {
      for (int k = 0; k < 4; k++)
      {
        Tresult[j][k] = 0;
      }
    }

    for (int a = 0; a < 4; ++a)
    {
      for (int b = 0; b < 4; ++b)
      {
        for (int k = 0; k < 4; ++k)
        {
          Tresult[a][b] += TOriginal[a][k] * T90X[k][b];
        }
      }
    }
    for (int j = 0; j < 3; j++)
    {
      for (int k = 0; k < 3; k++)
      {
        out << Tresult[j][k] << " ";
      }
    }
    for (int k = 0; k < 3; k++)
    {
      out << TOriginal[k][3] << " ";
    }

    for (int j = 0; j < 4; j++)
    {
      in >> temp;
      if (j != 3)
      {
        out << temp << " ";
      }
      else
      {
        out << temp;
      }
    }
    //Avoid double space when we finish the cameras
    if (i < num_views - 1)
    {
      out << "\n";
    }
    in.eof();
  }
  getline(in, line, '\n');
  out << line << "\n\n";

  double boudingBox[6];

  for (int k = 0; k < 6; k++)
  {
    in >> boudingBox[k];
  }
  double maxBouding = 0;
  for (int k = 0; k < 6; k++)
  {
    if(abs(boudingBox[k]) > maxBouding)
    {
      maxBouding = abs(boudingBox[k]);
    }
  }

  /*double pointsBouding[8][3] = { boudingBox[0], boudingBox[2],boudingBox[4],
                                 boudingBox[1], boudingBox[2],boudingBox[4],
                                 boudingBox[1], boudingBox[3],boudingBox[4],
                                 boudingBox[0], boudingBox[3],boudingBox[4],
                                 boudingBox[0], boudingBox[2],boudingBox[5],
                                 boudingBox[1], boudingBox[2],boudingBox[5],
                                 boudingBox[1], boudingBox[3],boudingBox[5],
                                 boudingBox[0], boudingBox[3],boudingBox[5],
  };
  double newPointsBouding[8][3];
  for (int j = 0; j < 8; j++)
  {
    for (int k = 0; k < 3; k++)
    {
      newPointsBouding[j][k] = R90X[k][0] * pointsBouding[j][0] + R90X[k][1] * pointsBouding[j][1] + R90X[k][2] * pointsBouding[j][2];
    }
  }
  double minY = newPointsBouding[0][1],maxY = newPointsBouding[0][1],minZ = newPointsBouding[0][2],maxZ = newPointsBouding[0][2];
  for (int j = 0; j < 8; j++)
  {
    if(newPointsBouding[j][1] < minY)
    {
      minY = newPointsBouding[j][1];
    }
    if (newPointsBouding[j][1] > maxY)
    {
      maxY = newPointsBouding[j][1];
    }
    if (newPointsBouding[j][2] < minZ)
    {
      minZ = newPointsBouding[j][2];
    }
    if (newPointsBouding[j][2] > maxZ)
    {
      maxZ = newPointsBouding[j][2];
    }
  }*/

  //out << boudingBox[0] << " " << boudingBox[1] << " " << boudingBox[4] << " " << boudingBox[5] << " " << -10 << " " << 10;
  out << -maxBouding << " " << maxBouding << " " << -maxBouding << " " << maxBouding << " " << -maxBouding << " " << maxBouding;

  while (getline(in, line, '\n'))
  {
    out << line << "\n";
  }
  in.close();
  //Overwrite the file
  std::ofstream outFile(filename.c_str());
  if (!outFile.good())
    throw util::FileException(filename, std::strerror(errno));

  outFile << out.rdbuf();
  outFile.close();
}

mve::Bundle::Ptr
load_sfm_bundle(std::string const& filename,
	std::vector<mve::NVMCameraInfo>* camera_info)
{
	std::ifstream in(filename.c_str());
	if (!in.good())
		throw util::FileException(filename, std::strerror(errno));

	// TODO: Handle multiple models.

	/* Read number of views. */
	int num_views = 0;
	in >> num_views;
	/* Discard the next empty line */
	{
		std::string temp;
		std::getline(in, temp);
	}
	if (num_views < 0 || num_views > 10000)
		throw util::Exception("Invalid number of views: ",
			util::string::get(num_views));

	/* Create new bundle and prepare NVM specific output. */
	mve::Bundle::Ptr bundle = mve::Bundle::create();
	mve::Bundle::Cameras& bundle_cams = bundle->get_cameras();
	bundle_cams.reserve(num_views);
	std::vector<mve::NVMCameraInfo> nvm_cams;
	nvm_cams.reserve(num_views);

	/* Read views. */
	std::cout << "NVM: Number of views: " << num_views << std::endl;
	std::string nvm_path = util::fs::dirname(filename);
	for (int i = 0; i < num_views; ++i)
	{
		mve::NVMCameraInfo nvm_cam;
		mve::CameraInfo bundle_cam;

		/* Filename*/
		in >> nvm_cam.filename;

		/* Camera rotation*/
		math::Matrix3f rot;
		for (int j = 0; j < 9; ++j)
			in >> rot[j];
		/* Camera translation*/
		math::Vec3f trans;
		for (int j = 0; j < 3; ++j)
			in >> trans[j];
		std::copy(rot.begin(), rot.end(), bundle_cam.rot);
		std::copy(trans.begin(), trans.end(), bundle_cam.trans);


		/* Focal length. */
		in >> bundle_cam.flen;

		/* Radial distortion is always zero with SFM file */
		nvm_cam.radial_distortion = 0.0f;
		bundle_cam.dist[0] = nvm_cam.radial_distortion;
		bundle_cam.dist[1] = 0.0f;

		/* If the filename is not absolute, make relative to NVM. */
		if (!util::fs::is_absolute(nvm_cam.filename))
			nvm_cam.filename = util::fs::join_path(nvm_path, nvm_cam.filename);

		/* Getting the extra information not used to go to the next line */
		float temp;
		in >> temp;
		in >> temp;
		in >> temp;
		in.eof();

		bundle_cams.push_back(bundle_cam);
		nvm_cams.push_back(nvm_cam);
	}
	in.close();

	if (camera_info != nullptr)
		std::swap(*camera_info, nvm_cams);

	return bundle;
}

void
from_nvm_scene(std::string const & nvm_file, std::vector<tex::TextureView> * texture_views) {
	std::vector<mve::NVMCameraInfo> nvm_cams;
	mve::Bundle::Ptr bundle = mve::load_nvm_bundle(nvm_file, &nvm_cams);
	mve::Bundle::Cameras& cameras = bundle->get_cameras();

	ProgressCounter view_counter("\tLoading", cameras.size());
#pragma omp parallel for
#if !defined(_MSC_VER)
	for (std::size_t i = 0; i < cameras.size(); ++i) {
#else
	for (std::int64_t i = 0; i < cameras.size(); ++i) {
#endif
		view_counter.progress<SIMPLE>();
		mve::CameraInfo& mve_cam = cameras[i];
		mve::NVMCameraInfo const& nvm_cam = nvm_cams[i];

		mve::ByteImage::Ptr image = mve::image::load_file(nvm_cam.filename);


		int const maxdim = max(image->width(), image->height());//std::max if you don't have windows.h
		mve_cam.flen = mve_cam.flen / static_cast<float>(maxdim);

		texture_views->push_back(tex::TextureView(i, mve_cam, nvm_cam.filename));//image_file));
		view_counter.inc();
	}
	}

void from_sfm_scene(std::string const & sfm_file, std::vector<tex::TextureView> * texture_views) 
{
	std::vector<mve::NVMCameraInfo> sfm_cams;
	mve::Bundle::Ptr bundle = load_sfm_bundle(sfm_file, &sfm_cams);
	mve::Bundle::Cameras& cameras = bundle->get_cameras();

	ProgressCounter view_counter("\tLoading", cameras.size());
#pragma omp parallel for
#if !defined(_MSC_VER)
	for (std::size_t i = 0; i < cameras.size(); ++i) {
#else
	for (std::int64_t i = 0; i < cameras.size(); ++i) {
#endif
		view_counter.progress<SIMPLE>();
		mve::CameraInfo& mve_cam = cameras[i];
		mve::NVMCameraInfo const& sfm_cam = sfm_cams[i];

		mve::ByteImage::Ptr image = mve::image::load_file(sfm_cam.filename);

		int const maxdim = max(image->width(), image->height());//std::max if you don't have windows.h
		mve_cam.flen = mve_cam.flen / static_cast<float>(maxdim);

		texture_views->push_back(tex::TextureView(i, mve_cam, sfm_cam.filename));//image_file));
		view_counter.inc();
	}
}

std::wstring s2ws(const std::string& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}

int startProcess(std::string path_with_command)
{
	std::wstring stemp = s2ws(path_with_command);
	LPWSTR path_command = const_cast<LPWSTR>(stemp.c_str());

	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	// Start the child process. 
	if (!CreateProcess(NULL,   // No module name (use command line)
		path_command,        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi)           // Pointer to PROCESS_INFORMATION structure
		)
	{
		printf("CreateProcess failed (%d).\n", GetLastError());
		return -1;
	}

	// Wait until child process exits.
	WaitForSingleObject(pi.hProcess, INFINITE);

	// Close process and thread handles. 
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
	return 1;
}

std::string calcTime(DWORD initial_time, DWORD final_time)
{
	DWORD time = final_time - initial_time;
	int seconds = (int)(time / 1000) % 60;
	int milisseconds = time - (seconds * 1000);
	int minutes = (int)((time / (1000 * 60)) % 60);
	int hours = (int)((time / (1000 * 60 * 60)) % 24);
	stringstream result;
	result << hours << ":" << minutes << ":" << seconds << "." << milisseconds;
	return result.str();
}

int texRecon(Arguments conf)
{
  Timer timer;
  util::WallTimer wtimer;
  if (!util::fs::dir_exists(util::fs::dirname(conf.out_prefix).c_str())) {
    std::cerr << "Destination directory does not exist!" << std::endl;
    std::exit(EXIT_FAILURE);
  }

  std::cout << "Load and prepare mesh: " << std::endl;
  mve::TriangleMesh::Ptr mesh;
  try {
    mesh = mve::geom::load_ply_mesh(conf.in_mesh);
  }
  catch (std::exception& e) {
    std::cerr << "\tCould not load mesh: " << e.what() << std::endl;
    std::exit(EXIT_FAILURE);
  }
  mve::MeshInfo mesh_info(mesh);
  tex::prepare_mesh(&mesh_info, mesh);

  std::cout << "Generating texture views: " << std::endl;
  tex::TextureViews texture_views;
  if(conf.in_scene.at(conf.in_scene.size()-2) == 'f')
  {
    from_sfm_scene(conf.in_scene, &texture_views);
  }
  else
  {
    from_nvm_scene(conf.in_scene, &texture_views);
  }
  
  //tex::generate_texture_views(conf.in_scene, &texture_views);

  //write_string_to_file(conf.out_prefix + ".conf", conf.to_string());//Configuração utilizada
  timer.measure("Loading");

  std::size_t const num_faces = mesh->get_faces().size() / 3;

  std::cout << "Building adjacency graph: " << std::endl;
  tex::Graph graph(num_faces);
  tex::build_adjacency_graph(mesh, mesh_info, &graph);

  if (conf.labeling_file.empty()) {
    std::cout << "View selection:" << std::endl;
    util::WallTimer rwtimer;

    tex::DataCosts data_costs(num_faces, texture_views.size());
    if (conf.data_cost_file.empty()) {
      tex::calculate_data_costs(mesh, &texture_views, conf.settings, &data_costs);

      if (conf.write_intermediate_results) {
        std::cout << "\tWriting data cost file... " << std::flush;
        tex::DataCosts::save_to_file(data_costs, conf.out_prefix + "_data_costs.spt");
        std::cout << "done." << std::endl;
      }
    }
    else {
      std::cout << "\tLoading data cost file... " << std::flush;
      try {
        tex::DataCosts::load_from_file(conf.data_cost_file, &data_costs);
      }
      catch (util::FileException e) {
        std::cout << "failed!" << std::endl;
        std::cerr << e.what() << std::endl;
        std::exit(EXIT_FAILURE);
      }
      std::cout << "done." << std::endl;
    }
    timer.measure("Calculating data costs");

    tex::view_selection(data_costs, &graph, conf.settings);
    timer.measure("Running MRF optimization");
    std::cout << "\tTook: " << rwtimer.get_elapsed_sec() << "s" << std::endl;

    /* Write labeling to file. */
    if (conf.write_intermediate_results) {
      std::vector<std::size_t> labeling(graph.num_nodes());
      for (std::size_t i = 0; i < graph.num_nodes(); ++i) {
        labeling[i] = graph.get_label(i);
      }
      vector_to_file(conf.out_prefix + "_labeling.vec", labeling);
    }
  }
  else {
    std::cout << "Loading labeling from file... " << std::flush;

    /* Load labeling from file. */
    std::vector<std::size_t> labeling = vector_from_file<std::size_t>(conf.labeling_file);
    if (labeling.size() != graph.num_nodes()) {
      std::cerr << "Wrong labeling file for this mesh/scene combination... aborting!" << std::endl;
      std::exit(EXIT_FAILURE);
    }

    /* Transfer labeling to graph. */
    for (std::size_t i = 0; i < labeling.size(); ++i) {
      const std::size_t label = labeling[i];
      if (label > texture_views.size()) {
        std::cerr << "Wrong labeling file for this mesh/scene combination... aborting!" << std::endl;
        std::exit(EXIT_FAILURE);
      }
      graph.set_label(i, label);
    }

    std::cout << "done." << std::endl;
  }

  tex::TextureAtlases texture_atlases;
  {
    /* Create texture patches and adjust them. */
    tex::TexturePatches texture_patches;
    tex::VertexProjectionInfos vertex_projection_infos;
    std::cout << "Generating texture patches:" << std::endl;
    tex::generate_texture_patches(graph, mesh, mesh_info, &texture_views,
      conf.settings, &vertex_projection_infos, &texture_patches);

    if (conf.settings.global_seam_leveling) {
      std::cout << "Running global seam leveling:" << std::endl;
      tex::global_seam_leveling(graph, mesh, mesh_info, vertex_projection_infos, &texture_patches);
      timer.measure("Running global seam leveling");
    }
    else {
      ProgressCounter texture_patch_counter("Calculating validity masks for texture patches", texture_patches.size());
#pragma omp parallel for schedule(dynamic)
#if !defined(_MSC_VER)
      for (std::size_t i = 0; i < texture_patches.size(); ++i) {
#else
      for (std::int64_t i = 0; i < texture_patches.size(); ++i) {
#endif
        texture_patch_counter.progress<SIMPLE>();
        TexturePatch::Ptr texture_patch = texture_patches[i];
        std::vector<math::Vec3f> patch_adjust_values(texture_patch->get_faces().size() * 3, math::Vec3f(0.0f));
        texture_patch->adjust_colors(patch_adjust_values);
        texture_patch_counter.inc();
      }
      timer.measure("Calculating texture patch validity masks");
      }

    if (conf.settings.local_seam_leveling) {
      std::cout << "Running local seam leveling:" << std::endl;
      tex::local_seam_leveling(graph, mesh, vertex_projection_infos, &texture_patches);
    }
    timer.measure("Running local seam leveling");

    /* Generate texture atlases. */
    std::cout << "Generating texture atlases:" << std::endl;
    tex::generate_texture_atlases(&texture_patches, &texture_atlases);
    }

  /* Create and write out obj model. */
  {
    std::cout << "Building objmodel:" << std::endl;
    tex::Model model;
    tex::build_model(mesh, texture_atlases, &model);
    timer.measure("Building OBJ model");

    std::cout << "\tSaving model... " << std::flush;
    tex::Model::save(model, conf.out_prefix);
    std::cout << "done." << std::endl;
    timer.measure("Saving");
  }

  std::cout << "Whole texturing procedure took: " << wtimer.get_elapsed_sec() << "s" << std::endl;
  timer.measure("Total");
  if (conf.write_timings) {
    timer.write_to_file(conf.out_prefix + "_timings.csv");
  }

  if (conf.write_view_selection_model) {
    texture_atlases.clear();
    std::cout << "Generating debug texture patches:" << std::endl;
    {
      tex::TexturePatches texture_patches;
      generate_debug_embeddings(&texture_views);
      tex::VertexProjectionInfos vertex_projection_infos; // Will only be written
      tex::generate_texture_patches(graph, mesh, mesh_info, &texture_views,
        conf.settings, &vertex_projection_infos, &texture_patches);
      tex::generate_texture_atlases(&texture_patches, &texture_atlases);
    }

    std::cout << "Building debug objmodel:" << std::endl;
    {
      tex::Model model;
      tex::build_model(mesh, texture_atlases, &model);
      std::cout << "\tSaving model... " << std::flush;
      tex::Model::save(model, conf.out_prefix + "_view_selection");
      std::cout << "done." << std::endl;
    }
  }
}

std::string preparePath(std::string path)
{
  std::replace(path.begin(), path.end(), '\\', '/');
  return "\"" + path + "\"";
}

bool testNumberOfVertexPLY(std::string path)
{
  std::string line;
  std::ifstream myfile(path);
  std::vector<std::string> tokens;
  int discartFirstLine;
  if (myfile.is_open())
  {
    std::size_t found;
    while (getline(myfile, line, '\n'))
    {
      found = line.find_last_of(" ");
      if (line.substr(0, found) == "element vertex")
      {
        return std::atoi(line.substr(found + 1).c_str()) != 0;
      }
    }
    myfile.close();
  }
  else
  {
    //("Unable to open the default parameters file");
    return false;
  }
  return false;
}

int completeProcess(int argc, char **argv)
{
	Arguments conf;
  int typeOfReconstruction = std::atoi(argv[2]);
  // 0 - Complete
  // 1 - MeshRecon only
  // 2 - TexRecon only
  // 3 - TexRecon only - with extra parameter to adjust the image path
  std::string martInstallationPath = argv[1];
  std::string resultPath;
  std::string resultFileName;
  //MeshRecon
  std::string imagePath;
  std::string boundingBoxType;
  std::string levelOfDetails;
  bool makeMesh = false;
  //TexRecon
  std::string plyPath;
  std::string nvmPath;
  std::string newImagePath = "";
  std::string dataTerm;
  std::string outlierRemoval;
  std::string geometricVisibilityTest;
  std::string globalSeamLeveling;
  std::string localSeamLeveling;
  std::string holeFilling;
  std::string keepUnseenFaces;
  bool makeTexture = false;

  switch (typeOfReconstruction)
  {
    case 0:
      imagePath = argv[3];
      resultPath = argv[4];
      boundingBoxType = argv[5];
      levelOfDetails = argv[6];
      dataTerm = argv[7];
      outlierRemoval = argv[8];
      geometricVisibilityTest = argv[9];
      globalSeamLeveling = argv[10];
      localSeamLeveling = argv[11];
      holeFilling = argv[12];
      keepUnseenFaces = argv[13];
      makeMesh = true;
      makeTexture = true;
    break;
  case 1:
    imagePath = argv[3];
    resultPath = argv[4];
    boundingBoxType = argv[5];
    levelOfDetails = argv[6];
    makeMesh = true;
  break;
  case 2:
    plyPath = argv[3];
    nvmPath = argv[4];
    resultPath = argv[5];
    dataTerm = argv[6];
    outlierRemoval = argv[7];
    geometricVisibilityTest = argv[8];
    globalSeamLeveling = argv[9];
    localSeamLeveling = argv[10];
    holeFilling = argv[11];
    keepUnseenFaces = argv[12];
    makeTexture = true;
  break;
  case 3:
    plyPath = argv[3];
    nvmPath = argv[4];
    resultPath = argv[5];
    dataTerm = argv[6];
    outlierRemoval = argv[7];
    geometricVisibilityTest = argv[8];
    globalSeamLeveling = argv[9];
    localSeamLeveling = argv[10];
    holeFilling = argv[11];
    keepUnseenFaces = argv[12];
    newImagePath = argv[13];
    makeTexture = true;
    break;
  default:
    cout << "Wrong parameters, do not put space after the \"=\". It should be like this: BoundingBox_Type =1" << endl;
    cin.get();
    return EXIT_FAILURE;
    break;
  }
  //Correct the result strings
  std::size_t found = resultPath.find_last_of("/");
  resultFileName = resultPath.substr(found + 1);
  resultPath = resultPath.substr(0, found);
  resultPath += "/";
  found = resultFileName.find_last_of(".");
  resultFileName = resultFileName.substr(0, found);
  

	DWORD initial_time = GetTickCount();
  DWORD visualSFM_time;
  DWORD nvm2sfm_time;
  DWORD meshRecon_init_time;
  DWORD meshRecon_refine_time;
  if (makeMesh)
  {
    //VisualSFM
    printf("Started VisualSFM\n");
    std::string visualParameters(martInstallationPath + "/vsfm/VisualSFM.exe sfm " + preparePath(imagePath) + " " + preparePath(resultPath + resultFileName+".nvm"));
    if (startProcess(visualParameters) == -1)
    {
      printf("Error with VisualSFM");
      cin.get();
      return EXIT_FAILURE;
    }
    visualSFM_time = GetTickCount();
    printf("Finished VisualSFM\n");
    printf("#######################################################################################\n");

    //Replace relative path to avoid problems
    replaceRelativePathFromNVM(resultPath + resultFileName + ".nvm", imagePath);
    //rotateCameraNVM(resultPath + "output.nvm");

    //nvm2sfm
    printf("Started nvm2sfm");
    std::string parameters(martInstallationPath + "/meshrecon/nvm2sfm.exe " + preparePath(resultPath + resultFileName + ".nvm") + " " + preparePath(resultPath + resultFileName + ".sfm") + " " + boundingBoxType);
    if (startProcess(parameters) == -1)
    {
      printf("Error with NVM2SFM");
      cin.get();
      return EXIT_FAILURE;
    }
    nvm2sfm_time = GetTickCount();
    printf("Finished nvm2sfm\n");
    printf("#######################################################################################\n");

    rotateCameraSFM(resultPath + resultFileName + ".sfm");

    //MeshRecon_init
    printf("Started MeshRecon_init\n");
    std::string paramINIT(martInstallationPath + "/meshrecon/MeshRecon_init.exe " + preparePath(resultPath + resultFileName + ".sfm") + " " + preparePath(resultPath + resultFileName + "_init.ply"));
    if (startProcess(paramINIT) == -1)
    {
      printf("Error with MeshRecon_init");
      cin.get();
      return EXIT_FAILURE;
    }
    meshRecon_init_time = GetTickCount();
    printf("Finished MeshRecon_init\n");
    printf("#######################################################################################\n");

    if (!testNumberOfVertexPLY(resultPath + resultFileName + "_init.ply"))
    {
      return EXIT_FAILURE;
    }

    //MeshRecon_refine
    printf("Started MeshRecon_refine\n");
    std::string parameters2(martInstallationPath + "/meshrecon/MeshRecon_refine.exe " + preparePath(resultPath + resultFileName + ".sfm") + " " + preparePath(resultPath + resultFileName + "_init.ply") + " " + preparePath(resultPath + resultFileName + "_refine.ply") + " " + levelOfDetails);
    if (startProcess(parameters2) == -1)
    {
      printf("Error with MeshRecon_refine");
      cin.get();
      return EXIT_FAILURE;
    }
    meshRecon_refine_time = GetTickCount();
    printf("Finished MeshRecon_refine\n");
    printf("#######################################################################################\n");

    //Rotate cameras to solve the problem of inverted axis(elevation tool)
    rotateCameraNVM(resultPath + resultFileName + ".nvm");

    conf.in_scene = resultPath + resultFileName + ".nvm";
    conf.in_mesh = resultPath + resultFileName + "_refine.ply";
    conf.out_prefix = util::fs::sanitize_path(resultPath + resultFileName + "_textured");
  }
  DWORD texRecon_time = GetTickCount();
  if (makeTexture)
  {
    if (newImagePath != "")
    {
      replaceRelativePathFromNVM(nvmPath, newImagePath);
    }
    if (!makeMesh)
    {
      conf.in_scene = nvmPath;
      conf.in_mesh = plyPath;
      conf.out_prefix = util::fs::sanitize_path(resultPath + resultFileName + "_textured");
      if (!testNumberOfVertexPLY(plyPath))
      {
        return EXIT_FAILURE;
      }
    }
    /* Set defaults for optional arguments. */
    conf.data_cost_file = "";
    conf.labeling_file = "";
    conf.settings.smoothness_term = (tex::SmoothnessTerm)0;
    conf.write_timings = false;
    conf.write_intermediate_results = false;
    conf.write_view_selection_model = false;

    conf.settings.data_term = (tex::DataTerm)std::stoi(dataTerm);
    conf.settings.outlier_removal = (tex::OutlierRemoval)std::stoi(outlierRemoval);
    conf.settings.geometric_visibility_test = std::stoi(geometricVisibilityTest);
    conf.settings.global_seam_leveling = std::stoi(globalSeamLeveling);
    conf.settings.local_seam_leveling = std::stoi(localSeamLeveling);
    conf.settings.hole_filling = std::stoi(holeFilling);
    conf.settings.keep_unseen_faces = std::stoi(keepUnseenFaces);
    printf("Started texrecon\n");
    texRecon(conf);
    printf("Finished texrecon\n");
    printf("#######################################################################################\n");
  }
	//
	DWORD final_time = GetTickCount();

	ofstream fileConfig(resultPath + "Configuration_"+ resultFileName + ".txt");
	if (fileConfig.is_open())
	{
    if (makeMesh)
    {
      fileConfig << "//MeshRecon\nBounding box type = " << boundingBoxType << "\n";
      fileConfig << "Level of details = " << levelOfDetails << "\n";
    }
    if (makeTexture)
    {
      fileConfig << "//TexRecon\nData term = " << choice_string<tex::DataTerm>(conf.settings.data_term) << "\n";
      fileConfig << "Smoothness term = " << choice_string<tex::SmoothnessTerm>(conf.settings.smoothness_term) << "\n";
      fileConfig << "Outlier removal = " << choice_string<tex::OutlierRemoval>(conf.settings.outlier_removal) << "\n";
      fileConfig << "Geometric visibility test = " << conf.settings.geometric_visibility_test << "\n";
      fileConfig << "Global seam leveling = " << conf.settings.global_seam_leveling << "\n";
      fileConfig << "Local seam leveling = " << conf.settings.local_seam_leveling << "\n";
      fileConfig << "Hole filling = " << conf.settings.hole_filling << "\n";
      fileConfig << "Keep unseen faces = " << conf.settings.keep_unseen_faces << "\n";
    }
		fileConfig << "Times(hours:minutes:seconds.milliseconds) " << "\n";
    if (makeMesh)
    {
      fileConfig << "VisualSFM " << calcTime(initial_time, visualSFM_time) << "\n";
      fileConfig << "NVM2SFM " << calcTime(visualSFM_time, nvm2sfm_time) << "\n";
      fileConfig << "MeshRecon init " << calcTime(nvm2sfm_time, meshRecon_init_time) << "\n";
      fileConfig << "MeshRecon refine " << calcTime(meshRecon_init_time, meshRecon_refine_time) << "\n";
    }
    if (makeTexture)
    {
      fileConfig << "TexRecon " << calcTime(texRecon_time, final_time) << "\n";
    }
		fileConfig << "Total " << calcTime(initial_time, final_time) << "\n";
		fileConfig.close();
	}
	else
	{
		cout << "Unable to save file" << endl;
	}
	return EXIT_SUCCESS;
}
int main(int argc, char **argv) 
{
  //cout << "Usage:TexRecon.exe <YourMesh.ply> <YourCameras.nvm> <newFilePath> <newImagePath>(optional)" << endl;
  /*std::string line = "projetomesh/MeshRecon_plus_TexRecon.exe 2 C:/Users/julia/Desktop/DatasetBase/teste/MESH_refine.ply C:/Users/julia/Desktop/DatasetBase/teste/output.nvm C:/Users/julia/Desktop/DatasetBase/teste/eita2 1 0 1 0 1 1 0";
  std::vector<std::string> tokens;
  std::istringstream iss(line);
  std::string token;
  //Used to discard what is before the '='
  char arra[20][100] = { { 0 } };
  int i = 0;
  while (std::getline(iss, token, ' '))
  {
    if (!token.empty())
    {
      tokens.push_back(token);
      strcpy(arra[i], token.c_str());
      i++;
    }
  }

  argc = tokens.size();
  char *ptr_array[20];
  for (int i = 0; i < 20; i++)
    ptr_array[i] = arra[i];
    */
	//return justTexture(argc, argv);//use this if you want to build TexRecon.exe
  //rotateCameraNVM("C:/Users/julia/Desktop/DatasetBase/correto/output.nvm");
  return completeProcess(argc, argv);//use this if you want to build MeshRecon_plus_TexRecon.exe
}
