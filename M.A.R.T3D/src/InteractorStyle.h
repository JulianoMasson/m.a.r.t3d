#pragma once
#ifndef __INTERACTORSTYLE_H__
#define __INTERACTORSTYLE_H__

#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkActor.h>
#include <vtkAbstractPicker.h>
#include <vtkLine.h>
#include <vtkTextActor.h>
#include <vtkTextProperty.h>
#include <vtkCellArray.h>
#include <vtkPolyDataMapper2D.h>
#include <vtkActor2D.h>
#include <vtkProperty2D.h>
#include <vtkPointPicker.h>
#include <vtkSmartPointer.h>
#include <vtkPropPicker.h>
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkObjectFactory.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRendererCollection.h>
#include <sstream>
#include <vector>
#include <vtkNew.h>
#include <vtkCellPicker.h>
#include <wx\string.h>
#include "Mesh.h"
#include <wx\msgdlg.h>
#include <vtkDelaunay3D.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkFillHolesFilter.h>
#include <vtkClipClosedSurface.h>
#include <vtkPlaneCollection.h>
#include <vtkMassProperties.h>
#include <vtkAppendPolyData.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkDataSetMapper.h>
#include <vtkCleanPolyData.h>
#include <vtkCaptionActor2D.h>
#include <wx/toolbar.h>
#include <wx/bitmap.h>
#include <vtkPolyDataNormals.h>
#include <vtkTriangleFilter.h>
#include <vtkUnstructuredGrid.h>
#include "ImplicitPlaneWidget.h"
#include "PoissonRecon.h"
#include <vtkCellLocator.h>
#include "LineWidget.h"
#include <vtkWorldPointPicker.h>

class InteractorStyle : public vtkInteractorStyleTrackballCamera
{
private:
  ~InteractorStyle();

  static void ProcessEvents(vtkObject* object, unsigned long event,
    void* clientdata, void* calldata);

  vtkNew<vtkPropPicker>  picker;
  vtkNew<vtkCellPicker> cellPicker;

  //CalcVolume
  vtkSmartPointer<LineWidget> lineWidget = NULL;
  vtkSmartPointer<vtkPropPicker>  propPickerVolume = NULL;
  vtkSmartPointer<ImplicitPlaneWidget> planeWidgetVolumeTool = NULL;
  std::vector<double*> polygonPointsVolumeTool;
  bool endCalcVolume = false;
  bool pickingPolygonVolume = false;
  Mesh* meshVolume = NULL;
  double polygonHeightCalcVolume = 1;
  vtkSmartPointer<vtkActor> actorUpdatingCalcVolume = NULL;
  vtkSmartPointer<vtkActor> planeActorVolume = NULL;
  wxToolBarToolBase* toolCalcVolume;
  wxBitmap* bmpToolCalcVolumeOFF;
  wxBitmap* bmpToolCalcVolumeON;
  //Last position of the plane
  double* lastOrigin = NULL;
  double* lastNormal = NULL;

  //FlyToPoint
  bool flyToPoint = false;

public:
  static InteractorStyle* New();
  vtkTypeMacro(InteractorStyle, vtkInteractorStyleTrackballCamera);
  //Tools
  bool statusVolumeTool = 0;
  bool statusCheckCamTool = 0;
  //For the image double click operation
  std::vector<Mesh*>* meshVector;
  wxTreeListCtrl* treeMesh;
  

  virtual void OnRightButtonDown();
  virtual void OnMouseWheelForward();
  virtual void OnMouseMove();
  virtual void OnKeyPress();
  void onLineWidget();
  void doubleClick();

  //Tools
  //Always need to call this method when the class is used, otherwise it will acess NULL pointers!
  void initializeToolBarItems(wxToolBarToolBase* toolCalcVolume, wxBitmap* bmpVolumeOff, wxBitmap* bmpVolumeOn);
  bool isAnyToolActive();
  void disableTools();
  void volumeTool(Mesh* meshSelected);

  //Volume
  void onLeftButtonDownVolume();
  bool computeVolume();
  bool isPlaneOnVolume();
  void endPlaneInteractionVolume();
  void updateClipping();
  bool hasSelection();
  void endPolygonInteraction();

  //flyToPoint
  void setFlyToPoint(bool flyToPoint);
  bool getFlyToPoint();

  
  
  /*
  Pick the position of the mouse in 3D if the actor is != NULL
  */
  int getMousePosition(vtkSmartPointer<vtkAbstractPicker> picker,double* point);
  /*
  Pick in the pointDisplay position if the actor is != NULL
  */
  int getMousePosition(vtkSmartPointer<vtkAbstractPicker> picker, double* pointDisplay, double* point);
  /*
  Pick the position of the mouse in 3D, using a Picker
  */
  int getMousePosition(double* point);
  /*
  Pick the position of the mouse in 3D if the actor is == actor(Parameter)
  */
  int getMousePosition(double* point, vtkSmartPointer<vtkActor> actor);
  /*
  Do a pick in the specified display position
  Input-> display position
  Output-> world position
  */
  double* pickPosition(double* pointDisplay);
  /*
  p1->Camera position
  PointCheckCam->Camera focal point
  polygon->Polygon that is treated as a plane
  */
  bool IntersectPlaneWithLine(double* p1, vtkSmartPointer<vtkPolygon> polygon, double* pointCheckCam);
  double* getDisplayPosition(double* point);
};

#endif