#pragma once
#ifndef __OUTPUTERRORWINDOW__H__
#define __OUTPUTERRORWINDOW__H__
#include "vtkOutputWindow.h"
#include <wx/log.h>


class OutputErrorWindow : public vtkOutputWindow
 {
	public:
		vtkTypeMacro(OutputErrorWindow, vtkOutputWindow);
		
		static OutputErrorWindow * New();
		virtual void DisplayText(const char*);
		virtual void DisplayErrorText(const char*);
		virtual void DisplayWarningText(const char*);
		virtual void DisplayGenericWarningText(const char*);
    //Test if there is some error or warning message
    bool checkOutputErrorWindow();
		bool error = 0;
		bool warning = 0;
		bool suppressMessages = 0;
		/*
		Clear error and warning flags
		*/
		void clearFlags();
		//Change to show or not the logs
		void setSuppressMessages(bool suppress);

	protected:
		OutputErrorWindow();
		virtual ~OutputErrorWindow();
	private:
		OutputErrorWindow(const OutputErrorWindow &);  // Not implemented.
		void operator=(const OutputErrorWindow &);  // Not implemented.
};

#endif