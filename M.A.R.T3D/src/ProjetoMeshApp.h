#pragma once
#ifndef __PROJETOMESHAPP__H__
#define __PROJETOMESHAPP__H__

#ifdef WIN32
#include "wx/msw/winundef.h"
#endif

#include <wx\wx.h>
#include <wx\cshelp.h>
#include <wx\fs_zip.h>
#include "FrmPrincipal.h"


class ProjetoMeshApp : public wxApp {
public:
	virtual bool OnInit();
	void OnTimerTimeout(wxTimerEvent& event);
	virtual int OnExit();
private:
	DECLARE_EVENT_TABLE()
	wxTimer* timer;
	FrmPrincipal* frmPrincipal;
	void OnLeftDClick(wxMouseEvent& event);
};

DECLARE_APP(ProjetoMeshApp)

enum
{
	idTimer
};


#endif
