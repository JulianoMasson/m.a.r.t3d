#pragma once
#include "vtkInteractionWidgetsModule.h" // For export macro
#include "vtk3DWidget.h"

#include "vtkActor.h"
#include "vtkCallbackCommand.h"
#include <vtkPropPicker.h>
#include "vtkPickingManager.h"
#include "vtkPlane.h"
#include "vtkPolyData.h"
#include "vtkProperty.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkCaptionActor2D.h"
#include "Utils.h"
#include "Draw.h"
#include "Mesh.h"
#include <wx\string.h>
#include <wx\textdlg.h> 
#include "Calibration.h"
#include "LineWidget.h"
#include <vtkPointPicker.h>

class CheckCamTool : public vtk3DWidget
{
public:
  /**
  * Instantiate the object.
  */
  static CheckCamTool *New();

  vtkTypeMacro(CheckCamTool, vtk3DWidget);

  //@{
  /**
  * Methods that satisfy the superclass' API.
  */
  virtual void SetEnabled(int);
  virtual void PlaceWidget(double bounds[6]);
  //@}

  //@{
  /**
  * Set the mesh that is going to be used
  */
  void setMesh(Mesh* mesh);
  //@}

  //@{
  /**
  * Set the mesh tree
  */
  void setTreeMesh(wxTreeListCtrl* treeMesh);
  //@}

protected:
  CheckCamTool();
  ~CheckCamTool();

  //handles the events
  static void ProcessEvents(vtkObject* object, unsigned long event,
    void* clientdata, void* calldata);

  vtkSmartPointer<LineWidget> lineWidget = NULL;
  vtkSmartPointer<vtkCellPicker> cellPicker = NULL;

  void createPicker();
  void checkVisibility();
  void destruct();
  void clearVisibleCameras();
  bool intersectPlaneWithLine(double * p1, vtkSmartPointer<vtkPolygon> polygon, double * pointCheckCam);

  Mesh* mesh = NULL;
  std::vector<Camera*> visibleCameras;
  std::vector<vtkSmartPointer<vtkActor>> validPoints;
  wxTreeListCtrl* treeMesh = NULL;

private:
  CheckCamTool(const CheckCamTool&) VTK_DELETE_FUNCTION;
  void operator=(const CheckCamTool&) VTK_DELETE_FUNCTION;
};
