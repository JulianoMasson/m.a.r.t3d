#include "Volume.h"

Volume::Volume(bool visible)
{
  text = NULL;
  polyData = NULL;
  actor = NULL;
  listItem = NULL;
  volume = -1;
  index = -1;
  this->visible = visible;
}

Volume::~Volume()
{
  
}

void Volume::destruct(vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree)
{
  if (text != NULL)
  {
    renderer->RemoveActor(text);
    text = NULL;
  }
  if(actor != NULL)
  { 
    renderer->RemoveActor(actor);
    actor = NULL;
  }
  if (listItem != NULL)
  {
    tree->DeleteItem(listItem);
    listItem = NULL;
  }
}

void Volume::setVisibility(bool visibility)
{
  if (actor != NULL && text != NULL)
  {
   actor->SetVisibility(visibility);
   text->SetVisibility(visibility);
   this->visible = visibility;
  }
}

bool Volume::getVisibility()
{
  return visible;
}

void Volume::setListItem(wxTreeListItem item)
{
  listItem = item;
}

wxTreeListItem Volume::getListItem()
{
  return listItem;
}

void Volume::updateText(wxString measureUnit)
{
  if (text != NULL)
  {
    wxString ss;
    ss << "#" << index << " - " << volume << measureUnit << "\u00B3";
    text->SetCaption(ss.utf8_str());
  }
}

void Volume::updateCalibration(double calibration, wxString measureUnit)
{
  if (polyData != NULL)
  {
    vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
    transform->Scale(calibration, calibration, calibration);
    vtkSmartPointer<vtkTransformFilter> transformFilter = vtkSmartPointer<vtkTransformFilter>::New();
    transformFilter->SetInputData(polyData);
    transformFilter->SetTransform(transform);
    transformFilter->Update();

    //Compute the volume
    vtkSmartPointer<vtkMassProperties> massProperties = vtkSmartPointer<vtkMassProperties>::New();
    massProperties->SetInputConnection(transformFilter->GetOutputPort());
    massProperties->Update();

    if ((massProperties->GetVolume() - massProperties->GetVolumeProjected()) * 10000 > massProperties->GetVolume())//If we are updating the calibration this is really unlikely to happen
    {
      wxMessageBox("Something went wrong", "Error", wxICON_ERROR);
    }
    else
    {
      volume = massProperties->GetVolume();
      updateText(measureUnit);
    }
  }
}

void Volume::transform(vtkSmartPointer<vtkTransform> T)
{
  if (actor != NULL)
  {
    vtkSmartPointer<vtkTransformFilter> transformFilter = vtkSmartPointer<vtkTransformFilter>::New();
    transformFilter->SetTransform(T);
    transformFilter->SetInputData(actor->GetMapper()->GetInputAsDataSet());
    transformFilter->Update();
    actor->GetMapper()->SetInputConnection(transformFilter->GetOutputPort());
    updateTextPosition();
  }
}

void Volume::updateTextPosition()
{
  text->SetAttachmentPoint(actor->GetCenter());
}
