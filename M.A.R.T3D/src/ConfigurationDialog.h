#pragma once
#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/spinctrl.h>
#include <wx/choice.h>
#include <wx/sizer.h>
#include <wx/statbox.h>
#include <wx/checkbox.h>
#include <wx/button.h>
#include <wx/dialog.h>
#include <sstream>
#include <vector>
#include <iostream>
#include <fstream>
#include <wx\log.h>
#include <wx\string.h>
#include <wx\stdpaths.h>
#include <vtksys\SystemTools.hxx>

class ConfigurationDialog : public wxDialog
{
public:
  ConfigurationDialog(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = "Configuration", const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(300, 460), long style = wxDEFAULT_DIALOG_STYLE);
  ~ConfigurationDialog();

  static void loadDefaultConfig();
  static wxString getParameters();
  static wxString getMeshReconParameters();
  static wxString getTexReconParameters();
	
  static bool getForceCMPMVS();

  //MeshRecon
  static std::string getBoundingBox();
  //0 - High 1 - Medium 2 - Low
  static std::string getLevelOfDetails();
  static void setLevelOfDetails(int newLevelOfDetails);
  static bool getForceLevelOfDetails();

  
private:
  DECLARE_EVENT_TABLE()

  void OnOK(wxCommandEvent& WXUNUSED(event));
  void OnBtDefault(wxCommandEvent& WXUNUSED(event));

  wxCheckBox* ckBForceCMPMVS;

  wxChoice* choiceBoundingBox;
  wxChoice* choiceLevelOfDetails;
  wxCheckBox* ckBForceLevelOfDetails;

  wxChoice* choiceDataTerm;
  wxChoice* choiceOutlierRemoval;
  wxCheckBox* ckBGeometricVisibilityTest;
  wxCheckBox* ckBGlobalSeamLeveling;
  wxCheckBox* ckBLocalSeamLeveling;
  wxCheckBox* ckBHoleFilling;
  wxCheckBox* ckBKeepUnseenFaces;

  //Used to load the default parameters
  static bool isFirstInstance;
  //CMPMVS
  static bool forceCMPMVS;
  //MeshRecon
  static int boundingBoxType;
  static int levelOfDetails;
  static bool forceLevelOfDetails;
  //TexRecon
  static int dataTerm;
  static int outlierRemoval;
  static bool geometricVisibilityTest;
  static bool globalSeamLeveling;
  static bool localSeamLeveling;
  static bool holeFilling;
  static bool keepUnseenFaces;

};
enum MyEnum
{
  idBtDefaultConfig
};