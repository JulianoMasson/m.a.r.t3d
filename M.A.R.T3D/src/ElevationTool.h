#pragma once

#include "vtkInteractionWidgetsModule.h" // For export macro
#include "vtk3DWidget.h"

#include "vtkActor.h"
#include "vtkCallbackCommand.h"
#include <vtkPropPicker.h>
#include "vtkPickingManager.h"
#include "vtkPlane.h"
#include "vtkPolyData.h"
#include "vtkProperty.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "captionActor2D.h"
#include "Utils.h"
#include "Draw.h"
#include "Mesh.h"
#include <wx\string.h>
#include <wx\log.h>
#include <vtkElevationFilter.h>
#include <vtkFloatArray.h>
#include <vtkLookupTable.h>
#include <vtkSliderWidget.h>
#include "sliderRep2D.h"
#include <vtkFollower.h>
//ElevationProfile
#include "LineWidget.h"

#include <vtkCutter.h>
#include <vtkStripper.h>
#include <vtkPoints.h>
#include <vtkSortDataArray.h>
#include <vtkXYPlotActor.h>
#include <vtkAxisActor2D.h>
#include <vtkCellData.h>
#include <vtkDelaunay3D.h>
#include <vtkUnstructuredGrid.h>
//

class ElevationTool : public vtk3DWidget
{
public:
  /**
  * Instantiate the object.
  */
  static ElevationTool *New();

  vtkTypeMacro(ElevationTool, vtk3DWidget);

  //@{
  /**
  * Methods that satisfy the superclass' API.
  */
  virtual void SetEnabled(int);
  virtual void PlaceWidget(double bounds[6]);
  //@}

  //@{
  /**
  * Set the mesh that is going to be used
  */
  void setMesh(Mesh* mesh);
  //@}

  //@{
  /**
  * Set the mesh tree
  */
  void setMeshTree(wxTreeListCtrl* treeMesh);
  //@}

protected:
  ElevationTool();
  ~ElevationTool();

  //Delete what is necessary to properly disable/kill this class
  void destruct();

  //handles the events
  static void ProcessEvents(vtkObject* object, unsigned long event,
    void* clientdata, void* calldata);

  // ProcessEvents() dispatches to these methods.
  void OnMiddleButtonDown();
  void OnKeyPressed();

  //Actors
  vtkSmartPointer<captionActor2D> textActor = NULL;

  //ElevationMap
  vtkSmartPointer<vtkPolyData> outputElevationFilter = NULL;
  vtkSmartPointer<vtkElevationFilter> elevationFilter = NULL;
  vtkSmartPointer<vtkActor> elevationMapActor = NULL;

  //Slider
  vtkSmartPointer<vtkSliderWidget> sliderWidget = NULL;
  void createSlider();
  void OnSliderChanged();

  void updateElevationMap();
  void updateText();
  double getElevationDistance(double dist);
  double getPointElevation(double* point);


  // Do the picking
  vtkSmartPointer<vtkPropPicker> propPicker = NULL;
  Mesh* mesh = nullptr;

  //Set the zero by clicking in the mesh
  bool setZeroByMouse = false;

  double* bounds = NULL;

  int getMousePosition(double * point);

  //ElevationProfile
  vtkSmartPointer<LineWidget> lineWidget = NULL;
  void createElevationProfileLine();
  /*
  test if point is inside PointA--------
                            |           |
                            |           |
                            |         pointB
                            -------------
  */
  bool isInsideBoundingBox(double* point, double* pointA, double* pointB);
  void increasePointDensity(vtkSmartPointer<vtkPoints> points, double* point1, double* point2);
  vtkSmartPointer<vtkActor> elevationProfileActor = NULL;
  //Plot
  vtkSmartPointer<vtkXYPlotActor> plotElevation = NULL;
  void createPlot(vtkSmartPointer<vtkPoints> points, double* point0Line, double* point1Line);

  //Avoid updating if there is no need
  double* lastElevationHeight = NULL;

  wxTreeListCtrl * treeMesh = NULL;

private:
  ElevationTool(const ElevationTool&) VTK_DELETE_FUNCTION;
  void operator=(const ElevationTool&) VTK_DELETE_FUNCTION;
};

