#pragma once
#include "vtkInteractionWidgetsModule.h" // For export macro
#include "vtk3DWidget.h"

#include "vtkActor.h"
#include "vtkCallbackCommand.h"
#include <vtkPropPicker.h>
#include "vtkPickingManager.h"
#include "vtkPlane.h"
#include "vtkPolyData.h"
#include "vtkProperty.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkCaptionActor2D.h"
#include "Utils.h"
#include "Draw.h"
#include "Mesh.h"
#include <wx\string.h>
#include <wx\textdlg.h> 
#include "Calibration.h"
#include "LineWidget.h"

class AlignWithAxisTool : public vtk3DWidget
{
public:
  /**
  * Instantiate the object.
  */
  static AlignWithAxisTool *New();

  vtkTypeMacro(AlignWithAxisTool, vtk3DWidget);

  //@{
  /**
  * Methods that satisfy the superclass' API.
  */
  virtual void SetEnabled(int);
  virtual void PlaceWidget(double bounds[6]);
  //@}

  //@{
  /**
  * Set the mesh that is going to be used
  */
  void setMesh(Mesh* mesh);
  //@}

protected:
  AlignWithAxisTool();
  ~AlignWithAxisTool();

  //handles the events
  static void ProcessEvents(vtkObject* object, unsigned long event,
    void* clientdata, void* calldata);

  vtkSmartPointer<LineWidget> lineWidget = NULL;

  // Controlling ivars
  void UpdateRepresentation();

  Mesh* mesh;

private:
  AlignWithAxisTool(const AlignWithAxisTool&) VTK_DELETE_FUNCTION;
  void operator=(const AlignWithAxisTool&) VTK_DELETE_FUNCTION;
};