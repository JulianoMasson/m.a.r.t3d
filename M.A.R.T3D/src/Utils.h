#pragma once
#ifndef __UTILS__H__
#define __UTILS__H__

#include "Camera.h"
#include <vtkMath.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkCamera.h>
#include <vtkWindowToImageFilter.h>
#include <vtkPNGWriter.h>
#include <vtkRenderWindow.h>
#include "Draw.h"
#include "Mesh.h"
#include <vtkImageImport.h>
#include <vtkInteractorObserver.h>
#include <vtkAbstractPicker.h>
#include <vtksys\SystemTools.hxx>
#include <Windows.h>
#include <iostream>
#include <sstream>
#include <wx/stdpaths.h>
#include <wx/log.h>
#include <vtkJPEGReader.h>
#include <vtkPNGReader.h>
#include <vtkBMPReader.h>
#include <vtkTIFFReader.h>
#include <vtkImageReader2.h>

class Utils {
public:
	Utils();
	~Utils();

	/*
	Update the renderer's active camera to see directly to the focal point
	*/
	static void Utils::updateCamera(vtkSmartPointer<vtkRenderer> renderer, Camera* cam)
	{
		if (cam->viewUp != NULL)
		{
			//Updating camera
			if (cam->width > cam->height)//I want all the image, so get the bigger side
			{
				renderer->GetActiveCamera()->SetViewAngle(vtkMath::DegreesFromRadians(2.0 * atan(cam->width / (2.0*cam->focalX))));
			}
			else
			{
				renderer->GetActiveCamera()->SetViewAngle(vtkMath::DegreesFromRadians(2.0 * atan(cam->height / (2.0*cam->focalY))));
			}
			renderer->GetActiveCamera()->SetPosition(cam->cameraPoints.at(0));
			renderer->GetActiveCamera()->SetFocalPoint(cam->cameraPoints.at(5));
			renderer->GetActiveCamera()->SetViewUp(cam->viewUp);
			//Necessary to make all actors visible
			renderer->ResetCameraClippingRange();
		}	
	}
	/*
	B
	|
	|
	|
	A------C
	static PointXYZ* Utils::getNormal(PointXYZ* a, PointXYZ* b, PointXYZ* c)
	{
		PointXYZ* ab = getVector(a, b);
		PointXYZ* ac = getVector(a, c);
		PointXYZ* n = new PointXYZ((ab->y*ac->z) - (ab->z*ac->y), (ab->z*ac->x) - (ab->x*ac->z), (ab->x*ac->y) - (ab->y*ac->x));
		return n;
	}*/

	/*
	True if the file exists, false otherwise
	*/
	static bool exists(const std::string& name) {
		struct stat buffer;
		return (stat(name.c_str(), &buffer) == 0);
	}

	static std::string getMTLFilenameFromOBJ(std::string filename)
	{
		ifstream myfile(filename);
		std::string line;
		std::string mtlFile = "";
		if (myfile.is_open())
		{
			while (getline(myfile, line, '\n'))
			{
				if (line.find("mtllib ",0) != std::string::npos)
				{
					mtlFile = vtksys::SystemTools::GetFilenameName( line.substr(line.find(' ', 0)+1) );
					break;
				}
				else if (line[0] == 'v')//the vertex list started, there is no mtllib
				{
					break;
				}
			}
		}
		myfile.close();
		return mtlFile;
	}

	static std::vector<std::string> getTextureNamesFromMTL(std::string filename)
	{
		ifstream mtlFile(filename);
		std::string line;
		std::vector<std::string> textureNames;
		if (mtlFile.is_open())
		{
			while (getline(mtlFile, line, '\n'))
			{
				if (line.find("map_Kd ", 0) != std::string::npos)
				{
					textureNames.push_back(line.substr(line.find(' ', 0) + 1));
				}
			}
		}
		mtlFile.close();
		return textureNames;
	}

	//Add the texture names in the MTL file
	static void addTextureNamesToMTL(std::string mtlFilename, std::vector<std::string> textureNames)
	{
		ifstream mtlFile(mtlFilename);
		std::string line;
		std::string newFile;
		unsigned int idxTexture = 0;
		if (mtlFile.is_open())
		{
			while (getline(mtlFile, line, '\n'))
			{
				newFile += line + "\n";
				if (line.find("Tr", 0) != std::string::npos)
				{
					newFile += "map_Kd " + textureNames.at(idxTexture) + "\n";
					idxTexture++;
				}
			}
		}
		mtlFile.close();
		ofstream newMtlFile(mtlFilename);
		if (newMtlFile.is_open())
		{
			newMtlFile << newFile;
		}
		newMtlFile.close();
	}

	static void takeSnapshot(wxString path, int magnification, bool getAlpha, vtkRenderWindow* renderWindow)
	{
		wxBeginBusyCursor(wxHOURGLASS_CURSOR);
		vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter = vtkSmartPointer<vtkWindowToImageFilter>::New();
		windowToImageFilter->SetInput(renderWindow);
		windowToImageFilter->SetScale(magnification);
    if (getAlpha)
    {
      windowToImageFilter->SetInputBufferTypeToRGBA();
    }
    else
    {
      windowToImageFilter->SetInputBufferTypeToRGB();
    }
		windowToImageFilter->ReadFrontBufferOff(); // read from the back buffer
		windowToImageFilter->Update();

		vtkSmartPointer<vtkPNGWriter> writer = vtkSmartPointer<vtkPNGWriter>::New();
		writer->SetFileName(path.c_str());
		writer->SetInputConnection(windowToImageFilter->GetOutputPort());
		writer->Write();
		renderWindow->Render();
		wxEndBusyCursor();
	}
	/*
	P1---Midpoint---P2
	*/
	static double* getMidpoint(double* p1, double* p2)
	{
		double* result = new double[3];
		for (int i = 0; i < 3; i++)
		{
			result[i] = (p1[i] + p2[i]) / 2;
		}
		return result;
	}
	static double* getMidpoint(double* p1, double* p2, double* p3)
	{
		double* result = new double[3];
		for (int i = 0; i < 3; i++)
		{
			result[i] = (p1[i] + p2[i] + p3[i]) / 3;
		}
		return result;
	}

	static void transformPoint(double* point, vtkSmartPointer<vtkMatrix4x4> matrixRT)
	{
		double x = (matrixRT->Element[0][0] * point[0] + matrixRT->Element[0][1] * point[1] + matrixRT->Element[0][2] * point[2] + matrixRT->Element[0][3]);
		double y = (matrixRT->Element[1][0] * point[0] + matrixRT->Element[1][1] * point[1] + matrixRT->Element[1][2] * point[2] + matrixRT->Element[1][3]);
		double z = (matrixRT->Element[2][0] * point[0] + matrixRT->Element[2][1] * point[1] + matrixRT->Element[2][2] * point[2] + matrixRT->Element[2][3]);
		point[0] = x; point[1] = y; point[2] = z;
	}
	/*
	p[0] = x; p[1] = y; p[2] = z;
	*/
	static double* createDoubleVector(double x, double y, double z)
	{
		double* p = new double[3];
		p[0] = x; p[1] = y; p[2] = z;
		return p;
	}
  static double* createDoubleVector(double* xyz)
  {
    double* p = new double[3];
    memcpy(p, xyz, sizeof(double) * 3);
    return p;
  }
	

  static double* getNormal(double* pointA, double* pointB, double* pointC)
  {
    double v1[3];
    double v2[3];
    double n[3];
    vtkMath::Subtract(pointB, pointA, v1);
    vtkMath::Subtract(pointB, pointC, v2);
    vtkMath::Normalize(v1);
    vtkMath::Normalize(v2);
    vtkMath::Cross(v1, v2, n);
    vtkMath::Normalize(n);
    double* n2 = Utils::createDoubleVector(n[0], n[1], n[2]);
    return n2;
  }

	/*
	A
	*
	*
	*
	B * * * C, compute the normal and return the (normal+pointB) nearest to pointTest, the pointTest is used to define the correct direction of the vector
	*/
	static double* getNormal(double* pointA, double* pointB, double* pointC, double* pointTest)
	{
		double v1[3];
		double v2[3];
		double n[3];
		vtkMath::Subtract(pointB, pointA, v1);
		vtkMath::Subtract(pointB, pointC, v2);
		vtkMath::Normalize(v1);
		vtkMath::Normalize(v2);
		vtkMath::Cross(v1, v2, n);
		vtkMath::Normalize(n);
		double* n2 = Utils::createDoubleVector(n[0], n[1], n[2]);
		vtkMath::MultiplyScalar(n2, -1);
		vtkMath::Add(n, pointB, v1);
		vtkMath::Add(n2, pointB, v2);
		if (vtkMath::Distance2BetweenPoints(v1, pointTest) > vtkMath::Distance2BetweenPoints(v2, pointTest))
		{
			return n2;
		}
		else
		{
			return n;
		}
	}

  //PointC is in the line defined by A and B?
  static bool isInLine(double* pointA, double* pointB, double* pointC)
  {
    return (abs(  (pointB[1] - pointA[1]) * pointC[0] - (pointB[0] - pointA[0]) * pointC[1]  + pointB[0] * pointA[1] - pointB[1] * pointA[0])/sqrt( pow(pointB[1] - pointA[1],2) + pow(pointB[0] - pointA[0], 2))) < 0.01;
  }

  static vtkSmartPointer<vtkImageData> wxImage2ImageData(wxImage img)
  {
    vtkSmartPointer<vtkImageImport> importer = vtkSmartPointer<vtkImageImport>::New();
    vtkSmartPointer<vtkImageData> imageData = vtkSmartPointer<vtkImageData>::New();
    importer->SetOutput(imageData);
    importer->SetDataSpacing(1, 1, 1);
    importer->SetDataOrigin(0, 0, 0);
    importer->SetWholeExtent(0, img.GetWidth() - 1, 0, img.GetHeight() - 1, 0, 0);
    importer->SetDataExtentToWholeExtent();
    importer->SetDataScalarTypeToUnsignedChar();
    importer->SetNumberOfScalarComponents(3);
    importer->SetImportVoidPointer(img.GetData());
    importer->Update();
    return imageData;
  }

  static void deletePoint(vtkSmartPointer<vtkPoints> points, vtkIdType id)
  {
    vtkSmartPointer<vtkPoints> newPoints = vtkSmartPointer<vtkPoints>::New();

    for (vtkIdType i = 0; i < points->GetNumberOfPoints(); i++)
    {
      if (i != id)
      {
        double p[3];
        points->GetPoint(i, p);
        newPoints->InsertNextPoint(p);
      }
    }

    points->ShallowCopy(newPoints);
  }

  static double distanceBetween2DisplayPoints(double* p0, double* p1)
  {
    return sqrt(pow(p0[0] - p1[0],2) + pow(p0[1] - p1[1], 2));
  }

  static bool isSamePoint(double* p0, double* p1)
  {
    return p0[0] == p1[0] && p0[1] == p1[1] && p0[2] == p1[2];
  }

  static void getSkewSym(double* v, vtkSmartPointer<vtkMatrix4x4> result)
  {
    result->SetElement(0, 0, 0);
    result->SetElement(0, 1, -v[2]);
    result->SetElement(0, 2, v[1]);
    result->SetElement(0, 3, 0);

    result->SetElement(1, 0, v[2]);
    result->SetElement(1, 1, 0);
    result->SetElement(1, 2, -v[0]);
    result->SetElement(1, 3, 0);

    result->SetElement(2, 0, -v[1]);
    result->SetElement(2, 1, v[0]);
    result->SetElement(2, 2, 0);
    result->SetElement(2, 3, 0);

    result->SetElement(3, 0, 0);
    result->SetElement(3, 1, 0);
    result->SetElement(3, 2, 0);
    result->SetElement(3, 3, 1);
  }

  static void multiplyMatrix4x4ByScalar(vtkSmartPointer<vtkMatrix4x4> mat, double scalar, vtkSmartPointer<vtkMatrix4x4> result)
  {
    for (unsigned int i = 0; i < 4; i++)
    {
      for (unsigned int j = 0; j < 4; j++)
      {
       result->SetElement(i,j,mat->GetElement(i,j)*scalar);
      } 
    }
  }

  static void sumMatrix4x4(vtkSmartPointer<vtkMatrix4x4> mat1, vtkSmartPointer<vtkMatrix4x4> mat2, vtkSmartPointer<vtkMatrix4x4> result)
  {
    for (unsigned int i = 0; i < 4; i++)
    {
      for (unsigned int j = 0; j < 4; j++)
      {
        result->SetElement(i, j, mat1->GetElement(i, j) + mat2->GetElement(i, j));
      }
    }
  }

  static double * getDisplayPosition(vtkSmartPointer<vtkRenderer> renderer, double * point)
  {
    double* display = new double[3];
    vtkInteractorObserver::ComputeWorldToDisplay(renderer, point[0], point[1], point[2], display);
    display[2] = 0;
    return display;
  }

  static bool pickPosition(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkAbstractPicker> picker, double* displayPosition, double * point)
  {
    if (picker->Pick(displayPosition[0], displayPosition[1], 0, renderer))
    {
      picker->GetPickPosition(point);
      return true;
    }
    return false;
  }

  static vtkSmartPointer<vtkActor> duplicateActor(vtkSmartPointer<vtkActor> actor)
  {
    vtkSmartPointer<vtkPolyData> polydata = vtkPolyData::SafeDownCast(actor->GetMapper()->GetInput());
    vtkSmartPointer<vtkTransformFilter> transformFilter = vtkSmartPointer<vtkTransformFilter>::New();
    vtkSmartPointer<vtkTransform> T = vtkSmartPointer<vtkTransform>::New();
    T->Identity();
    if (actor->GetUserMatrix() != NULL)
    {
      T->SetMatrix(actor->GetUserMatrix());
    }
    else if (actor->GetMatrix() != NULL)
    {
      T->SetMatrix(actor->GetMatrix());
    }
    transformFilter->SetTransform(T);
    transformFilter->SetInputData(polydata);
    transformFilter->Update();
    polydata = transformFilter->GetPolyDataOutput();
    vtkSmartPointer<vtkPolyData> polydataCopy = vtkSmartPointer<vtkPolyData>::New();
    polydataCopy->DeepCopy(polydata);

    vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->ScalarVisibilityOn();
    mapper->SetScalarModeToUsePointData();

    mapper->SetInputData(polydataCopy);

    vtkSmartPointer<vtkActor> newActor = vtkSmartPointer<vtkActor>::New();
    newActor->SetMapper(mapper);
    if (actor->GetTexture() != NULL)
    {
      vtkSmartPointer<vtkTexture> texture = vtkSmartPointer<vtkTexture>::New();
      vtkSmartPointer<vtkImageData> imgCopy = vtkSmartPointer<vtkImageData>::New();
      imgCopy->DeepCopy(actor->GetTexture()->GetInput());
      texture->SetInputData(imgCopy);
      newActor->SetTexture(texture);
      newActor->GetProperty()->SetInterpolationToPhong();
      newActor->GetProperty()->SetLighting(false);
    }
    return newActor;
  }

  static double getDistanceBetweenGeographicCoordinate(double lat1, double long1, double alt1, double lat2, double long2, double alt2)
  {
      double R = 6378.137;

      double dLat = vtkMath::RadiansFromDegrees(lat2) - vtkMath::RadiansFromDegrees(lat1);
      double dLon = vtkMath::RadiansFromDegrees(long2) - vtkMath::RadiansFromDegrees(long1);

      double a = sin(dLat / 2.0f) * sin(dLat / 2.0f) + cos(vtkMath::RadiansFromDegrees(lat1)) * cos(vtkMath::RadiansFromDegrees(lat2)) * sin(dLon / 2.0f) * sin(dLon / 2.0f);

      double c = 2 * atan2(sqrt(a), sqrt(1 - a));

      double d_xy = R * c * 1000;

      return sqrt(pow(d_xy,2) + pow(alt2 - alt1, 2));
  }

  static std::vector< std::vector< double> > multiplyMatrix(std::vector< std::vector<double> > m1, std::vector< std::vector<double> > m2)
  {
      std::vector< std::vector< double> > multiply;
      for (size_t i = 0; i < m1.size(); i++)
      {
          std::vector<double> row;
          multiply.push_back(row);
      }
      double sum = 0;
      for (size_t c = 0; c < m1.size(); c++)//number of rows of the first matrix
      {
          for (size_t d = 0; d < m2.at(0).size(); d++)//number of cols of the second matrix
          {
              for (size_t k = 0; k < m2.size(); k++)//number of rows of the second matrix
              {
                  sum = sum + m1[c][k] * m2[k][d];
              }
              multiply.at(c).push_back(sum);
              //multiply[c][d] = sum;
              sum = 0;
          }
      }
      return multiply;
  }

  static double* getVector(double* p1, double* p2)
  {
      double* p = new double[3];
      p[0] = p2[0] - p1[0]; p[1] = p2[1] - p1[1]; p[2] = p2[2] - p1[2];
      return p;
  }

  //This transform is used to align the vector A with the orientation of vector B
  static vtkSmartPointer<vtkTransform> getTransformToAlignVectors(double* a, double* b)
  {
      vtkMath::Normalize(a);
      vtkMath::Normalize(b);

      double rot_angle = acos(vtkMath::Dot(a, b));

      double* cVector = new double[3];
      vtkMath::Cross(a, b, cVector);

      vtkMath::Normalize(cVector);

      double x = cVector[0];
      double y = cVector[1];
      double z = cVector[2];

      double c = cos(rot_angle);

      double s = sin(rot_angle);

      vtkSmartPointer<vtkMatrix4x4> mat = vtkSmartPointer<vtkMatrix4x4>::New();

      mat->SetElement(0, 0, x*x*(1 - c) + c);
      mat->SetElement(0, 1, x*y*(1 - c) - z*s);
      mat->SetElement(0, 2, x*z*(1 - c) + y*s);
      mat->SetElement(0, 3, 0);
      mat->SetElement(1, 0, y*x*(1 - c) + z*s);
      mat->SetElement(1, 1, y*y*(1 - c) + c);
      mat->SetElement(1, 2, y*z*(1 - c) - x*s);
      mat->SetElement(1, 3, 0);
      mat->SetElement(2, 0, x*z*(1 - c) - y*s);
      mat->SetElement(2, 1, y*z*(1 - c) + x*s);
      mat->SetElement(2, 2, z*z*(1 - c) + c);
      mat->SetElement(2, 3, 0);
      mat->SetElement(3, 0, 0);
      mat->SetElement(3, 1, 0);
      mat->SetElement(3, 2, 0);
      mat->SetElement(3, 3, 1);

      vtkSmartPointer<vtkTransform> T = vtkSmartPointer<vtkTransform>::New();
      T->SetMatrix(mat);

      return T;
  }


  static double* getPlaneCoef(double* p1, double* p2, double* p3)
  {
      double a1 = p2[0] - p1[0];
      double b1 = p2[1] - p1[1];
      double c1 = p2[2] - p1[2];
      double a2 = p3[0] - p1[0];
      double b2 = p3[1] - p1[1];
      double c2 = p3[2] - p1[2];
      double a = b1 * c2 - b2 * c1;
      double b = a2 * c1 - a1 * c2;
      double c = a1 * b2 - b1 * a2;
      double d = (-a * p1[0] - b * p1[1] - c * p1[2]);
      double* plane_cof = new double[4];
      plane_cof[0] = a;
      plane_cof[1] = b;
      plane_cof[2] = c;
      plane_cof[3] = d;

      return plane_cof;
  }

  //Get the shortest distance between a plane and a point
  static double getDistancePlaneToPoint(double* planeCoef, double* point)
  {
      double dist = 0;
      for (size_t i = 0; i < 3; i++)
      {
          dist += planeCoef[i] * point[i];
      }

      dist += abs(planeCoef[3]);
      double sum = 0;
      for (size_t i = 0; i < 3; i++)
      {
          sum += planeCoef[i] * planeCoef[i];
      }

      dist /= sqrt(sum);

      return dist;
  }

  /*
  Get the PolyData of a specific actor
  */
  static vtkSmartPointer<vtkPolyData> getActorPolyData(vtkSmartPointer<vtkActor> actor)
  {
      if (actor == NULL)
      {
          return NULL;
      }
      vtkSmartPointer<vtkPolyData> meshPolyData = vtkPolyData::SafeDownCast(actor->GetMapper()->GetInput());
      vtkSmartPointer<vtkTransformFilter> transformFilter = vtkSmartPointer<vtkTransformFilter>::New();
      vtkSmartPointer<vtkTransform> T = vtkSmartPointer<vtkTransform>::New();
      if (actor->GetUserMatrix() != NULL)
      {
          T->SetMatrix(actor->GetUserMatrix());
      }
      else if (actor->GetMatrix() != NULL)
      {
          T->SetMatrix(actor->GetMatrix());
      }
      else
      {
          return meshPolyData;
      }
      transformFilter->SetTransform(T);
      transformFilter->SetInputData(meshPolyData);
      transformFilter->Update();
      meshPolyData = transformFilter->GetPolyDataOutput();
      return meshPolyData;
  }

  static std::wstring s2ws(const std::string& s)
  {
	  int len;
	  int slength = (int)s.length() + 1;
	  len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	  wchar_t* buf = new wchar_t[len];
	  MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	  std::wstring r(buf);
	  delete[] buf;
	  return r;
  }

  static int startProcess(std::string path_with_command)
  {
	  std::wstring stemp = s2ws(path_with_command);
	  LPWSTR path_command = const_cast<LPWSTR>(stemp.c_str());

	  STARTUPINFO si;
	  PROCESS_INFORMATION pi;

	  ZeroMemory(&si, sizeof(si));
	  si.cb = sizeof(si);
	  ZeroMemory(&pi, sizeof(pi));

	  // Start the child process. 
	  if (!CreateProcess(NULL,   // No module name (use command line)
		  path_command,        // Command line
		  NULL,           // Process handle not inheritable
		  NULL,           // Thread handle not inheritable
		  FALSE,          // Set handle inheritance to FALSE
		  0,              // No creation flags
		  NULL,           // Use parent's environment block
		  NULL,           // Use parent's starting directory 
		  &si,            // Pointer to STARTUPINFO structure
		  &pi)           // Pointer to PROCESS_INFORMATION structure
		  )
	  {
		  wxLogError("CreateProcess failed (%d).\n", GetLastError());
		  return 0;
	  }

	  // Wait until child process exits.
	  WaitForSingleObject(pi.hProcess, INFINITE);

	  // Close process and thread handles. 
	  CloseHandle(pi.hProcess);
	  CloseHandle(pi.hThread);
	  return 1;
  }

  static int startProcess(std::string path_with_command, std::string workingDirectory)
  {
	  std::wstring stemp = s2ws(path_with_command);
	  LPWSTR path_command = const_cast<LPWSTR>(stemp.c_str());

	  std::wstring stemp2 = s2ws(workingDirectory);
	  LPCWSTR working_dir = stemp2.c_str();

	  STARTUPINFO si;
	  PROCESS_INFORMATION pi;

	  ZeroMemory(&si, sizeof(si));
	  si.cb = sizeof(si);
	  ZeroMemory(&pi, sizeof(pi));

	  // Start the child process. 
	  if (!CreateProcess(NULL,   // No module name (use command line)
		  path_command,        // Command line
		  NULL,           // Process handle not inheritable
		  NULL,           // Thread handle not inheritable
		  FALSE,          // Set handle inheritance to FALSE
		  0,              // No creation flags
		  NULL,           // Use parent's environment block
		  working_dir,           // Use parent's starting directory 
		  &si,            // Pointer to STARTUPINFO structure
		  &pi)           // Pointer to PROCESS_INFORMATION structure
		  )
	  {
		  wxLogError("CreateProcess failed (%d).\n", GetLastError());
		  return 0;
	  }

	  // Wait until child process exits.
	  WaitForSingleObject(pi.hProcess, INFINITE);

	  // Close process and thread handles. 
	  CloseHandle(pi.hProcess);
	  CloseHandle(pi.hThread);
	  return 1;
  }

  //Adjust the path to be used with external processes
  static std::string preparePath(std::string path)
  {
	  std::replace(path.begin(), path.end(), '\\', '/');
	  return "\"" + path + "\"";
  }

  static std::string getExecutionPath()
  {
	  return vtksys::SystemTools::GetFilenamePath(wxStandardPaths::Get().GetExecutablePath().ToStdString());
  }

  static int getNumberOfCamerasSFM(std::string filename)
  {
	  std::ifstream sfmFile(filename);
	  std::string line;
	  if (sfmFile.is_open())
	  {
		  getline(sfmFile, line, '\n');
		  sfmFile.close();
		  return std::atoi(line.c_str());
	  }
	  return 0;
  }

  static int getNumberOfCamerasNVM(std::string filename)
  {
	  std::ifstream nvmFile(filename);
	  std::string line;
	  if (nvmFile.is_open())
	  {
		  getline(nvmFile, line, '\n');
		  getline(nvmFile, line, '\n');
		  getline(nvmFile, line, '\n');
		  nvmFile.close();
		  return std::atoi(line.c_str());
	  }
	  return 0;
  }

  static void removeExtraModelsFromNVM(std::string filename)
  {
	  std::ifstream nvmFile(filename);
	  std::stringstream out;
	  std::string line;
	  if (nvmFile.is_open())
	  {
		  //NVM_V3
		  for (unsigned int i = 0; i < 3; i++)
		  {
			  getline(nvmFile, line, '\n');
			  out << line << "\n";
		  }
		  int numberOfCameras = std::atoi(line.c_str());
		  for (int i = 0; i < numberOfCameras + 2; i++)
		  {
			  getline(nvmFile, line, '\n');
			  out << line << "\n";
		  }
		  int numberOfFeatures = std::atoi(line.c_str());
		  for (int i = 0; i < numberOfFeatures; i++)
		  {
			  getline(nvmFile, line, '\n');
			  out << line << "\n";
		  }
	  }
	  nvmFile.close();
	  //Overwrite the file
	  std::ofstream outFile(filename.c_str());
	  if (!outFile.good())
	  {
		  return;
	  }
	  outFile << out.rdbuf();
	  outFile.close();
  }

  //Write some T value inside a binary file
  template <class T>
  static void writeBin(std::ofstream &out, T val)
  {
	  out.write(reinterpret_cast<char*>(&val), sizeof(T));
  }

  static void savePLY(std::string filename, vtkSmartPointer<vtkPolyData> polyData)
  {
	  bool hasColor = false;
	  bool hasNormals = false;
	  bool hasFaces = false;
	  size_t qtdFaces;
	  size_t qtdPoints;
	  vtkSmartPointer<vtkPoints> points = polyData->GetPoints();
	  if (!points)
	  {
		  return;
	  }
	  qtdPoints = points->GetNumberOfPoints();
	  vtkSmartPointer<vtkUnsignedCharArray> colors = vtkUnsignedCharArray::SafeDownCast(polyData->GetPointData()->GetScalars());
	  if (colors)
	  {
		  hasColor = true;
	  }
	  vtkSmartPointer<vtkFloatArray> normals = vtkFloatArray::SafeDownCast(polyData->GetPointData()->GetNormals());
	  if (normals)
	  {
		  hasNormals = true;
	  }
	  qtdFaces = polyData->GetNumberOfCells();
	  if (qtdFaces != 0)
	  {
		  hasFaces = true;
	  }
	  //Write the ply file
	  std::ofstream outputFile(filename, std::ios::binary);
	  outputFile << "ply" << "\n";
	  outputFile << "format binary_little_endian 1.0" << "\n";
	  outputFile << "element vertex " << qtdPoints << "\n";
	  outputFile << "property float x" << "\n";
	  outputFile << "property float y" << "\n";
	  outputFile << "property float z" << "\n";
	  if (hasColor)
	  {
		  outputFile << "property uchar red" << "\n";
		  outputFile << "property uchar green" << "\n";
		  outputFile << "property uchar blue" << "\n";
	  }
	  if (hasNormals)
	  {
		  outputFile << "property float nx" << "\n";
		  outputFile << "property float ny" << "\n";
		  outputFile << "property float nz" << "\n";
	  }
	  if (hasFaces)
	  {
		  outputFile << "element face " <<qtdFaces << "\n";
		  outputFile << "property list uchar int vertex_indices" << "\n";
	  }
	  outputFile << "end_header" << "\n";
	  double* point, *color, *normal;
	  if (outputFile.is_open())
	  {
		  for (size_t i = 0; i < qtdPoints; i++)
		  {
			  point = points->GetPoint(i);
			  writeBin<float>(outputFile, point[0]);
			  writeBin<float>(outputFile, point[1]);
			  writeBin<float>(outputFile, point[2]);
			  if (hasColor)
			  {
				  color = colors->GetTuple3(i);
				  writeBin<unsigned char>(outputFile, color[0]);
				  writeBin<unsigned char>(outputFile, color[1]);
				  writeBin<unsigned char>(outputFile, color[2]);
			  }
			  if (hasNormals)
			  {
				  normal = normals->GetTuple3(i);
				  writeBin<float>(outputFile, normal[0]);
				  writeBin<float>(outputFile, normal[1]);
				  writeBin<float>(outputFile, normal[2]);
			  }
		  }
		  unsigned int sizeIds;
		  for (size_t i = 0; i < qtdFaces; i++)
		  {
			  vtkSmartPointer<vtkIdList> idList = vtkSmartPointer<vtkIdList>::New();
			  polyData->GetCellPoints(i, idList);
			  sizeIds = idList->GetNumberOfIds();
			  writeBin<unsigned char>(outputFile, sizeIds);
			  for (size_t j = 0; j < sizeIds; j++)
			  {
				  writeBin<int>(outputFile, idList->GetId(j));
			  }
		  }
	  }
	  outputFile.close();
  }

  static vtkSmartPointer<vtkImageData> loadImage(std::string filePath)
  {
	  std::string extension = vtksys::SystemTools::GetFilenameExtension(filePath);
	  vtkSmartPointer<vtkImageReader2> reader;
	  if (extension == ".jpg" || extension == ".JPG")
	  {
		  reader = vtkSmartPointer<vtkJPEGReader>::New();
	  }
	  else if (extension == ".png" || extension == ".PNG")
	  {
		  reader = vtkSmartPointer<vtkPNGReader>::New();
	  }
	  else if (extension == ".bmp" || extension == ".BMP")
	  {
		  reader = vtkSmartPointer<vtkBMPReader>::New();
	  }
	  else if (extension == ".tiff" || extension == ".TIFF")
	  {
		  reader = vtkSmartPointer<vtkTIFFReader>::New();
	  }
	  //else if (extension == ".pnm" || extension == ".PNM")
	  //{
		 // reader = vtkSmartPointer<vtkPNMReader>::New();
	  //}
	  else
	  {
		  return NULL;
	  }
	  reader->SetFileName(filePath.c_str());
	  reader->Update();
	  if (!reader->GetOutput())
	  {
		  return NULL;
	  }
	  return reader->GetOutput();
  }

};

#endif