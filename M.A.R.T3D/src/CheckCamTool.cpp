#include "CheckCamTool.h"

vtkStandardNewMacro(CheckCamTool);

void CheckCamTool::setMesh(Mesh * mesh)
{
  if (mesh != NULL)
  {
    this->mesh = mesh;
  }
}

void CheckCamTool::setTreeMesh(wxTreeListCtrl * treeMesh)
{
  this->treeMesh = treeMesh;
}

//----------------------------------------------------------------------------
CheckCamTool::CheckCamTool() : vtk3DWidget()
{
  this->EventCallbackCommand->SetCallback(CheckCamTool::ProcessEvents);
}

//----------------------------------------------------------------------------
CheckCamTool::~CheckCamTool()
{
  destruct();
  treeMesh = NULL;
}

//----------------------------------------------------------------------------
void CheckCamTool::SetEnabled(int enabling)
{
  if (!this->Interactor)
  {
    return;
  }

  if (enabling) //------------------------------------------------------------
  {
    if (this->Enabled) //already enabled, just return
    {
      return;
    }
    if (!this->CurrentRenderer)
    {
      this->SetCurrentRenderer(this->Interactor->FindPokedRenderer(
        this->Interactor->GetLastEventPosition()[0],
        this->Interactor->GetLastEventPosition()[1]));
      if (this->CurrentRenderer == NULL)
      {
        return;
      }
    }
    this->Enabled = 1;

    createPicker();

    // listen for the following events
    if (lineWidget == NULL)
    {
      lineWidget = vtkSmartPointer<LineWidget>::New();
      lineWidget->SetInteractor(this->Interactor);
      lineWidget->setCloseLoopOnFirstNode(true);
      if (mesh != NULL)
      {
        vtkSmartPointer<LineWidgetRepresentation> rep = vtkSmartPointer<LineWidgetRepresentation>::New();
        for (int i = 0; i < mesh->actors.size(); i++)
        {
          rep->addProp(mesh->actors.at(i));
        }
        if (mesh->actors.size() == 1)// if we have a lot of actors the cell locator will delay the picking
        {
          rep->addLocator(mesh->getCellLocator());
        }
        lineWidget->SetRepresentation(rep);
      }
      else
      {
        wxMessageBox("You should set the mesh before enabling the measure tool!", "Error", wxICON_ERROR);
      }
    }
    lineWidget->EnabledOn();
    lineWidget->AddObserver(vtkCommand::EndInteractionEvent, this->EventCallbackCommand, this->Priority);

    this->InvokeEvent(vtkCommand::EnableEvent, NULL);
  }

  else //disabling----------------------------------------------------------
  {
    if (!this->Enabled) //already disabled, just return
    {
      return;
    }
    this->Enabled = 0;

    // don't listen for events any more
    this->Interactor->RemoveObserver(this->EventCallbackCommand);

    // turn off the various actors
    destruct();

    this->InvokeEvent(vtkCommand::DisableEvent, NULL);
    this->SetCurrentRenderer(NULL);
  }

  this->Interactor->Render();
}

void CheckCamTool::PlaceWidget(double bounds[6])
{
}

//----------------------------------------------------------------------------
void CheckCamTool::ProcessEvents(vtkObject* vtkNotUsed(object),
  unsigned long event,
  void* clientdata,
  void* vtkNotUsed(calldata))
{
  CheckCamTool* self =
    reinterpret_cast<CheckCamTool *>(clientdata);

  //okay, let's do the right thing
  switch (event)
  {
  case vtkCommand::EndInteractionEvent:
    self->checkVisibility();
    break;
  }
}

void CheckCamTool::createPicker()
{
  if (cellPicker == NULL)
  {
    cellPicker = vtkSmartPointer<vtkCellPicker>::New();
  }
  cellPicker->InitializePickList();
  cellPicker->RemoveAllLocators();
  mesh->createPickList(cellPicker);
  cellPicker->AddLocator(mesh->getCellLocator());
}

//----------------------------------------------------------------------------
void CheckCamTool::checkVisibility()
{
  if (lineWidget == NULL)
  {
    return;
  }
  vtkSmartPointer<LineWidgetRepresentation> rep = lineWidget->GetRepresentation();
  if (!rep->isLoopClosed())
  {
    return;
  }
  wxBeginBusyCursor(wxHOURGLASS_CURSOR);
  vtkSmartPointer<vtkPoints> pointsLine = rep->getPoints();
  clearVisibleCameras();
  for (int i = 0; i < pointsLine->GetNumberOfPoints(); i++)
  {
    validPoints.push_back(Draw::createSphere(CurrentRenderer, pointsLine->GetPoint(i), 0.01, 255, 0, 0));
  }
  //Store the initial camera
  vtkSmartPointer<vtkCamera> initialCamera = vtkSmartPointer<vtkCamera>::New();
  initialCamera->DeepCopy(CurrentRenderer->GetActiveCamera());
  for (int i = 0; i < mesh->cameras.size(); i++)
  {
    Camera* cam = mesh->cameras.at(i);
    if (cam->imagePolygon != NULL)
    {
      //To avoid wasting time and processing, we first do a simple test to see if the point is in the camera FOV.
      int cont = 0;
      bool intersections = 1;
      while (intersections != 0 && cont < validPoints.size())
      {
        if (!intersectPlaneWithLine(cam->cameraPoints.at(0), cam->imagePolygon, validPoints.at(cont)->GetCenter()))
        {
          intersections = 0;
        }
        cont++;
      }
      //If every point is seen by the camera we check if there is anything obstructing the vision
      if (intersections)
      {
        Utils::updateCamera(CurrentRenderer, cam);
        int cont = 0;
        bool pointsVisibles = 1;
        while (pointsVisibles != 0 && cont < validPoints.size())
        {
          double* pointCheckCamDisplay = Utils::getDisplayPosition(CurrentRenderer, validPoints.at(cont)->GetCenter());
          double clickPos[3];
          Utils::pickPosition(CurrentRenderer, cellPicker, pointCheckCamDisplay, clickPos);
          if (vtkMath::Distance2BetweenPoints(clickPos, validPoints.at(cont)->GetCenter()) > 0.003)
          {
            pointsVisibles = 0;
          }
          delete pointCheckCamDisplay;
          cont++;
        }
        if (pointsVisibles)
        {
          visibleCameras.push_back(cam);
        }
      }
    }
    cam = NULL;
  }
  CurrentRenderer->SetActiveCamera(initialCamera);
  for (int i = 0; i < visibleCameras.size(); i++)
  {
    treeMesh->SetItemImage(visibleCameras.at(i)->getListItemCamera(), 0);
    visibleCameras.at(i)->actorFrustum->GetProperty()->SetColor(0, 255, 0);
  }
  treeMesh->Expand(mesh->getListItemMeshCameras());
  CurrentRenderer->GetRenderWindow()->Render();
  wxEndBusyCursor();
}

void CheckCamTool::destruct()
{
  clearVisibleCameras();
  if (lineWidget != NULL)
  {
    lineWidget->EnabledOff();
    lineWidget->RemoveObserver(this->EventCallbackCommand);
    lineWidget = NULL;
  }
  mesh = NULL;
  cellPicker = NULL;
}

void CheckCamTool::clearVisibleCameras()
{
  //clear the OK icon of the tree and the color of the camera actors
  for (int i = 0; i < visibleCameras.size(); i++)
  {
    treeMesh->SetItemImage(visibleCameras.at(i)->getListItemCamera(), -1);
    visibleCameras.at(i)->actorFrustum->GetProperty()->SetColor(255, 255, 255);
  }
  visibleCameras.clear();
  for (int i = 0; i < validPoints.size(); i++)
  {
    this->CurrentRenderer->RemoveActor(validPoints.at(i));
  }
  validPoints.clear();
}

bool CheckCamTool::intersectPlaneWithLine(double * p1, vtkSmartPointer<vtkPolygon> polygon, double * pointCheckCam)
{
  double n[3];
  polygon->ComputeNormal(polygon->GetPoints()->GetNumberOfPoints(), static_cast<double*>(polygon->GetPoints()->GetData()->GetVoidPointer(0)), n);
  double bounds[6];
  polygon->GetPoints()->GetBounds(bounds);
  double t;
  double intersection[3];
  if (vtkPlane::IntersectWithLine(p1, pointCheckCam, n, polygon->GetPoints()->GetPoint(0), t, intersection))
  {
    if (polygon->PointInPolygon(intersection, polygon->GetPoints()->GetNumberOfPoints(), static_cast<double*>(polygon->GetPoints()->GetData()->GetVoidPointer(0)), bounds, n))
    {
      return true;
    }
  }
  return false;
}