#pragma once
#ifndef __VOLUME__H__
#define __VOLUME__H__

#include <vtkSmartPointer.h>
#include <vtkCaptionActor2D.h>
#include <vtkPolyData.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkMassProperties.h>
#include <vtkRenderer.h>
#include <wx/msgdlg.h> 
#include <wx/treelist.h>
#include <vtkTransformFilter.h>
#include <vtkPolyDataMapper.h>

class Volume 
{
private:
  wxTreeListItem listItem;
  bool visible;
public:
  vtkSmartPointer<vtkCaptionActor2D> text;
  vtkSmartPointer<vtkPolyData> polyData;
  vtkSmartPointer<vtkActor> actor;
  double volume;
  //Index of the volume in the tree(Just to identify it, not the index of the vector)
  int index;

  Volume(bool visible);
  ~Volume();

  /*
  Should be called before the delete, it remove the actors from the scene and the listItem from the tree
  */
  void destruct(vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree);

  void setVisibility(bool visibility);
  bool getVisibility();

  void setListItem(wxTreeListItem item);
  wxTreeListItem getListItem();

  /*
  Update the measure unit
  */
  void updateText(wxString measureUnit);

  /*
  Re-calculate the volume and update the measure unit
  */
  void updateCalibration(double calibration, wxString measureUnit);

  /*
  Tranform the mesh using T
  */
  void transform(vtkSmartPointer<vtkTransform> T);

  /*
  Update the text position
  */
  void updateTextPosition();

};

#endif