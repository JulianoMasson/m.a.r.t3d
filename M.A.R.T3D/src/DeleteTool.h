#pragma once
#include "vtkInteractionWidgetsModule.h" // For export macro
#include "vtk3DWidget.h"

#include "vtkActor.h"
#include "vtkCallbackCommand.h"
#include <vtkPropPicker.h>
#include "vtkPickingManager.h"
#include "vtkPlane.h"
#include "vtkPolyData.h"
#include "vtkProperty.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkCaptionActor2D.h"
#include "Utils.h"
#include "Draw.h"
#include "Mesh.h"
#include <wx\string.h>
#include <wx\textdlg.h> 
#include "Calibration.h"
#include "LineWidget.h"
#include "vtkClipPolyData.h"
#include <vtkSelection.h>
#include <vtkSelectionNode.h>
#include <vtkInformation.h>
#include <vtkExtractSelectedIds.h>
#include <vtkDataSetSurfaceFilter.h>
#include <sstream>
#include <vtkExtractSelection.h>
#include <set>

class DeleteTool : public vtk3DWidget
{
public:
    /**
    * Instantiate the object.
    */
    static DeleteTool *New();

    vtkTypeMacro(DeleteTool, vtk3DWidget);

    //@{
    /**
    * Methods that satisfy the superclass' API.
    */
    virtual void SetEnabled(int);
    virtual void PlaceWidget(double bounds[6]);
    //@}

    //@{
    /**
    * Set the mesh that is going to be used
    */
    void setMesh(Mesh* mesh);
    //@}

    bool enterKeyPressed();

protected:
    DeleteTool();
    ~DeleteTool();

    //handles the events
    static void ProcessEvents(vtkObject* object, unsigned long event,
        void* clientdata, void* calldata);

    vtkSmartPointer<LineWidget> lineWidget = NULL;

    // Controlling ivars
    void UpdateRepresentation();
    void createActorPolygon();


    Mesh* mesh;

    //CELL or POINT
    int fieldType = -1;

    std::vector<vtkSmartPointer<vtkPolyData>> polyDatas;
    std::vector<vtkSmartPointer<vtkActor>> actorsPolygon;
    std::vector<vtkSmartPointer<vtkSelection>> selections;

private:
    DeleteTool(const DeleteTool&) VTK_DELETE_FUNCTION;
    void operator=(const DeleteTool&) VTK_DELETE_FUNCTION;
};
