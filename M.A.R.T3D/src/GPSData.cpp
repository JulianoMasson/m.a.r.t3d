#include "GPSData.h"

GPSData::GPSData()
{
}

GPSData::~GPSData()
{
}

void GPSData::setLongitude(double longitude)
{
	this->longitude = longitude;
}

double GPSData::getLongitude()
{
	return longitude;
}

void GPSData::setLatitude(double latitude)
{
	this->latitude = latitude;
}

double GPSData::getLatitude()
{
	return latitude;
}

void GPSData::setAltitude(double altitude)
{
	this->altitude = altitude;
}

double GPSData::getAltitude()
{
	return altitude;
}

#include "Utils.h"

double GPSData::getDistanceBetweenGPSData(GPSData * gpsData)
{
    return Utils::getDistanceBetweenGeographicCoordinate(this->latitude, this->longitude, this->altitude, gpsData->latitude, gpsData->longitude, gpsData->altitude);
}
