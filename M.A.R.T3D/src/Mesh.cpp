#include "Mesh.h"

Mesh::Mesh()
{
	listItemMesh = NULL;
	listItemMeshCameras = NULL;
	listItemMeshTexture = NULL;
  listItemMeshVolumes= NULL;
	visible = false;
	textureVisibility = false;
  qtdVolumes = 0;
  calibration = new Calibration();
}

Mesh::Mesh(bool meshVisibility, bool textureVisibility)
{
	listItemMesh = NULL;
	listItemMeshCameras = NULL;
	listItemMeshTexture = NULL;
  listItemMeshVolumes = NULL;
	visible = meshVisibility;
	this->textureVisibility = textureVisibility;
  qtdVolumes = 0;
  calibration = new Calibration();
}

Mesh::~Mesh()
{
	actors.clear();
	textureNames.clear();
	textures.clear();
	cameras.clear();
	volumes.clear();
	if (lastPosition != NULL)
	{
	  delete lastPosition;
	  lastPosition = NULL;
	}
}

void Mesh::destruct(vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree)
{
  destructCameras(renderer,tree);
  destructTextures(renderer, tree);
  destructVolumes(renderer, tree);
  destructVolume(renderer,tree);
  for (int i = 0; i < actors.size(); i++)
  {
    renderer->RemoveActor(actors.at(i));
  }
  actors.clear();
  if (listItemMesh != NULL)
  {
    tree->DeleteItem(listItemMesh);
    listItemMesh = NULL;
  }
  meshPolyData = NULL;
  meshCellLocator = NULL;
}

void Mesh::destructCameras(vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree)
{
  for (int i = 0; i < cameras.size(); i++)
  {
    cameras.at(i)->destruct(renderer,tree);
    delete cameras.at(i);
  }
  cameras.clear();
  if (listItemMeshCameras != NULL)
  {
    tree->DeleteItem(listItemMeshCameras);
    listItemMeshCameras = NULL;
  }
  if (camerasPath != NULL)
  {
    renderer->RemoveActor(camerasPath);
    camerasPath = NULL;
  }
}

void Mesh::destructTextures(vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree)
{
  textures.clear();
  if (listItemMeshTexture != NULL)
  {
    tree->DeleteItem(listItemMeshTexture);
    listItemMeshTexture = NULL;
  }
}

void Mesh::destructVolumes(vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree)
{
  for (int i = 0; i < volumes.size(); i++)
  {
    volumes.at(i)->destruct(renderer, tree);
    delete volumes.at(i);
  }
  volumes.clear();
  if (listItemMeshVolumes != NULL)
  {
    tree->DeleteItem(listItemMeshVolumes);
    listItemMeshVolumes = NULL;
  }
}

void Mesh::destructVolume(vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl * tree)
{
  if (volumeActor != NULL)
  {
    renderer->RemoveActor(volumeActor);
    volumeActor = NULL;
  }
  if (listItemMeshVolume != NULL)
  {
    tree->DeleteItem(listItemMeshVolume);
    listItemMeshVolume = NULL;
  }
}

void Mesh::deleteCamera(int cameraIndex, vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree)
{
  cameras.at(cameraIndex)->destruct(renderer,tree);
  delete cameras.at(cameraIndex);
  cameras.erase(cameras.begin() + cameraIndex);
}

void Mesh::deleteVolume(int volumeIndex, vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree)
{
  volumes.at(volumeIndex)->destruct(renderer, tree);
  delete volumes.at(volumeIndex);
  volumes.erase(volumes.begin() + volumeIndex);
}

void Mesh::createPickList(vtkSmartPointer<vtkAbstractPicker> picker)
{
  if (picker != NULL)
  {
    for (int i = 0; i < actors.size(); i++)
    {
      picker->AddPickList(actors.at(i));
    }
    for (int i = 0; i < volumes.size(); i++)
    {
      picker->AddPickList(volumes.at(i)->actor);
    }
    if (volumeActor != NULL)
    {
      picker->AddPickList(volumeActor);
    }
    picker->PickFromListOn();
  }
}

void Mesh::setListItemMesh(wxTreeListItem listItem)
{
	listItemMesh = listItem;
}

wxTreeListItem Mesh::getListItemMesh()
{
	return listItemMesh;
}

void Mesh::setListItemMeshCameras(wxTreeListItem listItem)
{
	listItemMeshCameras = listItem;
}

wxTreeListItem Mesh::getListItemMeshCameras()
{
	return listItemMeshCameras;
}

void Mesh::setListItemMeshTexture(wxTreeListItem listItem)
{
	listItemMeshTexture = listItem;
}

wxTreeListItem Mesh::getListItemMeshTexture()
{
	return listItemMeshTexture;
}

void Mesh::setListItemMeshVolumes(wxTreeListItem listItem)
{
  listItemMeshVolumes = listItem;
}

wxTreeListItem Mesh::getListItemMeshVolumes()
{
  return listItemMeshVolumes;
}

void Mesh::setVolumesVisibility(bool visible)
{
  for (int i = 0; i < volumes.size(); i++)
  {
    volumes.at(i)->setVisibility(visible);
  }
}

bool Mesh::getVolumesVisibility()
{
  for (int i = 0; i < volumes.size(); i++)
  {
    if (volumes.at(i)->getVisibility())
    {
      return true;
    }
  }
  return false;
}

void Mesh::addVolume(Volume* vol)
{
  volumes.push_back(vol);
  qtdVolumes++;
}

void Mesh::setMeshVisibility(bool visibility)
{
	for (int i = 0; i < actors.size(); i++)
	{
		actors.at(i)->SetVisibility(visibility);
	}
	visible = visibility;
}

bool Mesh::getMeshVisibility()
{
	return visible;
}

void Mesh::setVolumeVisibility(bool visible)
{
  volumeActor->SetVisibility(visible);
}

bool Mesh::getVolumeVisibility()
{
  return volumeActor->GetVisibility();
}

void Mesh::setListItemMeshVolume(wxTreeListItem listItem)
{
  listItemMeshVolume = listItem;
}

wxTreeListItem Mesh::getListItemMeshVolume()
{
  return listItemMeshVolume;
}

void Mesh::setCamerasVisibility(bool visibility)
{
	for (int i = 0; i < cameras.size(); i++)
	{
		cameras.at(i)->setVisibility(visibility);
	}
}

bool Mesh::getCamerasVisibility()
{
	for (int i = 0; i < cameras.size(); i++)
	{
		if (cameras.at(i)->getVisibility())
		{
      return true;
		}
	}
	return false;
}

bool Mesh::hasImages()
{
	for (int i = 0; i < cameras.size(); i++)
	{
		if (cameras.at(i)->imageActor != NULL)
		{
			return true;
		}
	}
	return false;
}

void Mesh::setTextureVisibility(bool visible)
{
	//PointCloud
	if (textures.size() == 0)
	{
		for (int i = 0; i < actors.size(); i++)
		{
			actors.at(i)->GetMapper()->SetScalarVisibility(visible);
		}
		textureVisibility = visible;
		return;
	}
	//Mesh
	if (textures.size() != actors.size())
	{
		return;
	}
	if (visible)
	{
		for (int i = 0; i < actors.size(); i++)
		{
			actors.at(i)->SetTexture(textures.at(i));
      actors.at(i)->GetMapper()->SetScalarVisibility(true);
			actors.at(i)->GetProperty()->SetInterpolationToPhong();
      actors.at(i)->GetProperty()->SetLighting(false);
		}
	}
	else
	{
		for (int i = 0; i < actors.size(); i++)
		{
			actors.at(i)->SetTexture(NULL);
      actors.at(i)->GetMapper()->SetScalarVisibility(false);
      actors.at(i)->GetProperty()->SetInterpolationToFlat();
      actors.at(i)->GetProperty()->SetAmbient(0);
      actors.at(i)->GetProperty()->SetLighting(true);
		}
	}
	textureVisibility = visible;
}

bool Mesh::getTextureVisibility()
{
	return textureVisibility;
}

int Mesh::checkCamerasFilePath()
{
	int missingImages = 0;
	for (int i = 0; i < cameras.size(); i++)
	{
		if (!Utils::exists(cameras.at(i)->filePath))
		{
			missingImages++;
		}
	}
	return missingImages;
}

int Mesh::checkCamerasFilePath(std::string newPath)
{
	if (newPath == "")
	{
		return checkCamerasFilePath();
	}
	int missingImages = 0;
	for (int i = 0; i < cameras.size(); i++)
	{
		if (!Utils::exists((newPath + "\\" + vtksys::SystemTools::GetFilenameName(cameras.at(i)->filePath))))
		{
			missingImages++;
		}
	}
	return missingImages;
}

void Mesh::createCamerasPath(vtkSmartPointer<vtkRenderer> renderer)
{
  if (cameras.at(0)->imageActor == NULL || camerasPath != NULL)
  {
    return;
  }
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
  for(int i = 0; i< cameras.size();i++)
  {
    points->InsertPoint(i,cameras.at(i)->cameraPoints.at(0));
  }
  camerasPath = Draw::create3DLine(renderer,points);
  camerasPath->SetVisibility(false);
}

void Mesh::setCameraPathVisibility(bool visible)
{
  if (camerasPath != NULL)
  {
    camerasPath->SetVisibility(visible);
  }
}

bool Mesh::getCameraPathVisibility()
{
  if (camerasPath != NULL)
  {
    return camerasPath->GetVisibility();
  }
  return false;
}

bool sortByFilename(Camera* i, Camera* j) 
{ 
  return (i->filePath<j->filePath); 
}
void Mesh::sortCameras()
{
  std::sort(cameras.begin(),cameras.end(), sortByFilename);
}

vtkSmartPointer<vtkPolyData> Mesh::getPolyData()
{
  if (actors.size() == 0)
  {
    return NULL;
  }
  if (lastPosition != NULL)
  {
    if (sqrt(vtkMath::Distance2BetweenPoints(actors.at(0)->GetPosition(), lastPosition)) < 1e-5  && meshPolyData != NULL)
    {
      return meshPolyData;
    }
    delete lastPosition;
    lastPosition = NULL;
  }
  if (actors.size() == 1)
  {
    meshPolyData = vtkPolyData::SafeDownCast(actors.at(0)->GetMapper()->GetInput());
  }
  else//We need to put all the actors in the same PolyData
  {
    vtkSmartPointer<vtkAppendPolyData> appendFilter = vtkSmartPointer<vtkAppendPolyData>::New();
    int size = actors.size();
    for (int i = 0; i < size; i++)
    {
      appendFilter->AddInputData(vtkPolyData::SafeDownCast(actors.at(i)->GetMapper()->GetInput()));
    }
    appendFilter->Update();
    meshPolyData = appendFilter->GetOutput();
    if (meshPolyData == NULL)
    {
      return NULL;
    }
    vtkSmartPointer<vtkCleanPolyData> cleaner = vtkSmartPointer<vtkCleanPolyData>::New();
    cleaner->SetInputData(meshPolyData);
    cleaner->Update();
    meshPolyData = cleaner->GetOutput();
  }
  vtkSmartPointer<vtkTransformFilter> transformFilter = vtkSmartPointer<vtkTransformFilter>::New();
  vtkSmartPointer<vtkTransform> T = vtkSmartPointer<vtkTransform>::New();
  if (actors.at(0)->GetUserMatrix() != NULL)
  {
    T->SetMatrix(actors.at(0)->GetUserMatrix());
  }
  else if (actors.at(0)->GetMatrix() != NULL)
  {
    T->SetMatrix(actors.at(0)->GetMatrix());
  }
  else
  {
    lastPosition = Utils::createDoubleVector(actors.at(0)->GetPosition());
    return meshPolyData;
  }
  transformFilter->SetTransform(T);
  transformFilter->SetInputData(meshPolyData);
  transformFilter->Update();
  meshPolyData = transformFilter->GetPolyDataOutput();
  lastPosition = Utils::createDoubleVector(actors.at(0)->GetPosition());
  return meshPolyData;
}

vtkSmartPointer<vtkCellLocator> Mesh::getCellLocator()
{
  if (meshCellLocator == NULL)
  {
    meshCellLocator = vtkSmartPointer<vtkCellLocator>::New();
    meshCellLocator->SetDataSet(this->getPolyData());
    meshCellLocator->BuildLocator();
  }
  return meshCellLocator;
}

void Mesh::setPickable(bool pickable)
{
  if (volumeActor != NULL)
  {
    volumeActor->SetPickable(pickable);
  }
  for (int i = 0; i < actors.size(); i++)
  {
    actors.at(i)->SetPickable(pickable);
  }
}

void Mesh::transform(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkTransform> T)
{
    if (camerasPath != NULL)
    {
        renderer->RemoveActor(camerasPath);
        camerasPath = NULL;
    }
  meshCellLocator = NULL;
  meshPolyData = NULL;
  vtkSmartPointer<vtkTransform> T3;
  for (size_t i = 0; i < actors.size(); i++)
  {
    vtkSmartPointer<vtkPolyData> polydata = vtkPolyData::SafeDownCast(actors.at(i)->GetMapper()->GetInput());
    vtkSmartPointer<vtkTransformFilter> transformFilter = vtkSmartPointer<vtkTransformFilter>::New();
    vtkSmartPointer<vtkMatrix4x4> lasT = vtkSmartPointer<vtkMatrix4x4>::New();
    lasT->Identity();
    if (actors.at(i)->GetUserMatrix() != NULL)
    {
      lasT = actors.at(0)->GetUserMatrix();
    }
    else if (actors.at(i)->GetMatrix() != NULL)
    {
      lasT = actors.at(i)->GetMatrix();
    }
    vtkSmartPointer<vtkMatrix4x4> resultT = vtkSmartPointer<vtkMatrix4x4>::New();
    vtkMatrix4x4::Multiply4x4(lasT, T->GetMatrix(), resultT);

    T3 = vtkSmartPointer<vtkTransform>::New();
    T3->SetMatrix(resultT);

    transformFilter->SetTransform(T3);
    transformFilter->SetInputData(polydata);
    transformFilter->Update();

    actors.at(i)->GetMapper()->SetInputDataObject(transformFilter->GetOutput());
    actors.at(i)->GetMapper()->Modified();
  }
  if (volumeActor != NULL)
  {
    vtkSmartPointer<vtkTransformFilter> transformFilter = vtkSmartPointer<vtkTransformFilter>::New();
    transformFilter->SetTransform(T3);
    transformFilter->SetInputData(vtkPolyData::SafeDownCast(volumeActor->GetMapper()->GetInputAsDataSet()));
    transformFilter->Update();
    volumeActor->GetMapper()->SetInputConnection(transformFilter->GetOutputPort());
  }
  for (int i = 0; i < volumes.size(); i++)
  {
    volumes.at(i)->transform(T);
  }
  for (int i = 0; i < cameras.size(); i++)
  {
    cameras.at(i)->transform(renderer, T);
  }
}

void Mesh::setCalibration(Calibration * cal)
{
  if (this->calibration != NULL)
  {
    delete this->calibration;
  }
  this->calibration = cal;
  //Update the volume's
  for (int j = 0; j < volumes.size(); j++)
  {
    volumes.at(j)->updateCalibration(calibration->getScaleFactor(), calibration->getMeasureUnit());
  }
}

Calibration* Mesh::getCalibration()
{
  return this->calibration;
}

void Mesh::updatePoints(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkPoints> points, vtkSmartPointer<vtkUnsignedCharArray> colors, vtkSmartPointer<vtkFloatArray> normals)
{
  vtkSmartPointer<vtkPolyData> polyData = this->getPolyData();
  if (polyData->GetPolys()->GetNumberOfCells() == 0)//it is a point cloud
  {
    for (int i = 0; i < actors.size(); i++)
    {
      renderer->RemoveActor(actors.at(i));
    }
    actors.clear();
    meshCellLocator = NULL;
    meshPolyData = NULL;
    actors.push_back(Draw::createPointCloud(renderer, points, colors, normals));
  }
}

void Mesh::updatePoints(vtkSmartPointer<vtkPoints> points, vtkSmartPointer<vtkUnsignedCharArray> colors, vtkSmartPointer<vtkFloatArray> normals)
{
  vtkSmartPointer<vtkPolyData> polyData = this->getPolyData();
  if (polyData->GetPolys()->GetNumberOfCells() == 0)//it is a point cloud
  {
    meshCellLocator = NULL;
    meshPolyData = NULL;
    meshPolyData = Draw::createPointCloud(points, colors, normals);
    actors.at(0)->GetMapper()->SetInputDataObject(meshPolyData);
    actors.at(0)->GetMapper()->Modified();
  }
}

void Mesh::updatePoints(vtkDataObject * data)
{
  vtkSmartPointer<vtkPolyData> polyData = this->getPolyData();
  if (polyData->GetPolys()->GetNumberOfCells() == 0)//it is a point cloud
  {
    meshCellLocator = NULL;
    meshPolyData = NULL;

    vtkSmartPointer<vtkTransformFilter> transformFilter = vtkSmartPointer<vtkTransformFilter>::New();
    vtkSmartPointer<vtkTransform> T = vtkSmartPointer<vtkTransform>::New();
    if (actors.at(0)->GetUserMatrix() != NULL)
    {
      T->SetMatrix(actors.at(0)->GetUserMatrix());
    }
    else if (actors.at(0)->GetMatrix() != NULL)
    {
      T->SetMatrix(actors.at(0)->GetMatrix());
    }
    T->Inverse();
    transformFilter->SetTransform(T);
    transformFilter->SetInputData(data);
    transformFilter->Update();

    actors.at(0)->GetMapper()->SetInputDataObject(transformFilter->GetOutput());
    actors.at(0)->GetMapper()->Modified();
  }
}

void Mesh::updateCells(vtkDataObject * data)
{
    meshCellLocator = NULL;
    meshPolyData = NULL;

    vtkSmartPointer<vtkTransformFilter> transformFilter = vtkSmartPointer<vtkTransformFilter>::New();
    vtkSmartPointer<vtkTransform> T = vtkSmartPointer<vtkTransform>::New();
    if (actors.at(0)->GetUserMatrix() != NULL)
    {
        T->SetMatrix(actors.at(0)->GetUserMatrix());
    }
    else if (actors.at(0)->GetMatrix() != NULL)
    {
        T->SetMatrix(actors.at(0)->GetMatrix());
    }
    T->Inverse();
    transformFilter->SetTransform(T);
    transformFilter->SetInputData(data);
    transformFilter->Update();

    actors.at(0)->GetMapper()->SetInputDataObject(transformFilter->GetOutput());
    actors.at(0)->GetMapper()->Modified();
}

void Mesh::updateCells(vtkDataObject * data, unsigned int actorIndex)
{
    meshCellLocator = NULL;
    meshPolyData = NULL;

    vtkSmartPointer<vtkTransformFilter> transformFilter = vtkSmartPointer<vtkTransformFilter>::New();
    vtkSmartPointer<vtkTransform> T = vtkSmartPointer<vtkTransform>::New();
    if (actors.at(actorIndex)->GetUserMatrix() != NULL)
    {
        T->SetMatrix(actors.at(actorIndex)->GetUserMatrix());
    }
    else if (actors.at(actorIndex)->GetMatrix() != NULL)
    {
        T->SetMatrix(actors.at(actorIndex)->GetMatrix());
    }
    T->Inverse();
    transformFilter->SetTransform(T);
    transformFilter->SetInputData(data);
    transformFilter->Update();

    actors.at(actorIndex)->GetMapper()->SetInputDataObject(transformFilter->GetOutput());
    actors.at(actorIndex)->GetMapper()->Modified();
}

void Mesh::updateActors(std::vector<vtkSmartPointer<vtkActor>> newActors)
{
  if (actors.size() != newActors.size())
  {
    return;
  }
  meshCellLocator = NULL;
  meshPolyData = NULL;
  for (size_t i = 0; i < actors.size(); i++)
  {
    vtkSmartPointer<vtkPolyData> polydata = vtkPolyData::SafeDownCast(newActors.at(i)->GetMapper()->GetInput());

    vtkSmartPointer<vtkTransformFilter> transformFilter = vtkSmartPointer<vtkTransformFilter>::New();
    vtkSmartPointer<vtkTransform> T = vtkSmartPointer<vtkTransform>::New();
    if (newActors.at(i)->GetUserMatrix() != NULL)
    {
      T->SetMatrix(newActors.at(i)->GetUserMatrix());
    }
    else if (newActors.at(i)->GetMatrix() != NULL)
    {
      T->SetMatrix(newActors.at(i)->GetMatrix());
    }
    T->Inverse();
    transformFilter->SetTransform(T);
    transformFilter->SetInputData(polydata);
    transformFilter->Update();

    actors.at(i)->GetMapper()->SetInputDataObject(transformFilter->GetOutput());
    actors.at(i)->GetMapper()->Modified();
  }
  
}

void Mesh::calibrateUsingGPSData(vtkSmartPointer<vtkRenderer> renderer)
{
    if (cameras.size() < 0)
    {
        return;
    }
    for (size_t i = 0; i < cameras.size(); i++)
    {
        if (cameras.at(i)->gpsData == NULL)
        {
            return;
        }
    }
    // theta = (X^T.X)^-1.X^T.Y
    size_t camSize = cameras.size();
    std::vector< std::vector<double> > X;
    std::vector< std::vector<double> > XTranspose;
    for (size_t i = 0; i < 2; i++)
    {
        std::vector<double> row;
        XTranspose.push_back(row);
    }
    std::vector< std::vector<double> > Y;
    double distanceVTK, distanceGPS;
    for (size_t i = 0; i < camSize; i++)
    {
        for (size_t j = i + 1; j < camSize; j++)
        {
            distanceVTK = cameras.at(i)->getDistanceBetweenCameraCenters(cameras.at(j));
            distanceGPS = cameras.at(i)->gpsData->getDistanceBetweenGPSData(cameras.at(j)->gpsData);
            std::vector<double> row;
            row.push_back(distanceVTK);
            row.push_back(1.0);
            X.push_back(row);

            XTranspose.at(0).push_back(distanceVTK);
            XTranspose.at(1).push_back(1.0);

            std::vector<double> row2;
            row2.push_back(distanceGPS);
            Y.push_back(row2);
        }
    }
    std::vector< std::vector<double> > resMult = Utils::multiplyMatrix(XTranspose, X);
    //inverse 2x2
    double det = resMult[0][0] * resMult[1][1] - resMult[0][1] * resMult[1][0];
    
    std::vector< std::vector<double> > inverse;
    std::vector<double> row;
    row.push_back(resMult[1][1] / det);
    row.push_back(-resMult[0][1] / det);
    inverse.push_back(row);

    std::vector<double> row2;
    row2.push_back(-resMult[1][0] / det);
    row2.push_back(resMult[0][0] / det);
    inverse.push_back(row2);

    std::vector< std::vector<double> > resMult2 = Utils::multiplyMatrix(XTranspose, Y);

    std::vector< std::vector<double> > result = Utils::multiplyMatrix(inverse, resMult2);

    this->setCalibration(new Calibration(result.at(0).at(0), "m"));
    this->calibration->setCalibratedUsingGPSData(true);



    //Transform
    double* cameraVector = new double[3];
    for (size_t i = 0; i < 3; i++)
    {
        cameraVector[i] = 0;
    }
    for (size_t i = 0; i < cameras.size(); i++)
    {
        double* p = Utils::getVector(cameras.at(i)->cameraPoints.at(5), cameras.at(i)->cameraPoints.at(0));
        for (size_t i = 0; i < 3; i++)
        {
            cameraVector[i] += p[i];
        }
        delete p;
    }
    for (size_t i = 0; i < 3; i++)
    {
        cameraVector[i] /= cameras.size();
    }
    //Align Z axis
    this->transform(renderer, Utils::getTransformToAlignVectors(cameraVector, Utils::createDoubleVector(0, 0, 1)));

    //Get minimum and maximum altitude
    double* bounds = this->getPolyData()->GetBounds();// 0 xmin, 1 xmax, 2 ymin, 3 ymax, 4 zmin, 5 zmax

    std::vector<double*> points;
    points.push_back(Utils::createDoubleVector(bounds[1], bounds[2], bounds[5]));//xmax, ymin, zmax
    points.push_back(Utils::createDoubleVector(bounds[0], bounds[2], bounds[5]));//xmin, ymin, zmax
    points.push_back(Utils::createDoubleVector(bounds[0], bounds[3], bounds[5]));//xmin, ymax, zmax
    points.push_back(Utils::createDoubleVector(bounds[1], bounds[2], bounds[4]));//xmax, ymin, zmin
    points.push_back(Utils::createDoubleVector(bounds[0], bounds[2], bounds[4]));//xmin, ymin, zmin
    points.push_back(Utils::createDoubleVector(bounds[0], bounds[3], bounds[4]));//xmin, ymax, zmin

    //double* normal = Utils::getNormal(points.at(0), points.at(1), points.at(2), cameras.at(0)->cameraPoints.at(0));

    //vtkMath::Normalize(normal);

    double* planeCoef = Utils::getPlaneCoef(points.at(0), points.at(1), points.at(2));
    double maxAlt = Utils::getDistancePlaneToPoint(planeCoef, cameras.at(0)->cameraPoints.at(0)) * this->calibration->getScaleFactor();
    delete planeCoef;

    double* planeCoef2 = Utils::getPlaneCoef(points.at(3), points.at(4), points.at(5));
    double minAlt = Utils::getDistancePlaneToPoint(planeCoef2, cameras.at(0)->cameraPoints.at(0)) * this->calibration->getScaleFactor();
    delete planeCoef2;

    this->calibration->setMinimumAltitude(cameras.at(0)->gpsData->getAltitude() - minAlt);
    this->calibration->setMaximumAltitude(cameras.at(0)->gpsData->getAltitude() - maxAlt);


    /*double dist = 0;
    for (size_t i = 0; i < 3; i++)
    {
        dist += planeCoef[i] * cameras.at(0)->cameraPoints.at(0)[i];
    }

    dist += abs(planeCoef[3]);
    double sum = 0;
    for (size_t i = 0; i < 3; i++)
    {
        sum += planeCoef[i] * planeCoef[i];
    }

    dist /= sqrt(sum);*/

    
    


    /*double angleVTK = atan2(cameras.at(0)->cameraPoints.at(0)[1] + cameras.at(0)->gpsData->getLongitude(), cameras.at(0)->cameraPoints.at(0)[0] + cameras.at(0)->gpsData->getLatitude());
    double angleGPS = atan2(cameras.at(0)->gpsData->getLongitude(), cameras.at(0)->gpsData->getLatitude());

    vtkSmartPointer<vtkTransform> T2 = vtkSmartPointer<vtkTransform>::New();
    T2->RotateZ(vtkMath::DegreesFromRadians(angleGPS - angleVTK));

    this->transform(renderer, T2);*/

}
