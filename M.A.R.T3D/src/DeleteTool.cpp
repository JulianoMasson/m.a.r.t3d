#include "DeleteTool.h"

vtkStandardNewMacro(DeleteTool);

void DeleteTool::setMesh(Mesh * mesh)
{
    if (mesh != NULL)
    {
        this->mesh = mesh;
        for (unsigned int i = 0; i < mesh->actors.size(); i++)
        {
            polyDatas.push_back(Utils::getActorPolyData(mesh->actors.at(i)));
        }
        if (mesh->getPolyData()->GetPolys()->GetNumberOfCells() != 0)
        {
            fieldType = vtkSelectionNode::CELL;
        }
        else
        {
            fieldType = vtkSelectionNode::POINT;
        }
    }
}

//----------------------------------------------------------------------------
DeleteTool::DeleteTool() : vtk3DWidget()
{
    this->EventCallbackCommand->SetCallback(DeleteTool::ProcessEvents);
}

//----------------------------------------------------------------------------
DeleteTool::~DeleteTool()
{
    if (lineWidget != NULL)
    {
        lineWidget->EnabledOff();
        lineWidget->RemoveObserver(this->EventCallbackCommand);
    }
    for (unsigned int i = 0; i < actorsPolygon.size(); i++)
    {
        CurrentRenderer->RemoveActor(actorsPolygon.at(i));
    }
    actorsPolygon.clear();
    selections.clear();
    polyDatas.clear();
    mesh = NULL;
}

//----------------------------------------------------------------------------
void DeleteTool::SetEnabled(int enabling)
{
    if (!this->Interactor)
    {
        return;
    }

    if (enabling) //------------------------------------------------------------
    {
        if (this->Enabled) //already enabled, just return
        {
            return;
        }
        if (!this->CurrentRenderer)
        {
            this->SetCurrentRenderer(this->Interactor->FindPokedRenderer(
                this->Interactor->GetLastEventPosition()[0],
                this->Interactor->GetLastEventPosition()[1]));
            if (this->CurrentRenderer == NULL)
            {
                return;
            }
        }
        this->Enabled = 1;

        // listen for the following events
        vtkRenderWindowInteractor *i = this->Interactor;
        i->AddObserver(vtkCommand::MouseMoveEvent,
            this->EventCallbackCommand, this->Priority);
        i->AddObserver(vtkCommand::LeftButtonPressEvent,
            this->EventCallbackCommand, this->Priority);
        i->AddObserver(vtkCommand::KeyPressEvent,
            this->EventCallbackCommand, this->Priority);

        if (lineWidget == NULL)
        {
            lineWidget = vtkSmartPointer<LineWidget>::New();
            lineWidget->SetInteractor(this->Interactor);
            lineWidget->setCloseLoopOnFirstNode(true);
            if (mesh != NULL)
            {
                vtkSmartPointer<LineWidgetRepresentation> rep = vtkSmartPointer<LineWidgetRepresentation>::New();
                rep->set2DRepresentation(true);
                lineWidget->SetRepresentation(rep);
            }
            else
            {
                wxMessageBox("You should set the mesh before enabling the measure tool!", "Error", wxICON_ERROR);
            }
        }
        lineWidget->EnabledOn();
        lineWidget->AddObserver(vtkCommand::StartInteractionEvent, this->EventCallbackCommand, this->Priority);
        lineWidget->AddObserver(vtkCommand::InteractionEvent, this->EventCallbackCommand, this->Priority);
        lineWidget->AddObserver(vtkCommand::EndInteractionEvent, this->EventCallbackCommand, this->Priority);

        this->InvokeEvent(vtkCommand::EnableEvent, NULL);
    }

    else //disabling----------------------------------------------------------
    {
        if (!this->Enabled) //already disabled, just return
        {
            return;
        }
        this->Enabled = 0;

        // don't listen for events any more
        this->Interactor->RemoveObserver(this->EventCallbackCommand);

        // turn off the various actors
        if (lineWidget != NULL)
        {
            lineWidget->EnabledOff();
            lineWidget->RemoveObserver(this->EventCallbackCommand);
            lineWidget = NULL;
        }
        for (unsigned int i = 0; i < actorsPolygon.size(); i++)
        {
            CurrentRenderer->RemoveActor(actorsPolygon.at(i));
        }
        actorsPolygon.clear();
        selections.clear();
        polyDatas.clear();
        mesh = NULL;

        this->InvokeEvent(vtkCommand::DisableEvent, NULL);
        this->SetCurrentRenderer(NULL);
    }

    this->Interactor->Render();
}

void DeleteTool::PlaceWidget(double bounds[6])
{
}

//----------------------------------------------------------------------------
void DeleteTool::ProcessEvents(vtkObject* vtkNotUsed(object),
    unsigned long event,
    void* clientdata,
    void* vtkNotUsed(calldata))
{
    DeleteTool* self =
        reinterpret_cast<DeleteTool *>(clientdata);

    //okay, let's do the right thing
    switch (event)
    {
    case vtkCommand::MouseMoveEvent:
    case vtkCommand::StartInteractionEvent:
    case vtkCommand::InteractionEvent:
    case vtkCommand::EndInteractionEvent:
        self->UpdateRepresentation();
        break;
    case vtkCommand::LeftButtonPressEvent://Do not let the user rotate the camera
    {
        if (self->lineWidget != NULL)
        {
            if (self->lineWidget->GetRepresentation()->getPoints() != NULL)
            {
                self->EventCallbackCommand->SetAbortFlag(1);
            }
        }
        break;
    }
    case vtkCommand::KeyPressEvent:
    {
        char key = self->Interactor->GetKeyCode();
        if (key == 'I')
        {
            for (unsigned int i = 0; i < self->selections.size(); i++)
            {
                self->selections.at(i)->GetNode(0)->GetProperties()->Set(vtkSelectionNode::INVERSE(), !self->selections.at(i)->GetNode(0)->GetProperties()->Get(vtkSelectionNode::INVERSE()));
            }
            self->createActorPolygon();
            self->Interactor->Render();
        }
        break;
    }
    }
}

//----------------------------------------------------------------------------
void DeleteTool::UpdateRepresentation()
{
    if (lineWidget != NULL)
    {
        vtkSmartPointer<LineWidgetRepresentation> rep = lineWidget->GetRepresentation();
        if (rep->isLoopClosed() && actorsPolygon.size() == 0)
        {
            wxBeginBusyCursor(wxHOURGLASS_CURSOR);
            //Creating the 2D polygon
            vtkSmartPointer<vtkPoints> pointsLine = rep->getPoints();
            size_t size = pointsLine->GetNumberOfPoints() - 1;//The last point is equal to the first
            vtkSmartPointer<vtkPolygon> polygon = vtkSmartPointer<vtkPolygon>::New();
            for (size_t i = 0; i < size; i++)
            {
                polygon->GetPoints()->InsertNextPoint(Utils::getDisplayPosition(CurrentRenderer, pointsLine->GetPoint(i)));
            }
            //Computing parameters for the method PointInPolygon
            double n[3];
            polygon->ComputeNormal(polygon->GetPoints()->GetNumberOfPoints(), static_cast<double*>(polygon->GetPoints()->GetData()->GetVoidPointer(0)), n);
            double bounds[6];
            polygon->GetPoints()->GetBounds(bounds);
            size_t numberOfPoints = polygon->GetPoints()->GetNumberOfPoints();
            double* voidPointer = static_cast<double*>(polygon->GetPoints()->GetData()->GetVoidPointer(0));
            size_t selectedElements = 0;
            for (unsigned int k = 0; k < polyDatas.size(); k++)
            {
                //Get Mesh points
                vtkSmartPointer<vtkPoints> pointsMesh = polyDatas.at(k)->GetPoints();
                if (pointsMesh == NULL)
                {
                    continue;
                }
                size_t sizeCloud = pointsMesh->GetNumberOfPoints();

                //List of points inside the polygon
                vtkSmartPointer<vtkIdTypeArray> idsInside = vtkSmartPointer<vtkIdTypeArray>::New();
                idsInside->SetNumberOfComponents(1);
                for (size_t i = 0; i < sizeCloud; i++)
                {
                    if (polygon->PointInPolygon(Utils::getDisplayPosition(CurrentRenderer, pointsMesh->GetPoint(i)), numberOfPoints, voidPointer, bounds, n))
                    {
                        idsInside->InsertNextValue(i);
                    }
                }

                std::set<vtkIdType> uniqueIds;
                if (fieldType == vtkSelectionNode::CELL)
                {
                    size = idsInside->GetNumberOfValues();
                    for (unsigned int i = 0; i < size; i++)
                    {
                        vtkSmartPointer<vtkIdList> idListTemp = vtkSmartPointer<vtkIdList>::New();
                        polyDatas.at(k)->GetPointCells(idsInside->GetValue(i), idListTemp);
                        for (size_t i = 0; i < idListTemp->GetNumberOfIds(); i++)
                        {
                            uniqueIds.insert(idListTemp->GetId(i));
                        }
                    }
                }
                else
                {
                    size = idsInside->GetNumberOfValues();
                    for (unsigned int i = 0; i < size; i++)
                    {
                        uniqueIds.insert(idsInside->GetValue(i));
                    }
                }
                selectedElements += uniqueIds.size();

                vtkSmartPointer<vtkIdTypeArray> idsTemp = vtkSmartPointer<vtkIdTypeArray>::New();
                idsTemp->SetNumberOfComponents(1);
                std::set<vtkIdType>::iterator it;
                for (it = uniqueIds.begin(); it != uniqueIds.end(); ++it) {
                    idsTemp->InsertNextValue(*it);
                }

                vtkSmartPointer<vtkSelection> selection = vtkSmartPointer<vtkSelection>::New();

                vtkSmartPointer<vtkSelectionNode> selectionNode = vtkSmartPointer<vtkSelectionNode>::New();
                selectionNode->SetFieldType(fieldType);
                selectionNode->SetContentType(vtkSelectionNode::INDICES);
                selectionNode->SetSelectionList(idsTemp);

                selection->AddNode(selectionNode);

                selections.push_back(selection);
            }

            
            if (selectedElements == 0)
            {
                wxMessageBox("Nothing was selected", "Warning", wxICON_WARNING);
                rep->reset();
                lineWidget->EnabledOff();
                lineWidget->EnabledOn();
                selections.clear();
            }
            else
            {
                createActorPolygon();
                if (lineWidget != NULL)
                {
                    lineWidget->EnabledOff();
                    lineWidget->RemoveObserver(this->EventCallbackCommand);
                    lineWidget = NULL;
                }
            }
            this->Interactor->Render();
            wxEndBusyCursor();
        }
    }

}

void DeleteTool::createActorPolygon()
{
    if (actorsPolygon.size() != 0)
    {
        for (unsigned int i = 0; i < actorsPolygon.size(); i++)
        {
            CurrentRenderer->RemoveActor(actorsPolygon.at(i));
        }
        actorsPolygon.clear();
    }
    for (unsigned int i = 0; i < selections.size(); i++)
    {
        actorsPolygon.push_back(Draw::createPointCloud(CurrentRenderer, polyDatas.at(i), selections.at(i), 255, 0, 0));
    }
}

bool DeleteTool::enterKeyPressed()
{
    if (selections.size() != 0)
    {
        wxBeginBusyCursor(wxHOURGLASS_CURSOR);
        for (unsigned int i = 0; i < selections.size(); i++)
        {
            vtkSmartPointer<vtkPolyData> poly = NULL;
            if (fieldType == vtkSelectionNode::CELL)
            {
                poly = polyDatas.at(i);
                vtkSmartPointer<vtkIdTypeArray> idsTemp = vtkIdTypeArray::SafeDownCast(selections.at(i)->GetNode(0)->GetSelectionList());
                for (unsigned int j = 0; j < idsTemp->GetNumberOfValues(); j++)
                {
                    poly->DeleteCell(idsTemp->GetValue(j));
                }
                poly->RemoveDeletedCells();
            }
            else
            {
                //Delete all mesh
                if (polyDatas.at(i)->GetNumberOfCells() != selections.at(i)->GetNode(0)->GetSelectionList()->GetNumberOfValues())
                {
                    selections.at(i)->GetNode(0)->GetProperties()->Set(vtkSelectionNode::INVERSE(), !selections.at(i)->GetNode(0)->GetProperties()->Get(vtkSelectionNode::INVERSE()));
                    vtkSmartPointer<vtkExtractSelectedIds> extractSelectedIds = vtkSmartPointer<vtkExtractSelectedIds>::New();
                    extractSelectedIds->SetInputData(0, polyDatas.at(i));
                    extractSelectedIds->SetInputData(1, selections.at(i));
                    extractSelectedIds->Update();

                    vtkSmartPointer<vtkDataSetSurfaceFilter> surfaceFilter = vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
                    surfaceFilter->SetInputConnection(extractSelectedIds->GetOutputPort());
                    surfaceFilter->Update();

                    poly = surfaceFilter->GetOutput();
                }
                else
                {
                    poly = vtkSmartPointer<vtkPolyData>::New();
                }
            }
            mesh->updateCells(poly, i);
        }
        this->SetEnabled(false);
        wxEndBusyCursor();
        return true;
    }
    return false;
}
