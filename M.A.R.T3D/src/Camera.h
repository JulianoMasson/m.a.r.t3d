#pragma once
#ifndef __CAMERA__H__
#define __CAMERA__H__

#include <wx\treebase.h>
#include <wx\vector.h>
#include <vtkActor.h>
#include <vtkImageActor.h>
#include <vtkMatrix4x4.h>
#include <wx/treelist.h>
#include <vtkSmartPointer.h>
#include <vtkPolygon.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include "exif.h"
#include "GPSData.h"

class Camera {
private:
	wxTreeListItem listItemCamera;
	wxTreeListItem listItemGPSData;
	bool visible;
public:
	Camera();
	~Camera();

	/*
	Should be called before the delete, it remove the actors from the scene and the listItem from the tree
	*/
	void destruct(vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree);

	/*
	Tranform the mesh using T
	*/
	void transform(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkTransform> T);
	void setListItemCamera(wxTreeListItem listItem);
	wxTreeListItem getListItemCamera();
	void setVisibility(bool visible);
	bool getVisibility();
	/*
	Calc the camera points:
	origin of the camera = [0]
	[1]--------[2]
	|		        |
	|----[5]----|
	|           |
	[4]--------[3]
	[5]->the distance(z coordinate) is actually 5% less than the others points, to avoid exceed the image with zoon when we do Utils::updateCamera
	*/
	int calcCameraPoints();
	int changeViewUp();
	/*
	The View Up vector of the camera
	*/
	double* viewUp;
	/*
	0 - UP
	1 - Right
	2 - Down
	3 - Left
	*/
	int viewUpDirection;

	vtkSmartPointer<vtkActor> actorFrustum;
	vtkSmartPointer<vtkImageActor> imageActor;
	int height;
	int width;
	vtkSmartPointer<vtkMatrix4x4> cameraMatrixInverted;
	/*Vector with these points
	origin of the camera = [0]
	[1]--------[2]
	|		    |
	|----[5]----|
	|           |
	[4]--------[3]
	[5]->the distance(z coordinate) is actually 5% less than the others points, to avoid exceed the image with zoon when we do Utils::updateCamera
	*/
	std::vector<double*> cameraPoints;
	/*
	We use this to make some colliding tests faster
	[1]--------[2]
	|		    |
	|           |
	|           |
	[4]--------[3]
	*/
	vtkSmartPointer<vtkPolygon> imagePolygon;
	double focalX;
	double focalY;
	//The path from the nvm file, it is not changed if you change the path in the load
	std::string filePath;

	/*
	Get the Latitude, Longitude and Altitude data.
	*/
	void updateGPSData();

	/*
	GPS Data extracted from the image.
	*/
	GPSData* gpsData = NULL;

	double getDistanceBetweenCameraCenters(Camera* c);
	void createActorFrustrum(vtkSmartPointer<vtkRenderer> renderer);
	void createImageActor(vtkSmartPointer<vtkRenderer> renderer);


	
};

#endif