#pragma once
#ifndef __FRMPRINCIPAL_H__
#define __FRMPRINCIPAL_H__
#include <vtkAutoInit.h>
VTK_MODULE_INIT(vtkRenderingOpenGL2);

#define vtkRenderingCore_AUTOINIT 3(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingOpenGL2)
#define vtkRenderingVolume_AUTOINIT 1(vtkRenderingVolumeOpenGL2)

#include "VRDialog.h"//this is why this header is here #error directive: gl.h included before glew.h
#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/panel.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/sizer.h>
#include <wx/frame.h>
#include "wxVTKRenderWindowInteractor.h"
//InteractorStyle
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkActor.h>
#include <vtkLine.h>
#include <vtkTextActor.h>
#include <vtkTextProperty.h>
#include <vtkCellArray.h>
#include <vtkPolyDataMapper2D.h>
#include <vtkActor2D.h>
#include <vtkProperty2D.h>
#include <vtkPointPicker.h>
#include <vtkSmartPointer.h>
#include <vtkPropPicker.h>
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkObjectFactory.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRendererCollection.h>
#include <sstream>
#include <vector>
//
#include <vtkOBJImporter.h>
#include <vtkNew.h>
#include <vtkCamera.h>
#include <vtkCommand.h>
#include <vtksys/SystemTools.hxx>
#include <vtkAssemblyPath.h>
#include <vtkProperty.h>
#include <wx/treelist.h>
#include "Mesh.h"
#include <wx/numdlg.h> 
#include <vtkMath.h>
#include "Camera.h"
#include <vtkJPEGReader.h>
#include <vtkImageActor.h>
#include <vtkTransform.h>
#include <vtkLineSource.h>
#include <vtkImageActor.h>
#include <vtkImageMapper3D.h>
#include <wx/image.h>
#include <wx/choicdlg.h> 
#include <vtkCellPicker.h>
#include <vtkProp3DCollection.h>
#include <math.h>
#include "Draw.h"
#include <vtkImageData.h>
#include <wx/log.h>
#include <vtkPlane.h>
#include <wx/splitter.h>
#include <vtkPolygon.h>
#include <wx/progdlg.h>
#include <wx/utils.h> 
#include "OutputErrorWindow.h"
#include "Utils.h"
#include <wx/dirdlg.h>
#include <wx/stdpaths.h>
#include <wx/filename.h>
#include <wx/dir.h>
#include <vtkPLYReader.h>
#include <vtkOBJReader.h>
#include <wx/aboutdlg.h>
#include <vtkWindowToImageFilter.h>
#include <vtkPNGWriter.h>
#include <vtkPNGReader.h>
#include <vtkMatrix3x3.h>
#include <vtkQuaternion.h>
#include <vtkVector.h>
#include <vtkOBBTree.h>
#include <vtkBMPReader.h>
#include <vtkTIFFReader.h>
#include <vtkPNMReader.h>
#include <vtkPointData.h>
#include <vtkPLY.h>
#include <vtkDataSetMapper.h>
#include <vtkDelaunay3D.h>
#include <vtkFillHolesFilter.h>
#include <vtkClipClosedSurface.h>
#include <vtkPlaneCollection.h>
#include <vtkMassProperties.h>
#include <vtkAppendPolyData.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include "InteractorStyle.h"
#include <wx\event.h>
#include "DistanceTool.h"
#include "AngleTool.h"
#include "ReconstructionDialog.h"
#include <vtkPolyData.h>
#include "ElevationTool.h"
#include <vtkAxesActor.h>
#include "AxisWidget.h"
#include "TransformDialog.h"
#include "ConfigurationDialog.h"
#include "ReconstructionDialog.h"
#include "ViewTool.h"
#include "SnapshotTool.h"
#include <wx/html/helpctrl.h>
#include <vtkPLYWriter.h>
#include "AlignWithAxisTool.h"
#include "CheckCamTool.h"
#include "AboutDialog.h"
#include "DeleteTool.h"
#include <vtkOBJExporter.h>

using namespace std;

class FrmPrincipal : public wxFrame
{
private:
	DECLARE_EVENT_TABLE()

  //VTK
  vtkSmartPointer<vtkRenderer> renderer = NULL;

  //Help
  wxHtmlHelpController*  helpController;

  //Axis
  vtkSmartPointer<vtkOrientationMarkerWidget> axisWidget = NULL;

  //Tools
  vtkSmartPointer<DistanceTool> dTool = NULL;
  vtkSmartPointer<AngleTool> aTool = NULL;
  vtkSmartPointer<ElevationTool> elevationTool = NULL;
  vtkSmartPointer<ViewTool> viewTool = NULL;
  vtkSmartPointer<AlignWithAxisTool> aligWithAxisTool = NULL;
  vtkSmartPointer<CheckCamTool> checkCamTool = NULL;
  vtkSmartPointer<DeleteTool> deleteTool = NULL;

  void disableTools();
  void disableMeasureTools();
  //Disable all tools but the tool in the parameter
  /*
  Distance - 0
  Angle - 1
  Elevation - 2
  CheckCam - 3
  Volume - 4
  Delete points and faces - 5
  */
  void disableMeasureTools(int toolToAvoid);


  vtkSmartPointer<InteractorStyle> iterStyle;
	//Load
	//OBJ
	int loadTexturedOBJFile(string filenameOBJ, string filenameMTL);
	int loadOBJFile(string filenameOBJ);
	//PLY
	int loadPLYFile(string filename);
	//Camera
	int loadCameraParameters(string path, Mesh* selectedMesh);
	int loadCameraParametersFromSFMFile(string pathSFM, Mesh* selectedMesh);
	int loadCameraParametersFromNVMFile(string pathNVM, Mesh* selectedMesh);

	/*selectedMesh: The index of the desired mesh in the mesh vector
	newPath: If you want to load the images in other path instead of the default in the cam->filepath
	*/
	void loadCameras(Mesh* selectedMesh,std::string newPath);
	
	//Tree
	wxTreeListItem addItemToTree(wxTreeListItem parentItem, string itemName, wxCheckBoxState state);
  //Get the selected Mesh from the tree
  Mesh* getMeshFromTree();

	//Splitter
	bool isTreeVisible;
	void setTreeVisibility(bool visibility);

  //Menu
	void OnMenuOpen(wxCommandEvent& event);
	void OnMenuOpenCameras(wxCommandEvent& event);
	void OnMenuExportMesh(wxCommandEvent& event);
	void OnMenuExportPoisson(wxCommandEvent& event);
	void OnMenuSnapshot(wxCommandEvent& event);
	void OnMenuTransform(wxCommandEvent& event);
	void OnMenuShowAxis(wxCommandEvent& event);
	void OnMenuShowViews(wxCommandEvent& event);
	void OnMenuFlyToPoint(wxCommandEvent& event);
	void OnMenuPoisson(wxCommandEvent& event);
	void OnMenuHelp(wxCommandEvent& event);
	void OnMenuAbout(wxCommandEvent& event);
  //Tools
	void OnToolDelete(wxCommandEvent& event);
	void OnToolPoints(wxCommandEvent& event);
	void OnToolWireframe(wxCommandEvent& event);
	void OnToolSurface(wxCommandEvent& event);
	void OnToolMeasure(wxCommandEvent& event);
	void OnToolAngle(wxCommandEvent& event);
	void OnToolCalcVolume(wxCommandEvent& event);
	void OnToolElevation(wxCommandEvent& event);
	void OnToolCalibration(wxCommandEvent& event);
	void OnToolCheckCamVisibility(wxCommandEvent& event);
	void OnToolStartReconstruction(wxCommandEvent& event);
	void OnToolSnapshot(wxCommandEvent& event);
	void OnToolLight(wxCommandEvent& event);
	void OnToolDeletePointsFaces(wxCommandEvent& event);
	void OnToolVR(wxCommandEvent& event);
  /*
  Select checkbox tree
  */
	void OnTreeChoice(wxTreeListEvent& event);
  /*
  Double Click tree
  */
	void OnTreeItemActivated(wxTreeListEvent& event);
	void OnClose(wxCloseEvent& event);
	void OnSplitterDClick(wxSplitterEvent& event);
	void OnSashPosChanged(wxSplitterEvent& event);
	void OnSize(wxSizeEvent& event);
	void OnKeyPress(wxKeyEvent& event);
	void OnDropFiles(wxDropFilesEvent& event);

protected:
	//Execution path
	//std::string executionPath;
	//VTK
	vtkSmartPointer<OutputErrorWindow> outputErrorWindow;
	std::vector<Mesh*> meshVector;
	
	//WX
	wxSplitterWindow* splitterWind;
	wxBoxSizer* boxSizer;
	vtkSmartPointer<wxVTKRenderWindowInteractor> vtk_panel = NULL;
	//Menu
	wxMenuBar* menuBar;
	wxMenu* menuFile;
	wxMenuItem* menuItemOpen;
	wxMenuItem* menuItemSnapshot;
	wxMenuItem* menuItemShowAxis;
	wxMenuItem* menuItemShowViews;
	wxMenuItem* menuItemFlyToPoint;
	wxMenu* menuReconstruction;
	wxMenuItem* menuItemPoisson;
	wxMenu* menuHelp;
	wxMenuItem* menuItemHelp;
	wxMenuItem* menuItemAbout;
	//ToolBar
	wxToolBar* toolBar;
	wxToolBarToolBase* toolOpen;
	wxToolBarToolBase* toolDelete;
	wxToolBarToolBase* toolPoints;
	wxToolBarToolBase* toolWireframe;
	wxToolBarToolBase* toolSurface;
	wxToolBarToolBase* toolMeasure;
	wxToolBarToolBase* toolAngle;
	wxToolBarToolBase* toolCalcVolume;
	wxToolBarToolBase* toolElevation;
	wxToolBarToolBase* toolCalibration;
	wxToolBarToolBase* toolCheckCamVisibility;
	wxToolBarToolBase* toolStartReconstruction;
	wxToolBarToolBase* toolDeletePoints;
	wxToolBarToolBase* toolVR;
	//Bitmaps
	wxBitmap bmpToolMeasureOFF;
	wxBitmap bmpToolMeasureON;
	wxBitmap bmpToolAngleOFF;
	wxBitmap bmpToolAngleON;
	wxBitmap bmpToolCalcVolumeOFF;
	wxBitmap bmpToolCalcVolumeON;
	wxBitmap bmpToolElevationOFF;
	wxBitmap bmpToolElevationON;
	wxBitmap bmpToolCheckCamVisibilityOFF;
	wxBitmap bmpToolCheckCamVisibilityON;
	wxBitmap bmpToolDeletePointsFacesOFF;
	wxBitmap bmpToolDeletePointsFacesON;
	wxBitmap bmpToolVROFF;
	wxBitmap bmpToolVRON;

	//Tree
	wxTreeListCtrl* treeMesh;
	wxBitmap bmpTreeCheckCamera;
public:
	FrmPrincipal(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(500, 300), long style = wxDEFAULT_FRAME_STYLE | wxTAB_TRAVERSAL);

	~FrmPrincipal();

  //Help
  wxHtmlHelpController* getHelpController();

	//Load
	void loadSelector(wxArrayString paths, bool needMessageBox);

  void OnLeftDClick(wxMouseEvent& event);

};
enum {
	idMenuOpen,
    idErrorEnter,//Do not use this ID, it will trigger when you press Enter
	idMenuOpenCameras,
	idMenuExportMesh,
	idMenuExportPoisson,
	idMenuSnapshot,
	idMenuTransform,
	idMenuShowAxis,
	idMenuShowViews,
	idMenuFlyToPoint,
	idMenuPoisson,
	idMenuAbout,
	idMenuHelp,
	idToolOpen,
	idToolDelete,
	idToolPoints,
	idToolWireframe,
	idToolSurface,
	idToolMeasure,
	idToolAngle,
	idToolCalcVolume,
	idToolElevation,
	idToolCalibration,
	idToolCheckCamVisibility,
	idToolStartReconstruction,
	idToolSnapshot,
	idToolLight,
	idToolDeletePointsFaces,
	idToolVR,
	idSplitterWindow
};

#endif //__FRMPRINCIPAL_H__