#include "InteractorStyle.h"


vtkStandardNewMacro(InteractorStyle);

InteractorStyle::~InteractorStyle()
{
  if (lastOrigin != NULL)
  {
    delete lastOrigin;
  }
  if (lastNormal != NULL)
  {
    delete lastNormal;
  }
}

void InteractorStyle::OnRightButtonDown()
{
  if (statusVolumeTool && !endCalcVolume)
  {
    onLeftButtonDownVolume();
  }
  // Forward events
  vtkInteractorStyleTrackballCamera::OnRightButtonDown();
}
void InteractorStyle::OnMouseWheelForward()
{
  if (flyToPoint && picker->Pick(this->GetInteractor()->GetEventPosition()[0], this->GetInteractor()->GetEventPosition()[1], 0, this->GetDefaultRenderer()))
  {
    this->Interactor->FlyTo(this->GetDefaultRenderer(), picker->GetPickPosition());
  }
  else
  {
    // Forward events
    vtkInteractorStyleTrackballCamera::OnMouseWheelForward();
  }
}
void InteractorStyle::OnMouseMove()
{
  onLineWidget();
  // Forward events
  vtkInteractorStyleTrackballCamera::OnMouseMove();
}
void InteractorStyle::OnKeyPress()
{
  if (statusVolumeTool)
  {
    char key = this->Interactor->GetKeyCode();
    if (polygonPointsVolumeTool.size() > 0)
    {
      if (key == '=')
      {
        polygonHeightCalcVolume += 0.1;
        updateClipping();
      }
      else if (key == '-')
      {
        polygonHeightCalcVolume -= 0.1;
        if (polygonHeightCalcVolume <= 0.11)
        {
          polygonHeightCalcVolume = 0.1;
        }
        updateClipping();
      }
      else if (this->Interactor->GetControlKey() && key == 'Z')
      {
        wxDELETE(polygonPointsVolumeTool.back());
        polygonPointsVolumeTool.pop_back();
        if (polygonPointsVolumeTool.size() == 2)
        {
          for (size_t i = 0; i < 2; i++)
          {
            wxDELETE(polygonPointsVolumeTool.back());
            polygonPointsVolumeTool.pop_back();
          }
          polygonPointsVolumeTool.clear();
        }
        updateClipping();
      }
    }
    if (this->Interactor->GetControlKey() && key == 'B')
    {
      if (lastOrigin != NULL && lastNormal != NULL)
      {
        planeWidgetVolumeTool->SetOrigin(lastOrigin);
        planeWidgetVolumeTool->SetNormal(lastNormal);
        this->GetDefaultRenderer()->GetRenderWindow()->Render();
      }
    }
  }

}
void InteractorStyle::doubleClick()
{
  if(meshVector->size() > 0)
  {
    //Double click to go to the camera
    double clickPos[3];
    cellPicker->Pick(this->GetInteractor()->GetEventPosition()[0], this->GetInteractor()->GetEventPosition()[1], 0, this->GetDefaultRenderer());
    cellPicker->GetPickPosition(clickPos);
    if (cellPicker->GetMapper() != NULL)
    {
      for (int i = 0; i < meshVector->size(); i++)
      {
        if (meshVector->at(i)->hasImages())
        {
          for (int k = 0; k < meshVector->at(i)->cameras.size(); k++)
          {
			  if (meshVector->at(i)->cameras.at(k)->imageActor)
			  {
				  if (cellPicker->GetMapper() == meshVector->at(i)->cameras.at(k)->imageActor->GetMapper())
				  {
					  Utils::updateCamera(this->GetDefaultRenderer(), meshVector->at(i)->cameras.at(k));
					  treeMesh->Select(meshVector->at(i)->cameras.at(k)->getListItemCamera());
					  this->GetDefaultRenderer()->GetRenderWindow()->Render();
					  break;
				  }
			  }
          }
        }
      }
    }
  }
}

//Tools
void InteractorStyle::initializeToolBarItems(wxToolBarToolBase * toolCalcVolume, wxBitmap * bmpVolumeOff, wxBitmap * bmpVolumeOn)
{
  //Volume
  this->toolCalcVolume = toolCalcVolume;
  this->bmpToolCalcVolumeOFF = bmpVolumeOff;
  this->bmpToolCalcVolumeON = bmpVolumeOn;
}
bool InteractorStyle::isAnyToolActive()
{
  if (statusCheckCamTool || statusVolumeTool)
  {
    return true;
  }
  return false;
}
void InteractorStyle::disableTools()
{
  if (statusVolumeTool)
  {
    volumeTool(NULL);
  }
}
void InteractorStyle::volumeTool(Mesh* meshSelected)
{
  statusVolumeTool = !statusVolumeTool;
  //Update toolBar
  if (statusVolumeTool)
  {
    toolCalcVolume->SetNormalBitmap(*bmpToolCalcVolumeON);
  }
  else
  {
    toolCalcVolume->SetNormalBitmap(*bmpToolCalcVolumeOFF);
  }
  toolCalcVolume->GetToolBar()->Realize();
  if (statusVolumeTool)
  {
    meshVolume = meshSelected;
    propPickerVolume = vtkSmartPointer<vtkPropPicker>::New();
    meshVolume->createPickList(propPickerVolume);

    planeWidgetVolumeTool = vtkSmartPointer<ImplicitPlaneWidget>::New();
    planeWidgetVolumeTool->SetInteractor(this->GetInteractor());
    planeWidgetVolumeTool->GetPlaneProperty()->SetColor(1.0, 1.0, 0.0);
    planeWidgetVolumeTool->GetSelectedPlaneProperty()->SetColor(0.0, 1.0, 0.0);
    planeWidgetVolumeTool->GetSelectedPlaneProperty()->SetOpacity(1.0);
    planeWidgetVolumeTool->TubingOff();
    planeWidgetVolumeTool->OutlineTranslationOff();
    planeWidgetVolumeTool->OriginTranslationOff();
    planeWidgetVolumeTool->PickingManagedOff();
    planeWidgetVolumeTool->SetPlaceFactor(1.25);
    planeWidgetVolumeTool->SetDiagonalRatio(0.05);
    if (meshVolume->volumeActor != NULL)
    {
      planeWidgetVolumeTool->PlaceWidget(meshVolume->volumeActor->GetBounds());
    }
    else
    {
      planeWidgetVolumeTool->PlaceWidget(meshVolume->actors.at(0)->GetBounds());
    }    
  }
  else
  {
    for (int i = 0; i < polygonPointsVolumeTool.size(); i++)
    {
      delete polygonPointsVolumeTool.at(i);
    }
    if (actorUpdatingCalcVolume != NULL)
    {
      this->GetDefaultRenderer()->RemoveActor(actorUpdatingCalcVolume);
      actorUpdatingCalcVolume = NULL;
    }
    if (planeActorVolume != NULL)
    {
      this->GetDefaultRenderer()->RemoveActor(planeActorVolume);
      planeActorVolume = NULL;
    }
    planeWidgetVolumeTool->Off();
    planeWidgetVolumeTool = NULL;
    polygonPointsVolumeTool.clear();
    pickingPolygonVolume = false;
    endCalcVolume = false;
    meshVolume = NULL;
    propPickerVolume = NULL;
    polygonHeightCalcVolume = 1;

    if (lineWidget != NULL)
    {
      lineWidget->EnabledOff();
      lineWidget->RemoveObserver(this->EventCallbackCommand);
      lineWidget = NULL;
    }

    this->GetDefaultRenderer()->GetRenderWindow()->Render();
  }
}

//Volume
void InteractorStyle::onLeftButtonDownVolume()
{
  double* pickedPosition = new double[3];
  if (getMousePosition(propPickerVolume,pickedPosition))
  {
    if (!pickingPolygonVolume && !planeWidgetVolumeTool->GetEnabled())//Plane
    {
      planeWidgetVolumeTool->SetOrigin(pickedPosition);
      planeWidgetVolumeTool->SetNormal(0,0,1);
      planeWidgetVolumeTool->UpdatePlacement();
      planeWidgetVolumeTool->On();
    }
  }
  else
  {
    delete pickedPosition;
  }
}
bool InteractorStyle::computeVolume()
{
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
  for (int i = 0; i < polygonPointsVolumeTool.size(); i++)
  {
    points->InsertNextPoint(polygonPointsVolumeTool.at(i));
  }
  //To change the height of the polygon you should multiply the n vector
  double* n = Utils::createDoubleVector(planeWidgetVolumeTool->GetNormal());
  vtkMath::MultiplyScalar(n, polygonHeightCalcVolume);
  int size = polygonPointsVolumeTool.size();
  for (int i = 0; i < size; i++)
  {
    double* r = new double[3];
    vtkMath::Add(n, polygonPointsVolumeTool.at(i),r);
    points->InsertNextPoint(r);
    polygonPointsVolumeTool.push_back(r);
  }
  //Mesh to be tested
  vtkSmartPointer<vtkPolyData> meshPolyData;
  if (meshVolume->volumeActor != NULL)
  {
    meshPolyData = vtkPolyData::SafeDownCast(meshVolume->volumeActor->GetMapper()->GetInputAsDataSet());
  }
  else
  {
    meshPolyData = meshVolume->getPolyData();
  }
   
  
  //Points in polyData format
  vtkSmartPointer<vtkPolyData> polyDataPoints = vtkSmartPointer<vtkPolyData>::New();
  polyDataPoints->SetPoints(points);

  //Create the planes to use in the vtkClipClosedSurface
  vtkSmartPointer<vtkPlaneCollection> planes = vtkSmartPointer<vtkPlaneCollection>::New();
  int numPoints = points->GetNumberOfPoints() / 2;
  for (int i = 0; i < 2; i++)//The top and bottom planes
  {
    vtkSmartPointer<vtkPlane> plane = vtkSmartPointer<vtkPlane>::New();
    plane->SetOrigin(polygonPointsVolumeTool.at(i + i*numPoints));
    plane->SetNormal(n[0] + i*(-2 * n[0]), n[1] + i*(-2 * n[1]), n[2] + i*(-2 * n[2]));
    planes->AddItem(plane);
  }
  for (int i = 0; i < numPoints; i++)
  {
    vtkSmartPointer<vtkPlane> plane = vtkSmartPointer<vtkPlane>::New();
    plane->SetOrigin(polygonPointsVolumeTool.at(i));
    if ((i + 1) == numPoints)//Last plane
    {
      plane->SetNormal(Utils::getNormal(polygonPointsVolumeTool.at(i + numPoints),
        polygonPointsVolumeTool.at(i),
        polygonPointsVolumeTool.at(0),
        polyDataPoints->GetCenter()));
    }
    else//Planes in the side
    {
      plane->SetNormal(Utils::getNormal(polygonPointsVolumeTool.at(i + numPoints),
        polygonPointsVolumeTool.at(i),
        polygonPointsVolumeTool.at(i + 1),
        polyDataPoints->GetCenter()));
    }
    planes->AddItem(plane);
  }
  delete n;
  //DEBUG
  /*for (int i = 0; i < planes->GetNumberOfItems(); i++)
  {
    Draw::createPlane(this->GetDefaultRenderer(),planes->GetItem(i)->GetOrigin(), planes->GetItem(i)->GetNormal(),255,0,0);
  }*/
  //

  vtkSmartPointer<vtkClipClosedSurface> clipperClosed = vtkSmartPointer<vtkClipClosedSurface>::New();
  clipperClosed->SetInputData(meshPolyData);
  clipperClosed->SetClippingPlanes(planes);
  clipperClosed->Update();
  if (clipperClosed->GetOutput()->GetPolys()->GetNumberOfCells() == 0)
  {
    wxMessageBox("Nothing was selected", "Error", wxICON_ERROR);
    return 0;
  }
  //It is necessary to scale the polyData
  vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
  double calibrationFactor = meshVolume->getCalibration()->getScaleFactor();
  transform->Scale(calibrationFactor, calibrationFactor, calibrationFactor);
  vtkSmartPointer<vtkTransformFilter> transformFilter = vtkSmartPointer<vtkTransformFilter>::New();
  transformFilter->SetInputConnection(clipperClosed->GetOutputPort());
  transformFilter->SetTransform(transform);
  transformFilter->Update();

  //Pass to triangles
  vtkSmartPointer<vtkTriangleFilter> triangleFilter = vtkSmartPointer<vtkTriangleFilter>::New();
  triangleFilter->SetInputConnection(transformFilter->GetOutputPort());
  triangleFilter->Update();

  //Compute the volume
  vtkSmartPointer<vtkMassProperties> massProperties = vtkSmartPointer<vtkMassProperties>::New();
  massProperties->SetInputConnection(triangleFilter->GetOutputPort());
  massProperties->Update();
  
  wxString stringCalcVolume = "";
  wxString measureUnit = meshVolume->getCalibration()->getMeasureUnit();
  if ((massProperties->GetVolume() - massProperties->GetVolumeProjected()) * 10000 > massProperties->GetVolume())
  {
    wxMessageBox("Something went wrong", "Error", wxICON_ERROR);
    //return 0;
    stringCalcVolume << "#" << meshVolume->qtdVolumes << " Error - " << massProperties->GetVolume() << "\u00B3";
  }
  else
  {
    stringCalcVolume << "#" << meshVolume->qtdVolumes << " - " << massProperties->GetVolume() << measureUnit << "\u00B3";
  }
  Volume* volume = new Volume(1);
  volume->index = meshVolume->qtdVolumes;
  volume->volume = massProperties->GetVolume();
  volume->polyData = clipperClosed->GetOutput();
  volume->actor = Draw::createPolyData(this->GetDefaultRenderer(), volume->polyData, 1.0, 1.0, 0.0);
  volume->text = Draw::createText(this->GetDefaultRenderer(), volume->actor->GetCenter(), stringCalcVolume, 24, 1.0, 1.0, 1.0);
  if (meshVolume->getListItemMeshVolumes() == NULL)
  {
    meshVolume->setListItemMeshVolumes(treeMesh->AppendItem(meshVolume->getListItemMesh(), "Volumes"));
  }
  wxString name;
  name << "#" << meshVolume->qtdVolumes;
  volume->setListItem(treeMesh->AppendItem(meshVolume->getListItemMeshVolumes(), name));
  meshVolume->addVolume(volume);
  treeMesh->CheckItem(meshVolume->getListItemMeshVolumes(), wxCHK_CHECKED);
  treeMesh->CheckItem(volume->getListItem(), wxCHK_CHECKED);
  return 1;
}
bool InteractorStyle::isPlaneOnVolume()
{
  return planeWidgetVolumeTool->GetEnabled();
}
void InteractorStyle::endPlaneInteractionVolume()
{
  pickingPolygonVolume = true;
  if (lastOrigin != NULL)
  {
    delete lastOrigin;
  }
  lastOrigin = Utils::createDoubleVector(planeWidgetVolumeTool->GetOrigin());
  if (lastNormal != NULL)
  {
    delete lastNormal;
  }
  lastNormal = Utils::createDoubleVector(planeWidgetVolumeTool->GetNormal());
  planeActorVolume = planeWidgetVolumeTool->GetPlaneActor();
  planeWidgetVolumeTool->Off();
  this->GetDefaultRenderer()->AddActor(planeActorVolume);
  planeActorVolume->GetProperty()->SetColor(0,0,0);
  propPickerVolume->AddPickList(planeActorVolume);

  if (lineWidget == NULL)
  {
    lineWidget = vtkSmartPointer<LineWidget>::New();
    lineWidget->SetInteractor(this->Interactor);
    lineWidget->setCloseLoopOnFirstNode(true);
    if (planeActorVolume != NULL)
    {
      vtkSmartPointer<LineWidgetRepresentation> rep = vtkSmartPointer<LineWidgetRepresentation>::New();
      rep->addProp(planeActorVolume);
      vtkSmartPointer<vtkCellLocator> cellLocator = vtkSmartPointer<vtkCellLocator>::New();
      cellLocator->SetDataSet(planeActorVolume->GetMapper()->GetInputAsDataSet());
      cellLocator->BuildLocator();
      rep->addLocator(cellLocator);
      lineWidget->SetRepresentation(rep);
    }
  }
  lineWidget->EnabledOn();
  lineWidget->AddObserver(vtkCommand::EndInteractionEvent, this->EventCallbackCommand, this->Priority);

  this->GetDefaultRenderer()->GetRenderWindow()->Render();
}
void InteractorStyle::updateClipping()
{
  if (polygonPointsVolumeTool.size() < 3)
  {
    if (actorUpdatingCalcVolume != NULL)
    {
      this->GetDefaultRenderer()->RemoveActor(actorUpdatingCalcVolume);
      actorUpdatingCalcVolume = NULL;
      this->GetDefaultRenderer()->GetRenderWindow()->Render();
    }
    return;
  }
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
  for (int i = 0; i < polygonPointsVolumeTool.size(); i++)
  {
    points->InsertNextPoint(polygonPointsVolumeTool.at(i));
  }
  //To change the height of the polygon you should multiply the n vector
  double* n = Utils::createDoubleVector(planeWidgetVolumeTool->GetNormal());
  vtkMath::MultiplyScalar(n,polygonHeightCalcVolume);
  double r[3];
  int size = polygonPointsVolumeTool.size();
  for (int i = 0; i < size; i++)
  {
    vtkMath::Add(n, polygonPointsVolumeTool.at(i),r);
    points->InsertNextPoint(r);
  }
  delete n;
  vtkSmartPointer<vtkPolyData> polyDataPoints = vtkSmartPointer<vtkPolyData>::New();
  polyDataPoints->SetPoints(points);

  vtkSmartPointer<vtkDelaunay3D> delaunay3D = vtkSmartPointer<vtkDelaunay3D>::New();
  delaunay3D->SetInputData(polyDataPoints);
  delaunay3D->Update();

  if (actorUpdatingCalcVolume != NULL)
  {
    this->GetDefaultRenderer()->RemoveActor(actorUpdatingCalcVolume);
    actorUpdatingCalcVolume = NULL;
  }
  actorUpdatingCalcVolume = Draw::createDataSet(this->GetDefaultRenderer(),delaunay3D->GetOutput(),1.0,0.0,0.0);
  actorUpdatingCalcVolume->GetProperty()->SetOpacity(0.3);
  actorUpdatingCalcVolume->SetPickable(false);
  this->GetDefaultRenderer()->GetRenderWindow()->Render();
}
bool InteractorStyle::hasSelection()
{
  if (polygonPointsVolumeTool.size() >= 3)
  {
    return true;
  }
  return false;
}
void InteractorStyle::endPolygonInteraction()
{
  wxBeginBusyCursor(wxHOURGLASS_CURSOR);
  computeVolume();
  endCalcVolume = true;
  volumeTool(NULL);
  wxEndBusyCursor();
}
void InteractorStyle::setFlyToPoint(bool flyToPoint)
{
  this->flyToPoint = flyToPoint;
}
bool InteractorStyle::getFlyToPoint()
{
  return flyToPoint;
}
void InteractorStyle::onLineWidget()
{
  if (lineWidget != NULL)//Polygon
  {
    vtkSmartPointer<LineWidgetRepresentation> rep = lineWidget->GetRepresentation();
    if (rep->isLoopClosed())
    {
      vtkSmartPointer<vtkPoints> pointsLine = rep->getPoints();
      polygonPointsVolumeTool.clear();
      for (vtkIdType i = 0; i < pointsLine->GetNumberOfPoints() - 1; i++)
      {
        polygonPointsVolumeTool.push_back(Utils::createDoubleVector(pointsLine->GetPoint(i)));
      }
      updateClipping();
    }
    else
    {
      if (actorUpdatingCalcVolume != NULL)
      {
        this->GetDefaultRenderer()->RemoveActor(actorUpdatingCalcVolume);
        actorUpdatingCalcVolume = NULL;
        this->GetDefaultRenderer()->GetRenderWindow()->Render();
      }
    }
  }
}

//Utils
int InteractorStyle::getMousePosition(vtkSmartPointer<vtkAbstractPicker> picker, double * point)
{
  if (picker->Pick(this->GetInteractor()->GetEventPosition()[0], this->GetInteractor()->GetEventPosition()[1], 0, this->GetDefaultRenderer()))
  {
    picker->GetPickPosition(point);
    return 1;
  }
  return 0;
}

int InteractorStyle::getMousePosition(vtkSmartPointer<vtkAbstractPicker> picker, double * pointDisplay, double * point)
{
  if (picker->Pick(pointDisplay[0], pointDisplay[1], 0, this->GetDefaultRenderer()))
  {
    picker->GetPickPosition(point);
    return 1;
  }
  return 0;
}

int InteractorStyle::getMousePosition(double * point)
{
  picker->Pick(this->GetInteractor()->GetEventPosition()[0], this->GetInteractor()->GetEventPosition()[1], 0, this->GetDefaultRenderer());
  picker->GetPickPosition(point);
  if (picker->GetActor() != NULL)
  {
    return 1;
  }
  return 0;
}

int InteractorStyle::getMousePosition(double * point, vtkSmartPointer<vtkActor> actor)
{
  if (actor == NULL)
  {
    return getMousePosition(point);
  }
  picker->Pick(this->GetInteractor()->GetEventPosition()[0], this->GetInteractor()->GetEventPosition()[1], 0, this->GetDefaultRenderer());
  picker->GetPickPosition(point);
  if (picker->GetActor() == actor)
  {
    return 1;
  }
  return 0;
}

double * InteractorStyle::pickPosition(double * pointDisplay)
{
  double* clickPos = new double[3];
  picker->Pick(pointDisplay, this->GetDefaultRenderer());
  picker->GetPickPosition(clickPos);
  if (picker->GetActor() != NULL)
  {
    return clickPos;
  }
  delete clickPos;
  return NULL;
}

bool InteractorStyle::IntersectPlaneWithLine(double * p1, vtkSmartPointer<vtkPolygon> polygon, double * pointCheckCam)
{
  double n[3];
  polygon->ComputeNormal(polygon->GetPoints()->GetNumberOfPoints(), static_cast<double*>(polygon->GetPoints()->GetData()->GetVoidPointer(0)), n);
  double bounds[6];
  polygon->GetPoints()->GetBounds(bounds);
  double t;
  double intersection[3];
  if (vtkPlane::IntersectWithLine(p1, pointCheckCam, n, polygon->GetPoints()->GetPoint(0), t, intersection))
  {
    if (polygon->PointInPolygon(intersection, polygon->GetPoints()->GetNumberOfPoints(), static_cast<double*>(polygon->GetPoints()->GetData()->GetVoidPointer(0)), bounds, n))
    {
      //Draw::createSphere(this->GetDefaultRenderer(), pointIntersection, 0.007, 0, 0, 255);
      return true;
    }
    //Draw::createSphere(this->GetDefaultRenderer(), pointIntersection, 0.007, 255, 0, 0);
  }
  return false;
}

double * InteractorStyle::getDisplayPosition(double * point)
{
  double* display = new double[3];
  vtkInteractorObserver::ComputeWorldToDisplay(this->GetDefaultRenderer(), point[0], point[1], point[2], display);
  return display;
}
