#pragma once

#include "Utils.h"
#include <wx/log.h>

class HelperMeshRecon
{
public:
	HelperMeshRecon();
	~HelperMeshRecon();

	static int testNumberOfVertexPLY(std::string filename)
	{
		std::ifstream plyFile(filename);
		std::string line;
		if (plyFile.is_open())
		{
			std::size_t found;
			while (getline(plyFile, line, '\n'))
			{
				found = line.find_last_of(" ");
				if (line.substr(0, found) == "element vertex")
				{
					return std::atoi(line.substr(found + 1).c_str());
				}
			}
			plyFile.close();
		}
		return 0;
	}

	static bool executeNVM2SFM(std::string nvmPath, std::string sfmPath, std::string boundingBoxType)
	{
		std::string parameters(Utils::preparePath(Utils::getExecutionPath() + "/meshrecon/nvm2sfm.exe") + " " + Utils::preparePath(nvmPath) + " " + Utils::preparePath(sfmPath) + " " + boundingBoxType);
		if (!Utils::startProcess(parameters))
		{
			wxLogError("Error with NVM2SFM");
			return 0;
		}
		return 1;
	}

	static bool executeMeshRecon(std::string sfmPath, std::string plyPath, std::string levelOfDetails)
	{
		std::string plyInit = plyPath.substr(0, plyPath.size() - 4) + "_init.ply";
		std::string paramInit(Utils::preparePath(Utils::getExecutionPath() + "/meshrecon/MeshRecon_init.exe") + " "
							+ Utils::preparePath(sfmPath) + " " + Utils::preparePath(plyInit));
		if (!Utils::startProcess(paramInit))
		{
			wxLogError("Error with MeshRecon_init");
			return 0;
		}
		if (testNumberOfVertexPLY(plyInit) == 0)
		{
			wxLogError("No mesh was generated with MeshRecon_init");
			return 0;
		}
		std::string plyRefine = plyPath.substr(0, plyPath.size() - 4) + "_refine.ply";
		std::string paramRefine(Utils::preparePath(Utils::getExecutionPath() + "/meshrecon/MeshRecon_refine.exe") + " "
			+ Utils::preparePath(sfmPath) + " " + Utils::preparePath(plyInit) + " " + Utils::preparePath(plyRefine) + " " + levelOfDetails);
		if (!Utils::startProcess(paramRefine))
		{
			wxLogError("Error with MeshRecon_refine");
			return 0;
		}
		return 1;
	}

private:

};