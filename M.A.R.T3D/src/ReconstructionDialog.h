#pragma once
#include <wx\dialog.h>
#include <wx\sizer.h>
#include <wx\checkbox.h>
#include <wx\stattext.h>
#include <wx\filepicker.h>
#include <wx\bmpbuttn.h>
#include "ConfigurationDialog.h"
#include <wx\dir.h>
#include "Utils.h"
#include <sstream>
#include <wx\log.h>
#include <windows.h>
#include "HelperVisualSFM.h"
#include "HelperMeshRecon.h"
#include "HelperCMPMVS.h"
#include <gl\GL.h>

#define GL_GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX 0x9048
#define GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX 0x9049


class ReconstructionDialog : public wxDialog
{
public:
  ReconstructionDialog(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = "Reconstruction/Texturization", const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);
  ~ReconstructionDialog();

  wxString getOutputPath();
  
private:
  DECLARE_EVENT_TABLE()

  void OnOK(wxCommandEvent& WXUNUSED(event));
  void OnBtConfig(wxCommandEvent& WXUNUSED(event));
  void OnCheckBoxs(wxCommandEvent& WXUNUSED(event));
  void OnDirPickerImages(wxFileDirPickerEvent& WXUNUSED(event));
  void OnDirPickerOutput(wxFileDirPickerEvent& WXUNUSED(event));

  std::string getTextureParameters(std::string nvmPath, std::string meshPath);

  //Mesh
  //True - OK
  //False - wrong
  bool testImageFolder(wxString imagePath);
  int widthDefault = -1;
  int heightDefault = -1;

  //Texturization
  /*
  True - The image paths are OK.
  False - The image paths are wrong.
  */
  bool getNVMImagePaths(std::string nvmPath, std::vector<std::string>* imagePaths);
  bool checkNVMImagePaths(std::vector<std::string>* imagePaths);
  bool checkNewNVMImagePaths(std::vector<std::string>* imagePaths, std::string newPath);

  //Window
  wxBoxSizer* bSizer;

  wxCheckBox* cbGenerateMesh;
  wxCheckBox* cbTexturize;

  //Mesh
  wxStaticText* textImages;
  wxDirPickerCtrl* pickerImages;
  wxStaticText* textOutputMesh;
  wxFilePickerCtrl* pickerOutputMesh;
  
  //Texture
  wxStaticText* textMesh;
  wxFilePickerCtrl* pickerMesh;
  wxStaticText* textNVM;
  wxFilePickerCtrl* pickerNVM;
  wxStaticText* textOutputTexture;
  wxFilePickerCtrl* pickerOutputTexture;

};

enum {
  idCBMesh,
  idCBTexture,
  idBtConfig,
  idDirPickerImages,
  idDirPickerOutput
};
