#pragma once
#include "vtkInteractionWidgetsModule.h" // For export macro
#include "vtk3DWidget.h"

#include "vtkActor.h"
#include "vtkCallbackCommand.h"
#include <vtkPropPicker.h>
#include "vtkPickingManager.h"
#include "vtkPlane.h"
#include "vtkPolyData.h"
#include "vtkProperty.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkCaptionActor2D.h"
#include "Utils.h"
#include "Draw.h"
#include "Mesh.h"
#include <wx\string.h>
#include <wx\textdlg.h> 
#include "Calibration.h"
#include "LineWidget.h"

class DistanceTool : public vtk3DWidget
{
public:
  /**
  * Instantiate the object.
  */
  static DistanceTool *New();

  vtkTypeMacro(DistanceTool, vtk3DWidget);

  //@{
  /**
  * Methods that satisfy the superclass' API.
  */
  virtual void SetEnabled(int);
  virtual void PlaceWidget(double bounds[6]);
  //@}


  //@{
  /**
  * Get the measured dsitance.
  */
  double getDistance();
  //@}

  //@{
  /**
  * True if the measure is finished.
  */
  bool hasFinished();
  //@}

  //@{
  /**
  * Update the text with the distance between point1 and point2.
  */
  void updateText(double* point1, double* point2);
  //@}

  //@{
  /**
  * Update the text. Util if you changed the mesh calibration
  */
  void updateText();
  //@}

  //@{
  /**
  * Set the mesh that is going to be used
  */
  void setMesh(Mesh* mesh);
  //@}

  //@{
  /**
  * Update the calibration
  */
  void updateCalibration();
  //@}

protected:
  DistanceTool();
  ~DistanceTool();

  //handles the events
  static void ProcessEvents(vtkObject* object, unsigned long event,
    void* clientdata, void* calldata);

  vtkSmartPointer<LineWidget> lineWidget = NULL;

  //Actors
  vtkSmartPointer<vtkCaptionActor2D> textActor = NULL;

  // Controlling ivars
  void UpdateRepresentation();

  Mesh* mesh;

  double distance = -1;

private:
  DistanceTool(const DistanceTool&) VTK_DELETE_FUNCTION;
  void operator=(const DistanceTool&) VTK_DELETE_FUNCTION;
};
