#pragma once

#include <iostream>
#include <wx\string.h>

class Calibration
{
private:
  double scaleFactor = 1.0;
  std::string measureUnit = "";

  //GPS
  bool calibratedUsingGPSData = false;
  double minAltitude = -1;
  double maxAltitude = -1;



public:
  Calibration();
  Calibration(double scaleFactor, std::string measureUnit);
  ~Calibration();

  //Return the distance with the right scale
  double getCalibratedDistance(double dist);
  //Return a text with the right distance and the measure unit
  std::string getCalibratedText(double dist);
  //Return a text with the right distance and the measure unit
  std::string getCalibratedText(double dist, double deltaX, double deltaY, double deltaZ);
  //Return the scale factor
  double getScaleFactor();
  //Return the measure unit
  std::string getMeasureUnit();

  //Set if the calibration was done by GPSData
  void setCalibratedUsingGPSData(bool calibratedUsingGPSData);
  //Get if the calibration was done by GPSData
  bool getCalibratedUsingGPSData();
  //Set the minimum altitude
  void setMinimumAltitude(double minAltitude);
  //Return minimum altitude
  double getMinimumAltitude();
  //Set the maximum altitude
  void setMaximumAltitude(double maxAltitude);
  //Return minimum altitude
  double getMaximumAltitude();
};