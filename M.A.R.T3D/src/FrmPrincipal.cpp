#include "FrmPrincipal.h"

BEGIN_EVENT_TABLE(FrmPrincipal, wxFrame)
EVT_MENU(idMenuOpen, FrmPrincipal::OnMenuOpen)
EVT_MENU(idMenuOpenCameras, FrmPrincipal::OnMenuOpenCameras)
EVT_MENU(idMenuTransform, FrmPrincipal::OnMenuTransform)
EVT_MENU(idMenuExportMesh, FrmPrincipal::OnMenuExportMesh)
EVT_MENU(idMenuExportPoisson, FrmPrincipal::OnMenuExportPoisson)
EVT_MENU(idMenuSnapshot, FrmPrincipal::OnMenuSnapshot)
EVT_MENU(idMenuShowAxis, FrmPrincipal::OnMenuShowAxis)
EVT_MENU(idMenuShowViews, FrmPrincipal::OnMenuShowViews)
EVT_MENU(idMenuFlyToPoint, FrmPrincipal::OnMenuFlyToPoint)
EVT_MENU(idMenuPoisson, FrmPrincipal::OnMenuPoisson)
EVT_MENU(idMenuHelp, FrmPrincipal::OnMenuHelp)
EVT_MENU(idMenuAbout, FrmPrincipal::OnMenuAbout)
EVT_TOOL(idToolOpen, FrmPrincipal::OnMenuOpen)
EVT_TOOL(idToolDelete, FrmPrincipal::OnToolDelete)
EVT_TOOL(idToolPoints, FrmPrincipal::OnToolPoints)
EVT_TOOL(idToolWireframe, FrmPrincipal::OnToolWireframe)
EVT_TOOL(idToolSurface, FrmPrincipal::OnToolSurface)
EVT_TOOL(idToolMeasure, FrmPrincipal::OnToolMeasure)
EVT_TOOL(idToolAngle, FrmPrincipal::OnToolAngle)
EVT_TOOL(idToolCalcVolume, FrmPrincipal::OnToolCalcVolume)
EVT_TOOL(idToolElevation, FrmPrincipal::OnToolElevation)
EVT_TOOL(idToolCalibration, FrmPrincipal::OnToolCalibration)
EVT_TOOL(idToolCheckCamVisibility, FrmPrincipal::OnToolCheckCamVisibility)
EVT_TOOL(idToolStartReconstruction, FrmPrincipal::OnToolStartReconstruction)
EVT_TOOL(idToolSnapshot, FrmPrincipal::OnToolSnapshot)
EVT_TOOL(idToolLight, FrmPrincipal::OnToolLight)
EVT_TOOL(idToolDeletePointsFaces, FrmPrincipal::OnToolDeletePointsFaces)
EVT_TOOL(idToolVR, FrmPrincipal::OnToolVR)
EVT_CLOSE(FrmPrincipal::OnClose)
EVT_SIZE(FrmPrincipal::OnSize)
EVT_SPLITTER_DCLICK(idSplitterWindow, FrmPrincipal::OnSplitterDClick)
EVT_SPLITTER_SASH_POS_CHANGED(idSplitterWindow, FrmPrincipal::OnSashPosChanged)
EVT_CHAR_HOOK(FrmPrincipal::OnKeyPress)
EVT_DROP_FILES(FrmPrincipal::OnDropFiles)
END_EVENT_TABLE()

FrmPrincipal::FrmPrincipal(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxFrame(parent, id, title, pos, size, style)
{
  this->SetIcon(wxICON(AAAAAPROGRAM));
  //Help
  helpController = new wxHtmlHelpController(this, wxHF_TOOLBAR | wxHF_CONTENTS | wxHF_INDEX | wxHF_SEARCH | wxHF_BOOKMARKS | wxHF_PRINT);
  //VTK
  outputErrorWindow = vtkSmartPointer<OutputErrorWindow>::New();
  vtkOutputWindow::SetInstance(outputErrorWindow);
  //WX
  this->SetSizeHints(wxDefaultSize, wxDefaultSize);

  //Menu
  menuBar = new wxMenuBar(0);
  menuFile = new wxMenu();
  menuItemOpen = new wxMenuItem(menuFile, idMenuOpen, "Open...", wxEmptyString, wxITEM_NORMAL);
  menuFile->Append(menuItemOpen);
  menuFile->Append(new wxMenuItem(menuFile, idMenuOpenCameras, "Open cameras...", wxEmptyString, wxITEM_NORMAL));
  menuFile->AppendSeparator();
  menuFile->Append(new wxMenuItem(menuFile, idMenuExportMesh, "Export mesh..."));
  menuFile->Append(new wxMenuItem(menuFile, idMenuExportPoisson, "Export poisson..."));
  menuFile->AppendSeparator();
  menuItemSnapshot = new wxMenuItem(menuFile, idMenuSnapshot, "Take a snapshot...", wxEmptyString, wxITEM_NORMAL);
  menuFile->Append(new wxMenuItem(menuFile, idMenuTransform, "Tranform..."));
  menuFile->Append(menuItemSnapshot);
  menuFile->AppendSeparator();
  menuItemShowAxis = new wxMenuItem(menuFile, idMenuShowAxis, "Show Axis", wxEmptyString, wxITEM_CHECK);
  menuFile->Append(menuItemShowAxis);
  menuItemShowViews = new wxMenuItem(menuFile, idMenuShowViews, "Show Views", wxEmptyString, wxITEM_CHECK);
  menuFile->Append(menuItemShowViews);
  menuItemFlyToPoint = new wxMenuItem(menuFile, idMenuFlyToPoint, "Fly to point", wxEmptyString, wxITEM_CHECK);
  menuFile->Append(menuItemFlyToPoint);

  menuReconstruction = new wxMenu();
  menuItemPoisson = new wxMenuItem(menuReconstruction, idMenuPoisson, "Poisson...", wxEmptyString, wxITEM_NORMAL);
  menuReconstruction->Append(menuItemPoisson);

  menuHelp = new wxMenu();
  menuItemHelp = new wxMenuItem(menuHelp, idMenuHelp, "Help...", wxEmptyString, wxITEM_NORMAL);
  menuHelp->Append(menuItemHelp);
  menuHelp->AppendSeparator();
  menuItemAbout = new wxMenuItem(menuHelp, idMenuAbout, "About...", wxEmptyString, wxITEM_NORMAL);
  menuHelp->Append(menuItemAbout);

  menuBar->Append(menuFile, "File");
  menuBar->Append(menuReconstruction, "Reconstruction");
  menuBar->Append(menuHelp, "Help");

  //Bitmpas
  bmpToolMeasureOFF = wxICON(ICON_MEASURE_OFF);
  bmpToolMeasureON = wxICON(ICON_MEASURE_ON);
  bmpToolAngleOFF = wxICON(ICON_ANGLE_OFF);
  bmpToolAngleON = wxICON(ICON_ANGLE_ON);
  bmpToolCalcVolumeOFF = wxICON(ICON_CALC_VOLUME_OFF);
  bmpToolCalcVolumeON = wxICON(ICON_CALC_VOLUME_ON);
  bmpToolElevationOFF = wxICON(ICON_ELEVATION_OFF);
  bmpToolElevationON = wxICON(ICON_ELEVATION_ON);
  bmpToolCheckCamVisibilityOFF = wxICON(ICON_CHECKCAM_OFF);
  bmpToolCheckCamVisibilityON = wxICON(ICON_CHECKCAM_ON);
  bmpToolDeletePointsFacesOFF = wxICON(ICON_DELETE_POINTS_OFF);
  bmpToolDeletePointsFacesON = wxICON(ICON_DELETE_POINTS_ON);
  bmpToolVROFF = wxICON(ICON_VR_OFF);
  bmpToolVRON = wxICON(ICON_VR_ON);

  //ToolBar
  toolBar = this->CreateToolBar(wxTB_HORIZONTAL, wxID_ANY);
  toolOpen = toolBar->AddTool(idToolOpen, "ToolOpen", wxICON(ICON_OPEN), wxNullBitmap, wxITEM_NORMAL, "Open...");
  toolDelete = toolBar->AddTool(idToolDelete, "ToolDelete", wxICON(ICON_DELETE), wxNullBitmap, wxITEM_NORMAL, "Delete");
  toolBar->AddSeparator();
  toolPoints = toolBar->AddTool(idToolPoints, "ToolPoints", wxICON(ICON_POINTS), wxNullBitmap, wxITEM_NORMAL, "Points");
  toolWireframe = toolBar->AddTool(idToolWireframe, "ToolWireframe", wxICON(ICON_WIREFRAME), wxNullBitmap, wxITEM_NORMAL, "Wireframe");
  toolSurface = toolBar->AddTool(idToolSurface, "ToolSurface", wxICON(ICON_SURFACE), wxNullBitmap, wxITEM_NORMAL, "Surface");
  toolBar->AddSeparator();
  toolMeasure = toolBar->AddTool(idToolMeasure, "ToolMeasure", bmpToolMeasureOFF, wxNullBitmap, wxITEM_NORMAL, "Measure");
  toolAngle = toolBar->AddTool(idToolAngle, "ToolAngle", bmpToolAngleOFF, wxNullBitmap, wxITEM_NORMAL, "Angle");
  toolCalcVolume = toolBar->AddTool(idToolCalcVolume, "ToolCalcVolume", bmpToolCalcVolumeOFF, wxNullBitmap, wxITEM_NORMAL, "Calculate volume");
  toolElevation = toolBar->AddTool(idToolElevation, "ToolElevation", bmpToolElevationOFF, wxNullBitmap, wxITEM_NORMAL, "Elevation map");
  toolCalibration = toolBar->AddTool(idToolCalibration, "ToolCalibration", wxICON(ICON_CALIBRATION), wxNullBitmap, wxITEM_NORMAL, "Calibration...");
  toolCheckCamVisibility = toolBar->AddTool(idToolCheckCamVisibility, "ToolCheckCamVisibility", bmpToolCheckCamVisibilityOFF, wxNullBitmap, wxITEM_NORMAL, "Check camera visibility");
  toolBar->AddSeparator();
  toolStartReconstruction = toolBar->AddTool(idToolStartReconstruction, "ToolStartReconstruction", wxICON(ICON_START_RECONS), wxNullBitmap, wxITEM_NORMAL, "Start reconstruction...");
  toolBar->AddTool(idToolSnapshot, "ToolSnapshot", wxICON(ICON_SNAPSHOT), wxNullBitmap, wxITEM_NORMAL, "Take a snapshot...");

  toolBar->AddTool(idToolLight, "ToolLight", wxICON(ICON_LIGHT), wxNullBitmap, wxITEM_NORMAL, "Light");
  toolDeletePoints = toolBar->AddTool(idToolDeletePointsFaces, "DeletePointsFaces", bmpToolDeletePointsFacesOFF, wxNullBitmap, wxITEM_NORMAL, "Delete points/faces");
  toolVR = toolBar->AddTool(idToolVR, "VR", bmpToolVROFF, wxNullBitmap, wxITEM_NORMAL, "Send to VR...");

  //InteractorStyle
  iterStyle = vtkSmartPointer<InteractorStyle>::New();
  iterStyle->initializeToolBarItems(toolCalcVolume, &bmpToolCalcVolumeOFF, &bmpToolCalcVolumeON);

  //Splitter
  splitterWind = new wxSplitterWindow(this, idSplitterWindow, wxDefaultPosition, wxDefaultSize, wxSP_3D, "Splitter");
  splitterWind->SetMinimumPaneSize(2);//Needed otherwise it will unslip in the double click

  //TreeListCtrl
  treeMesh = new wxTreeListCtrl(splitterWind, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTL_SINGLE | wxTL_NO_HEADER | wxTL_CHECKBOX);
  treeMesh->AppendColumn(wxT("Meshs"), wxCOL_WIDTH_AUTOSIZE, wxALIGN_LEFT, 1);
  //Load image
  bmpTreeCheckCamera = wxICON(ICON_TREE_CHECKCAM);
  wxImage imgTreeCheckCamera = bmpTreeCheckCamera.ConvertToImage();
  imgTreeCheckCamera.Rescale(16,16);
  //Define the image that will be shown if the camera is seeing the point on checkCamVisibility
  wxImageList *treeMeshImageList = new wxImageList(imgTreeCheckCamera.GetWidth(), imgTreeCheckCamera.GetHeight(), true);
  treeMeshImageList->Add(imgTreeCheckCamera);
  treeMesh->AssignImageList(treeMeshImageList);

  //Panel
  vtk_panel = new wxVTKRenderWindowInteractor(splitterWind, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, "VTK");

  renderer = vtkSmartPointer<vtkRenderer>::New();
  vtkSmartPointer<vtkRenderWindow> renWin;

  renWin = vtk_panel->GetRenderWindow();
  renWin->AddRenderer(renderer.GetPointer());
  iterStyle->SetInteractor(vtk_panel);
  iterStyle->SetDefaultRenderer(renderer.GetPointer());
  renWin->GetInteractor()->SetInteractorStyle(iterStyle.Get());

  splitterWind->SplitVertically(vtk_panel, treeMesh, 0);
  setTreeVisibility(false);

  //IterStyle
  iterStyle->meshVector = &meshVector;
  iterStyle->treeMesh = treeMesh;

  //Tools
  dTool = vtkSmartPointer<DistanceTool>::New();
  dTool->SetInteractor(vtk_panel);
  aTool = vtkSmartPointer<AngleTool>::New();
  aTool->SetInteractor(vtk_panel);
  elevationTool = vtkSmartPointer<ElevationTool>::New();
  elevationTool->SetInteractor(vtk_panel);
  elevationTool->setMeshTree(treeMesh);
  viewTool = vtkSmartPointer<ViewTool>::New();
  viewTool->setMeshVector(&meshVector);
  viewTool->setTree(treeMesh);
  viewTool->SetInteractor(vtk_panel);
  aligWithAxisTool = vtkSmartPointer<AlignWithAxisTool>::New();
  aligWithAxisTool->SetInteractor(vtk_panel);
  checkCamTool = vtkSmartPointer<CheckCamTool>::New();
  checkCamTool->SetInteractor(vtk_panel);
  checkCamTool->setTreeMesh(treeMesh);
  deleteTool = vtkSmartPointer<DeleteTool>::New();
  deleteTool->SetInteractor(vtk_panel);


  //Axis
  vtkSmartPointer<vtkAxesActor> axisActor = vtkSmartPointer<vtkAxesActor>::New();
  axisWidget = vtkSmartPointer<vtkOrientationMarkerWidget>::New();
  axisWidget->SetOutlineColor(0.9300, 0.5700, 0.1300);
  axisWidget->SetOrientationMarker(axisActor);
  axisWidget->SetInteractor(vtk_panel->GetRenderWindow()->GetInteractor());
  axisWidget->SetViewport(0.0, 0.0, 0.4, 0.4);
  axisWidget->SetEnabled(true);
  axisWidget->InteractiveOff();
  axisWidget->SetEnabled(false);
  

  //Sizer
  boxSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(splitterWind, 1, wxEXPAND | wxALL, 0);

  this->SetMenuBar(menuBar);
  toolBar->Realize();

  this->SetSizer(boxSizer);
  this->Layout();

  this->Centre(wxBOTH);

  Connect(wxEVT_TREELIST_ITEM_CHECKED, wxTreeListEventHandler(FrmPrincipal::OnTreeChoice), NULL, this);
  Connect(wxEVT_TREELIST_ITEM_ACTIVATED, wxTreeListEventHandler(FrmPrincipal::OnTreeItemActivated), NULL, this);
}

FrmPrincipal::~FrmPrincipal()
{
}

//Menu
void FrmPrincipal::OnMenuOpen(wxCommandEvent& event)
{
  wxFileDialog* openDialog = new wxFileDialog(this, "Find some cool 3D", "", "", "OBJ and PLY files (*.obj;*.ply)|*.obj;*.ply", wxFD_FILE_MUST_EXIST | wxFD_MULTIPLE);
  if (openDialog->ShowModal() == wxID_OK)
  {
    wxArrayString paths;
    openDialog->GetPaths(paths);
    loadSelector(paths, 0);
  }
  delete openDialog;
}
void FrmPrincipal::OnMenuOpenCameras(wxCommandEvent & event)
{
  Mesh* mesh = getMeshFromTree();
  if (mesh == NULL)
  {
    return;
  }
  wxFileDialog* openDialog = new wxFileDialog(this, "Find some NVM/SFM file", "", "", "NVM and SFM files (*.nvm;*.sfm)|*.nvm;*.sfm", wxFD_FILE_MUST_EXIST);
  if (openDialog->ShowModal() == wxID_OK)
  {
    mesh->destructCameras(renderer,treeMesh);
    std::string path = openDialog->GetPath();
    bool status = 0;
    if (vtksys::SystemTools::GetFilenameLastExtension(path) == ".sfm")
    {
      status = loadCameraParametersFromSFMFile(path, mesh);
    }
    else
    {
      status = loadCameraParametersFromNVMFile(path, mesh);
    }
    if (status)
    {
      loadCameras(mesh, "");
    }
  }
  delete openDialog;
}
void FrmPrincipal::OnMenuExportMesh(wxCommandEvent & event)
{
	Mesh* mesh = getMeshFromTree();
	if (mesh != NULL)
	{
		wxFileDialog* saveDialog;
		if (mesh->textures.size() == 0)
		{
			saveDialog = new wxFileDialog(this, "Save the mesh", "", "", "PLY files (*.ply)|*.ply", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
			if (saveDialog->ShowModal() == wxID_OK)
			{
				wxBeginBusyCursor(wxHOURGLASS_CURSOR);
				Utils::savePLY(saveDialog->GetPath().ToStdString(), mesh->getPolyData());
				wxEndBusyCursor();
			}
		}
		else
		{
			saveDialog = new wxFileDialog(this, "Save the mesh", "", "", "OBJ files (*.obj)|*.obj", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
			if (saveDialog->ShowModal() == wxID_OK)
			{
				wxBeginBusyCursor(wxHOURGLASS_CURSOR);
				vtkNew<vtkRenderer> rendererAux;
				vtkNew<vtkRenderWindow> renWinAux;
				renWinAux->AddRenderer(rendererAux);
				for(auto actor: mesh->actors)
				{
					rendererAux->AddActor(actor);
				}
				vtkSmartPointer<vtkOBJExporter> objExport = vtkSmartPointer<vtkOBJExporter>::New();
				objExport->SetRenderWindow(renWinAux);
				std::string baseFilename = saveDialog->GetPath().substr(0, saveDialog->GetPath().size() - 4);
				objExport->SetFilePrefix(baseFilename.c_str());
				objExport->Write();
				Utils::addTextureNamesToMTL(baseFilename + ".mtl", mesh->textureNames);
				wxEndBusyCursor();
			}
		}
		delete saveDialog;
	}
}
void FrmPrincipal::OnMenuExportPoisson(wxCommandEvent & event)
{
  Mesh* mesh = getMeshFromTree();
  if (mesh != NULL)
  {
    if (mesh->volumeActor == NULL)
    {
      wxMessageBox("Please select some mesh that have a poisson reconstruction", "Error", wxICON_ERROR, this);
      return;
    }
    wxFileDialog* saveDialog = new wxFileDialog(this, "Save the poisson result", "", "", "PLY files (*.ply)|*.ply", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
    if (saveDialog->ShowModal() == wxID_OK)
    {
      wxBeginBusyCursor(wxHOURGLASS_CURSOR);
      vtkSmartPointer<vtkPLYWriter> plyWriter = vtkSmartPointer<vtkPLYWriter>::New();
      plyWriter->SetFileName(saveDialog->GetPath().c_str());
      plyWriter->SetInputData(mesh->volumeActor->GetMapper()->GetInput());
      plyWriter->SetArrayName("RGB");
      plyWriter->Write();
      wxEndBusyCursor();
    }
    delete saveDialog;
  }
}
void FrmPrincipal::OnMenuSnapshot(wxCommandEvent & event)
{
  SnapshotTool* snapshot = new SnapshotTool();
  snapshot->takeSnapshot(vtk_panel->GetRenderWindow(),true);
  delete snapshot;
}
void FrmPrincipal::OnMenuTransform(wxCommandEvent & event)
{
  Mesh* mesh = getMeshFromTree();
  if (mesh != NULL)
  {
    if (aligWithAxisTool->GetEnabled())
    {
      aligWithAxisTool->SetEnabled(false);
      return;
    }
    disableMeasureTools();
    TransformDialog* transformDialog = new TransformDialog(this);
    int id = transformDialog->ShowModal();
    if (id == wxID_OK)
    {
      wxBeginBusyCursor(wxHOURGLASS_CURSOR);
      mesh->transform(renderer, transformDialog->transform);
      renderer->ResetCamera(mesh->actors.at(0)->GetBounds());
      renderer->ResetCameraClippingRange();
      vtk_panel->GetRenderWindow()->Render();
      wxEndBusyCursor();
    }
    else if (id == TransformDialog::idAlignWithTool)
    {
      aligWithAxisTool->setMesh(mesh);
      aligWithAxisTool->SetEnabled(true);
    }
    delete transformDialog;
  }
}
void FrmPrincipal::OnMenuShowAxis(wxCommandEvent & event)
{
  axisWidget->SetEnabled(menuItemShowAxis->IsChecked());
  vtk_panel->GetRenderWindow()->Render();  
}
void FrmPrincipal::OnMenuShowViews(wxCommandEvent & event)
{
  viewTool->SetEnabled(menuItemShowViews->IsChecked());
  vtk_panel->GetRenderWindow()->Render();
}
void FrmPrincipal::OnMenuFlyToPoint(wxCommandEvent & event)
{
  this->iterStyle->setFlyToPoint(!this->iterStyle->getFlyToPoint());
}
void FrmPrincipal::OnMenuPoisson(wxCommandEvent & event)
{
  Mesh* mesh = getMeshFromTree();
  if (mesh != NULL)
  {
    int depth = wxGetNumberFromUser("Choose the depth of the Poisson reconstruction, \nmore depth more details and more processing time", "Depth", "Poisson reconstruction", 8, 1, 100, this);
    if (depth == -1)
    {
      return;
    }
    wxBeginBusyCursor(wxHOURGLASS_CURSOR);
    vtkSmartPointer<vtkPolyData> polyData = mesh->getPolyData();
    if (polyData->GetPointData()->GetNormals() == NULL)
    {
      //Just generate mesh for cells, not points
      vtkSmartPointer<vtkPolyDataNormals> normalGenerator = vtkSmartPointer<vtkPolyDataNormals>::New();
      normalGenerator->SetInputData(polyData);
      normalGenerator->Update();
      polyData = normalGenerator->GetOutput();
      if (polyData->GetPointData()->GetNormals() == NULL)//It is a point cloud 
      {
        wxMessageBox("The point cloud has no normals", "Error", wxICON_ERROR);
        wxEndBusyCursor();
        return;
      }
    }
    PoissonRecon* poisson = new PoissonRecon();
    polyData = poisson->getPoisson(polyData, depth);
    delete poisson;
    if (polyData == NULL)
    {
      wxMessageBox("It was not possible to reconstruct", "Error", wxICON_ERROR);
      wxEndBusyCursor();
      return;
    }
    mesh->destructVolume(renderer,treeMesh);//clear the actual reconstruction, if any.
    mesh->volumeActor = Draw::createPolyData(renderer, polyData);
    std::stringstream ss;
    ss << "Poisson Reconstruction (Depth " << depth << ")";
    mesh->setListItemMeshVolume(treeMesh->AppendItem(mesh->getListItemMesh(), ss.str()));
    treeMesh->CheckItem(mesh->getListItemMeshVolume(), wxCHK_CHECKED);
    vtk_panel->GetRenderWindow()->Render();
    wxEndBusyCursor();
  }
  
}
void FrmPrincipal::OnMenuHelp(wxCommandEvent & event)
{
  helpController->DisplayContents();
  return;
}
void FrmPrincipal::OnMenuAbout(wxCommandEvent & event)
{
  AboutDialog* about = new AboutDialog(this);
  about->ShowModal();
  delete about;
}
//ToolBar
void FrmPrincipal::OnToolDelete(wxCommandEvent & event)
{
  wxTreeListItem item = treeMesh->GetSelection();
  if (!item.IsOk())
  {
    wxMessageBox("Please select something on the tree", "Error", wxICON_ERROR, this);
    return;
  }
  disableTools();
  for (int i = 0; i < meshVector.size(); i++)
  {
    if (meshVector.at(i)->getListItemMesh() == item)
    {
      meshVector.at(i)->destruct(renderer, treeMesh);
      delete meshVector.at(i);
      meshVector.erase(meshVector.begin() + i);
      if (meshVector.size() == 0)
      {
        setTreeVisibility(false);
        this->Layout();
      }
      renderer->GetRenderWindow()->Render();
      return;
    }
    else if (meshVector.at(i)->getListItemMeshCameras() == item)
    {
      meshVector.at(i)->destructCameras(renderer, treeMesh);
      renderer->GetRenderWindow()->Render();
      return;
    }
    else if (meshVector.at(i)->getListItemMeshVolumes() == item)
    {
      meshVector.at(i)->destructVolumes(renderer, treeMesh);
      renderer->GetRenderWindow()->Render();
      return;
    }
    else if (meshVector.at(i)->getListItemMeshVolume() == item)
    {
      meshVector.at(i)->destructVolume(renderer, treeMesh);
      renderer->GetRenderWindow()->Render();
      return;
    }
    for (int j = 0; j < meshVector.at(i)->cameras.size(); j++)
    {
      if (meshVector.at(i)->cameras.at(j)->getListItemCamera() == item)
      {
        meshVector.at(i)->deleteCamera(j, renderer, treeMesh);
        renderer->GetRenderWindow()->Render();
        return;
      }
    }
    for (int j = 0; j < meshVector.at(i)->volumes.size(); j++)
    {
      if (meshVector.at(i)->volumes.at(j)->getListItem() == item)
      {
        meshVector.at(i)->deleteVolume(j, renderer, treeMesh);
        renderer->GetRenderWindow()->Render();
        return;
      }
    }
  }
}
void FrmPrincipal::OnToolPoints(wxCommandEvent& event)
{
  if (meshVector.size() == 0)
  {
    return;
  }
  for (int i = 0; i < meshVector.size(); i++)
  {
    for (int j = 0; j < meshVector.at(i)->actors.size(); j++)
    {
      meshVector.at(i)->actors.at(j)->GetProperty()->SetRepresentationToPoints();
    }
  }
  vtk_panel->GetRenderWindow()->Render();
}
void FrmPrincipal::OnToolWireframe(wxCommandEvent& event)
{
  if (meshVector.size() == 0)
  {
    return;
  }
  for (int i = 0; i < meshVector.size(); i++)
  {
    for (int j = 0; j < meshVector.at(i)->actors.size(); j++)
    {
      meshVector.at(i)->actors.at(j)->GetProperty()->SetRepresentationToWireframe();
    }
  }
  vtk_panel->GetRenderWindow()->Render();
}
void FrmPrincipal::OnToolSurface(wxCommandEvent& event)
{
  if (meshVector.size() == 0)
  {
    return;
  }
  for (int i = 0; i < meshVector.size(); i++)
  {
    for (int j = 0; j < meshVector.at(i)->actors.size(); j++)
    {
      meshVector.at(i)->actors.at(j)->GetProperty()->SetRepresentationToSurface();
    }
  }
  vtk_panel->GetRenderWindow()->Render();
}
void FrmPrincipal::OnToolMeasure(wxCommandEvent& event)
{
  disableMeasureTools(0);
  if (!dTool->GetEnabled())
  {
    Mesh* mesh = getMeshFromTree();
    if (mesh == NULL)
    {
      return;
    }
    dTool->setMesh(mesh);
    dTool->SetEnabled(true);
    toolMeasure->SetNormalBitmap(bmpToolMeasureON);
  }
  else
  {
    dTool->SetEnabled(false);
    toolMeasure->SetNormalBitmap(bmpToolMeasureOFF);
  }
  toolBar->Realize();
}
void FrmPrincipal::OnToolAngle(wxCommandEvent& event)
{
  disableMeasureTools(1);
  if (!aTool->GetEnabled())
  {
    Mesh* mesh = getMeshFromTree();
    if (mesh == NULL)
    {
      return;
    }
    aTool->setMesh(mesh);
    aTool->SetEnabled(true);
    toolAngle->SetNormalBitmap(bmpToolAngleON);
  }
  else
  {
    aTool->SetEnabled(false);
    toolAngle->SetNormalBitmap(bmpToolAngleOFF);
  }
  toolBar->Realize();
}
void FrmPrincipal::OnToolCalcVolume(wxCommandEvent & event)
{
  if (iterStyle->statusVolumeTool)
  {
    iterStyle->volumeTool(NULL);
    return;
  }
  disableMeasureTools(4);
  Mesh* mesh = getMeshFromTree();
  if (mesh == NULL)
  {
    return;
  }
  if (mesh->volumeActor == NULL)
  {
    wxBeginBusyCursor(wxHOURGLASS_CURSOR);
    //Test if the mesh is watertight
    vtkSmartPointer<vtkPolyData> polyData = mesh->getPolyData();
    if (polyData == NULL)
    {
      wxMessageBox("It was not possible to calculate the volume", "Error", wxICON_ERROR);
      wxEndBusyCursor();
      return;
    }
    if (polyData->GetPolys()->GetNumberOfCells() != 0)//Mesh
    {
      vtkSmartPointer<vtkFeatureEdges> featureEdges = vtkSmartPointer<vtkFeatureEdges>::New();
      featureEdges->SetInputData(polyData);
      featureEdges->BoundaryEdgesOn();
      featureEdges->FeatureEdgesOff();
      featureEdges->ManifoldEdgesOff();
      featureEdges->NonManifoldEdgesOff();
      featureEdges->Update();
      if (featureEdges->GetOutput()->GetPoints()->GetNumberOfPoints() == 0)//Closed mesh
      {
        wxEndBusyCursor();
        iterStyle->volumeTool(mesh);
        return;
      }
    }
    //PointCloud or open mesh
    if (polyData->GetPointData()->GetNormals() == NULL)
    {
      wxMessageBox("You need a mesh/point cloud with normals to calculate the volume", "Error", wxICON_ERROR);
      wxEndBusyCursor();
      return;
    }
    if (polyData->GetPoints() == NULL)
    {
      wxMessageBox("You need a mesh/point cloud with points to calculate the volume", "Error", wxICON_ERROR);
      wxEndBusyCursor();
      return;
    }
    PoissonRecon* poisson = new PoissonRecon();
    polyData = poisson->getPoisson(polyData, 8);
    delete poisson;
    if (polyData == NULL)
    {
      wxMessageBox("It was not possible to calculate the volume", "Error", wxICON_ERROR);
      wxEndBusyCursor();
      return;
    }
    mesh->volumeActor = Draw::createPolyData(renderer, polyData);
    mesh->setListItemMeshVolume(treeMesh->AppendItem(mesh->getListItemMesh(), "Poisson Reconstruction (Depth 8)"));
    treeMesh->CheckItem(mesh->getListItemMeshVolume(), wxCHK_CHECKED);
    vtk_panel->GetRenderWindow()->Render();
    wxEndBusyCursor();
  }
  iterStyle->volumeTool(mesh);
}
void FrmPrincipal::OnToolElevation(wxCommandEvent & event)
{
  disableMeasureTools(2);
  if (!elevationTool->GetEnabled())
  {
    Mesh* mesh = getMeshFromTree();
    if (mesh == NULL)
    {
      return;
    }
    wxBeginBusyCursor(wxHOURGLASS_CURSOR);
    elevationTool->setMesh(mesh);
    elevationTool->SetEnabled(true);
    toolElevation->SetNormalBitmap(bmpToolElevationON);
    wxEndBusyCursor();
  }
  else
  {
    elevationTool->SetEnabled(false);
    toolElevation->SetNormalBitmap(bmpToolElevationOFF);
  }
  toolBar->Realize();
}
void FrmPrincipal::OnToolCalibration(wxCommandEvent& event)
{
  if (dTool->hasFinished())
  {
    dTool->updateCalibration();
  }
  else if (dTool->GetEnabled())
  {
    wxMessageBox("Finish the measure and then come back", "Warning", wxICON_DEFAULT_TYPE, this);
  }
  else
  {
      Mesh* mesh = getMeshFromTree();
      if (mesh == NULL)
      {
          return;
      }
      if (mesh->cameras.size() > 0)
      {
          if (mesh->cameras.at(0)->gpsData != NULL)
          {
              //GPS calibration
              if (wxMessageBox("Do you want to calibrate using GPS data?", "", wxYES_NO | wxICON_QUESTION, this) == 2)
              {
                  wxBeginBusyCursor(wxHOURGLASS_CURSOR);
                  mesh->calibrateUsingGPSData(renderer);
                  wxEndBusyCursor();
                  vtk_panel->GetRenderWindow()->Render();
              }
              else
              {
                  wxMessageBox("Measure some object that you know the real measure and then come back", "Warning", wxICON_DEFAULT_TYPE, this);
              }
          }
          else
          {
              wxMessageBox("Measure some object that you know the real measure and then come back", "Warning", wxICON_DEFAULT_TYPE, this);
          }
      }
      else
      {
          wxMessageBox("Measure some object that you know the real measure and then come back", "Warning", wxICON_DEFAULT_TYPE, this);
      }
  }
}
void FrmPrincipal::OnToolCheckCamVisibility(wxCommandEvent & event)
{
  if (!checkCamTool->GetEnabled())
  {
    Mesh* mesh = getMeshFromTree();
    if (mesh == NULL)
    {
      return;
    }
    if (!mesh->hasImages())
    {
      wxMessageBox("This mesh does not have any images!", "Error", wxICON_ERROR, this);
      return;
    }
    checkCamTool->setMesh(mesh);
    checkCamTool->SetEnabled(true);
    toolCheckCamVisibility->SetNormalBitmap(bmpToolCheckCamVisibilityON);
  }
  else
  {
    checkCamTool->SetEnabled(false);
    toolCheckCamVisibility->SetNormalBitmap(bmpToolCheckCamVisibilityOFF);
  }
  toolBar->Realize();
}
void FrmPrincipal::OnToolStartReconstruction(wxCommandEvent & event)
{
  ReconstructionDialog* recons = new ReconstructionDialog(this);
  if (recons->ShowModal() == wxID_OK)
  {
    if (wxMessageBox("The reconstruction was a success! Do you wanna see it?", "", wxYES_NO | wxICON_QUESTION, this) == 2)
    {
      wxArrayString paths;
      paths.push_back(recons->getOutputPath());
      loadSelector(paths, 0);
    }
  }
  delete recons;
}
void FrmPrincipal::OnToolSnapshot(wxCommandEvent & event)
{
  SnapshotTool* snapshot = new SnapshotTool();
  snapshot->takeSnapshot(vtk_panel->GetRenderWindow());
  delete snapshot;
  return;
}
void FrmPrincipal::OnToolLight(wxCommandEvent & event)
{
  vtkSmartPointer<vtkActorCollection> ac = renderer->GetActors();
  vtkCollectionSimpleIterator ait;
  for (ac->InitTraversal(ait); vtkActor* actor = ac->GetNextActor(ait); )
  {
    for (actor->InitPathTraversal(); vtkAssemblyPath* path = actor->GetNextPath(); )
    {
      vtkSmartPointer<vtkActor> apart = reinterpret_cast <vtkActor*> (path->GetLastNode()->GetViewProp());
      apart->GetProperty()->SetLighting(!(apart->GetProperty()->GetLighting()));
    }
  }
  vtk_panel->GetRenderWindow()->Render();
}
void FrmPrincipal::OnToolDeletePointsFaces(wxCommandEvent & event)
{
  disableMeasureTools(5);
  if (!deleteTool->GetEnabled())
  {
    Mesh* mesh = getMeshFromTree();
    if (mesh == NULL)
    {
      return;
    }
    wxBeginBusyCursor(wxHOURGLASS_CURSOR);
    deleteTool->setMesh(mesh);
    deleteTool->SetEnabled(true);
    toolDeletePoints->SetNormalBitmap(bmpToolDeletePointsFacesON);
    wxEndBusyCursor();
  }
  else
  {
    deleteTool->SetEnabled(false);
    toolDeletePoints->SetNormalBitmap(bmpToolDeletePointsFacesOFF);
  }
  toolBar->Realize();
}
void FrmPrincipal::OnToolVR(wxCommandEvent & event)
{
  Mesh* mesh = getMeshFromTree();
  if (mesh == NULL)
  {
    return;
  }
  VRDialog* vrDialog = new VRDialog(this, mesh);
  vrDialog->setCamera(renderer->GetActiveCamera());
  toolVR->SetNormalBitmap(bmpToolVRON);
  toolBar->Realize();
  vrDialog->ShowModal();
  delete vrDialog;
  toolVR->SetNormalBitmap(bmpToolVROFF);
  toolBar->Realize();
}
//Tree
void FrmPrincipal::OnTreeChoice(wxTreeListEvent& event)
{
  wxTreeListItem item = event.GetItem();
  if (!item.IsOk())
  {
    return;
  }
  for (int i = 0; i < meshVector.size(); i++)
  {
    if (meshVector.at(i)->getListItemMesh() == item)
    {
      meshVector.at(i)->setMeshVisibility(!meshVector.at(i)->getMeshVisibility());
      break;
    }
    else if (meshVector.at(i)->getListItemMeshCameras() == item)
    {
      //Here i just need to copy the checkstate
      meshVector.at(i)->setCamerasVisibility(treeMesh->GetCheckedState(item));
      treeMesh->CheckItemRecursively(item, (wxCheckBoxState)meshVector.at(i)->getCamerasVisibility());
      break;
    }
    else if (meshVector.at(i)->getListItemMeshTexture() == item)
    {
      meshVector.at(i)->setTextureVisibility(!meshVector.at(i)->getTextureVisibility());
      break;
    }
    else if (meshVector.at(i)->getListItemMeshVolumes() == item)
    {
      //Here i just need to copy the checkstate
      meshVector.at(i)->setVolumesVisibility(treeMesh->GetCheckedState(item));
      treeMesh->CheckItemRecursively(item, (wxCheckBoxState)meshVector.at(i)->getVolumesVisibility());
      break;
    }
    else if (meshVector.at(i)->getListItemMeshVolume() == item)
    {
      meshVector.at(i)->setVolumeVisibility(!meshVector.at(i)->getVolumeVisibility());
      break;
    }
    for (int j = 0; j < meshVector.at(i)->cameras.size(); j++)
    {
      if (meshVector.at(i)->cameras.at(j)->getListItemCamera() == item)
      {
        meshVector.at(i)->cameras.at(j)->setVisibility(!meshVector.at(i)->cameras.at(j)->getVisibility());
        break;
      }
    }
    for (int j = 0; j < meshVector.at(i)->volumes.size(); j++)
    {
      if (meshVector.at(i)->volumes.at(j)->getListItem() == item)
      {
        meshVector.at(i)->volumes.at(j)->setVisibility(!meshVector.at(i)->volumes.at(j)->getVisibility());
        break;
      }
    }
  }
  vtk_panel->SetFocus();
  disableMeasureTools(3);
  renderer->ResetCameraClippingRange();
  vtk_panel->GetRenderWindow()->Render();
}
void FrmPrincipal::OnTreeItemActivated(wxTreeListEvent & event)
{
  wxTreeListItem item = event.GetItem();
  if (!item.IsOk())
  {
    return;
  }
  vtk_panel->SetFocus();
  for (int i = 0; i < meshVector.size(); i++)
  {
    if (meshVector.at(i)->getListItemMesh() == item)
    {
      meshVector.at(i)->setMeshVisibility(true);
      treeMesh->CheckItem(item, wxCHK_CHECKED);
      renderer->ResetCamera(meshVector.at(i)->getPolyData()->GetBounds());
      renderer->ResetCameraClippingRange();
      vtk_panel->GetRenderWindow()->Render();
      return;
    }
    else if (meshVector.at(i)->getListItemMeshCameras() == item)
    {
      return;
    }
    else if (meshVector.at(i)->getListItemMeshVolumes() == item)
    {
      return;
    }
    else if (meshVector.at(i)->getListItemMeshTexture() == item)
    {
      return;
    }
    else if (meshVector.at(i)->getListItemMeshVolume() == item)
    {
      meshVector.at(i)->setVolumeVisibility(true);
      treeMesh->CheckItem(item, wxCHK_CHECKED);
      renderer->ResetCamera(meshVector.at(i)->volumeActor->GetBounds());
      renderer->ResetCameraClippingRange();
      vtk_panel->GetRenderWindow()->Render();
      return;
    }
    for (int j = 0; j < meshVector.at(i)->cameras.size(); j++)
    {
      if (meshVector.at(i)->cameras.at(j)->getListItemCamera() == item)
      {
        meshVector.at(i)->cameras.at(j)->setVisibility(true);
        treeMesh->CheckItem(item, wxCHK_CHECKED);
        Utils::updateCamera(renderer, meshVector.at(i)->cameras.at(j));
		meshVector.at(i)->cameras.at(j)->createImageActor(renderer);
        vtk_panel->GetRenderWindow()->Render();
        return;
      }
    }
    for (int j = 0; j < meshVector.at(i)->volumes.size(); j++)
    {
      if (meshVector.at(i)->volumes.at(j)->getListItem() == item)
      {
        meshVector.at(i)->volumes.at(j)->setVisibility(true);
        treeMesh->CheckItem(item, wxCHK_CHECKED);
        renderer->ResetCamera(meshVector.at(i)->volumes.at(j)->actor->GetBounds());
        renderer->ResetCameraClippingRange();
        vtk_panel->GetRenderWindow()->Render();
        return;
      }
    }
  }
}
wxTreeListItem FrmPrincipal::addItemToTree(wxTreeListItem parentItem, string itemName, wxCheckBoxState state)
{
  wxTreeListItem newTreeItem = treeMesh->AppendItem(parentItem, wxString(itemName));
  treeMesh->CheckItem(newTreeItem, state);
  if (!isTreeVisible)
  {
    setTreeVisibility(true);
  }
  return newTreeItem;
}
Mesh * FrmPrincipal::getMeshFromTree()
{
  wxTreeListItem item;
  if (meshVector.size() == 1)//Avoid annoying the user if there is just one mesh on the tree
  {
    item = meshVector.at(0)->getListItemMesh();
  }
  else
  {
    item = treeMesh->GetSelection();
  }
  if (!item.IsOk())
  {
    wxMessageBox("Please select some mesh on the tree", "Error", wxICON_ERROR, this);
    return NULL;
  }
  //If the person selects a texture or a camera, get the mesh item
  while (treeMesh->GetItemParent(item) != treeMesh->GetRootItem())
  {
    item = treeMesh->GetItemParent(item);
  }
  for (int i = 0; i < meshVector.size(); i++)
  {
    if (item == meshVector.at(i)->getListItemMesh())
    {
      treeMesh->CheckItem(item, wxCHK_CHECKED);
      meshVector.at(i)->setMeshVisibility(true);
      vtk_panel->GetRenderWindow()->Render();
      return meshVector.at(i);
    }
  }
  return NULL;
}

void FrmPrincipal::loadSelector(wxArrayString paths, bool needMessageBox)
{
  outputErrorWindow->clearFlags();
  std::string path;
  wxProgressDialog* progDialog = new wxProgressDialog("Meshs", "Loading " + paths.Item(0), paths.size(), this, wxPD_SMOOTH | wxPD_AUTO_HIDE | wxPD_APP_MODAL);
  //This is needed to avoid the lost of focus when the mesh is loaded directly from the file on Windows.(The problem is related to the Hide inside the progDialog->Destroy())
  int showMessage = 0;
  for (int i = 0; i < paths.size(); i++)
  {
    path = paths.Item(i).ToStdString();
    progDialog->Update(i, "Loading " + vtksys::SystemTools::GetFilenameName(path));
    string extension = vtksys::SystemTools::GetFilenameLastExtension(path);
    if (extension == ".obj" || extension == ".OBJ")
    {
      string filenameMTL = Utils::getMTLFilenameFromOBJ(path);
      if (filenameMTL != "")
      {
        filenameMTL = vtksys::SystemTools::GetFilenamePath(path) + "/" + filenameMTL;
        if (!Utils::exists(filenameMTL))
        {
          wxMessageBox("Could not find the MTL file", "Error", wxICON_ERROR, this);
          filenameMTL = "";
        }
      }
      if (filenameMTL == "")
      {
        //load just .obj
        showMessage = loadOBJFile(path);
      }
      else if (loadTexturedOBJFile(path, filenameMTL))
      {
        showMessage = 1;
        if (loadCameraParameters(path, meshVector.back()))
        {
          loadCameras(meshVector.back(), "");
        }
        else
        {
          showMessage = 0;
        }
      }
    }
    else if (extension == ".ply" || extension == ".PLY")
    {
      showMessage = loadPLYFile(path);
    }
    else
    {
      wxMessageBox("Could not open the " + extension + " extension", "Error", wxICON_ERROR, this);
    }
  }
  if (showMessage && needMessageBox)
  {
    wxMessageBox("Success on loading the meshs!");//We need this to keep the window with focus
  }
  delete progDialog;
  paths.clear();
}
int FrmPrincipal::loadTexturedOBJFile(string filenameOBJ, string filenameMTL)
{
  int initialValue = renderer->GetActors()->GetNumberOfItems();
  vtkSmartPointer<vtkOBJImporter> objImporter = vtkSmartPointer<vtkOBJImporter>::New();
  objImporter->SetFileName(filenameOBJ.data());
  objImporter->SetFileNameMTL(filenameMTL.data());
  objImporter->SetTexturePath(vtksys::SystemTools::GetFilenamePath(filenameOBJ).data());
  objImporter->SetRenderWindow(this->vtk_panel->GetRenderWindow());
  wxBeginBusyCursor(wxHOURGLASS_CURSOR);
  objImporter->Update();
  if (!outputErrorWindow->checkOutputErrorWindow())
  {
    wxEndBusyCursor();
    //Clear wrong actors
    vtkSmartPointer<vtkActorCollection> actorCollection = renderer->GetActors();
    actorCollection->InitTraversal();
    for (vtkIdType i = 0; i < actorCollection->GetNumberOfItems(); i++)
    {
      if (i < initialValue)
      {
        actorCollection->GetNextActor();
      }
      else
      {
        renderer->RemoveActor(actorCollection->GetNextActor());
      }
    }
    return 0;
  }
  renderer->GetActiveCamera()->SetPosition(10, 10, 10);
  renderer->ResetCamera();
  renderer->GetRenderWindow()->Render();
  Mesh* mesh = new Mesh(1, 1);
  vtkSmartPointer<vtkActorCollection> actorCollection = renderer->GetActors();
  actorCollection->InitTraversal();
  for (vtkIdType i = 0; i < actorCollection->GetNumberOfItems(); i++)
  {
    if (i < initialValue)
    {
      actorCollection->GetNextActor();
    }
    else
    {
      mesh->actors.push_back(actorCollection->GetNextActor());
      mesh->textures.push_back(mesh->actors.back()->GetTexture());
	  mesh->actors.back()->GetProperty()->SetAmbient(false);
	  mesh->actors.back()->GetProperty()->SetLighting(false);
    }
  }
  mesh->textureNames = Utils::getTextureNamesFromMTL(filenameMTL);
  mesh->setListItemMesh(addItemToTree(treeMesh->GetRootItem(), vtksys::SystemTools::GetFilenameName(filenameOBJ), wxCHK_CHECKED));
  mesh->setListItemMeshTexture(addItemToTree(mesh->getListItemMesh(), "Texture", wxCHK_CHECKED));
  mesh->meshName = vtksys::SystemTools::GetFilenameName(filenameOBJ);
  mesh->getPolyData();
  meshVector.push_back(mesh);
  this->Update();
  wxEndBusyCursor();
  return 1;
}
int FrmPrincipal::loadOBJFile(string filenameOBJ)
{
  vtkSmartPointer<vtkOBJReader> reader = vtkSmartPointer<vtkOBJReader>::New();
  reader->SetFileName(filenameOBJ.c_str());
  reader->Update();
  if (!outputErrorWindow->checkOutputErrorWindow())
  {
    return 0;
  }
  // Visualize
  bool hasColor = false;
  vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  if (reader->GetOutput()->GetPolys()->GetNumberOfCells() == 0)//it is a point cloud
  {
    mapper->ScalarVisibilityOn();
    mapper->SetScalarModeToUsePointData();
    vtkSmartPointer<vtkCellArray> vertices = vtkSmartPointer<vtkCellArray>::New();
    vertices->InsertNextCell(reader->GetOutput()->GetPoints()->GetNumberOfPoints());
    for (vtkIdType i = 0; i < reader->GetOutput()->GetPoints()->GetNumberOfPoints(); i++)
    {
      vertices->InsertCellPoint(i);
    }
    reader->GetOutput()->SetVerts(vertices);
    if (reader->GetOutput()->GetPointData()->GetScalars() != NULL)//has color
    {
      hasColor = true;
    }
    mapper->SetInputData(reader->GetOutput());
  }
  else
  {
    mapper->SetInputConnection(reader->GetOutputPort());
    if (reader->GetOutput()->GetPointData()->GetScalars() != NULL)
    {
      hasColor = true;
    }
  }
  vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
  actor->SetMapper(mapper);
  renderer->AddActor(actor);
  renderer->GetActiveCamera()->SetPosition(10, 10, 10);
  renderer->ResetCamera();
  renderer->GetRenderWindow()->Render();
  Mesh* mesh = new Mesh(1, 0);
  mesh->actors.push_back(actor);
  mesh->setListItemMesh(addItemToTree(treeMesh->GetRootItem(), vtksys::SystemTools::GetFilenameName(filenameOBJ), wxCHK_CHECKED));
  if (hasColor)
  {
    mesh->setTextureVisibility(true);
    mesh->setListItemMeshTexture(addItemToTree(mesh->getListItemMesh(), "Texture", wxCHK_CHECKED));
  }
  mesh->meshName = vtksys::SystemTools::GetFilenameName(filenameOBJ);
  meshVector.push_back(mesh);
  this->Update();
  return 1;
}
int FrmPrincipal::loadCameraParameters(string path, Mesh * selectedMesh)
{
  if (selectedMesh == NULL)
  {
    wxMessageBox("No mesh was selected", "Error", wxICON_ERROR, this);
    return 0;
  }
  std::string parametersFile;
  std::size_t found = path.find_last_of("_");
  if (found != std::string::npos)
  {
    parametersFile = path.substr(0, found);
    if (Utils::exists(parametersFile + ".sfm"))
    {
      return loadCameraParametersFromSFMFile(parametersFile + ".sfm", selectedMesh);
    }
    else if (Utils::exists(parametersFile + ".nvm"))
    {
      return loadCameraParametersFromNVMFile(parametersFile + ".nvm", selectedMesh);
    }
  }
  int answer = wxMessageBox("It was not possible to load any camera parametes file(.SFM or .NVM), do you want to select it manually", "Error", wxYES_NO | wxICON_ERROR, this);
  if (answer == wxYES)
  {
    wxFileDialog* parametersDialog = new wxFileDialog(this, "Find the parameters file(.SFM or .NVM)", "", "", "SFM and NVM files (*.sfm;*.nvm)|*.sfm;*.nvm", wxFD_FILE_MUST_EXIST);
    if (parametersDialog->ShowModal() == wxID_OK)
    {
      std::string path = parametersDialog->GetPath();
      delete parametersDialog;
      if (vtksys::SystemTools::GetFilenameLastExtension(path) == ".sfm")
      {
        return loadCameraParametersFromSFMFile(path, selectedMesh);
      }
      else
      {
        return loadCameraParametersFromNVMFile(path, selectedMesh);
      }
    }
    delete parametersDialog;
  }
  return 0;
}
int FrmPrincipal::loadCameraParametersFromSFMFile(string pathSFM, Mesh* selectedMesh)
{
  if (selectedMesh == NULL)
  {
    wxMessageBox("Any mesh was selected", "Error", wxICON_ERROR, this);
    return 0;
  }
  ifstream myfile(pathSFM);
  if (!myfile.good())
  {
    wxMessageBox("Could not open " + vtksys::SystemTools::GetFilenameName(pathSFM) + " file", "Error", wxICON_ERROR, this);
    return 0;
  }
  string line;
  vector<wxString> tokens;
  getline(myfile, line, '\n');
  int qtdCameras = stoi(line);
  if (qtdCameras == 0)
  {
    wxMessageBox("It was not possible to load any cameras", "Error", wxICON_ERROR, this);
    return 0;
  }
  //Discard the line after the quantity of cameras
  getline(myfile, line, '\n');
  while (getline(myfile, line, '\n') && selectedMesh->cameras.size() < qtdCameras)
  {
    istringstream iss(line);
    string token;
    while (getline(iss, token, ' ') && tokens.size() != 15)
    {
      tokens.push_back(token);
    }
    if (tokens.size() == 15)//15 because we do not use the centerX and centerY, they are just the img height and width divided by 2 
    {
      Camera* cam = new Camera();
      cam->filePath = tokens.at(0).ToStdString();
      vtkSmartPointer<vtkMatrix4x4> matrixRT = vtkSmartPointer<vtkMatrix4x4>::New();
      //Rotation
      tokens.at(1).ToDouble(&matrixRT->Element[0][0]); tokens.at(2).ToDouble(&matrixRT->Element[0][1]); tokens.at(3).ToDouble(&matrixRT->Element[0][2]);
      tokens.at(4).ToDouble(&matrixRT->Element[1][0]); tokens.at(5).ToDouble(&matrixRT->Element[1][1]); tokens.at(6).ToDouble(&matrixRT->Element[1][2]);
      tokens.at(7).ToDouble(&matrixRT->Element[2][0]); tokens.at(8).ToDouble(&matrixRT->Element[2][1]); tokens.at(9).ToDouble(&matrixRT->Element[2][2]);
      //Translation
      tokens.at(10).ToDouble(&matrixRT->Element[0][3]);
      tokens.at(11).ToDouble(&matrixRT->Element[1][3]);
      tokens.at(12).ToDouble(&matrixRT->Element[2][3]);
      //Rest
      matrixRT->Element[3][0] = matrixRT->Element[3][1] = matrixRT->Element[3][2] = 0; matrixRT->Element[3][3] = 1;
      matrixRT->Invert();
      cam->cameraMatrixInverted = matrixRT;
      tokens.at(13).ToDouble(&cam->focalX);
      tokens.at(14).ToDouble(&cam->focalY);
	  cam->updateGPSData();
      selectedMesh->cameras.push_back(cam);
      tokens.clear();
    }
  }
  myfile.close();
  if (selectedMesh->cameras.size() == 0)
  {
    wxMessageBox("It was not possible to load any cameras", "Error", wxICON_ERROR, this);
    return 0;
  }
  else if (selectedMesh->cameras.size() < qtdCameras)
  {
    wxMessageBox("It was not possible to load all cameras", "Warning", wxICON_WARNING, this);
  }
  return 1;
}
int FrmPrincipal::loadCameraParametersFromNVMFile(string pathNVM, Mesh * selectedMesh)
{
  std::ifstream in(pathNVM.c_str());
  if (!in.good())
  {
    wxMessageBox("Could not open " + vtksys::SystemTools::GetFilenameName(pathNVM) + " file", "Error", wxICON_ERROR, this);
    return 0;
  }
  //Check NVM file signature
  std::string signature;
  in >> signature;
  if (signature != "NVM_V3")
  {
    wxMessageBox("Invalid NVM signature in " + vtksys::SystemTools::GetFilenameName(pathNVM) + " file", "Error", wxICON_ERROR, this);
    return 0;
  }
  //Discard the rest of the line
  std::getline(in, signature);
  //Read number of views
  int qtdCameras = 0;
  in >> qtdCameras;
  if (qtdCameras < 0 || qtdCameras > 10000)
  {
    wxMessageBox("Invalid number of cameras in " + vtksys::SystemTools::GetFilenameName(pathNVM) + " file", "Error", wxICON_ERROR, this);
    return 0;
  }
  //Read views
  for (int i = 0; i < qtdCameras; ++i)
  {
    Camera* cam = new Camera();
    //Filename and focal length
    in >> cam->filePath;
    in >> cam->focalX;
    cam->focalY = cam->focalX;
    vtkQuaternion<double> *quaternion = new vtkQuaternion<double>;
    //Camera rotation and center
    double quat[4];
    for (int j = 0; j < 4; ++j)
      in >> quat[j];

    quaternion->Set(quat);
    double rotation[3][3];
    quaternion->ToMatrix3x3(rotation);

    double center[3], trans[3];
    for (int j = 0; j < 3; ++j)
      in >> center[j];

    trans[0] = trans[1] = trans[2] = 0;
    for (int j = 0; j < 3; j++)
    {
      for (int k = 0; k < 3; k++)
      {
        trans[j] += rotation[j][k] * (-center[k]);
      }
    }
    vtkSmartPointer<vtkMatrix4x4> matrixRT = vtkSmartPointer<vtkMatrix4x4>::New();
    //Rotation
    for (int j = 0; j < 3; j++)
    {
      for (int k = 0; k < 3; k++)
      {
        matrixRT->Element[j][k] = rotation[j][k];
      }
    }
    //Translation
    for (int j = 0; j < 3; j++)
    {
      matrixRT->Element[j][3] = trans[j];

    }
    //Rest
    matrixRT->Element[3][0] = matrixRT->Element[3][1] = matrixRT->Element[3][2] = 0; matrixRT->Element[3][3] = 1;
    matrixRT->Invert();
    cam->cameraMatrixInverted = matrixRT;
	cam->updateGPSData();
    selectedMesh->cameras.push_back(cam);

    float temp;
    in >> temp;//We are not using radial distortion
    in >> temp;
    in.eof();
  }
  in.close();
  if (selectedMesh->cameras.size() == 0)
  {
    wxMessageBox("It was not possible to load any cameras", "Error", wxICON_ERROR, this);
    return 0;
  }
  else if (selectedMesh->cameras.size() < qtdCameras)
  {
    wxMessageBox("It was not possible to load all cameras", "Warning", wxICON_WARNING, this);
  }
  return 1;
}
void FrmPrincipal::loadCameras(Mesh* selectedMesh, std::string newPath)
{
	if (selectedMesh->getListItemMeshCameras() == NULL)
	{
		selectedMesh->setListItemMeshCameras(addItemToTree(selectedMesh->getListItemMesh(), "Cameras", wxCHK_UNCHECKED));
	}
	selectedMesh->setCamerasVisibility(false);
	//Trying to load the images, to see if it is necessary to change the path
	bool loadImages = true;
	int missingImages = selectedMesh->checkCamerasFilePath(newPath);
	if (missingImages != 0)
	{
		loadImages = false;
		int answer;
		wxDirDialog* imageDialog = new wxDirDialog(this, "Choose the image folder of " + selectedMesh->meshName, "", wxDD_DEFAULT_STYLE);
		do
		{
			answer = wxMessageBox("Could not find " + std::to_string(missingImages) + " images of " + selectedMesh->meshName + ", you want to change the path of the images?", "Error", wxYES_NO | wxICON_QUESTION, this);
			if (answer == wxYES)
			{
				if (imageDialog->ShowModal() == wxID_OK)
				{
					newPath = imageDialog->GetPath();
					missingImages = selectedMesh->checkCamerasFilePath(newPath);
				}
				else
				{
					answer = wxNO;
				}
			}
		} while (missingImages != 0 && answer == wxYES);
		delete imageDialog;
		if (missingImages == 0)
		{
			for (auto cam : selectedMesh->cameras)
			{
				cam->filePath = newPath + "\\" + vtksys::SystemTools::GetFilenameName(cam->filePath);
			}
			loadImages = true;
		}
	}
	//Progress
	wxProgressDialog* progDialog = new wxProgressDialog("Images", "Loading images", selectedMesh->cameras.size(), 0, wxPD_AUTO_HIDE | wxPD_SMOOTH);
	//SortByName
	selectedMesh->sortCameras();
	unsigned int i = 0;
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	std::wstring wide_string;
	for (auto cam : selectedMesh->cameras)
	{
		if (loadImages)
		{
			wide_string = std::wstring(cam->filePath.begin(), cam->filePath.end());
			Gdiplus::Image* img = new Gdiplus::Image(wide_string.c_str());
			cam->height = img->GetHeight();
			cam->width = img->GetWidth();
			delete img;
			if (cam->calcCameraPoints())
			{
				cam->createActorFrustrum(renderer);
				if (cam->getListItemCamera() == NULL)
				{
					cam->setListItemCamera(addItemToTree(selectedMesh->getListItemMeshCameras(), vtksys::SystemTools::GetFilenameName(cam->filePath), wxCHK_UNCHECKED));
				}
				else
				{
					treeMesh->SetItemText(cam->getListItemCamera(), vtksys::SystemTools::GetFilenameName(cam->filePath));
				}
			}
		}
		else
		{
			if (cam->getListItemCamera() == NULL)
			{
				cam->setListItemCamera(addItemToTree(selectedMesh->getListItemMeshCameras(), "Error on load: " + vtksys::SystemTools::GetFilenameName(cam->filePath), wxCHK_UNCHECKED));
			}
			else
			{
				treeMesh->SetItemText(cam->getListItemCamera(), "Error on load: " + vtksys::SystemTools::GetFilenameName(cam->filePath));
			}
		}
		cam->setVisibility(false);
		treeMesh->CheckItem(cam->getListItemCamera(), wxCHK_UNCHECKED);
		cam = NULL;
		progDialog->Update(i + 1);
		i++;
	}
	delete progDialog;
	treeMesh->CheckItem(selectedMesh->getListItemMeshCameras(), wxCHK_UNCHECKED);
	this->vtk_panel->GetRenderWindow()->Render();
}

int FrmPrincipal::loadPLYFile(string filename)
{
  vtkSmartPointer<vtkPLYReader> reader = vtkSmartPointer<vtkPLYReader>::New();
  reader->SetFileName(filename.c_str());
  reader->Update();
  if (!outputErrorWindow->checkOutputErrorWindow())
  {
    return 0;
  }
  bool hasTexture = false, hasColor = false;
  vtkSmartPointer<vtkTexture> colorTexture;
  //Loading
  vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->ScalarVisibilityOn();
  mapper->SetScalarModeToUsePointData();
  vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
  actor->SetMapper(mapper);
  if (reader->GetOutput()->GetPolys()->GetNumberOfCells() == 0)//it is a point cloud
  {
    vtkSmartPointer<vtkCellArray> vertices = vtkSmartPointer<vtkCellArray>::New();
    vertices->InsertNextCell(reader->GetOutput()->GetPoints()->GetNumberOfPoints());
    for (vtkIdType i = 0; i < reader->GetOutput()->GetPoints()->GetNumberOfPoints(); i++)
    {
      vertices->InsertCellPoint(i);
    }
    reader->GetOutput()->SetVerts(vertices);
    if (reader->GetOutput()->GetPointData()->GetScalars() != NULL)//has color
    {
      hasColor = true;
    }
    mapper->SetInputData(reader->GetOutput());
  }
  else//it is a mesh
  {
    mapper->SetInputConnection(reader->GetOutputPort());
    if (reader->GetOutput()->GetPointData()->GetTCoords() != NULL)//Test to see if there are texture coordinates.
    {
      //Read the ply file to get the comments that have the TextureFle
      PlyFile* plyFile;
      int nelems, fileType;
      char **elist;
      float version;
      if (!(plyFile = vtkPLY::ply_open_for_reading(filename.c_str(), &nelems, &elist, &fileType, &version)))
      {
        wxMessageBox("Could not read the " + vtksys::SystemTools::GetFilenameName(filename) + " file", "Error", wxICON_ERROR, this);
        return 0;
      }
      //Get the name of the textures
      int numComments;
      char** comments = vtkPLY::ply_get_comments(plyFile, &numComments);
      if (numComments != 0)
      {
        string test = "";
        string filnameTexture = "";
        for (int i = 0; i < numComments; i++)
        {
          test = comments[i];
          if (test.substr(0, test.find(' ')) == "TextureFile")
          {
            filnameTexture = test.substr(test.find(' ') + 1);
          }
        }
        if (filename != "")
        {
          filnameTexture = vtksys::SystemTools::GetFilenamePath(filename) + "/" + filnameTexture;
          vtkSmartPointer<vtkImageData> imgData = Utils::loadImage(filnameTexture);
          if (imgData != NULL)
          {
            colorTexture = vtkSmartPointer<vtkTexture>::New();
            colorTexture->SetInputData(imgData);
            colorTexture->InterpolateOn();
            actor->SetTexture(colorTexture);
            hasTexture = true;
          }
        }
      }
      vtkPLY::ply_close(plyFile);//this already free the comments and the elist
    }
    else if (reader->GetOutput()->GetPointData()->GetScalars() != NULL)
    {
      hasColor = true;
    }
  }
  // Visualize
  renderer->AddActor(actor);
  renderer->GetActiveCamera()->SetPosition(10, 10, 10);
  renderer->ResetCamera();
  renderer->GetRenderWindow()->Render();
  Mesh* mesh = new Mesh(1, 0);
  mesh->setListItemMesh(addItemToTree(treeMesh->GetRootItem(), vtksys::SystemTools::GetFilenameName(filename), wxCHK_CHECKED));
  if (hasColor || hasTexture)
  {
    mesh->setTextureVisibility(true);
    mesh->setListItemMeshTexture(addItemToTree(mesh->getListItemMesh(), "Texture", wxCHK_CHECKED));
  }
  if (hasTexture)
  {
    mesh->textures.push_back(colorTexture);
  }
  mesh->actors.push_back(actor);
  mesh->meshName = vtksys::SystemTools::GetFilenameName(filename);
  meshVector.push_back(mesh);
  this->Update();
  return 1;
}

void FrmPrincipal::setTreeVisibility(bool visibility)
{
  int x, y;
  this->GetSize(&x, &y);
  if (x >= 0)
  {
    if (visibility)//If we want to show, we need to calc the correct size
    {
      if ((x - x*.7) > 250)//More than 250 is too much
      {
        x = x - 250;
      }
      else
      {
        x = x*.7;//Tree get 30% of the screen
      }
    }
    splitterWind->SetSashPosition(x, true);
    isTreeVisible = visibility;
  }
}

void FrmPrincipal::OnClose(wxCloseEvent& event)
{
  axisWidget->SetEnabled(false);
  viewTool->SetEnabled(false);
  disableTools();
  for (int i = 0; i < meshVector.size(); i++)
  {
    meshVector.at(i)->destruct(renderer, treeMesh);
    delete meshVector.at(i);
  }
  meshVector.clear();
  vtk_panel->Delete();
  event.Skip();
}

void FrmPrincipal::OnSplitterDClick(wxSplitterEvent & event)
{
  setTreeVisibility(!isTreeVisible);
  event.Skip();
}

void FrmPrincipal::OnSashPosChanged(wxSplitterEvent & event)
{
  int x, y;
  this->GetSize(&x, &y);
  if (x >= 0)
  {
    if (splitterWind->GetSashPosition() < x*.90)
    {
      isTreeVisible = true;
    }
    else
    {
      isTreeVisible = false;
    }
  }
  event.Skip();
}

void FrmPrincipal::OnSize(wxSizeEvent & event)
{
  if (splitterWind != NULL)
  {
    setTreeVisibility(isTreeVisible);
  }
  wxSize* size = &this->GetSize();
  if (size != NULL && vtk_panel != NULL)
  {
    vtk_panel->UpdateSize(size->x, size->y);
  }
  event.Skip();
}

void FrmPrincipal::OnKeyPress(wxKeyEvent & event)
{
  char key = (char)event.GetKeyCode();
  if (event.ControlDown())
  {
    if (key == 'S')
    {
      SnapshotTool* snapshot = new SnapshotTool();
      snapshot->takeSnapshot(vtk_panel->GetRenderWindow());
      delete snapshot;
    }
    else if (key == 'M')
    {
      this->OnToolMeasure((wxCommandEvent)NULL);
    }
    else if (key == 'A')
    {
      this->OnToolAngle((wxCommandEvent)NULL);
    }
    else if (key == 'I')
    {
      this->OnToolCalibration((wxCommandEvent)NULL);
    }
    else if (key == 'K')
    {
      this->OnToolCheckCamVisibility((wxCommandEvent)NULL);
    }
    else if (key == 'T')
    {
      Mesh* mesh = getMeshFromTree();
      if (mesh != NULL)
      {
        mesh->setTextureVisibility(!mesh->getTextureVisibility());
        treeMesh->CheckItem(mesh->getListItemMeshTexture(), (wxCheckBoxState)mesh->getTextureVisibility());
        vtk_panel->GetRenderWindow()->Render();
      }
    }
    else if (key == 'L')
    {
      OnToolLight((wxCommandEvent)NULL);
    }
    else if (key == 'R')
    {
      renderer->ResetCamera();
      renderer->GetRenderWindow()->Render();
    }
    else if (key == 'V')
    {
      wxTreeListItem cameraItem = treeMesh->GetSelection();
      if (!cameraItem.IsOk())
      {
        return;
      }
      wxTreeListItem meshItem = treeMesh->GetItemParent(treeMesh->GetItemParent(cameraItem));
      for (int i = 0; i < meshVector.size(); i++)
      {
        if (meshVector.at(i)->getListItemMesh() == meshItem)
        {
          if (meshVector.at(i)->cameras.size() == 0)
          {
            wxMessageBox("There is no camera to change the view UP", "Error", wxICON_ERROR);
            return;
          }
          for (int k = 0; k < meshVector.at(i)->cameras.size(); k++)
          {
            if (meshVector.at(i)->cameras.at(k)->getListItemCamera() == cameraItem)
            {
              if (meshVector.at(i)->cameras.at(k)->changeViewUp())
              {
                Utils::updateCamera(renderer, meshVector.at(i)->cameras.at(k));
                vtk_panel->GetRenderWindow()->Render();
                return;
              }
            }
          }
        }
      }
    }
    else if (key == 'U')
    {
      //Representation models
      //VTK_POINTS    0
      //VTK_WIREFRAME 1
      //VTK_SURFACE   2
      int oldRepresentation = 2;
      for (int i = 0; i < meshVector.size(); i++)
      {
        Mesh* mesh = meshVector.at(i);
        for (int j = 0; j < mesh->actors.size(); j++)
        {
          if (mesh->actors.at(j)->GetProperty()->GetRepresentation() < 2)
          {
            oldRepresentation = mesh->actors.at(j)->GetProperty()->GetRepresentation();
            mesh->actors.at(j)->GetProperty()->SetRepresentationToSurface();
          }
        }
      }
      //now we can do the pick
      double* focalDisplay = iterStyle->getDisplayPosition(renderer->GetActiveCamera()->GetFocalPoint());
      double clickPos[3];
      vtkSmartPointer<vtkPropPicker>  picker = vtkSmartPointer<vtkPropPicker>::New();
      picker->Pick(focalDisplay[0], focalDisplay[1], 0, renderer);
      picker->GetPickPosition(clickPos);
      //Let's back to the old representation
      if (oldRepresentation != 2)
      {
        for (int i = 0; i < meshVector.size(); i++)
        {
          Mesh* mesh = meshVector.at(i);
          for (int j = 0; j < mesh->actors.size(); j++)
          {
            mesh->actors.at(j)->GetProperty()->SetRepresentation(oldRepresentation);
          }
        }
      }
      //We put it down here to avoid spending time rendering the change in the representation
      if (picker->GetActor() != NULL)
      {
        renderer->GetActiveCamera()->SetFocalPoint(clickPos);
        renderer->ResetCameraClippingRange();
        renderer->GetRenderWindow()->Render();
      }
      //props.clear();
      delete focalDisplay;
    }
    else if (key == 'F')
    {
      this->ShowFullScreen(!this->IsFullScreen());
    }
    else if (key == 'N')
    {
      Mesh* mesh = getMeshFromTree();
      if (mesh != NULL)
      {
		  if (mesh->cameras.size() != 0)
		  {
			  mesh->createCamerasPath(renderer);
			  mesh->setCameraPathVisibility(!mesh->getCameraPathVisibility());
			  vtk_panel->GetRenderWindow()->Render();
		  }
      }
    }
    else if (key == 'E')
    {
      this->OnToolElevation((wxCommandEvent)NULL);
    }
    else if (key == 'D')
    {
      OnToolDeletePointsFaces((wxCommandEvent)NULL);
    }
  }
  if (key == WXK_ESCAPE)
  {
    disableTools();
  }
  else if (key == WXK_DELETE)
  {
      if (deleteTool->GetEnabled())
      {
          if (deleteTool->enterKeyPressed())
          {
              toolDeletePoints->SetNormalBitmap(bmpToolDeletePointsFacesOFF);
              toolBar->Realize();
          }
      }
      else
      {
          OnToolDelete((wxCommandEvent)NULL);
      }
  }
  else if (key == 'r' || key == WXK_RETURN)//Necessary to be here because VTK does not support ENTER Key // 'r' is the NUMPAD_ENTER the WXK_NUMPAD_ENTER DOES NOT WORK
  {
    if(iterStyle->statusVolumeTool)
    {
      if (iterStyle->isPlaneOnVolume())
      {
        iterStyle->endPlaneInteractionVolume();
      }
      else if (iterStyle->hasSelection())
      {
        iterStyle->endPolygonInteraction();
      }
    }
    if (deleteTool->GetEnabled())
    {
        if (deleteTool->enterKeyPressed())
        {
            toolDeletePoints->SetNormalBitmap(bmpToolDeletePointsFacesOFF);
            toolBar->Realize();
        }
    }
  }
  else if (event.GetKeyCode() == WXK_F1)
  {
    OnMenuHelp((wxCommandEvent) NULL);
  }
  event.Skip();
}

void FrmPrincipal::OnDropFiles(wxDropFilesEvent& event)
{
  if (event.GetNumberOfFiles() != 0)
  {
    wxArrayString paths;
    for (int i = 0; i < event.GetNumberOfFiles(); i++)
    {
      paths.push_back(event.GetFiles()[i]);
    }
    if (paths.size() == 1)
    {
      std::string path = paths.Item(0).ToStdString();
      std::string extension = vtksys::SystemTools::GetFilenameLastExtension(path);
      if (extension == ".sfm" || extension == ".nvm")
      {
        Mesh* mesh = getMeshFromTree();
        if (mesh == NULL)
        {
          return;
        }
        mesh->destructCameras(renderer, treeMesh);
        bool status = 0;
        if (extension == ".sfm")
        {
          status = loadCameraParametersFromSFMFile(path, mesh);
        }
        else
        {
          status = loadCameraParametersFromNVMFile(path, mesh);
        }
        if (status)
        {
          loadCameras(mesh, "");
        }
      }
      else
      {
        loadSelector(paths, 0);
      }
    }
    else
    {
      loadSelector(paths, 0);
    }
    
  }
  event.Skip();
}

void FrmPrincipal::OnLeftDClick(wxMouseEvent & event)
{
  int x, y;
  int w, h;
  vtk_panel->GetLastEventPosition(x, y);
  vtk_panel->GetClientSize(&w, &h);
  wxPoint p = event.GetPosition();
  if (iterStyle != NULL && x == p.x && (h - (y + 1)) == p.y)
  {
    iterStyle->doubleClick();
  }
  //.Skip is used in the ProjetoMeshApp::OnLeftDClick
}

wxHtmlHelpController * FrmPrincipal::getHelpController()
{
  return helpController;
}

void FrmPrincipal::disableTools()
{
  this->ShowFullScreen(false);
  disableMeasureTools();
  iterStyle->disableTools();
}

void FrmPrincipal::disableMeasureTools()
{
  disableMeasureTools(-1);
}

void FrmPrincipal::disableMeasureTools(int toolToAvoid)
{
  if (dTool->GetEnabled() && toolToAvoid != 0)
  {
    OnToolMeasure((wxCommandEvent)NULL);
  }
  if (aTool->GetEnabled() && toolToAvoid != 1)
  {
    OnToolAngle((wxCommandEvent)NULL);
  }
  if (elevationTool->GetEnabled() && toolToAvoid != 2)
  {
    OnToolElevation((wxCommandEvent)NULL);
  }
  if (checkCamTool->GetEnabled() && toolToAvoid != 3)
  {
    OnToolCheckCamVisibility((wxCommandEvent)NULL);
  }
  if (iterStyle->statusVolumeTool && toolToAvoid != 4)
  {
    iterStyle->volumeTool(NULL);
  }
  if (deleteTool->GetEnabled() && toolToAvoid != 5)
  {
      deleteTool->SetEnabled(false);
  }
}