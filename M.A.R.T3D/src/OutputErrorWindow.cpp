#include "OutputErrorWindow.h"
#include "vtkObjectFactory.h"

vtkStandardNewMacro(OutputErrorWindow);

OutputErrorWindow::OutputErrorWindow()
{

}
OutputErrorWindow::~OutputErrorWindow()
{
}
void OutputErrorWindow::DisplayText(const char* text)
{
	if (!text)
	{
		return;
	}
	if (!suppressMessages)
	{
		wxLogMessage(text);
	}
	
}

void OutputErrorWindow::DisplayErrorText(const char * text)
{
	if (!text)
	{
		return;
	}
	error = 1;
	if (!suppressMessages)
	{
		wxLogError(text);
	}
}

void OutputErrorWindow::DisplayWarningText(const char * text)
{
	if (!text)
	{
		return;
	}
	warning = 1;
	if (!suppressMessages)
	{
		wxLogWarning(text);
	}
}

void OutputErrorWindow::DisplayGenericWarningText(const char * text)
{
	if (!text)
	{
		return;
	}
	warning = 1;
	if (!suppressMessages)
	{
		wxLogWarning(text);
	}
}

bool OutputErrorWindow::checkOutputErrorWindow()
{
  if (error)
  {
    clearFlags();
    return 0;
  }
  else if (warning)
  {
    clearFlags();
  }
  return 1;
}

void OutputErrorWindow::clearFlags()
{
	error = 0;
	warning = 0;
}

void OutputErrorWindow::setSuppressMessages(bool suppress)
{
	suppressMessages = suppress;
}
