#include "Calibration.h"

Calibration::Calibration()
{
}

Calibration::Calibration(double scaleFactor, std::string measureUnit)
{
  if (scaleFactor > 0.0)
  {
    this->scaleFactor = scaleFactor;
  }
  this->measureUnit = measureUnit;
}

Calibration::~Calibration()
{
}

double Calibration::getCalibratedDistance(double dist)
{
  return dist*scaleFactor;
}

std::string Calibration::getCalibratedText(double dist)
{
  wxString ss;
  ss << wxString::Format("%.3f", dist*scaleFactor) << measureUnit;
  return ss.utf8_str();
}

std::string Calibration::getCalibratedText(double dist, double deltaX, double deltaY, double deltaZ)
{
  wxString ss;
  ss << wxString::Format("%.3f", dist*scaleFactor) << measureUnit 
    << " (" << wxString::Format("%.2f", deltaX*scaleFactor)
    << ", " << wxString::Format("%.2f", deltaY*scaleFactor)
    << ", " << wxString::Format("%.2f", deltaZ*scaleFactor) << ")";
  return ss.utf8_str();
}

double Calibration::getScaleFactor()
{
  return scaleFactor;
}

std::string Calibration::getMeasureUnit()
{
  return measureUnit;
}

void Calibration::setCalibratedUsingGPSData(bool calibratedUsingGPSData)
{
    this->calibratedUsingGPSData = calibratedUsingGPSData;
}

bool Calibration::getCalibratedUsingGPSData()
{
    return calibratedUsingGPSData;
}

void Calibration::setMinimumAltitude(double minAltitude)
{
    this->minAltitude = minAltitude;
}

double Calibration::getMinimumAltitude()
{
    return minAltitude;
}

void Calibration::setMaximumAltitude(double maxAltitude)
{
    this->maxAltitude = maxAltitude;
}

double Calibration::getMaximumAltitude()
{
    return maxAltitude;
}
