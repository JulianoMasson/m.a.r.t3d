#pragma once
#include <wx\dialog.h>
#include <wx\sizer.h>
#include <wx\stattext.h>
#include <wx\statbmp.h>
#include <wx\hyperlink.h>
#include <wx\statline.h>

class AboutDialog : public wxDialog
{
public:
  AboutDialog(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = "About", const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(500,400), long style = wxDEFAULT_DIALOG_STYLE);
  ~AboutDialog();

private:

};