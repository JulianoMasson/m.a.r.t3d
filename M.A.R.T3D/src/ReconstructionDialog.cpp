#include "ReconstructionDialog.h"


wxBEGIN_EVENT_TABLE(ReconstructionDialog, wxDialog)
  EVT_BUTTON(wxID_OK, ReconstructionDialog::OnOK)
  EVT_BUTTON(idBtConfig, ReconstructionDialog::OnBtConfig)
  EVT_CHECKBOX(idCBMesh, ReconstructionDialog::OnCheckBoxs)
  EVT_CHECKBOX(idCBTexture, ReconstructionDialog::OnCheckBoxs)
  EVT_DIRPICKER_CHANGED(idDirPickerImages, ReconstructionDialog::OnDirPickerImages)
  EVT_FILEPICKER_CHANGED(idDirPickerOutput, ReconstructionDialog::OnDirPickerOutput)
wxEND_EVENT_TABLE()


ReconstructionDialog::ReconstructionDialog(wxWindow * parent, wxWindowID id, const wxString & title, const wxPoint & pos, const wxSize & size, long style) : wxDialog(parent, id, title, pos, size, style)
{
  this->SetSizeHints(wxDefaultSize, wxDefaultSize);
  bSizer = new wxBoxSizer(wxVERTICAL);

  cbGenerateMesh = new wxCheckBox(this,idCBMesh,"Generate mesh");
  bSizer->Add(cbGenerateMesh, 0, wxALL, 5);
  cbTexturize = new wxCheckBox(this, idCBTexture, "Generate texture");
  bSizer->Add(cbTexturize, 0, wxALL, 5);

  //Mesh
  wxStaticBoxSizer* sbSizerMesh;
  sbSizerMesh = new wxStaticBoxSizer(new wxStaticBox(this, wxID_ANY, "Mesh"), wxVERTICAL);

  textImages = new wxStaticText(sbSizerMesh->GetStaticBox(), wxID_ANY, "Select the image folder:");
  sbSizerMesh->Add(textImages, 0, wxALL, 5);
  pickerImages = new wxDirPickerCtrl(sbSizerMesh->GetStaticBox(), idDirPickerImages,wxEmptyString,"Select the image folder");
  sbSizerMesh->Add(pickerImages, 0, wxALL, 5);

  textOutputMesh = new wxStaticText(sbSizerMesh->GetStaticBox(), wxID_ANY, "Select the output file:");
  sbSizerMesh->Add(textOutputMesh, 0, wxALL, 5);
  pickerOutputMesh = new wxFilePickerCtrl(sbSizerMesh->GetStaticBox(), idDirPickerOutput, wxEmptyString, "Select the output file", "PLY files(*.ply) | *.ply", wxDefaultPosition, wxDefaultSize, wxFLP_USE_TEXTCTRL | wxFLP_SAVE | wxFLP_OVERWRITE_PROMPT);
  sbSizerMesh->Add(pickerOutputMesh, 0, wxALL, 5);

  bSizer->Add(sbSizerMesh, 0, wxALL, 5);

  //Texture
  wxStaticBoxSizer* sbSizerTexture;
  sbSizerTexture = new wxStaticBoxSizer(new wxStaticBox(this, wxID_ANY, "Texture"), wxVERTICAL);

  textMesh = new wxStaticText(sbSizerTexture->GetStaticBox(), wxID_ANY, "Select the mesh to be texturized:");
  sbSizerTexture->Add(textMesh, 0, wxALL, 5);
  pickerMesh = new wxFilePickerCtrl(sbSizerTexture->GetStaticBox(), wxID_ANY, wxEmptyString, "Select the mesh to be texturized", "PLY files (*.ply)|*.ply");
  sbSizerTexture->Add(pickerMesh, 0, wxALL, 5);

  textNVM = new wxStaticText(sbSizerTexture->GetStaticBox(), wxID_ANY, "Select the NVM file:");
  sbSizerTexture->Add(textNVM, 0, wxALL, 5);
  pickerNVM = new wxFilePickerCtrl(sbSizerTexture->GetStaticBox(), wxID_ANY, wxEmptyString, "Select the NVM file", "NVM files (*.nvm)|*.nvm");
  sbSizerTexture->Add(pickerNVM, 0, wxALL, 5);

  textOutputTexture = new wxStaticText(sbSizerTexture->GetStaticBox(), wxID_ANY, "Select the output file:");
  sbSizerTexture->Add(textOutputTexture, 0, wxALL, 5);
  pickerOutputTexture = new wxFilePickerCtrl(sbSizerTexture->GetStaticBox(), idDirPickerOutput, wxEmptyString, "Select the output file", "OBJ files(*.obj) | *.obj", wxDefaultPosition, wxDefaultSize, wxFLP_USE_TEXTCTRL | wxFLP_SAVE | wxFLP_OVERWRITE_PROMPT);
  sbSizerTexture->Add(pickerOutputTexture, 0, wxALL, 5);

  bSizer->Add(sbSizerTexture, 0, wxALL, 5);

  //Bts
  wxBoxSizer* bSizerBts = new wxBoxSizer(wxHORIZONTAL);

  bSizerBts->Add(new wxBitmapButton(this, idBtConfig, wxICON(ICON_CONFIG)), 0, wxALL | wxALIGN_CENTER, 5);

  bSizerBts->Add(new wxButton(this, wxID_OK, "OK"), 0, wxALL | wxALIGN_CENTER, 5);

  bSizerBts->Add(new wxButton(this, wxID_CANCEL, "Cancel"), 0, wxALL | wxALIGN_CENTER, 5);

  bSizer->Add(bSizerBts, 0, wxALIGN_RIGHT, 5);

  OnCheckBoxs((wxCommandEvent)NULL);

  this->SetSizer(bSizer);
  this->Layout();
  this->Fit();
  this->Centre(wxBOTH);
}

ReconstructionDialog::~ReconstructionDialog()
{
}

wxString ReconstructionDialog::getOutputPath()
{
  wxString path;
  if (cbTexturize->IsChecked())
  {
    path = pickerOutputTexture->GetPath();
  }
  else
  {
    path = pickerOutputMesh->GetPath();
  }
  std::replace(path.begin(), path.end(), '\\', '/');
  std::size_t found = path.find_last_of(".");
  path = path.substr(0, found);
  if (cbTexturize->IsChecked())
  {
    path += "_textured.obj";
  }
  else
  {
    path += "_refine.ply";
  }
  return path;
}


void ReconstructionDialog::OnOK(wxCommandEvent & WXUNUSED)
{
  if (cbGenerateMesh->IsChecked())
  {
	  if (!cbTexturize->IsChecked())
	  {
		  if (pickerImages->GetPath() == "" || pickerOutputMesh->GetPath() == "")
		  {
			  wxLogError("Please fulfill all the fields correctly.");
			  return;
		  }
	  }
	  else
	  {
		  if (pickerImages->GetPath() == "" || pickerOutputTexture->GetPath() == "")
		  {
			  wxLogError("Please fulfill all the fields correctly.");
			  return;
		  }
	  }
  }
  else if (cbTexturize->IsChecked())
  {
	  if (pickerNVM->GetPath() == "" || pickerMesh->GetPath() == "" || pickerOutputTexture->GetPath() == "")
	  {
		  wxLogError("Please fulfill all the fields correctly.");
		  return;
	  }
  }
  else
  {
	  wxLogError("Please select at least one checkbox.");
	  return;
  }
  //Mesh
  std::string nvmPath;
  std::string meshPath;
  if (cbGenerateMesh->IsChecked())
  {
	  if (!cbTexturize->IsChecked())
	  {
		  nvmPath = pickerOutputMesh->GetPath();
	  }
	  else
	  {
		  nvmPath = pickerOutputTexture->GetPath();
	  }
	  nvmPath = nvmPath.substr(0, nvmPath.size() - 4) + ".nvm";
	  if (!HelperVisualSFM::executeVisualSFM(pickerImages->GetPath().ToStdString(), nvmPath))
	  {
		  return;
	  }
	  GLint nCurAvailMemoryInKB = 0;
	  glGetIntegerv(GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX, &nCurAvailMemoryInKB);
	  double memoryNeeded = widthDefault * heightDefault * Utils::getNumberOfCamerasNVM(nvmPath) / 1000;
	  int levelOfDetailsAllowed = -1;
	  if (nCurAvailMemoryInKB / 1024 > memoryNeeded * 0.025)
	  {
		  levelOfDetailsAllowed = 0;
	  }
	  else if (nCurAvailMemoryInKB / 1024 > memoryNeeded * 0.008)
	  {
		  levelOfDetailsAllowed = 1;
	  }
	  else if (nCurAvailMemoryInKB/1024 > memoryNeeded * 0.003)
	  {
		  levelOfDetailsAllowed = 2;
	  }
	  bool useCPMVS = false;
	  if (std::atoi(ConfigurationDialog::getLevelOfDetails().c_str()) < levelOfDetailsAllowed && !ConfigurationDialog::getForceLevelOfDetails())
	  {
		  if (levelOfDetailsAllowed >= 0)
		  {
			  wxLogWarning("Not enough RAM to use this level of details, lowering the settings");
			  ConfigurationDialog::setLevelOfDetails(levelOfDetailsAllowed);
		  }
		  else
		  {
			  wxLogWarning("Not enough RAM to use MeshRecon, using CMPMVS", "Warning");
			  useCPMVS = true;
		  }
	  }
	  if (useCPMVS || ConfigurationDialog::getForceCMPMVS())
	  {
		  Utils::removeExtraModelsFromNVM(nvmPath);
		  std::string nvmPathDense = nvmPath.substr(0, nvmPath.size() - 4) + "_dense.nvm";
		  if (!HelperVisualSFM::executeVisualSFMDense(nvmPath, nvmPathDense))
		  {
			  return;
		  }
		  std::string mvsPath = nvmPathDense.substr(0, nvmPathDense.size() - 4) + ".nvm.cmp\\00\\data\\";
		  std::string mvsFirstRunFile = nvmPathDense.substr(0, nvmPathDense.size() - 4) + ".nvm.cmp\\00\\mvs_first_run.ini";
		  HelperCMPMVS::createMVSFirstRun(mvsFirstRunFile, mvsPath, nvmPath);
		  if (!HelperCMPMVS::executeCMPMVS(mvsFirstRunFile))
		  {
			  return;
		  }
		  std::string mvsLargeScaleFile = nvmPathDense.substr(0, nvmPathDense.size() - 4) + ".nvm.cmp\\00\\mvs_large_scale.ini";
		  HelperCMPMVS::createMVSLargeScale(mvsLargeScaleFile, mvsPath, nvmPath);
		  if (!HelperCMPMVS::executeCMPMVS(mvsLargeScaleFile))
		  {
			  return;
		  }
		  //Move the file!
		  std::string originalPLY = nvmPathDense.substr(0, nvmPathDense.size() - 4) + ".nvm.cmp\\00\\data\\_OUT_LARGE_SCALE\\meshAvImgCol.ply";
		  if (!Utils::exists(originalPLY))
		  {
			  wxLogError("No mesh generated from CMPMVS");
			  return;
		  }
		  std::wstring stemp = Utils::s2ws(originalPLY);
		  LPCWSTR original = stemp.c_str();

		  std::string newPLY = nvmPath.substr(0, nvmPath.size() - 4) + "_refine.ply";
		  std::wstring stemp2 = Utils::s2ws(newPLY);
		  LPCWSTR newPath = stemp2.c_str();

		  if (!MoveFile(original, newPath))
		  {
			  wxLogError("MoveFile failed (%d)", GetLastError());
			  return;
		  }
	  }
	  else
	  {
		  std::string sfmPath = nvmPath.substr(0, nvmPath.size() - 4) + ".sfm";
		  if (!HelperMeshRecon::executeNVM2SFM(nvmPath, sfmPath, ConfigurationDialog::getBoundingBox()))
		  {
			  return;
		  }
		  if (!HelperMeshRecon::executeMeshRecon(sfmPath, sfmPath, ConfigurationDialog::getLevelOfDetails()))
		  {
			  return;
		  }
	  }
	  meshPath = nvmPath.substr(0, nvmPath.size() - 4) + "_refine.ply";
	  if (!cbTexturize->IsChecked())
	  {
		  EndModal(wxID_OK);
	  }
  }
  if (cbGenerateMesh->IsChecked() && cbTexturize->IsChecked())
  {
	  std::string texReconParams = Utils::preparePath(Utils::getExecutionPath() + "/projetomesh/MeshRecon_plus_TexRecon.exe") + " martintalationpathNotUsed " + getTextureParameters(nvmPath, meshPath);
	  if (!Utils::startProcess(texReconParams))
	  {
		  wxLogError("Error with TexRecon");
		  return;
	  }
	  EndModal(wxID_OK);
  }
  else if (cbTexturize->IsChecked())
  {
	  std::string texReconParams = Utils::preparePath(Utils::getExecutionPath() + "/projetomesh/MeshRecon_plus_TexRecon.exe") + " martintalationpathNotUsed " + getTextureParameters(pickerNVM->GetPath().ToStdString(), pickerMesh->GetPath().ToStdString());
	  if (!Utils::startProcess(texReconParams))
	  {
		  wxLogError("Error with TexRecon");
		  return;
	  }
	  EndModal(wxID_OK);
  }
}

void ReconstructionDialog::OnBtConfig(wxCommandEvent & WXUNUSED)
{
  ConfigurationDialog* config = new ConfigurationDialog(this);
  config->ShowModal();
  delete config;
}

void ReconstructionDialog::OnCheckBoxs(wxCommandEvent & WXUNUSED)
{
  if (cbGenerateMesh->IsChecked() && cbTexturize->IsChecked())
  {
    textImages->Enable();
    pickerImages->Enable();
    textOutputMesh->Enable(false);
    pickerOutputMesh->Enable(false);
    
    textMesh->Enable(false);
    pickerMesh->Enable(false);
    textNVM->Enable(false);
    pickerNVM->Enable(false);
    textOutputTexture->Enable();
    pickerOutputTexture->Enable();
  }
  else
  {
    textImages->Enable(cbGenerateMesh->IsChecked());
    pickerImages->Enable(cbGenerateMesh->IsChecked());
    textOutputMesh->Enable(cbGenerateMesh->IsChecked());
    pickerOutputMesh->Enable(cbGenerateMesh->IsChecked());

    textMesh->Enable(cbTexturize->IsChecked());
    pickerMesh->Enable(cbTexturize->IsChecked());
    textNVM->Enable(cbTexturize->IsChecked());
    pickerNVM->Enable(cbTexturize->IsChecked());
    textOutputTexture->Enable(cbTexturize->IsChecked());
    pickerOutputTexture->Enable(cbTexturize->IsChecked());
  }
}

void ReconstructionDialog::OnDirPickerImages(wxFileDirPickerEvent & WXUNUSED)
{
	wxBeginBusyCursor();
	if (!testImageFolder(pickerImages->GetPath()))
	{
		wxEndBusyCursor();
		pickerImages->SetPath("");
		return;
	}
	wxEndBusyCursor();
  wxString reconsPath = pickerImages->GetPath() + "\\Reconstruction";
  if (pickerOutputTexture->IsEnabled() && pickerOutputTexture->GetPath() == "" || pickerOutputMesh->IsEnabled() && pickerOutputMesh->GetPath() == "")
  {
    wxString reconsPathTemp = reconsPath;
    int folderNum = 0;
    while (_mkdir(reconsPathTemp))
    {
      folderNum++;
      reconsPathTemp = reconsPath;
      reconsPathTemp << "_" << folderNum;
      if (folderNum == 100)//If something go really wrong
      {
        return;
      }
    }
    if (pickerOutputTexture->IsEnabled())
    {
      pickerOutputTexture->SetPath(reconsPathTemp + "\\mesh.obj");
    }
    else if (pickerOutputMesh->IsEnabled())
    {
      pickerOutputMesh->SetPath(reconsPathTemp + "\\mesh.ply");
    }
  }
}

void ReconstructionDialog::OnDirPickerOutput(wxFileDirPickerEvent & WXUNUSED)
{
  if (cbTexturize->IsChecked())
  {
    if (pickerOutputTexture->GetPath().find(' ', 0) != wxNOT_FOUND)
    {
      wxLogError("Do not select a folder/filename with spaces in the name");
      pickerOutputTexture->SetPath("");
    }
  }
  else
  {
    if (pickerOutputMesh->GetPath().find(' ', 0) != wxNOT_FOUND)
    {
      wxLogError("Do not select a folder/filename with spaces in the name");
      pickerOutputMesh->SetPath("");
    }
  }

}

std::string ReconstructionDialog::getTextureParameters(std::string nvmPath, std::string meshPath)
{
  std::string newImagePath = "";
  std::string typeOfReconstruction = "2";
  std::vector<std::string>* imagePaths = new std::vector<std::string>;
  if (getNVMImagePaths(nvmPath, imagePaths))
  {
    if (!checkNVMImagePaths(imagePaths))
    {
      typeOfReconstruction = "3";
      wxMessageBox("Please select the correct image folder","Error",wxICON_ERROR);
      wxDirDialog* imageDialog = new wxDirDialog(this, "Choose the image folder", "", wxDD_DEFAULT_STYLE);
      //Just stop when all the images are in the selected folder
      bool insist = 0;
      do
      {
        if (insist)
        {
          wxMessageBox("The folder still wrong, please select the correct image folder", "Warning", wxICON_WARNING); 
        }
        insist = 1;
        if (imageDialog->ShowModal() == wxID_OK)
        {
          newImagePath = imageDialog->GetPath();
        }
        else
        {
          delete imagePaths;
          delete imageDialog;
          return "";
        }
      } while (!checkNewNVMImagePaths(imagePaths, newImagePath));
      delete imageDialog;
    }
  }
  delete imagePaths;
  std::string parameters = typeOfReconstruction + " " + Utils::preparePath(meshPath) + " " + Utils::preparePath(nvmPath) + " " + Utils::preparePath(pickerOutputTexture->GetPath().ToStdString()) + " " + ConfigurationDialog::getTexReconParameters();
  if (newImagePath != "")
  {
    parameters += " " + Utils::preparePath(newImagePath);
  }
  return parameters;
}

using namespace std;
#include <gdiplus.h>
bool ReconstructionDialog::testImageFolder(wxString imagePath)
{
  if (imagePath.find(' ',0) != wxNOT_FOUND)
  {
    wxLogError("Do not select a folder with spaces in the name");
    return false;
  }
  wxDir* directory = new wxDir(imagePath);
  if (!directory->IsOpened())
  {
    wxLogError("Could not open the folder");
    return false;
  }
  wxArrayString* files = new wxArrayString();
  directory->GetAllFiles(imagePath, files, wxEmptyString, wxDIR_FILES);
  if (files->size() == 0)
  {
    wxLogError("There is no image in the folder");
    return false;
  }
  wxString extension;
  int qtdImages = 0;
  Gdiplus::GdiplusStartupInput gdiplusStartupInput;
  ULONG_PTR gdiplusToken;
  GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
  for (int i = 0; i < files->size(); i++)
  {
    extension = files->Item(i).SubString(files->Item(i).size() - 4, files->Item(i).size());
    if (extension == ".jpg" || extension == ".JPG")
    {
      Gdiplus::Image* img = new Gdiplus::Image(files->Item(i));
      if (img != NULL)
      {
        if (qtdImages == 0)
        {
          heightDefault = img->GetHeight();
          widthDefault = img->GetWidth();
        }
        if (img->GetHeight() != heightDefault || img->GetWidth() != widthDefault)
        {
          wxLogError("The images must have the same resolution");
          return false;
        }
        qtdImages++;
      }
      else
      {
        wxLogError("Error on load the image");
        return false;
      }
      delete img;
    }
  }
  Gdiplus::GdiplusShutdown(gdiplusToken);
  if (qtdImages <= 1)
  {
    wxLogError("You need at least two .jpg images");
    return false;
  }
  return true;
}

bool ReconstructionDialog::getNVMImagePaths(std::string nvmPath, std::vector<std::string>* imagePaths)
{
  std::ifstream in(nvmPath.c_str());
  if (!in.good())
  {
    wxMessageBox("Could not open " + vtksys::SystemTools::GetFilenameName(nvmPath) + " file", "Error", wxICON_ERROR, this);
    return 0;
  }
  //Check NVM file signature
  std::string signature;
  in >> signature;
  if (signature != "NVM_V3")
  {
    wxMessageBox("Invalid NVM signature in " + vtksys::SystemTools::GetFilenameName(nvmPath) + " file", "Error", wxICON_ERROR, this);
    return 0;
  }
  //Discard the rest of the line
  std::getline(in, signature);
  //Read number of views
  int qtdCameras = 0;
  in >> qtdCameras;
  if (qtdCameras < 0 || qtdCameras > 10000)
  {
    wxMessageBox("Invalid number of cameras in " + vtksys::SystemTools::GetFilenameName(nvmPath) + " file", "Error", wxICON_ERROR, this);
    return 0;
  }
  //Read views
  std::string filePath;
  for (int i = 0; i < qtdCameras; ++i)
  {
    //get the filePath
    in >> filePath;
    imagePaths->push_back(filePath);
    //Used to jump to the next line
    in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  }
  in.close();
  return 1;
}
bool ReconstructionDialog::checkNVMImagePaths(std::vector<std::string>* imagePaths)
{
  for (int i = 0; i < imagePaths->size(); i++)
  {
    if (!Utils::exists(imagePaths->at(i)))
    {
      return false;
    }
  }
  return true;
}
bool ReconstructionDialog::checkNewNVMImagePaths(std::vector<std::string>* imagePaths, std::string newPath)
{
  if (newPath == "")
  {
    return checkNVMImagePaths(imagePaths);
  }
  for (int i = 0; i < imagePaths->size(); i++)
  {
    if (!Utils::exists((newPath + "\\" + vtksys::SystemTools::GetFilenameName(imagePaths->at(i)))))
    {
      return false;
    }
  }
  return true;
}