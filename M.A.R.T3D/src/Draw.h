#pragma once
#ifndef __DRAW__H__
#define __DRAW__H__

#include <vtkSmartPointer.h>
#include <vtkActor.h>
#include <vtkLineSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkSphereSource.h>
#include <vtkProperty.h>
#include <vtkLine.h>
#include <vtkActor2D.h>
#include <vtkPolyDataMapper2D.h>
#include <vtkProperty2D.h>
#include <vtkCellArray.h>
#include <vtkTextProperty.h>
#include <vtkFeatureEdges.h>
#include <vtkPolygon.h>
#include <vtkPlaneSource.h>
#include <vtkImageActor.h>
#include <vtkImageMapper3D.h>
#include <vector>
#include <vtkCaptionActor2D.h>
#include <vtkTextActor.h>
#include <vtkDataSetMapper.h>
#include <vtkDataSet.h>
#include <vtkPolyLine.h>
#include <vtkRegularPolygonSource.h>
#include "captionActor2D.h"
#include <vtkFloatArray.h>
#include <vtkDataSetMapper.h>
#include <vtkExtractSelectedIds.h>
#include <vtkSelection.h>

class Draw {
public:
	Draw();
	~Draw();

	static vtkSmartPointer<vtkActor> create3DLine(vtkSmartPointer<vtkRenderer> renderer, double* point1, double* point2)
	{
		vtkSmartPointer<vtkLineSource> lineSource = vtkSmartPointer<vtkLineSource>::New();
		lineSource->SetPoint1(point1);
		lineSource->SetPoint2(point2);
		//Create a mapper and actor
		vtkSmartPointer<vtkPolyDataMapper> mapperLine = vtkSmartPointer<vtkPolyDataMapper>::New();
		mapperLine->SetInputConnection(lineSource->GetOutputPort());
		vtkSmartPointer<vtkActor> actorLine = vtkSmartPointer<vtkActor>::New();
		actorLine->SetMapper(mapperLine);
		//avoid picking this actor, to make the "set focus" command better
		actorLine->SetPickable(false);
		renderer->AddActor(actorLine);
		return actorLine;
	}
  static vtkSmartPointer<vtkActor> create3DLine(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkPoints> points)
  {
    vtkSmartPointer<vtkLineSource> lineSource = vtkSmartPointer<vtkLineSource>::New();
    lineSource->SetPoints(points);
    //Create a mapper and actor
    vtkSmartPointer<vtkPolyDataMapper> mapperLine = vtkSmartPointer<vtkPolyDataMapper>::New();
    mapperLine->SetInputConnection(lineSource->GetOutputPort());
    vtkSmartPointer<vtkActor> actorLine = vtkSmartPointer<vtkActor>::New();
    actorLine->SetMapper(mapperLine);
    //avoid picking this actor, to make the "set focus" command better
    actorLine->SetPickable(false);
    renderer->AddActor(actorLine);
    return actorLine;
  }
	/*vtkSmartPointer<vtkActor> Draw::create3DPolygonEdges(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkPolygon> polygon)
	{
	// Add the polygon to a list of polygons
	vtkSmartPointer<vtkCellArray> polygons = vtkSmartPointer<vtkCellArray>::New();
	polygons->InsertNextCell(polygon);
	// Create a PolyData
	vtkSmartPointer<vtkPolyData> polygonPolyData = vtkSmartPointer<vtkPolyData>::New();
	polygonPolyData->SetPoints(polygon->GetPoints());
	polygonPolyData->SetPolys(polygons);
	//Extract Edges
	vtkSmartPointer<vtkFeatureEdges> featureEdges = vtkSmartPointer<vtkFeatureEdges>::New();
	featureEdges->SetInputData(polygonPolyData);
	featureEdges->BoundaryEdgesOn();
	featureEdges->FeatureEdgesOff();
	featureEdges->ManifoldEdgesOff();
	featureEdges->NonManifoldEdgesOff();
	featureEdges->Update();
	// Visualize
	vtkSmartPointer<vtkPolyDataMapper> edgeMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	edgeMapper->SetInputConnection(featureEdges->GetOutputPort());
	vtkSmartPointer<vtkActor> edgeActor = vtkSmartPointer<vtkActor>::New();
	edgeActor->SetMapper(edgeMapper);
	edgeActor->GetProperty()->SetColor(255, 0, 0);
	renderer->AddActor(edgeActor);
	return edgeActor;
	}*/
  static vtkSmartPointer<captionActor2D> createNumericIndicator(vtkSmartPointer<vtkRenderer> renderer, double* point3DPosition, wxString text, double fontSize, double r, double g, double b)
  {
    vtkSmartPointer<captionActor2D> actor = createNumericIndicator(point3DPosition, text, fontSize, r, g, b);
    renderer->AddActor(actor);
    return actor;
  }
  static vtkSmartPointer<captionActor2D> createNumericIndicator(double* point3DPosition, wxString text, double fontSize, double r, double g, double b)
  {
    vtkSmartPointer<captionActor2D> actor = vtkSmartPointer<captionActor2D>::New();
    actor->SetCaption(text.utf8_str());
    actor->SetPosition(50, 50);
    actor->SetAttachmentPoint(point3DPosition);
    actor->BorderOff();
    actor->LeaderOn();
    actor->SetThreeDimensionalLeader(false);
    //Add Sphere
    vtkSmartPointer<vtkSphereSource> sphereSource = vtkSmartPointer<vtkSphereSource>::New();
    sphereSource->SetRadius(3);
    actor->SetLeaderGlyphConnection(sphereSource->GetOutputPort());
    actor->SetMaximumLeaderGlyphSize(10);
    actor->SetMinimumLeaderGlyphSize(10);

    actor->SetPadding(0);
    actor->GetTextActor()->SetTextScaleModeToNone();
    actor->GetCaptionTextProperty()->SetFontSize(fontSize);
    actor->GetCaptionTextProperty()->SetFontFamilyToArial();
    actor->GetCaptionTextProperty()->SetColor(r, g, b);
    actor->GetCaptionTextProperty()->SetShadow(1);
    return actor;
  }
	static vtkSmartPointer<vtkActor> createSphere(vtkSmartPointer<vtkRenderer> renderer, double* pointCenter, double radius, double r, double g, double b)
	{
		vtkSmartPointer<vtkSphereSource> sphereSource = vtkSmartPointer<vtkSphereSource>::New();
		sphereSource->SetRadius(radius);
		//Create a mapper and actor
		vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		mapper->SetInputConnection(sphereSource->GetOutputPort());
		vtkSmartPointer<vtkActor> actorSphere = vtkSmartPointer<vtkActor>::New();
		actorSphere->SetMapper(mapper);
		actorSphere->GetProperty()->SetColor(r, g, b);
    actorSphere->SetPosition(pointCenter);
		renderer->AddActor(actorSphere);
		return actorSphere;
	}
  static vtkSmartPointer<vtkActor2D> create2DLine(vtkSmartPointer<vtkRenderer> renderer, double* point1, double* point2, vtkSmartPointer<vtkPolyData> polyData2D, double lineWidth, double r, double g, double b)
	{
		vtkSmartPointer<vtkPoints> pointsLine2D = vtkSmartPointer<vtkPoints>::New();
		pointsLine2D->SetNumberOfPoints(2);
		pointsLine2D->Allocate(2);
		pointsLine2D->InsertPoint(0, point1);
		pointsLine2D->InsertPoint(1, point2);
		vtkSmartPointer<vtkCellArray> cellsLine2D = vtkSmartPointer<vtkCellArray>::New();
		cellsLine2D->Initialize();
		vtkSmartPointer<vtkLine> line2D = vtkSmartPointer<vtkLine>::New();
		line2D->GetPointIds()->SetId(0, 0);
		line2D->GetPointIds()->SetId(1, 1);
		cellsLine2D->InsertNextCell(line2D);
		polyData2D->Initialize();
		polyData2D->SetPoints(pointsLine2D);
		polyData2D->SetLines(cellsLine2D);
		polyData2D->Modified();
		vtkSmartPointer<vtkCoordinate> coordinateLine2D = vtkSmartPointer<vtkCoordinate>::New();
		coordinateLine2D->SetCoordinateSystemToWorld();
		vtkSmartPointer<vtkPolyDataMapper2D> mapperLine2D = vtkSmartPointer<vtkPolyDataMapper2D>::New();
		mapperLine2D->SetInputData(polyData2D);
		mapperLine2D->SetTransformCoordinate(coordinateLine2D);
		mapperLine2D->ScalarVisibilityOn();
		mapperLine2D->SetScalarModeToUsePointData();
		mapperLine2D->Update();
		vtkSmartPointer<vtkActor2D> actorLine = vtkSmartPointer<vtkActor2D>::New();
		actorLine->SetMapper(mapperLine2D);
		actorLine->GetProperty()->SetLineWidth(lineWidth);
		actorLine->GetProperty()->SetColor(r, g, b);
		renderer->AddActor2D(actorLine);
		return actorLine;
	}
  static vtkSmartPointer<vtkActor2D> create2DLine(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkPoints> points, double lineWidth, double r, double g, double b)
  {
    vtkSmartPointer<vtkPolyLine> polyLine = vtkSmartPointer<vtkPolyLine>::New();
    polyLine->GetPointIds()->SetNumberOfIds(points->GetNumberOfPoints());
    for (unsigned int i = 0; i < points->GetNumberOfPoints(); i++)
    {
      polyLine->GetPointIds()->SetId(i, i);
    }
    // Create a cell array to store the lines in and add the lines to it
    vtkSmartPointer<vtkCellArray> cells = vtkSmartPointer<vtkCellArray>::New();
    cells->InsertNextCell(polyLine);

    // Create a polydata to store everything in
    vtkSmartPointer<vtkPolyData> polyData2D = vtkSmartPointer<vtkPolyData>::New();
    polyData2D->Initialize();
    polyData2D->SetPoints(points);
    polyData2D->SetLines(cells);
    polyData2D->Modified();
    vtkSmartPointer<vtkCoordinate> coordinateLine2D = vtkSmartPointer<vtkCoordinate>::New();
    coordinateLine2D->SetCoordinateSystemToWorld();
    vtkSmartPointer<vtkPolyDataMapper2D> mapperLine2D = vtkSmartPointer<vtkPolyDataMapper2D>::New();
    mapperLine2D->SetInputData(polyData2D);
    mapperLine2D->SetTransformCoordinate(coordinateLine2D);
    mapperLine2D->ScalarVisibilityOn();
    mapperLine2D->SetScalarModeToUsePointData();
    mapperLine2D->Update();
    vtkSmartPointer<vtkActor2D> actorLine = vtkSmartPointer<vtkActor2D>::New();
    actorLine->SetMapper(mapperLine2D);
    actorLine->GetProperty()->SetLineWidth(lineWidth);
    actorLine->GetProperty()->SetColor(r, g, b);
    renderer->AddActor2D(actorLine);
    return actorLine;
  }
  static vtkSmartPointer<vtkActor2D> create2DLineAngle(vtkSmartPointer<vtkRenderer> renderer, double* point1, double* point2, double* point3, vtkSmartPointer<vtkPolyData> polyData2D, double lineWidth, double r, double g, double b)
	{
		vtkSmartPointer<vtkPoints> pointsLine2D = vtkSmartPointer<vtkPoints>::New();
		pointsLine2D->SetNumberOfPoints(3);
		pointsLine2D->Allocate(3);
		pointsLine2D->InsertPoint(0, point1);
		pointsLine2D->InsertPoint(1, point2);
		pointsLine2D->InsertPoint(2, point3);
		vtkSmartPointer<vtkCellArray> cellsLine2D = vtkSmartPointer<vtkCellArray>::New();
		cellsLine2D->Initialize();
		vtkSmartPointer<vtkLine> line2D = vtkSmartPointer<vtkLine>::New();
		line2D->GetPointIds()->SetId(0, 0);
		line2D->GetPointIds()->SetId(1, 1);
		vtkSmartPointer<vtkLine> line2D_2 = vtkSmartPointer<vtkLine>::New();
		line2D_2->GetPointIds()->SetId(0, 1);
		line2D_2->GetPointIds()->SetId(1, 2);
		cellsLine2D->InsertNextCell(line2D);
		cellsLine2D->InsertNextCell(line2D_2);
		polyData2D->Initialize();
		polyData2D->SetPoints(pointsLine2D);
		polyData2D->SetLines(cellsLine2D);
		polyData2D->Modified();
		vtkSmartPointer<vtkCoordinate> coordinateLine2D = vtkSmartPointer<vtkCoordinate>::New();
		coordinateLine2D->SetCoordinateSystemToWorld();
		vtkSmartPointer<vtkPolyDataMapper2D> mapperLine2D = vtkSmartPointer<vtkPolyDataMapper2D>::New();
		mapperLine2D->SetInputData(polyData2D);
		mapperLine2D->SetTransformCoordinate(coordinateLine2D);
		mapperLine2D->ScalarVisibilityOn();
		mapperLine2D->SetScalarModeToUsePointData();
		mapperLine2D->Update();
		vtkSmartPointer<vtkActor2D> actorLine = vtkSmartPointer<vtkActor2D>::New();
		actorLine->SetMapper(mapperLine2D);
		actorLine->GetProperty()->SetLineWidth(lineWidth);
		actorLine->GetProperty()->SetColor(r, g, b);
		renderer->AddActor2D(actorLine);
		return actorLine;
	}
	/*
	This method is used to create a 2D rectangle using display coordinates!
	*/
	static vtkSmartPointer<vtkActor2D> create2DRectangle(vtkSmartPointer<vtkRenderer> renderer, double* point1, double* point2, double* point3, double* point4, vtkSmartPointer<vtkPolyData> polyData2D, double lineWidth, double r, double g, double b)
	{
		vtkSmartPointer<vtkPoints> pointsLine2D = vtkSmartPointer<vtkPoints>::New();
		pointsLine2D->SetNumberOfPoints(3);
		pointsLine2D->Allocate(3);
		pointsLine2D->InsertPoint(0, point1);
		pointsLine2D->InsertPoint(1, point2);
		pointsLine2D->InsertPoint(2, point3);
		pointsLine2D->InsertPoint(3, point4);
		vtkSmartPointer<vtkCellArray> cellsLine2D = vtkSmartPointer<vtkCellArray>::New();
		cellsLine2D->Initialize();
		vtkSmartPointer<vtkLine> line2D_0 = vtkSmartPointer<vtkLine>::New();
		line2D_0->GetPointIds()->SetId(0, 0);
		line2D_0->GetPointIds()->SetId(1, 1);
		vtkSmartPointer<vtkLine> line2D_1 = vtkSmartPointer<vtkLine>::New();
		line2D_1->GetPointIds()->SetId(0, 1);
		line2D_1->GetPointIds()->SetId(1, 2);
		vtkSmartPointer<vtkLine> line2D_2 = vtkSmartPointer<vtkLine>::New();
		line2D_2->GetPointIds()->SetId(0, 2);
		line2D_2->GetPointIds()->SetId(1, 3);
		vtkSmartPointer<vtkLine> line2D_3 = vtkSmartPointer<vtkLine>::New();
		line2D_3->GetPointIds()->SetId(0, 3);
		line2D_3->GetPointIds()->SetId(1, 0);
		cellsLine2D->InsertNextCell(line2D_0);
		cellsLine2D->InsertNextCell(line2D_1);
		cellsLine2D->InsertNextCell(line2D_2);
		cellsLine2D->InsertNextCell(line2D_3);
		polyData2D->Initialize();
		polyData2D->SetPoints(pointsLine2D);
		polyData2D->SetLines(cellsLine2D);
		polyData2D->Modified();
		vtkSmartPointer<vtkCoordinate> coordinateLine2D = vtkSmartPointer<vtkCoordinate>::New();
		coordinateLine2D->SetCoordinateSystemToDisplay();
		vtkSmartPointer<vtkPolyDataMapper2D> mapperLine2D = vtkSmartPointer<vtkPolyDataMapper2D>::New();
		mapperLine2D->SetInputData(polyData2D);
		mapperLine2D->SetTransformCoordinate(coordinateLine2D);
		mapperLine2D->ScalarVisibilityOn();
		mapperLine2D->SetScalarModeToUsePointData();
		mapperLine2D->Update();
		vtkSmartPointer<vtkActor2D> actorLine = vtkSmartPointer<vtkActor2D>::New();
		actorLine->SetMapper(mapperLine2D);
		actorLine->GetProperty()->SetLineWidth(lineWidth);
		actorLine->GetProperty()->SetColor(r, g, b);
		//try to avoid some erros
		actorLine->SetPickable(false);
		renderer->AddActor2D(actorLine);
		return actorLine;
	}
	static vtkSmartPointer<vtkCaptionActor2D> createText(vtkSmartPointer<vtkRenderer> renderer, double* point3DPosition, wxString text, double fontSize, double r, double g, double b)
	{
		vtkSmartPointer<vtkCaptionActor2D> actor = vtkSmartPointer<vtkCaptionActor2D>::New();
		actor->SetCaption(text.utf8_str());
		actor->SetAttachmentPoint(point3DPosition);
    actor->BorderOff();
    actor->LeaderOff();
    actor->SetPadding(0);
    actor->GetTextActor()->SetTextScaleModeToNone();
		actor->GetCaptionTextProperty()->SetFontSize(fontSize);
    actor->GetCaptionTextProperty()->SetFontFamilyToArial();
		actor->GetCaptionTextProperty()->SetColor(r, g, b);
		actor->GetCaptionTextProperty()->SetShadow(1);
		renderer->AddActor(actor);
		return actor;
	}
	static vtkSmartPointer<vtkActor> createPlane(vtkSmartPointer<vtkRenderer> renderer, double* origin, double* point1, double* point2, double scale, double r, double g, double b)
	{
    if (scale <= 0)
    {
      scale = 1;
    }
    //double* center = Utils::getMidpoint(origin, point1, point2);//For some reason I cant use Utils in this class
    double* center = new double[3];
    for (int i = 0; i < 3; i++)
    {
      center[i] = (origin[i] + point1[i] + point2[i]) / 3;
    }
    double* vetor1 = new double[3];
    vtkMath::Subtract(point1, center, vetor1);
    vtkMath::Normalize(vetor1);
    vtkMath::MultiplyScalar(vetor1, scale);
    vtkMath::Add(center, vetor1, vetor1);
    double* vetor2 = new double[3];
    vtkMath::Subtract(origin, center, vetor2);
    vtkMath::Normalize(vetor2);
    vtkMath::MultiplyScalar(vetor2, scale);
    vtkMath::Add(center, vetor2, vetor2);
    double* vetor3 = new double[3];
    vtkMath::Subtract(point2, center, vetor3);
    vtkMath::Normalize(vetor3);
    vtkMath::MultiplyScalar(vetor3, scale);
    vtkMath::Add(center, vetor3, vetor3);

	 	vtkSmartPointer<vtkPlaneSource> planeSource = vtkSmartPointer<vtkPlaneSource>::New();
		planeSource->SetOrigin(vetor2);
		planeSource->SetPoint1(vetor1);
		planeSource->SetPoint2(vetor3);
		//Create a mapper and actor
		vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		mapper->SetInputConnection(planeSource->GetOutputPort());
		vtkSmartPointer<vtkActor> actorPlane = vtkSmartPointer<vtkActor>::New();
		actorPlane->SetMapper(mapper);
		actorPlane->GetProperty()->SetColor(r, g, b);
		renderer->AddActor(actorPlane);
		return actorPlane;
	}
  static vtkSmartPointer<vtkActor> createPlane(vtkSmartPointer<vtkRenderer> renderer, double* center, double* normal, double r, double g, double b)
  {
    vtkSmartPointer<vtkPlaneSource> planeSource = vtkSmartPointer<vtkPlaneSource>::New();
    planeSource->SetCenter(center);
    planeSource->SetNormal(normal);
    //Create a mapper and actor
    vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->SetInputConnection(planeSource->GetOutputPort());
    vtkSmartPointer<vtkActor> actorPlane = vtkSmartPointer<vtkActor>::New();
    actorPlane->SetMapper(mapper);
    actorPlane->GetProperty()->SetColor(r, g, b);
    renderer->AddActor(actorPlane);
    return actorPlane;
  }
	static vtkSmartPointer<vtkActor> createPolygon(vtkSmartPointer<vtkRenderer> renderer, std::vector<double*> polygonPoints, double r, double g, double b)
	{
		vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
		for (int i = 0; i < polygonPoints.size(); i++)
		{
			points->InsertNextPoint(polygonPoints.at(i));
		}
		// Create the polygon
		vtkSmartPointer<vtkPolygon> polygon = vtkSmartPointer<vtkPolygon>::New();
		polygon->GetPointIds()->SetNumberOfIds(points->GetNumberOfPoints());
		for (int i = 0; i < points->GetNumberOfPoints(); i++)
		{
			polygon->GetPointIds()->SetId(i, i);
		}
		// Add the polygon to a list of polygons
		vtkSmartPointer<vtkCellArray> polygons = vtkSmartPointer<vtkCellArray>::New();
		polygons->InsertNextCell(polygon);
		// Create a PolyData
		vtkSmartPointer<vtkPolyData> polygonPolyData = vtkSmartPointer<vtkPolyData>::New();
		polygonPolyData->SetPoints(points);
		polygonPolyData->SetPolys(polygons);
		vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		mapper->SetInputData(polygonPolyData);
		vtkSmartPointer<vtkActor> actorPolygon = vtkSmartPointer<vtkActor>::New();
		actorPolygon->SetMapper(mapper);
		actorPolygon->GetProperty()->SetColor(r, g, b);
		renderer->AddActor(actorPolygon);
		return actorPolygon;
	}
  static vtkSmartPointer<vtkImageActor> createImageActor(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkImageData> imgData)
  {
    if (imgData == NULL)
    {
      return NULL;
    }
    vtkSmartPointer<vtkImageActor> actor = vtkSmartPointer<vtkImageActor>::New();
    actor->GetMapper()->SetInputData(imgData);
    renderer->AddActor(actor);
    return actor;
  }
  static vtkSmartPointer<vtkActor> createPolyData(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkPolyData> polyData, double r, double g, double b)
  {
    //Create a mapper and actor
    vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->SetInputData(polyData);
    vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
    actor->SetMapper(mapper);
    actor->GetProperty()->SetColor(r, g, b);
    renderer->AddActor(actor);
    return actor;
  }
  static vtkSmartPointer<vtkActor> createPolyData(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkPolyData> polyData)
  {
    //Create a mapper and actor
    vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->ScalarVisibilityOn();
    mapper->SetScalarModeToUsePointData();
    mapper->SetInputData(polyData);
    vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
    actor->SetMapper(mapper);
    renderer->AddActor(actor);
    return actor;
  }
  static vtkSmartPointer<vtkActor> createDataSet(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkDataSet> dataSet, double r, double g, double b)
  {
    //Create a mapper and actor
    vtkSmartPointer<vtkDataSetMapper> mapper = vtkSmartPointer<vtkDataSetMapper>::New();
    mapper->SetInputData(dataSet);
    vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
    actor->SetMapper(mapper);
    actor->GetProperty()->SetColor(r, g, b);
    renderer->AddActor(actor);
    return actor;
  }
  /*
  origin of the camera = p0
  p1--------p2
  |		       |
  |  pCenter |<--- Looking from p0 to pCenter
  |          |
  p4--------p3
  */
  static vtkSmartPointer<vtkActor> createFrustum(vtkSmartPointer<vtkRenderer> renderer, std::vector<double*> cameraPoints)
  {
    vtkSmartPointer<vtkLineSource> lineSource = vtkSmartPointer<vtkLineSource>::New();
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    points->InsertNextPoint(cameraPoints.at(0));
    points->InsertNextPoint(cameraPoints.at(1));
    points->InsertNextPoint(cameraPoints.at(4));
    points->InsertNextPoint(cameraPoints.at(0));
    points->InsertNextPoint(cameraPoints.at(3));
    points->InsertNextPoint(cameraPoints.at(4));
    points->InsertNextPoint(cameraPoints.at(1));
    points->InsertNextPoint(cameraPoints.at(2));
    points->InsertNextPoint(cameraPoints.at(0));
    points->InsertNextPoint(cameraPoints.at(2));
    points->InsertNextPoint(cameraPoints.at(3));
    lineSource->SetPoints(points);
    //Create a mapper and actor
    vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->SetInputConnection(lineSource->GetOutputPort());
    vtkSmartPointer<vtkActor> actorFrustum = vtkSmartPointer<vtkActor>::New();
    actorFrustum->SetMapper(mapper);
    //avoid picking this actor, to make the "set focus" command better
    actorFrustum->SetPickable(false);
    renderer->AddActor(actorFrustum);
    return actorFrustum;
  }
  static vtkSmartPointer<vtkActor> createPointCloud(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkPolyData> meshPolyData, vtkSmartPointer<vtkSelection> selection, double r, double g, double b)
  {
    vtkSmartPointer<vtkExtractSelectedIds> extractSelectedIds = vtkSmartPointer<vtkExtractSelectedIds>::New();
    extractSelectedIds->SetInputData(0, meshPolyData);
    extractSelectedIds->SetInputData(1, selection);
    extractSelectedIds->Update();

    vtkSmartPointer<vtkDataSetMapper> mapper = vtkSmartPointer<vtkDataSetMapper>::New();
    mapper->SetInputConnection(extractSelectedIds->GetOutputPort());
    mapper->SetScalarVisibility(false);

    vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
    actor->SetMapper(mapper);
    actor->GetProperty()->SetColor(1.0, 0.0, 0.0);
    actor->GetProperty()->SetPointSize(2);
    renderer->AddActor(actor);
    return actor;
  }
  static vtkSmartPointer<vtkActor> createPointCloud(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkPoints> points, double r, double g, double b)
  {
    vtkSmartPointer<vtkCellArray> vertices = vtkSmartPointer<vtkCellArray>::New();
    size_t numberOfPoints = points->GetNumberOfPoints();
    vertices->InsertNextCell(numberOfPoints);
    for (size_t i = 0; i < numberOfPoints; i++)
    {
      vertices->InsertCellPoint(i);
    }
    vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();
    polyData->SetPoints(points);
    polyData->SetVerts(vertices);
    vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->SetInputData(polyData);
    vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
    actor->SetMapper(mapper);
    actor->GetProperty()->SetColor(r, g, b);
    renderer->AddActor(actor);
    return actor;
  }
  static vtkSmartPointer<vtkActor> createPointCloud(vtkSmartPointer<vtkPoints> points, double r, double g, double b)
  {
    vtkSmartPointer<vtkCellArray> vertices = vtkSmartPointer<vtkCellArray>::New();
    size_t numberOfPoints = points->GetNumberOfPoints();
    vertices->InsertNextCell(numberOfPoints);
    for (size_t i = 0; i < numberOfPoints; i++)
    {
      vertices->InsertCellPoint(i);
    }
    vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();
    polyData->SetPoints(points);
    polyData->SetVerts(vertices);
    vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->SetInputData(polyData);
    vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
    actor->SetMapper(mapper);
    actor->GetProperty()->SetColor(r, g, b);
    return actor;
  }
  static vtkSmartPointer<vtkActor> createPointCloud(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkPoints> points, vtkSmartPointer<vtkUnsignedCharArray> colors, vtkSmartPointer<vtkFloatArray> normals)
  {
    vtkSmartPointer<vtkCellArray> vertices = vtkSmartPointer<vtkCellArray>::New();
    size_t numberOfPoints = points->GetNumberOfPoints();
    vertices->InsertNextCell(numberOfPoints);
    for (size_t i = 0; i < numberOfPoints; i++)
    {
      vertices->InsertCellPoint(i);
    }
    vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();
    polyData->SetPoints(points);
    if (colors != NULL)
    {
      polyData->GetPointData()->SetScalars(colors);
    }
    if (normals != NULL)
    {
      polyData->GetPointData()->SetNormals(normals);
    }
    polyData->SetVerts(vertices);
    vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->SetInputData(polyData);
    vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
    actor->SetMapper(mapper);
    renderer->AddActor(actor);
    return actor;
  }

  static vtkSmartPointer<vtkPolyData> createPointCloud(vtkSmartPointer<vtkPoints> points, vtkSmartPointer<vtkUnsignedCharArray> colors, vtkSmartPointer<vtkFloatArray> normals)
  {
    vtkSmartPointer<vtkCellArray> vertices = vtkSmartPointer<vtkCellArray>::New();
    size_t numberOfPoints = points->GetNumberOfPoints();
    vertices->InsertNextCell(numberOfPoints);
    for (size_t i = 0; i < numberOfPoints; i++)
    {
      vertices->InsertCellPoint(i);
    }
    vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();
    polyData->SetPoints(points);
    if (colors != NULL)
    {
      polyData->GetPointData()->SetScalars(colors);
    }
    if (normals != NULL)
    {
      polyData->GetPointData()->SetNormals(normals);
    }
    polyData->SetVerts(vertices);
    return polyData;
  }

};


#endif