#pragma once

#include "Utils.h"
#include <wx/log.h>

class HelperVisualSFM
{
public:
	HelperVisualSFM();
	~HelperVisualSFM();

	static void replaceRelativePathFromNVM(std::string filename, std::string newImgPath)
	{
		std::ifstream in(filename.c_str());
		std::stringstream out;
		if (!in.good())
		{
			return;
		}
		//NVM_V3 line
		std::string line;
		getline(in, line, '\n');
		out << line << "\n\n";

		/* Read number of views. */
		int num_views = 0;
		in >> num_views;
		out << num_views << "\n";

		if (num_views < 0 || num_views > 10000)
		{
			return;
		}

		std::string path;
		std::string imgName;
		std::string pathSeparator = "";
		for (int i = 0; i < num_views; ++i)
		{
			/* Filename*/
			in >> path;
			//Find the image name in the relative path  
			imgName = "";
			for (int k = path.size() - 1; k >= 0; k--)
			{
				if (path.at(k) == '\\' || path.at(k) == '/')
				{
					imgName = path.substr(k + 1, path.size());
					break;
				}
			}
			if (imgName != "")
			{
				out << newImgPath << "\\" << imgName << " ";
			}
			else
			{
				out << newImgPath << "\\" << path << " ";
			}


			double temp;
			for (int j = 0; j < 10; j++)
			{
				in >> temp;
				if (j != 9)
				{
					out << temp << " ";
				}
				else
				{
					out << temp;
				}
			}
			//Avoid double space when we finish the cameras
			if (i < num_views - 1)
			{
				out << "\n";
			}
			in.eof();
		}
		while (getline(in, line, '\n'))
		{
			out << line << "\n";
		}
		in.close();
		//Overwrite the file
		std::ofstream outFile(filename.c_str());
		if (!outFile.good())
		{
			return;
		}
		outFile << out.rdbuf();
		outFile.close();
	}

	static bool executeVisualSFM(std::string imagesPath, std::string nvmPath)
	{
		std::string visualParameters(Utils::preparePath(Utils::getExecutionPath() + "/vsfm/VisualSFM.exe") + " sfm " + Utils::preparePath(imagesPath) + " " + Utils::preparePath(nvmPath));
		if (!Utils::startProcess(visualParameters))
		{
			wxLogError("Error with VisualSFM");
			return 0;
		}
		replaceRelativePathFromNVM(nvmPath, imagesPath);
		return 1;
	}

	static bool executeVisualSFMDense(std::string nvmSparsePath, std::string nvmDensePath)
	{
		std::string visualParameters(Utils::preparePath(Utils::getExecutionPath() + "/vsfm/VisualSFM.exe") + " sfm+loadnvm+cmp " + Utils::preparePath(nvmSparsePath) + " " + Utils::preparePath(nvmDensePath));
		if (!Utils::startProcess(visualParameters))
		{
			wxLogError("Error with VisualSFMDense");
			return 0;
		}
		return 1;
	}

private:

};