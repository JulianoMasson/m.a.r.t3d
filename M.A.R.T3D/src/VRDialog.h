#pragma once
#include <wx\dialog.h>
#include <wx\sizer.h>
#include <wx\stattext.h>
#include <wx\button.h>
#include "Mesh.h"
//#include <vtkglew\include\GL\glew.h>
#include <vtkNew.h>
#include <vtkOpenVRCamera.h>
#include <vtkOpenVRRenderer.h>
#include <vtkOpenVRRenderWindow.h>
#include <vtkOpenVRRenderWindowInteractor.h>
#include "InteractorStyleVR.h"
#include <vtkLight.h>
#include <wx\wx.h>
#include <wx\thread.h>
#include <wx\event.h> 

class VRDialog;

class ThreadVR : public wxThread
{
public:
  ThreadVR(VRDialog *handler)
    : wxThread(wxTHREAD_DETACHED)
  {
    m_pHandler = handler;
  }
  ~ThreadVR();
  void setMesh(Mesh* mesh);
  void stopVR();

protected:
  virtual ExitCode Entry();
  VRDialog *m_pHandler;
  Mesh* mesh = NULL;
  vtkNew<InteractorStyleVR> iterStyle;
};

class VRDialog : public wxDialog
{
public:
  VRDialog(wxWindow* parent, Mesh* mesh, wxWindowID id = wxID_ANY, const wxString& title = "Virtual reality", const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(400, 100), long style = wxDEFAULT_DIALOG_STYLE);
  ~VRDialog();

  void setCamera(vtkSmartPointer<vtkCamera> camera);

  //******************
  ThreadVR* m_pThread;
  wxCriticalSection m_pThreadCS;
  void DoStartThread();
  void OnThreadCompletion(bool error);
  vtkSmartPointer<vtkCamera> camera2D;

private:
  DECLARE_EVENT_TABLE()

  void setMesh(Mesh* mesh);
  void OnClose(wxCloseEvent& event);

  wxTimer* timer = NULL;
  void OnTimerTimeout(wxTimerEvent& event);

  enum {
    idBtStopVR,
    idTimer
  };

  
  void OnBtStopVR(wxCommandEvent& WXUNUSED(event));

  Mesh* meshOriginal = NULL;
  Mesh* meshCopy = NULL;
  int qtdClosePressed = 0;
  bool errorVR = false;

};