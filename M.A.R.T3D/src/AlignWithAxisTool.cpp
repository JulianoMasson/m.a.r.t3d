#include "AlignWithAxisTool.h"

vtkStandardNewMacro(AlignWithAxisTool);

void AlignWithAxisTool::setMesh(Mesh * mesh)
{
  if (mesh != NULL)
  {
    this->mesh = mesh;
  }
}

//----------------------------------------------------------------------------
AlignWithAxisTool::AlignWithAxisTool() : vtk3DWidget()
{
  this->EventCallbackCommand->SetCallback(AlignWithAxisTool::ProcessEvents);
}

//----------------------------------------------------------------------------
AlignWithAxisTool::~AlignWithAxisTool()
{
  if (lineWidget != NULL)
  {
    lineWidget->EnabledOff();
    lineWidget->RemoveObserver(this->EventCallbackCommand);
  }
  mesh = NULL;
}

//----------------------------------------------------------------------------
void AlignWithAxisTool::SetEnabled(int enabling)
{
  if (!this->Interactor)
  {
    return;
  }

  if (enabling) //------------------------------------------------------------
  {
    if (this->Enabled) //already enabled, just return
    {
      return;
    }
    if (!this->CurrentRenderer)
    {
      this->SetCurrentRenderer(this->Interactor->FindPokedRenderer(
        this->Interactor->GetLastEventPosition()[0],
        this->Interactor->GetLastEventPosition()[1]));
      if (this->CurrentRenderer == NULL)
      {
        return;
      }
    }
    this->Enabled = 1;

    // listen for the following events
    //vtkRenderWindowInteractor *i = this->Interactor;

    if (lineWidget == NULL)
    {
      lineWidget = vtkSmartPointer<LineWidget>::New();
      lineWidget->SetInteractor(this->Interactor);
      lineWidget->setMaxNumberOfNodes(2);
      if (mesh != NULL)
      {
        vtkSmartPointer<LineWidgetRepresentation> rep = vtkSmartPointer<LineWidgetRepresentation>::New();
        for (int i = 0; i < mesh->actors.size(); i++)
        {
          rep->addProp(mesh->actors.at(i));
        }
        if (mesh->actors.size() == 1)// if we have a lot of actors the cell locator will delay the picking
        {
          rep->addLocator(mesh->getCellLocator());
        }
        lineWidget->SetRepresentation(rep);
      }
      else
      {
        wxMessageBox("You should set the mesh before enabling the measure tool!", "Error", wxICON_ERROR);
      }
    }
    lineWidget->EnabledOn();
    lineWidget->AddObserver(vtkCommand::EndInteractionEvent, this->EventCallbackCommand, this->Priority);

    this->InvokeEvent(vtkCommand::EnableEvent, NULL);
  }

  else //disabling----------------------------------------------------------
  {
    if (!this->Enabled) //already disabled, just return
    {
      return;
    }
    this->Enabled = 0;

    // don't listen for events any more
    this->Interactor->RemoveObserver(this->EventCallbackCommand);

    if (lineWidget != NULL)
    {
      lineWidget->EnabledOff();
      lineWidget->RemoveObserver(this->EventCallbackCommand);
      lineWidget = NULL;
    }
    mesh = NULL;

    this->InvokeEvent(vtkCommand::DisableEvent, NULL);
    this->SetCurrentRenderer(NULL);
  }

  this->Interactor->Render();
}

void AlignWithAxisTool::PlaceWidget(double bounds[6])
{
}

//----------------------------------------------------------------------------
void AlignWithAxisTool::ProcessEvents(vtkObject* vtkNotUsed(object),
  unsigned long event,
  void* clientdata,
  void* vtkNotUsed(calldata))
{
  AlignWithAxisTool* self =
    reinterpret_cast<AlignWithAxisTool *>(clientdata);

  //okay, let's do the right thing
  switch (event)
  {
  case vtkCommand::EndInteractionEvent:
    self->UpdateRepresentation();
    break;
  }
}

//----------------------------------------------------------------------------
void AlignWithAxisTool::UpdateRepresentation()
{
  if (lineWidget != NULL)
  {
    vtkSmartPointer<LineWidgetRepresentation> rep = lineWidget->GetRepresentation();
    vtkSmartPointer<vtkPoints> pointsLine = rep->getPoints();
    if (pointsLine == NULL)
    {
      return;
    }
    if (pointsLine->GetNumberOfPoints() != 2)
    {
      return;
    }
    /*double* point0 = Utils::createDoubleVector(pointsLine->GetPoint(0));
    double* point1 = Utils::createDoubleVector(pointsLine->GetPoint(1));
    double* vectorUser = Utils::createDoubleVector(point1[0] - point0[0], point1[1] - point0[1], point1[2] - point0[2]);
    double* vectorZ = Utils::createDoubleVector(0, 0, 1);
    vtkMath::Normalize(vectorUser);
    double* v = new double[3];
    vtkMath::Cross(vectorUser, vectorZ, v);
    double c = vtkMath::Dot(vectorUser, vectorZ);
    double tht = acos(c);

    vtkSmartPointer<vtkMatrix4x4> matSkew = vtkSmartPointer<vtkMatrix4x4>::New();
    Utils::getSkewSym(v, matSkew);

    vtkSmartPointer<vtkMatrix4x4> matMult1 = vtkSmartPointer<vtkMatrix4x4>::New();
    Utils::multiplyMatrix4x4ByScalar(matSkew, sin(tht), matMult1);

    vtkSmartPointer<vtkMatrix4x4> matSkew2 = vtkSmartPointer<vtkMatrix4x4>::New();
    vtkMatrix4x4::Multiply4x4(matSkew, matSkew, matSkew2);

    vtkSmartPointer<vtkMatrix4x4> matMult2 = vtkSmartPointer<vtkMatrix4x4>::New();
    Utils::multiplyMatrix4x4ByScalar(matSkew2, 1 - cos(tht), matMult2);

    vtkSmartPointer<vtkMatrix4x4> matIdent = vtkSmartPointer<vtkMatrix4x4>::New();
    matIdent->Identity();

    vtkSmartPointer<vtkMatrix4x4> result = vtkSmartPointer<vtkMatrix4x4>::New();
    for (unsigned int i = 0; i < 4; i++)
    {
      for (unsigned int j = 0; j < 4; j++)
      {
        result->SetElement(i, j, matIdent->GetElement(i, j) + matMult1->GetElement(i, j) + matMult2->GetElement(i, j));
      }
    }
    vtkSmartPointer<vtkTransform> T = vtkSmartPointer<vtkTransform>::New();
    T->SetMatrix(result);
    double* point0Disp = Utils::getDisplayPosition(this->CurrentRenderer, point0);
    double* point1Disp = Utils::getDisplayPosition(this->CurrentRenderer, point1);
    if (point0Disp[1] > point1Disp[1])
    {
      T->RotateX(180);
    }
    delete point0Disp, point1Disp;
    vtkSmartPointer<vtkMatrix4x4> result2 = vtkSmartPointer<vtkMatrix4x4>::New();
    result2->DeepCopy(T->GetMatrix());
    result2->SetElement(3, 0, 0);
    result2->SetElement(3, 1, 0);
    result2->SetElement(3, 2, 0);
    result2->SetElement(0, 3, 0);
    result2->SetElement(1, 3, 0);
    result2->SetElement(2, 3, 0);
    result2->SetElement(3, 3, 1);
    T->SetMatrix(result2);*/
    wxBeginBusyCursor(wxHOURGLASS_CURSOR);
    double* point0 = Utils::createDoubleVector(pointsLine->GetPoint(0));
    double* point1 = Utils::createDoubleVector(pointsLine->GetPoint(1));
    double* vector = Utils::getVector(point0, point1);
    mesh->transform(this->CurrentRenderer, Utils::getTransformToAlignVectors(vector, Utils::createDoubleVector(0, 0, 1)));
    delete point0, point1, vector;
    wxEndBusyCursor();
    this->SetEnabled(false);
  }

}
