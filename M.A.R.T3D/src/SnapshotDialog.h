#pragma once
#include <wx\dialog.h>
#include <wx\sizer.h>
#include <wx\slider.h>
#include <wx\stattext.h>
#include <wx\filepicker.h>
#include <wx\msgdlg.h>
#include <wx\checkbox.h>

class SnapshotDialog : public wxDialog
{
public:
	SnapshotDialog(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = "Snapshot", const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE, int* resolution = NULL);
	~SnapshotDialog();

	wxString getPath();
	int getMagnification();
  bool getAlpha();

private:
	DECLARE_EVENT_TABLE()

	void OnOK(wxCommandEvent& WXUNUSED(event));
	void OnCancel(wxCommandEvent& WXUNUSED(event));
	void OnSliderChanged(wxCommandEvent& event);


	//Sizers
	wxBoxSizer* boxSizer;
	wxBoxSizer* boxSizerMag;
	wxBoxSizer* boxSizerDir;
  wxBoxSizer* boxSizerCheckBox;
	wxBoxSizer* boxSizerBts;

	//Texts
	wxStaticText* mainMessage;
	wxStaticText* dirMessage;
	wxStaticText* magMessage;
	wxStaticText* resMessage;
	wxString textResolution;

	//Dir
	wxFilePickerCtrl* filePicker;
	

	//Magnification
	wxSlider* magSlider;

  //Transparency
  wxCheckBox* checkBoxTransp;

	//Buttons
	wxButton* btOK;
	wxButton* btCancel;

	//Resolution
	int defaultValue;
	int* windowResolution;
};

enum {
	idMagSlider
};