#include "AngleTool.h"

vtkStandardNewMacro(AngleTool);

//----------------------------------------------------------------------------
AngleTool::AngleTool() : vtk3DWidget()
{
  this->EventCallbackCommand->SetCallback(AngleTool::ProcessEvents);
}

//----------------------------------------------------------------------------
AngleTool::~AngleTool()
{
  if (textActor != NULL)
  {
    this->CurrentRenderer->RemoveActor2D(textActor);
    textActor = NULL;
  }
  if (lineWidget != NULL)
  {
    lineWidget->EnabledOff();
    lineWidget->RemoveObserver(this->EventCallbackCommand);
  }
  mesh = NULL;
}

//----------------------------------------------------------------------------
void AngleTool::SetEnabled(int enabling)
{
  if (!this->Interactor)
  {
    return;
  }

  if (enabling) //------------------------------------------------------------
  {
    if (this->Enabled) //already enabled, just return
    {
      return;
    }
    if (!this->CurrentRenderer)
    {
      this->SetCurrentRenderer(this->Interactor->FindPokedRenderer(
        this->Interactor->GetLastEventPosition()[0],
        this->Interactor->GetLastEventPosition()[1]));
      if (this->CurrentRenderer == NULL)
      {
        return;
      }
    }
    this->Enabled = 1;

    // listen for the following events
    vtkRenderWindowInteractor *i = this->Interactor;
    i->AddObserver(vtkCommand::MouseMoveEvent,
      this->EventCallbackCommand, this->Priority);

    if (lineWidget == NULL)
    {
      lineWidget = vtkSmartPointer<LineWidget>::New();
      lineWidget->SetInteractor(this->Interactor);
      lineWidget->setMaxNumberOfNodes(3);
      if (mesh != NULL)
      {
        vtkSmartPointer<LineWidgetRepresentation> rep = vtkSmartPointer<LineWidgetRepresentation>::New();
        for (int i = 0; i < mesh->actors.size(); i++)
        {
          rep->addProp(mesh->actors.at(i));
        }
        if (mesh->actors.size() == 1)// if we have a lot of actors the cell locator will delay the picking
        {
          rep->addLocator(mesh->getCellLocator());
        }
        lineWidget->SetRepresentation(rep);
      }
      else
      {
        wxMessageBox("You should set the mesh before enabling the measure tool!", "Error", wxICON_ERROR);
      }
    }
    lineWidget->EnabledOn();
    lineWidget->AddObserver(vtkCommand::StartInteractionEvent, this->EventCallbackCommand, this->Priority);
    lineWidget->AddObserver(vtkCommand::InteractionEvent, this->EventCallbackCommand, this->Priority);
    lineWidget->AddObserver(vtkCommand::EndInteractionEvent, this->EventCallbackCommand, this->Priority);


    this->InvokeEvent(vtkCommand::EnableEvent, NULL);
  }

  else //disabling----------------------------------------------------------
  {
    if (!this->Enabled) //already disabled, just return
    {
      return;
    }
    this->Enabled = 0;

    // don't listen for events any more
    this->Interactor->RemoveObserver(this->EventCallbackCommand);

    // turn off the various actors
    if (textActor != NULL)
    {
      this->CurrentRenderer->RemoveActor2D(textActor);
      textActor = NULL;
    }
    if (lineWidget != NULL)
    {
      lineWidget->EnabledOff();
      lineWidget->RemoveObserver(this->EventCallbackCommand);
      lineWidget = NULL;
    }
    mesh = NULL;

    this->InvokeEvent(vtkCommand::DisableEvent, NULL);
    this->SetCurrentRenderer(NULL);
  }

  this->Interactor->Render();
}

void AngleTool::PlaceWidget(double bounds[6])
{
}


void AngleTool::updateText(double * point1, double * point2, double * point3)
{
  wxString ss;
  ss << wxString::Format("%.2f", getAngle(point1, point2, point3)) << "\u00B0";
  textActor->SetAttachmentPoint(point2);
  textActor->SetCaption(ss.utf8_str());
}

double AngleTool::getAngle(double * point1, double * point2, double * point3)
{
  double* v1 = new double[3];
  double* v2 = new double[3];
  vtkMath::Subtract(point1, point2, v1);
  vtkMath::Subtract(point3, point2, v2);
  angle = vtkMath::DegreesFromRadians(vtkMath::AngleBetweenVectors(v1, v2));
  delete v1, v2;
  return angle;
}

void AngleTool::setMesh(Mesh * mesh)
{
  if (mesh != NULL)
  {
    this->mesh = mesh;
  }
}

//----------------------------------------------------------------------------
void AngleTool::ProcessEvents(vtkObject* vtkNotUsed(object),
  unsigned long event,
  void* clientdata,
  void* vtkNotUsed(calldata))
{
  AngleTool* self =
    reinterpret_cast<AngleTool *>(clientdata);

  //okay, let's do the right thing
  switch (event)
  {
  case vtkCommand::MouseMoveEvent:
  case vtkCommand::StartInteractionEvent:
  case vtkCommand::InteractionEvent:
  case vtkCommand::EndInteractionEvent:
    self->UpdateRepresentation();
    break;
  }
}

//----------------------------------------------------------------------------
void AngleTool::UpdateRepresentation()
{
  if (lineWidget != NULL)
  {
    vtkSmartPointer<LineWidgetRepresentation> rep = lineWidget->GetRepresentation();
    vtkSmartPointer<vtkPoints> pointsLine = rep->getPoints();
    if (pointsLine == NULL)
    {
      if (textActor != NULL)
      {
        this->CurrentRenderer->RemoveActor2D(textActor);
        textActor = NULL;
        this->Interactor->Render();
      }
      return;
    }
    if (pointsLine->GetNumberOfPoints() != 3)
    {
      if (textActor != NULL)
      {
        this->CurrentRenderer->RemoveActor2D(textActor);
        textActor = NULL;
        this->Interactor->Render();
      }
      return;
    }
    double* point0 = Utils::createDoubleVector(pointsLine->GetPoint(0));
    double* point1 = Utils::createDoubleVector(pointsLine->GetPoint(1));
    double* point2 = Utils::createDoubleVector(pointsLine->GetPoint(2));
    if (textActor == NULL)
    {
      wxString ss;
      ss << wxString::Format("%.2f",getAngle(point0, point1, point2)) << "\u00B0";
      textActor = Draw::createText(this->CurrentRenderer, point1, ss, 24, 1.0, 1.0, 1.0);
      textActor->GetTextActor();
    }
    else
    {
      updateText(point0, point1, point2);
    }
  }
}