#include "DistanceTool.h"

vtkStandardNewMacro(DistanceTool);

double DistanceTool::getDistance()
{
  return distance;
}

bool DistanceTool::hasFinished()
{
  if (lineWidget != NULL)
  {
    return lineWidget->hasFinished();
  }
  return false;
}

void DistanceTool::updateText(double* point1, double* point2)
{
  this->distance = sqrt(vtkMath::Distance2BetweenPoints(point1, point2));
  textActor->SetAttachmentPoint(Utils::getMidpoint(point1, point2));
  textActor->SetCaption(mesh->getCalibration()->getCalibratedText(distance, abs(point1[0] - point2[0]), abs(point1[1] - point2[1]), abs(point1[2] - point2[2])).c_str());
}

void DistanceTool::updateText()
{
  textActor->SetCaption(mesh->getCalibration()->getCalibratedText(distance).c_str());
}

void DistanceTool::setMesh(Mesh * mesh)
{
  if (mesh != NULL)
  {
    this->mesh = mesh;
  }
}

void DistanceTool::updateCalibration()
{
  wxString realMeasureTxt = wxGetTextFromUser("Please insert the real object measure with the measure unit", "Calibration", "Ex.30cm");
  double realMeasure;
  realMeasureTxt.ToDouble(&realMeasure);
  if (realMeasure <= 0)
  {
    wxMessageBox("Wrong Number", "Error", wxICON_ERROR);
    return;
  }
  char c;
  wxString measureUnit;
  for (int i = 0; i < realMeasureTxt.length(); i++)
  {
    c = realMeasureTxt.at(i);
    if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
    {
      measureUnit << c;
    }
  }
  
  mesh->setCalibration(new Calibration(realMeasure / distance,measureUnit.ToStdString()));
  this->updateText();
  this->Interactor->GetRenderWindow()->Render();
}

//----------------------------------------------------------------------------
DistanceTool::DistanceTool() : vtk3DWidget()
{
  this->EventCallbackCommand->SetCallback(DistanceTool::ProcessEvents);
}

//----------------------------------------------------------------------------
DistanceTool::~DistanceTool()
{
  if (textActor != NULL)
  {
    this->CurrentRenderer->RemoveActor2D(textActor);
    textActor = NULL;
  }
  if (lineWidget != NULL)
  {
    lineWidget->EnabledOff();
    lineWidget->RemoveObserver(this->EventCallbackCommand);
  }
  mesh = NULL;
}

//----------------------------------------------------------------------------
void DistanceTool::SetEnabled(int enabling)
{
  if (!this->Interactor)
  {
    return;
  }

  if (enabling) //------------------------------------------------------------
  {
    if (this->Enabled) //already enabled, just return
    {
      return;
    }
    if (!this->CurrentRenderer)
    {
      this->SetCurrentRenderer(this->Interactor->FindPokedRenderer(
        this->Interactor->GetLastEventPosition()[0],
        this->Interactor->GetLastEventPosition()[1]));
      if (this->CurrentRenderer == NULL)
      {
        return;
      }
    }
    this->Enabled = 1;

    // listen for the following events
    vtkRenderWindowInteractor *i = this->Interactor;
    i->AddObserver(vtkCommand::MouseMoveEvent,
      this->EventCallbackCommand, this->Priority);

    if (lineWidget == NULL)
    {
      lineWidget = vtkSmartPointer<LineWidget>::New();
      lineWidget->SetInteractor(this->Interactor);
      lineWidget->setMaxNumberOfNodes(2);
      if (mesh != NULL)
      {
        vtkSmartPointer<LineWidgetRepresentation> rep = vtkSmartPointer<LineWidgetRepresentation>::New();
        for (int i = 0; i < mesh->actors.size(); i++)
        {
          rep->addProp(mesh->actors.at(i));
        }
        if (mesh->actors.size() == 1)// if we have a lot of actors the cell locator will delay the picking
        {
          rep->addLocator(mesh->getCellLocator());
        }
        lineWidget->SetRepresentation(rep);
      }
      else
      {
        wxMessageBox("You should set the mesh before enabling the measure tool!","Error", wxICON_ERROR);
      }
    }
    lineWidget->EnabledOn();
    lineWidget->AddObserver(vtkCommand::StartInteractionEvent, this->EventCallbackCommand, this->Priority);
    lineWidget->AddObserver(vtkCommand::InteractionEvent, this->EventCallbackCommand, this->Priority);
    lineWidget->AddObserver(vtkCommand::EndInteractionEvent, this->EventCallbackCommand, this->Priority);
    
    this->InvokeEvent(vtkCommand::EnableEvent, NULL);
  }

  else //disabling----------------------------------------------------------
  {
    if (!this->Enabled) //already disabled, just return
    {
      return;
    }
    this->Enabled = 0;

    // don't listen for events any more
    this->Interactor->RemoveObserver(this->EventCallbackCommand);

    // turn off the various actors
    if (textActor != NULL)
    {
      this->CurrentRenderer->RemoveActor2D(textActor);
      textActor = NULL;
    }
    if (lineWidget != NULL)
    {
      lineWidget->EnabledOff();
      lineWidget->RemoveObserver(this->EventCallbackCommand);
      lineWidget = NULL;
    }
    mesh = NULL;

    this->InvokeEvent(vtkCommand::DisableEvent, NULL);
    this->SetCurrentRenderer(NULL);
  }

  this->Interactor->Render();
}

void DistanceTool::PlaceWidget(double bounds[6])
{
}

//----------------------------------------------------------------------------
void DistanceTool::ProcessEvents(vtkObject* vtkNotUsed(object),
  unsigned long event,
  void* clientdata,
  void* vtkNotUsed(calldata))
{
  DistanceTool* self =
    reinterpret_cast<DistanceTool *>(clientdata);

  //okay, let's do the right thing
  switch (event)
  {
  case vtkCommand::MouseMoveEvent:
  case vtkCommand::StartInteractionEvent:
  case vtkCommand::InteractionEvent:
  case vtkCommand::EndInteractionEvent:
    self->UpdateRepresentation();
    break;
  }
}

//----------------------------------------------------------------------------
void DistanceTool::UpdateRepresentation()
{
  if (lineWidget != NULL)
  {
    vtkSmartPointer<LineWidgetRepresentation> rep = lineWidget->GetRepresentation();
    vtkSmartPointer<vtkPoints> pointsLine = rep->getPoints();
    if (pointsLine == NULL)
    {
      if (textActor != NULL)
      {
        this->CurrentRenderer->RemoveActor2D(textActor);
        textActor = NULL;
        this->Interactor->Render();
      }
      return;
    }
    if (pointsLine->GetNumberOfPoints() != 2)
    {
      return;
    }
    double* point0 = Utils::createDoubleVector(pointsLine->GetPoint(0));
    double* point1 = Utils::createDoubleVector(pointsLine->GetPoint(1));
    if (textActor == NULL)
    {
      distance = sqrt(vtkMath::Distance2BetweenPoints(point0, point1));
      wxString ss = mesh->getCalibration()->getCalibratedText(distance, abs(point0[0] - point1[0]), abs(point0[1] - point1[1]), abs(point0[2] - point1[2]));
      textActor = Draw::createText(this->CurrentRenderer, Utils::getMidpoint(point0, point1), ss, 24, 1.0, 1.0, 1.0);
    }
    else
    {
      updateText(point0, point1);
    }
  }

}
