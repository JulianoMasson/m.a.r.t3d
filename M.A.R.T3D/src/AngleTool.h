#pragma once

#ifndef AngleTool_h
#define AngleTool_h

#include "vtkInteractionWidgetsModule.h" // For export macro
#include "vtk3DWidget.h"

#include "vtkActor.h"
#include "vtkCallbackCommand.h"
#include <vtkPropPicker.h>
#include "vtkPickingManager.h"
#include "vtkPlane.h"
#include "vtkPolyData.h"
#include "vtkProperty.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkCaptionActor2D.h"
#include "Utils.h"
#include "Draw.h"
#include "Mesh.h"
#include <wx\string.h>
#include "LineWidget.h"

class AngleTool : public vtk3DWidget
{
public:
  /**
  * Instantiate the object.
  */
  static AngleTool *New();

  vtkTypeMacro(AngleTool, vtk3DWidget);

  //@{
  /**
  * Methods that satisfy the superclass' API.
  */
  virtual void SetEnabled(int);
  virtual void PlaceWidget(double bounds[6]);
  //@}

  //@{
  /**
  * Update the text with the angle between point1, point2 and point3.
  * point1
  * |
  * |
  * point2 - - - - point3
  */
  void updateText(double* point1, double* point2, double* point3);
  //@}

  //@{
  /**
  * Get the angle(in degrees) between point1, point2 and point3.
  * point1
  * |
  * |
  * point2 - - - - point3
  */
  double getAngle(double* point1, double* point2, double* point3);
  //@}

  //@{
  /**
  * Set the mesh that is going to be used
  */
  void setMesh(Mesh* mesh);
  //@}

protected:
  AngleTool();
  ~AngleTool();

  //handles the events
  static void ProcessEvents(vtkObject* object, unsigned long event,
    void* clientdata, void* calldata);

  vtkSmartPointer<LineWidget> lineWidget = NULL;

  //Actors
  vtkSmartPointer<vtkCaptionActor2D> textActor = NULL;

  // Controlling ivars
  void UpdateRepresentation();

  Mesh* mesh;

  double angle = -1;

private:
  AngleTool(const AngleTool&) VTK_DELETE_FUNCTION;
  void operator=(const AngleTool&) VTK_DELETE_FUNCTION;
};

#endif

