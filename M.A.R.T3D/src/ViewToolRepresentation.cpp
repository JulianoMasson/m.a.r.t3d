#include "ViewToolRepresentation.h"

vtkStandardNewMacro(ViewToolRepresentation);

//-------------------------------------------------------------------------
ViewToolRepresentation::ViewToolRepresentation()
{
  this->InteractionState = ViewToolRepresentation::Outside;

  //Images
  viewImages.push_back(wxBitmap(wxICON(ICON_TOP_VIEW)).ConvertToImage());
  viewImages.push_back(wxBitmap(wxICON(ICON_BOTTOM_VIEW)).ConvertToImage());
  viewImages.push_back(wxBitmap(wxICON(ICON_FRONT_VIEW)).ConvertToImage());
  viewImages.push_back(wxBitmap(wxICON(ICON_BACK_VIEW)).ConvertToImage());
  viewImages.push_back(wxBitmap(wxICON(ICON_LEFT_VIEW)).ConvertToImage());
  viewImages.push_back(wxBitmap(wxICON(ICON_RIGHT_VIEW)).ConvertToImage());
  
  for (int i = 0; i < viewImages.size(); i++)
  {
    vtkSmartPointer<vtkImageMapper> imageMapper = vtkSmartPointer<vtkImageMapper>::New();
    imageMapper->SetInputData(Utils::wxImage2ImageData(viewImages.at(i)));
    imageMapper->SetColorWindow(255);
    imageMapper->SetColorLevel(127.5);
    imageMapper->Update();

    viewActors.push_back(vtkSmartPointer<vtkActor2D>::New());
    viewActors.back()->SetMapper(imageMapper);
    viewActorsDisplayPosition.push_back(new int[2]);
  }
}

//-------------------------------------------------------------------------
ViewToolRepresentation::~ViewToolRepresentation()
{
  viewActors.clear();
  viewImages.clear();
}

bool ViewToolRepresentation::isInsideButton(int x, int y, int* pos)
{
  if ((x > pos[0] && x < pos[0] + HalfButtonSize*2) && (y > pos[1] && y < pos[1] + HalfButtonSize*2))
  {
    return true;
  }
  return false;
}

//-------------------------------------------------------------------------
void ViewToolRepresentation::StartWidgetInteraction(double eventPos[2])
{
  this->StartEventPosition[0] = eventPos[0];
  this->StartEventPosition[1] = eventPos[1];
}

//-------------------------------------------------------------------------
void ViewToolRepresentation::WidgetInteraction(double eventPos[2])
{
  this->Modified();
  this->BuildRepresentation();
}

//-------------------------------------------------------------------------
int ViewToolRepresentation::ComputeInteractionState(int X, int Y, int vtkNotUsed(modify))
{
  if (X != 0 && Y != 0)
  {
    for (int i = 0; i < viewActors.size(); i++)
    {
      if (isInsideButton(X, Y, viewActorsDisplayPosition.at(i)))
      {
        this->InteractionState = i + 1;
        return this->InteractionState;
      }
    }
  }
  this->InteractionState = ViewToolRepresentation::Outside;
  return this->InteractionState;
}

//-------------------------------------------------------------------------
void ViewToolRepresentation::BuildRepresentation()
{
  if (this->Renderer &&
    (this->GetMTime() > this->BuildTime ||
    (this->Renderer->GetVTKWindow() &&
      this->Renderer->GetVTKWindow()->GetMTime() > this->BuildTime)))
  {
    int *size = this->Renderer->GetSize();
    if (0 == size[0] || 0 == size[1])
    {
      // Renderer has no size yet: wait until the next
      // BuildRepresentation...
      return;
    }
    double tx = HalfButtonSize + 20;
    double ty = size[1] - (HalfButtonSize + 25);

    for (int i = 0; i < viewActors.size(); i++)
    {
      viewActorsDisplayPosition.at(i)[0] = 10 + i*(tx);
      viewActorsDisplayPosition.at(i)[1] = ty;
      viewActors.at(i)->SetDisplayPosition(viewActorsDisplayPosition.at(i)[0], ty);
    }

    this->BuildTime.Modified();
  }
}
//-------------------------------------------------------------------------
void ViewToolRepresentation::GetActors2D(vtkPropCollection *pc)
{
  for (int i = 0; i < viewActors.size(); i++)
  {
    pc->AddItem(viewActors.at(i));
  }
}

//-------------------------------------------------------------------------
void ViewToolRepresentation::ReleaseGraphicsResources(vtkWindow *w)
{
  for (int i = 0; i < viewActors.size(); i++)
  {
    viewActors.at(i)->ReleaseGraphicsResources(w);
  }
}

//-------------------------------------------------------------------------
int ViewToolRepresentation::RenderOverlay(vtkViewport *w)
{
  this->BuildRepresentation();
  int count = viewActors.at(0)->RenderOverlay(w);
  for (int i = 1; i < viewActors.size(); i++)
  {
    count += viewActors.at(i)->RenderOverlay(w);
  }
  return count;
}

//-------------------------------------------------------------------------
int ViewToolRepresentation::RenderOpaqueGeometry(vtkViewport *w)
{
  this->BuildRepresentation();
  int count = viewActors.at(0)->RenderOpaqueGeometry(w);
  for (int i = 1; i < viewActors.size(); i++)
  {
    count += viewActors.at(i)->RenderOpaqueGeometry(w);
  }
  return count;
}

//-----------------------------------------------------------------------------
int ViewToolRepresentation::RenderTranslucentPolygonalGeometry(vtkViewport *w)
{
  this->BuildRepresentation();
  int count = viewActors.at(0)->RenderTranslucentPolygonalGeometry(w);
  for (int i = 1; i < viewActors.size(); i++)
  {
    count += viewActors.at(i)->RenderTranslucentPolygonalGeometry(w);
  }
  return count;
}

//-----------------------------------------------------------------------------
// Description:
// Does this prop have some translucent polygonal geometry?
int ViewToolRepresentation::HasTranslucentPolygonalGeometry()
{
  this->BuildRepresentation();
  int count = viewActors.at(0)->HasTranslucentPolygonalGeometry();
  for (int i = 1; i < viewActors.size(); i++)
  {
    count += viewActors.at(i)->HasTranslucentPolygonalGeometry();
  }
  return count;
}

//-------------------------------------------------------------------------
void ViewToolRepresentation::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}