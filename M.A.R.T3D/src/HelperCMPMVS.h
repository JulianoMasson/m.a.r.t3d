#pragma once

#include "Utils.h"
#include <wx/log.h>
using namespace std;
#include <gdiplus.h>

class HelperCMPMVS
{
public:
	HelperCMPMVS();
	~HelperCMPMVS();

	static bool createMVSFirstRun(std::string mvsFirstRunFilename, std::string mvsPath, std::string nvmPath)
	{
		//Get infromations from NVM
		std::ifstream in(nvmPath.c_str());
		if (!in.good())
		{
			wxLogError("Could not load NVM");
			return 0;
		}
		std::string line;
		getline(in, line, '\n');
		/* Read number of views. */
		int num_views = 0;
		in >> num_views;
		std::string imgPath;
		in >> imgPath;
		in.close();

		int width, height;
		Gdiplus::GdiplusStartupInput gdiplusStartupInput;
		ULONG_PTR gdiplusToken;
		GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
		std::string extension = vtksys::SystemTools::GetFilenameLastExtension(imgPath);
		if (extension == ".jpg" || extension == ".JPG")
		{
			std::wstring wide_string = std::wstring(imgPath.begin(), imgPath.end());
			Gdiplus::Image* img = new Gdiplus::Image(wide_string.c_str());
			if (img)
			{
				width = img->GetWidth();
				height = img->GetHeight();
			}	
			else
			{
				wxLogError("Error on load the image");
				return 0;
			}
			delete img;
		}
		else
		{
			wxLogError("Image extension not supported");
			return 0;
		}
		Gdiplus::GdiplusShutdown(gdiplusToken);

		//Creating the .ini
		std::ofstream outFile(mvsFirstRunFilename.c_str());
		if (!outFile.good())
		{
			wxLogError("Could not create MVS first run");
			return 0;
		}
		outFile << "[global]\ndirName=\"" << mvsPath << "\"\n";
		outFile << "prefix=\"\"\n";
		outFile << "imgExt=\"jpg\"\n";
		outFile << "ncams=" << num_views << "\n";
		outFile << "width=" << width << "\n";
		outFile << "height=" << height << "\n";
		outFile << "scale=1\n";
		outFile << "workDirName=\"_tmp\"\n";
		outFile << "doPrepareData=TRUE\n";
		outFile << "doPrematchSifts=TRUE\n";
		outFile << "doPlaneSweepingSGM=TRUE\n";
		outFile << "doFuse=TRUE\n";
		outFile << "nTimesSimplify=0\n\n";
		outFile << "[prematching]\nminAngle=3.0\n\n";
		outFile << "[grow]\nminNumOfConsistentCams=6\n\n";
		outFile << "[filter]\nminNumOfConsistentCams=2\n\n";
		outFile << "[semiGlobalMatching]\nwsh=4\n\n";
		outFile << "[refineRc]\nwsh=4\n\n\n";
		outFile << "#do not erase empy lines after this comment otherwise it will crash ... bug\n\n\n";
		outFile.close();
		return 1;
	}

	static bool createMVSLargeScale(std::string mvsLargeScaleFilename, std::string mvsPath, std::string nvmPath)
	{
		//Get infromations from NVM
		std::ifstream in(nvmPath.c_str());
		if (!in.good())
		{
			wxLogError("Could not load NVM");
			return 0;
		}
		std::string line;
		getline(in, line, '\n');
		/* Read number of views. */
		int num_views = 0;
		in >> num_views;
		std::string imgPath;
		in >> imgPath;
		in.close();

		int width, height;
		Gdiplus::GdiplusStartupInput gdiplusStartupInput;
		ULONG_PTR gdiplusToken;
		GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
		std::string extension = vtksys::SystemTools::GetFilenameLastExtension(imgPath);
		if (extension == ".jpg" || extension == ".JPG")
		{
			std::wstring wide_string = std::wstring(imgPath.begin(), imgPath.end());
			Gdiplus::Image* img = new Gdiplus::Image(wide_string.c_str());
			if (img)
			{
				width = img->GetWidth();
				height = img->GetHeight();
			}
			else
			{
				wxLogError("Error on load the image");
				return 0;
			}
			delete img;
		}
		else
		{
			wxLogError("Image extension not supported");
			return 0;
		}
		Gdiplus::GdiplusShutdown(gdiplusToken);

		//Creating the .ini
		std::ofstream outFile(mvsLargeScaleFilename.c_str());
		if (!outFile.good())
		{
			wxLogError("Could not create MVS large scale");
			return 0;
		}
		outFile << "[global]\ndirName=\"" << mvsPath << "\"\n";
		outFile << "prefix=\"\"\n";
		outFile << "imgExt=\"jpg\"\n";
		outFile << "ncams=" << num_views << "\n";
		outFile << "width=" << width << "\n";
		outFile << "height=" << height << "\n";
		outFile << "scale=1\n";
		outFile << "workDirName=\"_tmp\"\n";
		outFile << "doPrepareData=FALSE\n";
		outFile << "doPrematchSifts=FALSE\n";
		outFile << "doPlaneSweepingSGM=FALSE\n";
		outFile << "doFuse=FALSE\n\n";

		outFile << "[uvatlas]\ntexSide=0\nscale=0\n\n";

		outFile << "[hallucinationsFiltering]\nuseSkyPrior=FALSE\ndoLeaveLargestFullSegmentOnly=FALSE\ndoRemoveHugeTriangles=TRUE\n\n";

		outFile << "[delanuaycut]\nsaveMeshTextured=FALSE\n\n";

		outFile << "[largeScale]\n";
		outFile << "workDirName=\"largeScaleMaxPts01024\"\n";
		outFile << "doReconstructSpaceAccordingToVoxelsArray=TRUE\n";
		outFile << "doGenerateAndReconstructSpaceMaxPts=FALSE\n";
		outFile << "doGenerateSpace=TRUE\n";
		outFile << "planMaxPts=3000000\n";
		outFile << "planMaxPtsPerVoxel=3000000\n";
		outFile << "nGridHelperVolumePointsDim=10\n";
		outFile << "joinMeshesSaveTextured=FALSE\n\n";

		outFile << "[meshEnergyOpt]\ndoOptimizeOrSmoothMesh=FALSE\n\n\n";

		outFile << "#do not erase empy lines after this comment otherwise it will crash ... bug\n\n\n\n";
		outFile.close();
		return 1;
	}

	static bool executeCMPMVS(std::string mvsPath)
	{
		std::string cmpmvsParameters(Utils::preparePath(Utils::getExecutionPath() + "/CMPMVS/CMPMVS.exe")+ " " + Utils::preparePath(mvsPath));
		if (!Utils::startProcess(cmpmvsParameters, Utils::getExecutionPath() + "/CMPMVS/"))
		{
			wxLogError("Error with CMPMVS");
			return 0;
		}
		return 1;
	}

private:

};