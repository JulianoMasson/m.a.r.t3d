#pragma once

class GPSData {
private:
	double latitude = -1;
	double longitude = -1;
	double altitude = -1;
public:
	GPSData();
	~GPSData();

	void setLongitude(double longitude);
	double getLongitude();

	void setLatitude(double latitude);
	double getLatitude();

	void setAltitude(double altitude);
	double getAltitude();

  /*
  Compute the distance bewtween two GPSData, it uses the latitude, longitude and altitude
  */
  double getDistanceBetweenGPSData(GPSData* gpsData);
};