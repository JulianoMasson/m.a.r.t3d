#include "ConfigurationDialog.h"


wxBEGIN_EVENT_TABLE(ConfigurationDialog, wxDialog)
  EVT_BUTTON(wxID_OK, ConfigurationDialog::OnOK)
  EVT_BUTTON(idBtDefaultConfig, ConfigurationDialog::OnBtDefault)
wxEND_EVENT_TABLE()

bool ConfigurationDialog::isFirstInstance = true;
//CMPMVS
bool ConfigurationDialog::forceCMPMVS = false;
//MeshRecon
int ConfigurationDialog::boundingBoxType = 0;
int ConfigurationDialog::levelOfDetails = 0;
bool ConfigurationDialog::forceLevelOfDetails = false;
//TexRecon
int ConfigurationDialog::dataTerm = 1;
int ConfigurationDialog::outlierRemoval = 0;
bool ConfigurationDialog::geometricVisibilityTest = true;
bool ConfigurationDialog::globalSeamLeveling = false;
bool ConfigurationDialog::localSeamLeveling = true;
bool ConfigurationDialog::holeFilling = true;
bool ConfigurationDialog::keepUnseenFaces = false;

ConfigurationDialog::ConfigurationDialog(wxWindow * parent, wxWindowID id, const wxString & title, const wxPoint & pos, const wxSize & size, long style) : wxDialog(parent, id, title, pos, size, style)
{
  if (isFirstInstance)
  {
    loadDefaultConfig();
    isFirstInstance = false;
  }
  this->SetSizeHints(wxDefaultSize, wxDefaultSize);

  wxBoxSizer* bSizer = new wxBoxSizer(wxVERTICAL);

  //CMPMVS
  wxStaticBoxSizer* sbSizerCMPMVS = new wxStaticBoxSizer(new wxStaticBox(this, wxID_ANY, "CMPMVS"), wxVERTICAL);

  wxFlexGridSizer* fgSizerCMPMVS = new wxFlexGridSizer(1, 2, 0, 0);
  fgSizerCMPMVS->SetFlexibleDirection(wxBOTH);
  fgSizerCMPMVS->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

  fgSizerCMPMVS->Add(new wxStaticText(sbSizerCMPMVS->GetStaticBox(), wxID_ANY, "Use CMPMVS"), 0, wxALL, 5);

  ckBForceCMPMVS = new wxCheckBox(sbSizerCMPMVS->GetStaticBox(), wxID_ANY, wxEmptyString);
  ckBForceCMPMVS->SetValue(forceCMPMVS);
  fgSizerCMPMVS->Add(ckBForceCMPMVS, 0, wxALL, 5);

  sbSizerCMPMVS->Add(fgSizerCMPMVS, 1, wxEXPAND, 5);

  bSizer->Add(sbSizerCMPMVS, 0, wxEXPAND, 5);

  //MeshRecon
  wxStaticBoxSizer* sbSizerMeshRecon = new wxStaticBoxSizer(new wxStaticBox(this, wxID_ANY, "MeshRecon"), wxVERTICAL);

  wxFlexGridSizer* fgSizerMeshRecon = new wxFlexGridSizer(2, 2, 0, 0);
  fgSizerMeshRecon->SetFlexibleDirection(wxBOTH);
  fgSizerMeshRecon->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

  fgSizerMeshRecon->Add(new wxStaticText(sbSizerMeshRecon->GetStaticBox(), wxID_ANY, "Bounding box type"), 0, wxALL, 5);

  wxArrayString choicesBoundingBox;
  choicesBoundingBox.Add("0"); choicesBoundingBox.Add("1");
  choiceBoundingBox = new wxChoice(sbSizerMeshRecon->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxDefaultSize, choicesBoundingBox);
  choiceBoundingBox->SetSelection(boundingBoxType);
  fgSizerMeshRecon->Add(choiceBoundingBox, 0, wxALL, 5);

  fgSizerMeshRecon->Add(new wxStaticText(sbSizerMeshRecon->GetStaticBox(), wxID_ANY, "Level of details"), 0, wxALL, 5);

  wxArrayString choicesLevelOfDetails;
  choicesLevelOfDetails.Add("High"); choicesLevelOfDetails.Add("Medium"); choicesLevelOfDetails.Add("Low");
  choiceLevelOfDetails = new wxChoice(sbSizerMeshRecon->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxDefaultSize, choicesLevelOfDetails);
  choiceLevelOfDetails->SetSelection(levelOfDetails);
  fgSizerMeshRecon->Add(choiceLevelOfDetails, 0, wxALL, 5);

  fgSizerMeshRecon->Add(new wxStaticText(sbSizerMeshRecon->GetStaticBox(), wxID_ANY, "Disable GPU memory check"), 0, wxALL, 5);

  ckBForceLevelOfDetails = new wxCheckBox(sbSizerMeshRecon->GetStaticBox(), wxID_ANY, wxEmptyString);
  ckBForceLevelOfDetails->SetValue(forceLevelOfDetails);
  fgSizerMeshRecon->Add(ckBForceLevelOfDetails, 0, wxALL, 5);


  sbSizerMeshRecon->Add(fgSizerMeshRecon, 1, wxEXPAND, 5);


  bSizer->Add(sbSizerMeshRecon, 0, wxEXPAND, 5);

  wxStaticBoxSizer* sbSizerTexRecon = new wxStaticBoxSizer(new wxStaticBox(this, wxID_ANY, "TexRecon"), wxVERTICAL);

  wxFlexGridSizer* fgSizerTexRecon = new wxFlexGridSizer(7, 2, 0, 0);
  fgSizerTexRecon->SetFlexibleDirection(wxBOTH);
  fgSizerTexRecon->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);


  fgSizerTexRecon->Add(new wxStaticText(sbSizerTexRecon->GetStaticBox(), wxID_ANY, "Data Term"), 0, wxALL, 5);

  wxArrayString choicesDataTerm;
  choicesDataTerm.Add("Area"); choicesDataTerm.Add("Gmi");
  choiceDataTerm = new wxChoice(sbSizerTexRecon->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxDefaultSize, choicesDataTerm);
  choiceDataTerm->SetSelection(dataTerm);
  fgSizerTexRecon->Add(choiceDataTerm, 0, wxALL, 5);

  fgSizerTexRecon->Add(new wxStaticText(sbSizerTexRecon->GetStaticBox(), wxID_ANY, "Outlier removal"), 0, wxALL, 5);

  wxArrayString choicesOutlierRemoval;
  choicesOutlierRemoval.Add("None"); choicesOutlierRemoval.Add("Gauss damping"); choicesOutlierRemoval.Add("Gauss clamping");
  choiceOutlierRemoval= new wxChoice(sbSizerTexRecon->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxDefaultSize, choicesOutlierRemoval);
  choiceOutlierRemoval->SetSelection(outlierRemoval);
  fgSizerTexRecon->Add(choiceOutlierRemoval, 0, wxALL, 5);

  fgSizerTexRecon->Add(new wxStaticText(sbSizerTexRecon->GetStaticBox(), wxID_ANY, "Geometric visibility test"), 0, wxALL, 5);

  ckBGeometricVisibilityTest = new wxCheckBox(sbSizerTexRecon->GetStaticBox(), wxID_ANY, wxEmptyString);
  ckBGeometricVisibilityTest->SetValue(geometricVisibilityTest);
  fgSizerTexRecon->Add(ckBGeometricVisibilityTest, 0, wxALL, 5);

  fgSizerTexRecon->Add(new wxStaticText(sbSizerTexRecon->GetStaticBox(), wxID_ANY, "Global seam leveling"), 0, wxALL, 5);

  ckBGlobalSeamLeveling = new wxCheckBox(sbSizerTexRecon->GetStaticBox(), wxID_ANY, wxEmptyString);
  ckBGlobalSeamLeveling->SetValue(globalSeamLeveling);
  fgSizerTexRecon->Add(ckBGlobalSeamLeveling, 0, wxALL, 5);

  fgSizerTexRecon->Add(new wxStaticText(sbSizerTexRecon->GetStaticBox(), wxID_ANY, "Local seam leveling"), 0, wxALL, 5);

  ckBLocalSeamLeveling = new wxCheckBox(sbSizerTexRecon->GetStaticBox(), wxID_ANY, wxEmptyString);
  ckBLocalSeamLeveling->SetValue(localSeamLeveling);
  fgSizerTexRecon->Add(ckBLocalSeamLeveling, 0, wxALL, 5);

  fgSizerTexRecon->Add(new wxStaticText(sbSizerTexRecon->GetStaticBox(), wxID_ANY, "Hole filling"), 0, wxALL, 5);

  ckBHoleFilling = new wxCheckBox(sbSizerTexRecon->GetStaticBox(), wxID_ANY, wxEmptyString);
  ckBHoleFilling->SetValue(holeFilling);
  fgSizerTexRecon->Add(ckBHoleFilling, 0, wxALL, 5);

  fgSizerTexRecon->Add(new wxStaticText(sbSizerTexRecon->GetStaticBox(), wxID_ANY, "Keep unseen faces"), 0, wxALL, 5);

  ckBKeepUnseenFaces = new wxCheckBox(sbSizerTexRecon->GetStaticBox(), wxID_ANY, wxEmptyString);
  ckBKeepUnseenFaces->SetValue(keepUnseenFaces);
  fgSizerTexRecon->Add(ckBKeepUnseenFaces, 0, wxALL, 5);


  sbSizerTexRecon->Add(fgSizerTexRecon, 1, wxEXPAND, 5);


  bSizer->Add(sbSizerTexRecon, 0, wxEXPAND, 5);



  wxBoxSizer* bSizerBts = new wxBoxSizer(wxHORIZONTAL);

  bSizerBts->Add(new wxButton(this, idBtDefaultConfig, "Default"), 0, wxALL, 5);

  bSizerBts->Add(new wxButton(this, wxID_OK, "OK"), 0, wxALL, 5);

  bSizerBts->Add(new wxButton(this, wxID_CANCEL, "Cancel"), 0, wxALL, 5);

  bSizer->Add(bSizerBts, 0, wxALIGN_RIGHT, 5);
  this->SetSizer(bSizer);
  this->Layout();

  this->Centre(wxBOTH);
}

ConfigurationDialog::~ConfigurationDialog()
{
}

void ConfigurationDialog::loadDefaultConfig()
{
  std::string line;
  std::ifstream myfile(vtksys::SystemTools::GetFilenamePath(wxStandardPaths::Get().GetExecutablePath().ToStdString())+"/projetomesh/parameters.txt");
  std::vector<wxString> tokens;
  int discartFirstLine;
  if (myfile.is_open())
  {
    while (getline(myfile, line, '\n'))
    {
      std::istringstream iss(line);
      std::string token;
      //Used to discard what is before the '='
      discartFirstLine = 1;
      while (std::getline(iss, token, '='))
      {
        if (!token.empty() && discartFirstLine == 0)
        {
          tokens.push_back(token.at(0));
        }
        discartFirstLine = 0;
      }
    }
    myfile.close();
  }
  else
  {
    wxLogError("Unable to open the default parameters file");
    return;
  }
  if (tokens.size() != 11)
  {
    wxLogError("The default parameters file is wrong");
    return;
  }
  forceCMPMVS = wxAtoi(tokens.at(0));
  boundingBoxType = wxAtoi(tokens.at(1));
  levelOfDetails = wxAtoi(tokens.at(2));
  forceLevelOfDetails = wxAtoi(tokens.at(3));
  dataTerm = wxAtoi(tokens.at(4));
  outlierRemoval = wxAtoi(tokens.at(5));
  geometricVisibilityTest = wxAtoi(tokens.at(6));
  globalSeamLeveling = wxAtoi(tokens.at(7));
  localSeamLeveling = wxAtoi(tokens.at(8));
  holeFilling = wxAtoi(tokens.at(9));
  keepUnseenFaces = wxAtoi(tokens.at(10));
}

wxString ConfigurationDialog::getParameters()
{
  wxString parameters;
  parameters << boundingBoxType << " " << levelOfDetails << " " << dataTerm << " " << outlierRemoval << " " << geometricVisibilityTest << " " << globalSeamLeveling << " " << localSeamLeveling
    << " " << holeFilling << " " <<keepUnseenFaces;
  return parameters;
}

wxString ConfigurationDialog::getMeshReconParameters()
{
  wxString parameters;
  parameters << boundingBoxType << " " << levelOfDetails;
  return parameters;
}

wxString ConfigurationDialog::getTexReconParameters()
{
  wxString parameters;
  parameters << dataTerm << " " << outlierRemoval << " " << geometricVisibilityTest << " " << globalSeamLeveling << " " << localSeamLeveling
    << " " << holeFilling << " " << keepUnseenFaces;
  return parameters;
}

bool ConfigurationDialog::getForceCMPMVS()
{
	return forceCMPMVS;
}

std::string ConfigurationDialog::getBoundingBox()
{
	return std::to_string(boundingBoxType);
}

std::string ConfigurationDialog::getLevelOfDetails()
{
	return std::to_string(levelOfDetails);
}

void ConfigurationDialog::setLevelOfDetails(int newLevelOfDetails)
{
	levelOfDetails = newLevelOfDetails;
}

bool ConfigurationDialog::getForceLevelOfDetails()
{
	return forceLevelOfDetails;
}

void ConfigurationDialog::OnOK(wxCommandEvent & WXUNUSED)
{
	forceCMPMVS = ckBForceCMPMVS->IsChecked();
	boundingBoxType = choiceBoundingBox->GetSelection();
	levelOfDetails = choiceLevelOfDetails->GetSelection();
	forceLevelOfDetails = ckBForceLevelOfDetails->IsChecked();
	dataTerm = choiceDataTerm->GetSelection();
	outlierRemoval = choiceOutlierRemoval->GetSelection();
	geometricVisibilityTest = ckBGeometricVisibilityTest->IsChecked();
	globalSeamLeveling = ckBGlobalSeamLeveling->IsChecked();
	localSeamLeveling = ckBLocalSeamLeveling->IsChecked();
	holeFilling = ckBHoleFilling->IsChecked();
	keepUnseenFaces = ckBKeepUnseenFaces->IsChecked();
	EndModal(wxID_OK);
}

void ConfigurationDialog::OnBtDefault(wxCommandEvent & WXUNUSED)
{
  loadDefaultConfig();
  ckBForceCMPMVS->SetValue(forceCMPMVS);
  choiceBoundingBox->SetSelection(boundingBoxType);
  choiceLevelOfDetails->SetSelection(levelOfDetails);
  ckBForceLevelOfDetails->SetValue(forceLevelOfDetails);
  choiceDataTerm->SetSelection(dataTerm);
  choiceOutlierRemoval->SetSelection(outlierRemoval);
  ckBGeometricVisibilityTest->SetValue(geometricVisibilityTest);
  ckBGlobalSeamLeveling->SetValue(globalSeamLeveling);
  ckBLocalSeamLeveling->SetValue(localSeamLeveling);
  ckBHoleFilling->SetValue(holeFilling);
  ckBKeepUnseenFaces->SetValue(keepUnseenFaces);
}
