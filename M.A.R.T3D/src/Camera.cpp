#include "Camera.h"
#include "Utils.h"

Camera::Camera()
{
	imageActor = NULL;
	imagePolygon = NULL;
	actorFrustum = NULL;
	height = -1;
	width = -1;
	focalX = -1;
	filePath = "";
	cameraMatrixInverted = NULL;
	listItemCamera = NULL;
	viewUp = NULL;
	viewUpDirection = -1;
	visible = false;
}

Camera::~Camera()
{
  for (int i = 0; i < cameraPoints.size(); i++)
  {
    delete cameraPoints.at(i);
  }
	cameraPoints.clear();
  if (viewUp != NULL)
  {
    delete viewUp;
  }
  if (gpsData != NULL)
  {
	  delete gpsData;
  }
}

void Camera::destruct(vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl * tree)
{
  if (imageActor != NULL)
  {
    renderer->RemoveActor(imageActor);
    imageActor = NULL;
  }
  if (actorFrustum != NULL)
  {
    renderer->RemoveActor(actorFrustum);
    actorFrustum = NULL;
  }
  if (listItemCamera != NULL)
  {
    tree->DeleteItem(listItemCamera);
    listItemCamera = NULL;
  }
}

void Camera::transform(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkTransform> T)
{
	if (actorFrustum != NULL)
	{
		actorFrustum->SetVisibility(true);
		//For some reason we can just transform whats was already rendered  at least once
		renderer->GetRenderWindow()->Render();
		vtkSmartPointer<vtkTransformFilter> transformFilter = vtkSmartPointer<vtkTransformFilter>::New();
		transformFilter->SetTransform(T);
		transformFilter->SetInputData(actorFrustum->GetMapper()->GetInputAsDataSet());
		transformFilter->Update();
		actorFrustum->GetMapper()->SetInputConnection(transformFilter->GetOutputPort());
		actorFrustum->SetVisibility(this->visible);

		vtkSmartPointer<vtkMatrix4x4> result = vtkSmartPointer<vtkMatrix4x4>::New();
		vtkMatrix4x4::Multiply4x4(T->GetMatrix(), cameraMatrixInverted, result);
		cameraMatrixInverted = result;
		calcCameraPoints();
		if (imageActor != NULL)
		{
			imageActor->SetUserMatrix(result);
		}
	}
}

void Camera::setListItemCamera(wxTreeListItem listItem)
{
	listItemCamera = listItem;
}

wxTreeListItem Camera::getListItemCamera()
{
	return listItemCamera;
}
void Camera::setVisibility(bool visible)
{
  if (actorFrustum != NULL)
  {
    actorFrustum->SetVisibility(visible);
  }
	if (imageActor != NULL)
	{
		imageActor->SetVisibility(visible);
	}
	this->visible = visible;
}
bool Camera::getVisibility()
{
	return visible;
}

int Camera::calcCameraPoints()
{
	if (width > 0 && height > 0 && cameraMatrixInverted != NULL)
	{
    for (int i = 0; i < cameraPoints.size(); i++)
    {
      delete cameraPoints.at(i);
    }
		cameraPoints.clear();
		if (viewUp != NULL)
		{
			delete viewUp;
			viewUp = NULL;
		}
		double* p1 = Utils::createDoubleVector(0, 0, 0); Utils::transformPoint(p1,cameraMatrixInverted);
		double dist = 0.5;
		double minX, minY, maxX, maxY;
		maxX = dist*(width / (2.0*focalX));
		minX = -maxX;
		maxY = dist*(height / (2.0*focalY));
		minY = -maxY;
		/*
		origin of the camera = p1
		p2--------p3
		|		       |
		|  pCenter |<--- Looking from p1 to pCenter
		|          |
		p5--------p4
		*/
		double* p2 = Utils::createDoubleVector(minX, minY, dist); Utils::transformPoint(p2, cameraMatrixInverted);
		double* p3 = Utils::createDoubleVector(maxX, minY, dist); Utils::transformPoint(p3, cameraMatrixInverted);
		double* p4 = Utils::createDoubleVector(maxX, maxY, dist); Utils::transformPoint(p4, cameraMatrixInverted);
		double* p5 = Utils::createDoubleVector(minX, maxY, dist); Utils::transformPoint(p5, cameraMatrixInverted);
		//we multiply for .95 because if we use 100% when we zoom in we pass the image
		double* pCenter = Utils::createDoubleVector(0, 0, dist*.95); Utils::transformPoint(pCenter, cameraMatrixInverted);
    double* pUP = Utils::createDoubleVector(0, maxY, dist*.95); Utils::transformPoint(pUP, cameraMatrixInverted);
		viewUpDirection = 0;
		cameraPoints.push_back(p1);
		cameraPoints.push_back(p2);
		cameraPoints.push_back(p3);
		cameraPoints.push_back(p4);
		cameraPoints.push_back(p5);
		cameraPoints.push_back(pCenter);
		viewUp = new double[3];
    vtkMath::Subtract(pCenter, pUP, viewUp);
		vtkMath::Normalize(viewUp);
		delete pUP;

		imagePolygon = vtkSmartPointer<vtkPolygon>::New();
		imagePolygon->GetPoints()->InsertNextPoint(p2);
		imagePolygon->GetPoints()->InsertNextPoint(p3);
		imagePolygon->GetPoints()->InsertNextPoint(p4);
		imagePolygon->GetPoints()->InsertNextPoint(p5);

		return 1;
	}
	return 0;
}

int Camera::changeViewUp()
{
	if (cameraMatrixInverted != NULL)
	{
		double dist = 0.5;
		double* pCenter = Utils::createDoubleVector(0, 0, dist*.95); Utils::transformPoint(pCenter, cameraMatrixInverted);
		
		double* pViewUp = NULL;
		if (viewUpDirection == -1 || viewUpDirection == 3)
		{
      double maxY = dist*(height / (2.0*focalY));
			pViewUp = Utils::createDoubleVector(0, maxY, dist*.95); Utils::transformPoint(pViewUp, cameraMatrixInverted);
			viewUpDirection = 0;
		}
		else if(viewUpDirection == 0)
		{
      double minX = -dist*(width / (2.0*focalX));
			pViewUp = Utils::createDoubleVector(minX, 0, dist*.95); Utils::transformPoint(pViewUp, cameraMatrixInverted);
			viewUpDirection = 1;
		}
		else if (viewUpDirection == 1)
		{
      double minY = -dist*(height / (2.0*focalY));
			pViewUp = Utils::createDoubleVector(0, minY, dist*.95); Utils::transformPoint(pViewUp, cameraMatrixInverted);
			viewUpDirection = 2;
		}
		else if (viewUpDirection == 2)
		{
      double maxX = dist*(width / (2.0*focalX));
			pViewUp = Utils::createDoubleVector(maxX, 0, dist*.95); Utils::transformPoint(pViewUp, cameraMatrixInverted);
			viewUpDirection = 3;
		}
		if (pViewUp == NULL)
		{
      delete pCenter;
			return 0;
		}
    vtkMath::Subtract(pCenter, pViewUp, viewUp);
		vtkMath::Normalize(viewUp);
    delete pCenter;
		delete pViewUp;
		return 1;
	}
	return 0;
}

void Camera::updateGPSData()
{
	// Read the JPEG file into a buffer
	FILE *fp = fopen(filePath.c_str(), "rb");
	if (!fp) 
	{
		return;
	}
	fseek(fp, 0, SEEK_END);
	unsigned long fsize = ftell(fp);
	rewind(fp);
	unsigned char *buf = new unsigned char[fsize];
	if (fread(buf, 1, fsize, fp) != fsize) 
	{
		delete[] buf;
		return;
	}
	fclose(fp);
	// Parse EXIF
	easyexif::EXIFInfo result;
	int code = result.parseFrom(buf, fsize);
	delete[] buf;
	if (code) 
	{
		return;
	}

	if (gpsData == NULL)
	{
		gpsData = new GPSData();
	}
	
	gpsData->setLatitude(result.GeoLocation.Latitude);
	gpsData->setLongitude(result.GeoLocation.Longitude);
	gpsData->setAltitude(result.GeoLocation.Altitude);
	

}

double Camera::getDistanceBetweenCameraCenters(Camera * c)
{
    return sqrt(vtkMath::Distance2BetweenPoints(this->cameraPoints.at(0), c->cameraPoints.at(0)));
}

void Camera::createActorFrustrum(vtkSmartPointer<vtkRenderer> renderer)
{
	if (actorFrustum != NULL)
	{
		renderer->RemoveActor(actorFrustum);
	}
	if (cameraPoints.size() == 0)
	{
		calcCameraPoints();
	}
	actorFrustum = Draw::createFrustum(renderer, cameraPoints);
}

void Camera::createImageActor(vtkSmartPointer<vtkRenderer> renderer)
{
	if (imageActor != NULL)
	{
		//Assuming that the camera never change its source
		return;
		//renderer->RemoveActor(imageActor);
	}
	imageActor = Draw::createImageActor(renderer, Utils::loadImage(filePath));
	if (imageActor != NULL)
	{
		double dist = 0.5;
		double minX, minY, maxX, maxY;
		maxX = dist * (width / (2.0*focalX));
		minX = -maxX;
		maxY = dist * (height / (2.0*focalY));
		minY = -maxY;
		imageActor->SetUserMatrix(cameraMatrixInverted);
		imageActor->SetScale(2 * (maxX / width), 2 * (maxY / height), 1);
		imageActor->RotateX(180);
		imageActor->SetPosition(minX, maxY, dist);
	}
}
