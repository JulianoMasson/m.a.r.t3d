#pragma once
#ifndef __MESH__H__
#define __MESH__H__

#include <vtkOBJImporter.h>
#include <wx\treebase.h>
#include <wx\vector.h>
#include <vtkActor.h>
#include <vtkTexture.h>
#include <vtkProperty.h>
#include "Camera.h"
#include <vector>
#include "Utils.h"
#include "Volume.h"
#include <vtksys\SystemTools.hxx>
#include <vtkAppendPolyData.h>
#include <vtkCleanPolyData.h>
#include <vtkAbstractPicker.h>
#include <vtkPointData.h>
#include <vtkTransformFilter.h>
#include "Calibration.h"
#include <vtkCellLocator.h>
#include <vtkFloatArray.h>

class Mesh {
private:
	wxTreeListItem listItemMesh;
  wxTreeListItem listItemMeshVolume;
	wxTreeListItem listItemMeshCameras;
	wxTreeListItem listItemMeshTexture;
  wxTreeListItem listItemMeshVolumes;
	bool visible;
	bool textureVisibility;
  Calibration* calibration = NULL;
  vtkSmartPointer<vtkPolyData> meshPolyData = NULL;
  vtkSmartPointer<vtkCellLocator> meshCellLocator = NULL;
  double* lastPosition = NULL;

public:
  std::vector<vtkSmartPointer<vtkActor>> actors;
  vtkSmartPointer<vtkActor> volumeActor = NULL;
  std::vector<std::string> textureNames;
  std::vector<vtkSmartPointer<vtkTexture>> textures;
  std::vector<Camera*> cameras;
  vtkSmartPointer<vtkActor> camerasPath = NULL;
  /*
  Use the addVolume to add some volumes to the mesh
  */
  std::vector<Volume*> volumes;
  /*
  Just to track how many volumes already passed through this mesh
  */
  int qtdVolumes;
  std::string meshName;


	Mesh();
	Mesh(bool meshVisibility, bool textureVisibility);
	~Mesh();

  /*
  Should be called before the delete, it remove the actors from the scene and the listItem from the tree
  */
  void destruct(vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree);

  void destructCameras(vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree);
  void destructTextures(vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree);
  void destructVolumes(vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree);
  void destructVolume(vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree);

  void deleteCamera(int cameraIndex, vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree);
  void deleteVolume(int volumeIndex, vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree);

  /*
  Create a pickList from the actors, and set the picker to pick from list
  */
  void createPickList(vtkSmartPointer<vtkAbstractPicker> picker);

	/*
	Set the wxTreeItem that represents this mesh on the tree
	*/
	void setListItemMesh(wxTreeListItem listItem);
	/*
	Get the wxTreeItem that represents this mesh on the tree
	*/
	wxTreeListItem getListItemMesh();
	/*
	Set the wxTreeItem that represents the item "cameras" of this mesh on the tree
	*/
	void setListItemMeshCameras(wxTreeListItem listItem);
	/*
	Get the wxTreeItem that represents the item "cameras" of this mesh on the tree
	*/
	wxTreeListItem getListItemMeshCameras();
	/*
	Set the wxTreeItem that represents the texture of this mesh on the tree
	*/
	void setListItemMeshTexture(wxTreeListItem listItem);
	/*
	Get the wxTreeItem that represents the texture of this mesh on the tree
	*/
	wxTreeListItem getListItemMeshTexture();
  /*
  Set the wxTreeItem that represents the volumes of this mesh on the tree
  */
  void setListItemMeshVolumes(wxTreeListItem listItem);
  /*
  Get the wxTreeItem that represents the volumes of this mesh on the tree
  */
  wxTreeListItem getListItemMeshVolumes();

  /*
  Set the visibility of all volumes
  */
  void setVolumesVisibility(bool visible);
  /*
  True if some volume is visible
  */
  bool getVolumesVisibility();
  /*
  Add a volume
  */
  void addVolume(Volume* vol);

	void setMeshVisibility(bool visible);
	bool getMeshVisibility();

  void setVolumeVisibility(bool visible);
  bool getVolumeVisibility();
  /*
  Set the wxTreeItem that represents the volume actor of this mesh on the tree
  */
  void setListItemMeshVolume(wxTreeListItem listItem);
  /*
  Get the wxTreeItem that represents the volume actor of this mesh on the tree
  */
  wxTreeListItem getListItemMeshVolume();


	void setCamerasVisibility(bool visible);
	/*
	True if some camera is visible
	*/
	bool getCamerasVisibility();
	bool hasImages();
	/*
	Enable/disable the textures
	*/
	void setTextureVisibility(bool visible);
	bool getTextureVisibility();
	/*
	Used to check if the cameras have the correct filePath.
	Return the number of missing images.
	*/
	int checkCamerasFilePath();
	/*
	Used to check if the new path has the cameras.
	Return the number of missing images.
	*/
	int checkCamerasFilePath(std::string newPath);

  /*
  Create a line connecting the cameras
  */
  void createCamerasPath(vtkSmartPointer<vtkRenderer> renderer);

  /*
  Enable/disable the camera path
  */
  void setCameraPathVisibility(bool visible);
  bool getCameraPathVisibility();

  /*
  Used to sort the cameras using the filename.
  */
  void sortCameras();

  /*
  Get the PolyData of the mesh
  */
  vtkSmartPointer<vtkPolyData> getPolyData();

  /*
  Get the CellLocator of the mesh
  */
  vtkSmartPointer<vtkCellLocator> getCellLocator();

  /*
  Set if the actors can be picked 
  */
  void setPickable(bool pickable);

  /*
  Tranform the mesh using T
  */
  void transform(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkTransform> T);

  /*
  Set the calibration
  */
  void setCalibration(Calibration* cal);

  /*
  Get the calibration
  */
  Calibration* getCalibration();
  
  /*
  Change the points of a point cloud
  */
  void updatePoints(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkPoints> points, vtkSmartPointer<vtkUnsignedCharArray> colors, vtkSmartPointer<vtkFloatArray> normals);
  void updatePoints(vtkSmartPointer<vtkPoints> points, vtkSmartPointer<vtkUnsignedCharArray> colors, vtkSmartPointer<vtkFloatArray> normals);
  void updatePoints(vtkDataObject* data);

  /*
  Change the cells of a mesh
  */
  void updateCells(vtkDataObject* data);

  /*
  Change the cells of an actor of this mesh
  */
  void updateCells(vtkDataObject* data, unsigned int actorIndex);

  /*
  Used to update the actors after changes in VR
  */
  void updateActors(std::vector<vtkSmartPointer<vtkActor> > newActors);

  /*
  Calibrate the mesh using GPS data
  */
  void calibrateUsingGPSData(vtkSmartPointer<vtkRenderer> renderer);

};

#endif
