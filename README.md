# README #

### How do I get set up? ###

Use Visual Studio 2015 or newer and CMake >= 3.14.

Libraries  
1 - wxWidgest >= 3.1.2 - You need to build it using the project wx_vcXX.sln (XX is your visual studio version) wich is inside the "wxWidgets-3.1.2\build\msw" folder.  
2 - VTK >= 8.2.0 - You need to build it adding the VR support, for this you need SDL2 >= 2.0.9 and OpenVR >= 1.5.17.  
3 - PoissonRecon - Included in the repository, used to create a poisson reconstruction for the volume tool.  
4 - OpenVR >= 1.5.17 - Should be the same used to build VTK.  
5 - NSIS >= 3.04 - Used to create the installer.  

### Using the M.A.R.T3D ###

If you do not want to compile the project you just need to execute the "SetupM.A.R.T.3D.msi" file. After this just execute the M.A.R.T 3D.exe.  

### Creating the installer ###

After compiling the solution with CMake build the project PACKAGE.  