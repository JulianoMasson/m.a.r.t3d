#undef FAST_COMPILE
#undef ARRAY_DEBUG
#define BRUNO_LEVY_FIX
#define FOR_RELEASE

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#if defined( _WIN32 ) || defined( _WIN64 )
#include <Windows.h>
#include <Psapi.h>
#endif // _WIN32 || _WIN64
#include "MyTime.h"
#include "MarchingCubes.h"
#include "Octree.h"
#include "SparseMatrix.h"
#include "CmdLineParser.h"
#include "PPolynomial.h"
#include "Ply.h"
#include "MemoryUsage.h"
#ifdef _OPENMP
#include "omp.h"
#endif // _OPENMP
#include "MultiGridOctreeData.h"

#define DEFAULT_FULL_DEPTH 5

#define XSTR(x) STR(x)
#define STR(x) #x
#if DEFAULT_FULL_DEPTH
#pragma message ( "[WARNING] Setting default full depth to " XSTR(DEFAULT_FULL_DEPTH) )
#endif // DEFAULT_FULL_DEPTH

#include <stdarg.h>

#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkPointData.h>
#include <vtkDoubleArray.h>
#include <vtkTriangle.h>

template< class Real >
struct ColorInfo
{
  static Point3D< Real > ReadASCII(FILE* fp)
  {
    Point3D< unsigned char > c;
    if (fscanf(fp, " %c %c %c ", &c[0], &c[1], &c[2]) != 3) fprintf(stderr, "[ERROR] Failed to read color\n"), exit(0);
    return Point3D< Real >((Real)c[0], (Real)c[1], (Real)c[2]);
  };
  static bool ValidPlyProperties(const bool* props) { return (props[0] || props[3]) && (props[1] || props[4]) && (props[2] || props[5]); }
  const static PlyPropertyPoisson PlyProperties[];
};

template<>
const PlyPropertyPoisson ColorInfo< float >::PlyProperties[] =
{
  { "r"     , PLY_UCHAR , PLY_FLOAT , int(offsetof(Point3D< float > , coords[0])) , 0 , 0 , 0 , 0 } ,
  { "g"     , PLY_UCHAR , PLY_FLOAT , int(offsetof(Point3D< float > , coords[1])) , 0 , 0 , 0 , 0 } ,
  { "b"     , PLY_UCHAR , PLY_FLOAT , int(offsetof(Point3D< float > , coords[2])) , 0 , 0 , 0 , 0 } ,
  { "red"   , PLY_UCHAR , PLY_FLOAT , int(offsetof(Point3D< float > , coords[0])) , 0 , 0 , 0 , 0 } ,
  { "green" , PLY_UCHAR , PLY_FLOAT , int(offsetof(Point3D< float > , coords[1])) , 0 , 0 , 0 , 0 } ,
  { "blue"  , PLY_UCHAR , PLY_FLOAT , int(offsetof(Point3D< float > , coords[2])) , 0 , 0 , 0 , 0 }
};
template<>
const PlyPropertyPoisson ColorInfo< double >::PlyProperties[] =
{
  { "r"     , PLY_UCHAR , PLY_DOUBLE , int(offsetof(Point3D< double > , coords[0])) , 0 , 0 , 0 , 0 } ,
  { "g"     , PLY_UCHAR , PLY_DOUBLE , int(offsetof(Point3D< double > , coords[1])) , 0 , 0 , 0 , 0 } ,
  { "b"     , PLY_UCHAR , PLY_DOUBLE , int(offsetof(Point3D< double > , coords[2])) , 0 , 0 , 0 , 0 } ,
  { "red"   , PLY_UCHAR , PLY_DOUBLE , int(offsetof(Point3D< double > , coords[0])) , 0 , 0 , 0 , 0 } ,
  { "green" , PLY_UCHAR , PLY_DOUBLE , int(offsetof(Point3D< double > , coords[1])) , 0 , 0 , 0 , 0 } ,
  { "blue"  , PLY_UCHAR , PLY_DOUBLE , int(offsetof(Point3D< double > , coords[2])) , 0 , 0 , 0 , 0 }
};

template< class Real >
struct OctreeProfiler
{
  Octree< Real >& tree;
  double t;

  OctreeProfiler(Octree< Real >& t) : tree(t) { ; }
  void start(void) { t = Time(), tree.resetLocalMemoryUsage(); }
  void print(const char* header) const
  {
    tree.memoryUsage();
#if defined( _WIN32 ) || defined( _WIN64 )
    if (header) printf("%s %9.1f (s), %9.1f (MB) / %9.1f (MB) / %9.1f (MB)\n", header, Time() - t, tree.localMemoryUsage(), tree.maxMemoryUsage(), PeakMemoryUsageMB());
    else         printf("%9.1f (s), %9.1f (MB) / %9.1f (MB) / %9.1f (MB)\n", Time() - t, tree.localMemoryUsage(), tree.maxMemoryUsage(), PeakMemoryUsageMB());
#else // !_WIN32 && !_WIN64
    if (header) printf("%s %9.1f (s), %9.1f (MB) / %9.1f (MB)\n", header, Time() - t, tree.localMemoryUsage(), tree.maxMemoryUsage());
    else         printf("%9.1f (s), %9.1f (MB) / %9.1f (MB)\n", Time() - t, tree.localMemoryUsage(), tree.maxMemoryUsage());
#endif // _WIN32 || _WIN64
  }
  void dumpOutput(const char* header) const
  {
  }
  void dumpOutput2(std::vector< char* >& comments, const char* header) const
  {
  }
};
