// PoissonRecon.cpp : Defines the exported functions for the DLL application.
//
#include "../include/PoissonRecon.h"
#include "PoissonReconAux.h"


cmdLineString
In("in"),
Out("out"),
VoxelGrid("voxel"),
XForm("xForm");

cmdLineReadable
#if defined( _WIN32 ) || defined( _WIN64 )
Performance("performance"),
#endif // _WIN32 || _WIN64
ShowResidual("showResidual"),
NoComments("noComments"),
PolygonMesh("polygonMesh"),
Confidence("confidence"),
NormalWeights("nWeights"),
NonManifold("nonManifold"),
ASCII("ascii"),
Density("density"),
LinearFit("linearFit"),
PrimalVoxel("primalVoxel"),
#ifndef FAST_COMPILE
Double("double"),
#endif // !FAST_COMPILE
Verbose("verbose");

cmdLineInt
#ifndef FAST_COMPILE
Degree("degree", 2),
#endif // !FAST_COMPILE
Depth("depth", 8),
CGDepth("cgDepth", 0),
KernelDepth("kernelDepth"),
AdaptiveExponent("adaptiveExp", 1),
Iters("iters", 8),
VoxelDepth("voxelDepth", -1),
FullDepth("fullDepth", DEFAULT_FULL_DEPTH),
#ifndef FAST_COMPILE
BType("bType", BOUNDARY_NEUMANN + 1),
#endif // !FAST_COMPILE
MaxSolveDepth("maxSolveDepth"),
Threads("threads", omp_get_num_procs());

cmdLineFloat
Color("color", 16.f),
SamplesPerNode("samplesPerNode", 1.5f),
Scale("scale", 1.1f),
CGSolverAccuracy("cgAccuracy", float(1e-3)),
LowResIterMultiplier("iterMultiplier", 1.f),
PointWeight("pointWeight", 4.f);


cmdLineReadable* params[] =
{
#ifndef FAST_COMPILE
  &Degree , &Double , &BType ,
#endif // !FAST_COMPILE
  &In , &Depth , &Out , &XForm ,
  &Scale , &Verbose , &CGSolverAccuracy , &NoComments , &LowResIterMultiplier ,
  &KernelDepth , &SamplesPerNode , &Confidence , &NormalWeights , &NonManifold , &PolygonMesh , &ASCII , &ShowResidual , &VoxelDepth ,
  &PointWeight , &VoxelGrid , &Threads , &MaxSolveDepth ,
  &AdaptiveExponent ,
  &Density ,
  &FullDepth ,
  &CGDepth , &Iters ,
  &Color ,
  &LinearFit ,
  &PrimalVoxel ,
#if defined( _WIN32 ) || defined( _WIN64 )
  &Performance ,
#endif // _WIN32 || _WIN64
};

// This is the constructor of a class that has been exported.
// see PoissonRecon.h for the class definition
PoissonRecon::PoissonRecon()
{
    return;
}


double Weight(double v, double start, double end)
{
  v = (v - start) / (end - start);
  if (v < 0) return 1.;
  else if (v > 1) return 0.;
  else
  {
    // P(x) = a x^3 + b x^2 + c x + d
    //		P (0) = 1 , P (1) = 0 , P'(0) = 0 , P'(1) = 0
    // =>	d = 1 , a + b + c + d = 0 , c = 0 , 3a + 2b + c = 0
    // =>	c = 0 , d = 1 , a + b = -1 , 3a + 2b = 0
    // =>	a = 2 , b = -3 , c = 0 , d = 1
    // =>	P(x) = 2 x^3 - 3 x^2 + 1
    return 2. * v * v * v - 3. * v * v + 1.;
  }
}


#if defined( _WIN32 ) || defined( _WIN64 )
double PeakMemoryUsageMB(void)
{
  HANDLE h = GetCurrentProcess();
  PROCESS_MEMORY_COUNTERS pmc;
  return GetProcessMemoryInfo(h, &pmc, sizeof(pmc)) ? ((double)pmc.PeakWorkingSetSize) / (1 << 20) : 0;
}
#endif // _WIN32 || _WIN64




template< class Real >
XForm4x4< Real > GetPointXForm(OrientedPointStream< Real >& stream, Real scaleFactor)
{
  Point3D< Real > min, max;
  stream.boundingBox(min, max);
  Point3D< Real > center = (max + min) / 2;
  Real scale = std::max< Real >(max[0] - min[0], std::max< Real >(max[1] - min[1], max[2] - min[2]));
  scale *= scaleFactor;
  for (int i = 0; i < 3; i++) center[i] -= scale / 2;
  XForm4x4< Real > tXForm = XForm4x4< Real >::Identity(), sXForm = XForm4x4< Real >::Identity();
  for (int i = 0; i < 3; i++) sXForm(i, i) = (Real)(1. / scale), tXForm(3, i) = -center[i];
  return sXForm * tXForm;
}

template< class Real, int Degree, BoundaryType BType, class Vertex >
vtkSmartPointer<vtkPolyData> _Execute(vtkSmartPointer<vtkPolyData> polyData)
{
  typedef typename Octree< Real >::template DensityEstimator< WEIGHT_DEGREE > DensityEstimator;
  typedef typename Octree< Real >::template InterpolationInfo< false > InterpolationInfo;
  typedef OrientedPointStream< Real > PointStream;
  typedef OrientedPointStreamWithData< Real, Point3D< Real > > PointStreamWithData;
  typedef TransformedOrientedPointStream< Real > XPointStream;
  typedef TransformedOrientedPointStreamWithData< Real, Point3D< Real > > XPointStreamWithData;
  Reset< Real >();
  int paramNum = sizeof(params) / sizeof(cmdLineReadable*);
  std::vector< char* > comments;

  XForm4x4< Real > xForm, iXForm;
  if (XForm.set)
  {
    FILE* fp = fopen(XForm.value, "r");
    if (!fp)
    {
      fprintf(stderr, "[WARNING] Could not read x-form from: %s\n", XForm.value);
      xForm = XForm4x4< Real >::Identity();
    }
    else
    {
      for (int i = 0; i < 4; i++) for (int j = 0; j < 4; j++)
      {
        float f;
        if (fscanf(fp, " %f ", &f) != 1) fprintf(stderr, "[ERROR] Execute: Failed to read xform\n"), exit(0);
        xForm(i, j) = (Real)f;
      }
      fclose(fp);
    }
  }
  else xForm = XForm4x4< Real >::Identity();

  char str[1024];
  for (int i = 0; i < paramNum; i++)
    if (params[i]->set)
    {
      params[i]->writeValue(str);
    }

  double startTime = Time();
  Real isoValue = 0;
  //Solve the problem of consecutive runs
  OctNode< TreeNodeData >::SetAllocator(0);
  Octree< Real > tree;
  OctreeProfiler< Real > profiler(tree);
  tree.threads = Threads.value;

  if (!MaxSolveDepth.set) MaxSolveDepth.value = Depth.value;

  OctNode< TreeNodeData >::SetAllocator(MEMORY_ALLOCATOR_BLOCK_SIZE);

  int kernelDepth = KernelDepth.set ? KernelDepth.value : Depth.value - 2;
  if (kernelDepth > Depth.value)
  {
    fprintf(stderr, "[WARNING] %s can't be greater than %s: %d <= %d\n", KernelDepth.name, Depth.name, KernelDepth.value, Depth.value);
    kernelDepth = Depth.value;
  }

  int pointCount;

  Real pointWeightSum;
  std::vector< typename Octree< Real >::PointSample >* samples = new std::vector< typename Octree< Real >::PointSample >();
  std::vector< ProjectiveData< Point3D< Real >, Real > >* sampleData = NULL;
  DensityEstimator* density = NULL;
  SparseNodeData< Point3D< Real >, NORMAL_DEGREE >* normalInfo = NULL;
  Real targetValue = (Real)0.5;
  // Read in the samples (and color data)
  {
    profiler.start();
    PointStream* pointStream;
    std::pair< OrientedPoint3D< Real >, Point3D< Real > >* pointsColor = NULL;
    OrientedPoint3D<Real>* points = NULL;
    //----------------------------------------------------------------------------------------------------------------------------
    if (Color.set) //has color
    {
      sampleData = new std::vector< ProjectiveData< Point3D< Real >, Real > >();
      pointsColor = (std::pair< OrientedPoint3D< Real >, Point3D< Real > >*) malloc(sizeof(std::pair< OrientedPoint3D< Real >, Point3D< Real > >)*polyData->GetNumberOfPoints());
      for (int i = 0; i < polyData->GetNumberOfPoints(); i++)
      {
        OrientedPoint3D<Real>* point = new OrientedPoint3D<Real>();
        Point3D<Real>* color = new Point3D<Real>();
        point->n.coords[0] = polyData->GetPointData()->GetNormals()->GetTuple(i)[0];
        point->n.coords[1] = polyData->GetPointData()->GetNormals()->GetTuple(i)[1];
        point->n.coords[2] = polyData->GetPointData()->GetNormals()->GetTuple(i)[2];
        point->p.coords[0] = polyData->GetPoint(i)[0];
        point->p.coords[1] = polyData->GetPoint(i)[1];
        point->p.coords[2] = polyData->GetPoint(i)[2];
        color->coords[0] = polyData->GetPointData()->GetScalars()->GetTuple(i)[0];
        color->coords[1] = polyData->GetPointData()->GetScalars()->GetTuple(i)[1];
        color->coords[2] = polyData->GetPointData()->GetScalars()->GetTuple(i)[2];
        std::pair< OrientedPoint3D< Real >, Point3D< Real > > pointColor = std::make_pair(*point, *color);
        pointsColor[i] = pointColor;
      }
      pointStream = new MemoryOrientedPointStreamWithData<Real, Point3D< Real >  >(polyData->GetNumberOfPoints(), pointsColor);
    }
    else
    {
      points = (OrientedPoint3D<Real>*) malloc(sizeof(OrientedPoint3D<Real>)*polyData->GetNumberOfPoints());
      for (int i = 0; i < polyData->GetNumberOfPoints(); i++)
      {
        OrientedPoint3D<Real>* point = new OrientedPoint3D<Real>();
        //memcpy(point->n.coords, polyData->GetPointData()->GetNormals()->GetTuple(i), sizeof(double) * 3);
        //memcpy(point->p.coords, polyData->GetPoint(i), sizeof(double) * 3);
        point->n.coords[0] = polyData->GetPointData()->GetNormals()->GetTuple(i)[0];
        point->n.coords[1] = polyData->GetPointData()->GetNormals()->GetTuple(i)[1];
        point->n.coords[2] = polyData->GetPointData()->GetNormals()->GetTuple(i)[2];
        point->p.coords[0] = polyData->GetPoint(i)[0];
        point->p.coords[1] = polyData->GetPoint(i)[1];
        point->p.coords[2] = polyData->GetPoint(i)[2];
        points[i] = *point;
      }
      pointStream = new MemoryOrientedPointStream<Real>(polyData->GetNumberOfPoints(), points);
    }
    //----------------------------------------------------------------------------------------------------------------------------
    XPointStream _pointStream(xForm, *pointStream);
    xForm = GetPointXForm(_pointStream, (Real)Scale.value) * xForm;
    if (sampleData)
    {
      XPointStreamWithData _pointStream(xForm, (PointStreamWithData&)*pointStream);
      pointCount = tree.template init< Point3D< Real > >(_pointStream, Depth.value, Confidence.set, *samples, sampleData);
    }
    else
    {
      XPointStream _pointStream(xForm, *pointStream);
      pointCount = tree.template init< Point3D< Real > >(_pointStream, Depth.value, Confidence.set, *samples, sampleData);
    }
    iXForm = xForm.inverse();
    delete pointStream;
    if (Color.set)
    {
      delete pointsColor;
    }
    else
    {
      delete points;
    }
#pragma omp parallel for num_threads( Threads.value )
    for (int i = 0; i < (int)samples->size(); i++) (*samples)[i].sample.data.n *= (Real)-1;
  }
  DenseNodeData< Real, Degree > solution;

  {
    DenseNodeData< Real, Degree > constraints;
    InterpolationInfo* iInfo = NULL;
    int solveDepth = MaxSolveDepth.value;

    tree.resetNodeIndices();

    // Get the kernel density estimator [If discarding, compute anew. Otherwise, compute once.]
    {
      profiler.start();
      density = tree.template setDensityEstimator< WEIGHT_DEGREE >(*samples, kernelDepth, SamplesPerNode.value);
      profiler.dumpOutput2(comments, "#   Got kernel density:");
    }

    // Transform the Hermite samples into a vector field [If discarding, compute anew. Otherwise, compute once.]
    {
      profiler.start();
      normalInfo = new SparseNodeData< Point3D< Real >, NORMAL_DEGREE >();
      *normalInfo = tree.template setNormalField< NORMAL_DEGREE >(*samples, *density, pointWeightSum, BType == BOUNDARY_NEUMANN);
      profiler.dumpOutput2(comments, "#     Got normal field:");
    }

    if (!Density.set) delete density, density = NULL;

    // Trim the tree and prepare for multigrid
    {
      profiler.start();
      std::vector< int > indexMap;

      constexpr int MAX_DEGREE = NORMAL_DEGREE > Degree ? NORMAL_DEGREE : Degree;
      tree.template inalizeForBroodedMultigrid< MAX_DEGREE, Degree, BType >(FullDepth.value, typename Octree< Real >::template HasNormalDataFunctor< NORMAL_DEGREE >(*normalInfo), &indexMap);

      if (normalInfo) normalInfo->remapIndices(indexMap);
      if (density) density->remapIndices(indexMap);
      profiler.dumpOutput2(comments, "#       Finalized tree:");
    }

    // Add the FEM constraints
    {
      profiler.start();
      constraints = tree.template initDenseNodeData< Degree >();
      tree.template addFEMConstraints< Degree, BType, NORMAL_DEGREE, BType >(FEMVFConstraintFunctor< NORMAL_DEGREE, BType, Degree, BType >(1., 0.), *normalInfo, constraints, solveDepth);
      profiler.dumpOutput2(comments, "#  Set FEM constraints:");
    }

    // Free up the normal info [If we don't need it for subseequent iterations.]
    delete normalInfo, normalInfo = NULL;

    // Add the interpolation constraints
    if (PointWeight.value > 0)
    {
      profiler.start();
      iInfo = new InterpolationInfo(tree, *samples, targetValue, AdaptiveExponent.value, (Real)PointWeight.value * pointWeightSum, (Real)0);
      tree.template addInterpolationConstraints< Degree, BType >(*iInfo, constraints, solveDepth);
      profiler.dumpOutput2(comments, "#Set point constraints:");
    }


    // Solve the linear system
    {
      profiler.start();
      typename Octree< Real >::SolverInfo solverInfo;
      solverInfo.cgDepth = CGDepth.value, solverInfo.iters = Iters.value, solverInfo.cgAccuracy = CGSolverAccuracy.value, solverInfo.verbose = Verbose.set, solverInfo.showResidual = ShowResidual.set, solverInfo.lowResIterMultiplier = std::max< double >(1., LowResIterMultiplier.value);
      solution = tree.template solveSystem< Degree, BType >(FEMSystemFunctor< Degree, BType >(0, 1., 0), iInfo, constraints, solveDepth, solverInfo);
      if (iInfo) delete iInfo, iInfo = NULL;
    }
  }

  CoredVectorMeshData< Vertex > mesh;

  {
    profiler.start();
    double valueSum = 0, weightSum = 0;
    typename Octree< Real >::template MultiThreadedEvaluator< Degree, BType > evaluator(&tree, solution, Threads.value);
#pragma omp parallel for num_threads( Threads.value ) reduction( + : valueSum , weightSum )
    for (int j = 0; j < samples->size(); j++)
    {
      ProjectiveData< OrientedPoint3D< Real >, Real >& sample = (*samples)[j].sample;
      Real w = sample.weight;
      if (w > 0) weightSum += w, valueSum += evaluator.value(sample.data.p / sample.weight, omp_get_thread_num(), (*samples)[j].node) * w;
    }
    isoValue = (Real)(valueSum / weightSum);
    if (!(Color.set && Color.value > 0) && samples) delete samples, samples = NULL;
    profiler.dumpOutput("Got average:");
  }

  if (VoxelGrid.set)
  {
    profiler.start();
    FILE* fp = fopen(VoxelGrid.value, "wb");
    if (!fp) fprintf(stderr, "Failed to open voxel file for writing: %s\n", VoxelGrid.value);
    else
    {
      int res = 0;
      PointerPoisson(Real) values = tree.template voxelEvaluate< Real, Degree, BType >(solution, res, isoValue, VoxelDepth.value, PrimalVoxel.set);
      fwrite(&res, sizeof(int), 1, fp);
      if (sizeof(Real) == sizeof(float)) fwrite(values, sizeof(float), res*res*res, fp);
      else
      {
        float *fValues = new float[res*res*res];
        for (int i = 0; i < res*res*res; i++) fValues[i] = float(values[i]);
        fwrite(fValues, sizeof(float), res*res*res, fp);
        delete[] fValues;
      }
      fclose(fp);
      DeletePointer(values);
    }
    profiler.dumpOutput("Got voxel grid:");
  }


  profiler.start();
  SparseNodeData< ProjectiveData< Point3D< Real >, Real >, DATA_DEGREE >* colorData = NULL;
  if (sampleData)
  {
    colorData = new SparseNodeData< ProjectiveData< Point3D< Real >, Real >, DATA_DEGREE >();
    *colorData = tree.template setDataField< DATA_DEGREE, false >(*samples, *sampleData, (DensityEstimator*)NULL);
    delete sampleData, sampleData = NULL;
    for (const OctNode< TreeNodeData >* n = tree.tree().nextNode(); n; n = tree.tree().nextNode(n))
    {
      ProjectiveData< Point3D< Real >, Real >* clr = (*colorData)(n);
      if (clr) (*clr) *= (Real)pow(Color.value, tree.depth(n));
    }
  }
  tree.template getMCIsoSurface< Degree, BType, WEIGHT_DEGREE, DATA_DEGREE >(density, colorData, solution, isoValue, mesh, !LinearFit.set, !NonManifold.set, PolygonMesh.set);

  if (colorData) delete colorData, colorData = NULL;


  //-----------------------------------------------------------
  vtkSmartPointer<vtkPolyData> polyResult = vtkSmartPointer<vtkPolyData>::New();
  vtkSmartPointer< vtkPoints > points = vtkSmartPointer< vtkPoints >::New();
  Vertex v;

  if (Color.set)
  {
    vtkSmartPointer<vtkUnsignedCharArray> colors = vtkSmartPointer<vtkUnsignedCharArray>::New();
    colors->SetName("RGB");
    colors->SetNumberOfComponents(3);
    polyResult->GetPointData()->SetScalars(colors);
    for (unsigned int i = 0; i < static_cast<unsigned int>(mesh.inCorePoints.size()); i++)
    {
      v = iXForm * mesh.inCorePoints.at(i);
      points->InsertNextPoint(v.point[0], v.point[1], v.point[2]);
      colors->InsertNextTuple3(v.color[0], v.color[1], v.color[2]);
    }
    for (unsigned int i = 0; i < static_cast<unsigned int>(mesh.outOfCorePointCount()); i++)
    {
      mesh.nextOutOfCorePoint(v);
      v = iXForm * (v);
      points->InsertNextPoint(v.point[0], v.point[1], v.point[2]);
      colors->InsertNextTuple3(v.color[0], v.color[1], v.color[2]);
    }
  }
  else
  {
    for (unsigned int i = 0; i < static_cast<unsigned int>(mesh.inCorePoints.size()); i++)
    {
      v = iXForm * mesh.inCorePoints.at(i);
      points->InsertNextPoint(v.point[0], v.point[1], v.point[2]);

    }
    for (unsigned int i = 0; i < static_cast<unsigned int>(mesh.outOfCorePointCount()); i++)
    {
      mesh.nextOutOfCorePoint(v);
      v = iXForm * (v);
      points->InsertNextPoint(v.point[0], v.point[1], v.point[2]);
    }
  }

  // write faces
  vtkSmartPointer< vtkCellArray > triangles = vtkSmartPointer< vtkCellArray >::New();
  std::vector< CoredVertexIndex > vertices;

  unsigned int nr_faces = mesh.polygonCount();
  for (unsigned int i = 0; i < nr_faces; i++)
  {
    vtkSmartPointer< vtkTriangle > triangle = vtkSmartPointer< vtkTriangle >::New();
    mesh.nextPolygon(vertices);

    if (!vertices[0].inCore)
    {
      vertices[0].idx += int(mesh.inCorePoints.size());
    }
    if (!vertices[1].inCore)
    {
      vertices[1].idx += int(mesh.inCorePoints.size());
    }
    if (!vertices[2].inCore)
    {
      vertices[2].idx += int(mesh.inCorePoints.size());
    }

    for (unsigned int j = 0; j < 3; j++)
    {
      triangle->GetPointIds()->SetId(j, vertices[j].idx);
    }

    triangles->InsertNextCell(triangle);
  }
  polyResult->SetPoints(points);
  polyResult->SetPolys(triangles);
  //-----------------------------------------------------------
  if (density) delete density, density = NULL;
  return polyResult;
}

template< class Real, int Degree, BoundaryType BType, class Vertex >
int _Execute(int argc, char* argv[])
{
  typedef typename Octree< Real >::template DensityEstimator< WEIGHT_DEGREE > DensityEstimator;
  typedef typename Octree< Real >::template InterpolationInfo< false > InterpolationInfo;
  typedef OrientedPointStream< Real > PointStream;
  typedef OrientedPointStreamWithData< Real, Point3D< Real > > PointStreamWithData;
  typedef TransformedOrientedPointStream< Real > XPointStream;
  typedef TransformedOrientedPointStreamWithData< Real, Point3D< Real > > XPointStreamWithData;
  Reset< Real >();
  int paramNum = sizeof(params) / sizeof(cmdLineReadable*);
  std::vector< char* > comments;

  if (Verbose.set) echoStdout = 1;

  XForm4x4< Real > xForm, iXForm;
  if (XForm.set)
  {
    FILE* fp = fopen(XForm.value, "r");
    if (!fp)
    {
      fprintf(stderr, "[WARNING] Could not read x-form from: %s\n", XForm.value);
      xForm = XForm4x4< Real >::Identity();
    }
    else
    {
      for (int i = 0; i < 4; i++) for (int j = 0; j < 4; j++)
      {
        float f;
        if (fscanf(fp, " %f ", &f) != 1) fprintf(stderr, "[ERROR] Execute: Failed to read xform\n"), exit(0);
        xForm(i, j) = (Real)f;
      }
      fclose(fp);
    }
  }
  else xForm = XForm4x4< Real >::Identity();

  DumpOutput2(comments, "Running Screened Poisson Reconstruction (Version 9.01)\n");
  char str[1024];
  for (int i = 0; i < paramNum; i++)
    if (params[i]->set)
    {
      params[i]->writeValue(str);
      if (strlen(str)) DumpOutput2(comments, "\t--%s %s\n", params[i]->name, str);
      else                DumpOutput2(comments, "\t--%s\n", params[i]->name);
    }

  double startTime = Time();
  Real isoValue = 0;

  Octree< Real > tree;
  OctreeProfiler< Real > profiler(tree);
  tree.threads = Threads.value;
  if (!In.set)
  {
    ShowUsage(argv[0]);
    return 0;
  }
  if (!MaxSolveDepth.set) MaxSolveDepth.value = Depth.value;

  OctNode< TreeNodeData >::SetAllocator(MEMORY_ALLOCATOR_BLOCK_SIZE);

  int kernelDepth = KernelDepth.set ? KernelDepth.value : Depth.value - 2;
  if (kernelDepth > Depth.value)
  {
    fprintf(stderr, "[WARNING] %s can't be greater than %s: %d <= %d\n", KernelDepth.name, Depth.name, KernelDepth.value, Depth.value);
    kernelDepth = Depth.value;
  }

  int pointCount;

  Real pointWeightSum;
  std::vector< typename Octree< Real >::PointSample >* samples = new std::vector< typename Octree< Real >::PointSample >();
  std::vector< ProjectiveData< Point3D< Real >, Real > >* sampleData = NULL;
  DensityEstimator* density = NULL;
  SparseNodeData< Point3D< Real >, NORMAL_DEGREE >* normalInfo = NULL;
  Real targetValue = (Real)0.5;
  // Read in the samples (and color data)
  {
    profiler.start();
    PointStream* pointStream;
    char* ext = GetFileExtension(In.value);
    if (Color.set && Color.value > 0)
    {
      sampleData = new std::vector< ProjectiveData< Point3D< Real >, Real > >();
      if (!strcasecmp(ext, "bnpts")) pointStream = new BinaryOrientedPointStreamWithData< Real, Point3D< Real >, float, Point3D< unsigned char > >(In.value);
      else if (!strcasecmp(ext, "ply")) pointStream = new    PLYOrientedPointStreamWithData< Real, Point3D< Real > >(In.value, ColorInfo< Real >::PlyProperties, 6, ColorInfo< Real >::ValidPlyProperties);
      else                                    pointStream = new  ASCIIOrientedPointStreamWithData< Real, Point3D< Real > >(In.value, ColorInfo< Real >::ReadASCII);
    }
    else
    {
      if (!strcasecmp(ext, "bnpts")) pointStream = new BinaryOrientedPointStream< Real, float >(In.value);
      else if (!strcasecmp(ext, "ply")) pointStream = new    PLYOrientedPointStream< Real >(In.value);
      else                                    pointStream = new  ASCIIOrientedPointStream< Real >(In.value);
    }
    delete[] ext;
    XPointStream _pointStream(xForm, *pointStream);
    xForm = GetPointXForm(_pointStream, (Real)Scale.value) * xForm;
    if (sampleData)
    {
      XPointStreamWithData _pointStream(xForm, (PointStreamWithData&)*pointStream);
      pointCount = tree.template init< Point3D< Real > >(_pointStream, Depth.value, Confidence.set, *samples, sampleData);
    }
    else
    {
      XPointStream _pointStream(xForm, *pointStream);
      pointCount = tree.template init< Point3D< Real > >(_pointStream, Depth.value, Confidence.set, *samples, sampleData);
    }
    iXForm = xForm.inverse();
    delete pointStream;
#pragma omp parallel for num_threads( Threads.value )
    for (int i = 0; i < (int)samples->size(); i++) (*samples)[i].sample.data.n *= (Real)-1;

    DumpOutput("Input Points / Samples: %d / %d\n", pointCount, samples->size());
    profiler.dumpOutput2(comments, "# Read input into tree:");
  }
  DenseNodeData< Real, Degree > solution;

  {
    DenseNodeData< Real, Degree > constraints;
    InterpolationInfo* iInfo = NULL;
    int solveDepth = MaxSolveDepth.value;

    tree.resetNodeIndices();

    // Get the kernel density estimator [If discarding, compute anew. Otherwise, compute once.]
    {
      profiler.start();
      density = tree.template setDensityEstimator< WEIGHT_DEGREE >(*samples, kernelDepth, SamplesPerNode.value);
      profiler.dumpOutput2(comments, "#   Got kernel density:");
    }

    // Transform the Hermite samples into a vector field [If discarding, compute anew. Otherwise, compute once.]
    {
      profiler.start();
      normalInfo = new SparseNodeData< Point3D< Real >, NORMAL_DEGREE >();
      *normalInfo = tree.template setNormalField< NORMAL_DEGREE >(*samples, *density, pointWeightSum, BType == BOUNDARY_NEUMANN);
      profiler.dumpOutput2(comments, "#     Got normal field:");
    }

    if (!Density.set) delete density, density = NULL;

    // Trim the tree and prepare for multigrid
    {
      profiler.start();
      std::vector< int > indexMap;

      constexpr int MAX_DEGREE = NORMAL_DEGREE > Degree ? NORMAL_DEGREE : Degree;
      tree.template inalizeForBroodedMultigrid< MAX_DEGREE, Degree, BType >(FullDepth.value, typename Octree< Real >::template HasNormalDataFunctor< NORMAL_DEGREE >(*normalInfo), &indexMap);

      if (normalInfo) normalInfo->remapIndices(indexMap);
      if (density) density->remapIndices(indexMap);
      profiler.dumpOutput2(comments, "#       Finalized tree:");
    }

    // Add the FEM constraints
    {
      profiler.start();
      constraints = tree.template initDenseNodeData< Degree >();
      tree.template addFEMConstraints< Degree, BType, NORMAL_DEGREE, BType >(FEMVFConstraintFunctor< NORMAL_DEGREE, BType, Degree, BType >(1., 0.), *normalInfo, constraints, solveDepth);
      profiler.dumpOutput2(comments, "#  Set FEM constraints:");
    }

    // Free up the normal info [If we don't need it for subseequent iterations.]
    delete normalInfo, normalInfo = NULL;

    // Add the interpolation constraints
    if (PointWeight.value > 0)
    {
      profiler.start();
      iInfo = new InterpolationInfo(tree, *samples, targetValue, AdaptiveExponent.value, (Real)PointWeight.value * pointWeightSum, (Real)0);
      tree.template addInterpolationConstraints< Degree, BType >(*iInfo, constraints, solveDepth);
      profiler.dumpOutput2(comments, "#Set point constraints:");
    }

    DumpOutput("Leaf Nodes / Active Nodes / Ghost Nodes: %d / %d / %d\n", (int)tree.leaves(), (int)tree.nodes(), (int)tree.ghostNodes());
    DumpOutput("Memory Usage: %.3f MB\n", float(MemoryInfo::Usage()) / (1 << 20));

    // Solve the linear system
    {
      profiler.start();
      typename Octree< Real >::SolverInfo solverInfo;
      solverInfo.cgDepth = CGDepth.value, solverInfo.iters = Iters.value, solverInfo.cgAccuracy = CGSolverAccuracy.value, solverInfo.verbose = Verbose.set, solverInfo.showResidual = ShowResidual.set, solverInfo.lowResIterMultiplier = std::max< double >(1., LowResIterMultiplier.value);
      solution = tree.template solveSystem< Degree, BType >(FEMSystemFunctor< Degree, BType >(0, 1., 0), iInfo, constraints, solveDepth, solverInfo);
      profiler.dumpOutput2(comments, "# Linear system solved:");
      if (iInfo) delete iInfo, iInfo = NULL;
    }
  }

  CoredFileMeshData< Vertex > mesh;

  {
    profiler.start();
    double valueSum = 0, weightSum = 0;
    typename Octree< Real >::template MultiThreadedEvaluator< Degree, BType > evaluator(&tree, solution, Threads.value);
#pragma omp parallel for num_threads( Threads.value ) reduction( + : valueSum , weightSum )
    for (int j = 0; j < samples->size(); j++)
    {
      ProjectiveData< OrientedPoint3D< Real >, Real >& sample = (*samples)[j].sample;
      Real w = sample.weight;
      if (w > 0) weightSum += w, valueSum += evaluator.value(sample.data.p / sample.weight, omp_get_thread_num(), (*samples)[j].node) * w;
    }
    isoValue = (Real)(valueSum / weightSum);
    if (!(Color.set && Color.value > 0) && samples) delete samples, samples = NULL;
    profiler.dumpOutput("Got average:");
    DumpOutput("Iso-Value: %e\n", isoValue);
  }

  if (VoxelGrid.set)
  {
    profiler.start();
    FILE* fp = fopen(VoxelGrid.value, "wb");
    if (!fp) fprintf(stderr, "Failed to open voxel file for writing: %s\n", VoxelGrid.value);
    else
    {
      int res = 0;
      PointerPoisson(Real) values = tree.template voxelEvaluate< Real, Degree, BType >(solution, res, isoValue, VoxelDepth.value, PrimalVoxel.set);
      fwrite(&res, sizeof(int), 1, fp);
      if (sizeof(Real) == sizeof(float)) fwrite(values, sizeof(float), res*res*res, fp);
      else
      {
        float *fValues = new float[res*res*res];
        for (int i = 0; i < res*res*res; i++) fValues[i] = float(values[i]);
        fwrite(fValues, sizeof(float), res*res*res, fp);
        delete[] fValues;
      }
      fclose(fp);
      DeletePointer(values);
    }
    profiler.dumpOutput("Got voxel grid:");
  }

  if (Out.set)
  {
    profiler.start();
    SparseNodeData< ProjectiveData< Point3D< Real >, Real >, DATA_DEGREE >* colorData = NULL;
    if (sampleData)
    {
      colorData = new SparseNodeData< ProjectiveData< Point3D< Real >, Real >, DATA_DEGREE >();
      *colorData = tree.template setDataField< DATA_DEGREE, false >(*samples, *sampleData, (DensityEstimator*)NULL);
      delete sampleData, sampleData = NULL;
      for (const OctNode< TreeNodeData >* n = tree.tree().nextNode(); n; n = tree.tree().nextNode(n))
      {
        ProjectiveData< Point3D< Real >, Real >* clr = (*colorData)(n);
        if (clr) (*clr) *= (Real)pow(Color.value, tree.depth(n));
      }
    }
    tree.template getMCIsoSurface< Degree, BType, WEIGHT_DEGREE, DATA_DEGREE >(density, colorData, solution, isoValue, mesh, !LinearFit.set, !NonManifold.set, PolygonMesh.set);
    DumpOutput("Vertices / Polygons: %d / %d\n", mesh.outOfCorePointCount() + mesh.inCorePoints.size(), mesh.polygonCount());
    if (PolygonMesh.set) profiler.dumpOutput2(comments, "#         Got polygons:");
    else                  profiler.dumpOutput2(comments, "#        Got triangles:");

    if (colorData) delete colorData, colorData = NULL;

    if (NoComments.set)
    {
      if (ASCII.set) PlyWritePolygons(Out.value, &mesh, PLY_ASCII, NULL, 0, iXForm);
      else            PlyWritePolygons(Out.value, &mesh, PLY_BINARY_NATIVE, NULL, 0, iXForm);
    }
    else
    {
      if (ASCII.set) PlyWritePolygons(Out.value, &mesh, PLY_ASCII, &comments[0], (int)comments.size(), iXForm);
      else            PlyWritePolygons(Out.value, &mesh, PLY_BINARY_NATIVE, &comments[0], (int)comments.size(), iXForm);
    }
  }
  if (density) delete density, density = NULL;
  DumpOutput2(comments, "#          Total Solve: %9.1f (s), %9.1f (MB)\n", Time() - startTime, tree.maxMemoryUsage());

  return 1;
}

#if defined( _WIN32 ) || defined( _WIN64 )
inline double to_seconds(const FILETIME& ft)
{
  const double low_to_sec = 100e-9; // 100 nanoseconds
  const double high_to_sec = low_to_sec*4294967296.0;
  return ft.dwLowDateTime*low_to_sec + ft.dwHighDateTime*high_to_sec;
}
#endif // _WIN32 || _WIN64

vtkSmartPointer<vtkPolyData> PoissonRecon::getPoisson(vtkSmartPointer<vtkPolyData> polyIn, int depth)
{
#ifdef ARRAY_DEBUG
	fprintf(stderr, "[WARNING] Running in array debugging mode\n");
#endif // ARRAY_DEBUG
#if defined( WIN32 ) && defined( MAX_MEMORY_GB )
	if (MAX_MEMORY_GB > 0)
	{
		SIZE_T peakMemory = 1;
		peakMemory <<= 30;
		peakMemory *= MAX_MEMORY_GB;
		printf("Limiting memory usage to %.2f GB\n", float(peakMemory >> 30));
		HANDLE h = CreateJobObject(NULL, NULL);
		AssignProcessToJobObject(h, GetCurrentProcess());

		JOBOBJECT_EXTENDED_LIMIT_INFORMATION jeli = { 0 };
		jeli.BasicLimitInformation.LimitFlags = JOB_OBJECT_LIMIT_JOB_MEMORY;
		jeli.JobMemoryLimit = peakMemory;
		if (!SetInformationJobObject(h, JobObjectExtendedLimitInformation, &jeli, sizeof(jeli)))
			fprintf(stderr, "Failed to set memory limit\n");
	}
#endif // defined( WIN32 ) && defined( MAX_MEMORY_GB )
	if (polyIn->GetPointData()->GetNormals() == NULL)
	{
		return NULL;
	}
	Depth.value = depth;
	if (polyIn->GetPointData()->GetScalars() != NULL)
	{
		Color.set = true;
		Color.value = 16;
	}
	else
	{
		Color.set = false;
	}
	return _Execute< float, 2, BOUNDARY_DIRICHLET, PlyColorVertex< float > >(polyIn);// PlyVertex< float > >(polyIn); it does not allow me to use PlyVertex
}