// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the POISSONRECON_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// POISSONRECON_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef POISSONRECON_EXPORTS
#define POISSONRECON_API __declspec(dllexport)
#else
#define POISSONRECON_API __declspec(dllimport)
#endif

#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkPointData.h>
#include <vtkDoubleArray.h>
#include <vtkTriangle.h>

// This class is exported from the PoissonRecon.dll
class POISSONRECON_API PoissonRecon {
public:
	PoissonRecon(void);
	vtkSmartPointer<vtkPolyData> getPoisson(vtkSmartPointer<vtkPolyData> polyIn, int depth);
};
